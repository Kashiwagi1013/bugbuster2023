using UnityEngine;
using System.Collections.Generic;

public class AttackPointController : MonoBehaviour
{

    [SerializeField] GameObject boss;//ボスオブジェクト
    [SerializeField] GameObject shellPrefab;//攻撃のPrefab
    [SerializeField] float shotIntervalPlus;//エネミー攻撃間隔
    [SerializeField] AudioClip shotSound;//攻撃効果音

    private GameObject target;//ターゲット指定
    private GameObject bossShell;//エネミーShell代入用
    private GameObject shellPool;//Shellオブジェクト格納
    private List<GameObject> listOfBossShell = new List<GameObject>();//エネミーShellを格納する用
    private float attackPosition;//エネミーとプレイヤーの距離差異を保存
    private float attackPositionRange = 50.0f;//攻撃範囲
    private float shotInterval;//攻撃間隔

    private void Awake()
    {
        PlayerSearch();
        ShellPoolSearch();
        BossShellInstantiate();
    }

    private void FixedUpdate()
    {
        AttakPointRange();
        PlayerTargetLockOn();
    }

    /// <summary>
    /// プレイヤータグ検索し取得
    /// </summary>
    private void PlayerSearch()
    {
        target = boss.GetComponent<BossController>().target;
    }

    /// <summary>
    /// ShellPoolタグ検索し取得
    /// </summary>
    private void ShellPoolSearch()
    {
        shellPool = GameObject.FindGameObjectWithTag("ShellPool");
    }

    /// <summary>
    /// プレイヤーをロックオン
    /// </summary>
    private void PlayerTargetLockOn()
    {
        this.transform.LookAt(target.transform.position);
    }

    /// <summary>
    /// エネミーの攻撃範囲
    /// </summary>
    private void AttakPointRange()
    {
        if (target.gameObject.activeSelf == true)
        {
            attackPosition = Vector3.Distance(transform.position, target.transform.position);

            if (attackPosition < attackPositionRange)
            {
                BossAttack();
            }
        }

        else if (target.gameObject.activeSelf == false)
            return;

    }

    /// <summary>
    /// 攻撃用のShellを生成
    /// </summary>
    private void BossShellInstantiate()
    {
        for (int i = 0; i < 15; i++)
            {
                bossShell = Instantiate(shellPrefab, transform.position, Quaternion.identity, shellPool.transform);
                bossShell.SetActive(false);
                listOfBossShell.Add(bossShell);
            }
    }

    /// <summary>
    /// アタックポイントの攻撃用のShellを削除(パブリックBossControllerで呼び出し)
    /// </summary>
    public void ShellDestroy()
    {
        for (int i = 0; i < listOfBossShell.Count; i++)
        {
            Destroy(listOfBossShell[i]);
        }

    }

    /// <summary>
    /// 0から順に非アクティブならアクティブにする処理
    /// </summary>
    /// <returns></returns>
    private GameObject GetBossShell()
    {
        for (int i = 0; i < listOfBossShell.Count; i++)
        {
            if (listOfBossShell[i].activeInHierarchy == false)
                return listOfBossShell[i];
        }

        return null;
    }

    /// <summary>
    /// 攻撃動作（間隔）
    /// </summary>
    private void BossAttack()
    {
        //プラス1加算する
        shotInterval += shotIntervalPlus;

        //加算の値に達すると1度攻撃する
        if (shotInterval % 100 <= 0)
        {

            bossShell = GetBossShell();

            if (bossShell == null)
                return;

            bossShell.transform.position = this.transform.position;
            bossShell.transform.rotation = this.transform.rotation;

            bossShell.SetActive(true);
            AudioSource.PlayClipAtPoint(shotSound, transform.position);

        }
    }
}
