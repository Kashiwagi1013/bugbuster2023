using UnityEngine;
using UnityEngine.UI;

public class BossGuardHelthBar : MonoBehaviour
{
    [SerializeField] GameObject bossGuard;//ボス
    [SerializeField] Slider bossGuardHpSlider;//HPスライダー

    private float currentHp;//現在HP管理用


    private void Start()
    {
        bossGuardHpSlider = GetComponent<Slider>();
    }

    private void FixedUpdate()
    {
        BossCurrentHP();
    }

    /// <summary>
    /// ボスのHPをスライダーへ反映
    /// </summary>
    private void BossCurrentHP()
    {
        currentHp = bossGuard.GetComponent<BossGuardController>().bossGuardHp;
        bossGuardHpSlider.value = currentHp;
    }
}
