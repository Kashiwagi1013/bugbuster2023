using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{
    [SerializeField] GameObject playerAttackPoint;//playerのアタックポイント
    [SerializeField] GameObject goalTarget;//ゴールのターゲット指定
    [SerializeField] GameObject damageEffactPrefab;//ダメージエフェクト
    [SerializeField] GameObject levelUpEffact;//レベルアップエフェクト
    [SerializeField] GameObject playerDeadEffect;//プレイヤー死亡時のエフェクト
    [SerializeField] GameObject engineerGauge;//エンジニアゲージ
    [SerializeField] GameObject limitOffSlider;//リミットオフスライダー
    [SerializeField] GameObject playerRetryButton;//プレイヤーリトライボタン
    [SerializeField] float playerJumpPower;//プレイヤーのジャンプ力
    [SerializeField] float playerMoveWallJumpPowerDown;//ムーブウォール時プレイヤーのジャンプ力を弱める
    [SerializeField] float enemyAttackDamage;//エネミーから受けるダメージ
    [SerializeField] float enemyAttackAway;//エネミーから受ける吹き飛ばされ力
    [SerializeField] FixedJoystick joystick;//プレイヤー移動用コントローラー
    [SerializeField] AudioClip spawnSound;//プレイヤーアクティブ時の効果音
    [SerializeField] AudioClip lockOnSound;//ロックオン効果音
    [SerializeField] AudioClip getItemSound;//アイテム取得時効果音
    [SerializeField] AudioClip levelUpSound;//攻撃効果音
    [SerializeField] AudioClip damageSound;//ダメージ受ける効果音
    [SerializeField] AudioClip jumpSound;//ジャンプ効果音
    [SerializeField] AudioClip playerDeadSound;//プレイヤー死亡時の効果音
    [SerializeField] Animator playerAnimator;//ア二メーター実行用
    [SerializeField] Animator playerAnimatorSave;//プレイヤーア二メーター一時保存用
    [SerializeField] Animator BusterSynchroAnimator;//バスターシンクロ切り替え用ア二メーター

    public float playerMoveSpeed;//プレイヤーの移動スピード力
    public float playerMaxMoveSpeed;//プレイヤーの移動スピード力最高速を指定
    public float playerHp;//プレイヤーヒットポイント
    public float engineerPoint;//エンジニアポイント
    public int playerGetExp;//プレイヤー取得経験値
    public int playerLevel = 1;//プレイヤーレベル
    public int playerDeadCount = 0;//プレイヤー死亡回数
    public bool playerFieldOutBool = false;//プレイヤーフィールドアウトブール

    private GameObject enemyCurrentTarget;//現在のエネミー（ターゲット）
    private GameObject damageEffact;//ダメージエフェクト代入用
    private GameObject damageEffectPool;//DamageEffectオブジェクト格納
    private List<GameObject> listOfDamageEffact = new List<GameObject>();//DamageEffectを格納する用
    private float playerJumpPowerSave;//ムーブウォール上でのジャンプ時に値変動するので元に戻す用
    private float engineerPointMax = 100;//エンジニアポイントの最大値
    private float playerCurrentMovingSpeed;
    private int playerMaxHp = 100;//プレイヤー最大HP
    private int playerLevel2Exp = 10;//プレイヤーレベル2Exp
    private int playerLevel3Exp = 20;//プレイヤーレベル3Exp
    private int playerLevel4Exp = 40;//プレイヤーレベル4Exp
    private int playerLevel5Exp = 80;//プレイヤーレベル5Exp
    private int playerLevel6Exp = 160;//プレイヤーレベル6Exp
    private int playerLevel7Exp = 260;//プレイヤーレベル7Exp
    private int playerLevel8Exp = 380;//プレイヤーレベル8Exp
    private int playerLevel9Exp = 500;//プレイヤーレベル9Exp
    private int playerLevel10Exp = 620;//プレイヤーレベル10Exp
    private int playerLevel11Exp = 740;//プレイヤーレベル11Exp
    private int playerLevel12Exp = 860;//プレイヤーレベル12Exp
    private int playerLevel13Exp = 980;//プレイヤーレベル13Exp
    private int playerLevel14Exp = 1100;//プレイヤーレベル14Exp
    private int playerLevel15Exp = 1220;//プレイヤーレベル15Exp
    private int playerLevel16Exp = 1340;//プレイヤーレベル16Exp
    private int playerLevel17Exp = 1460;//プレイヤーレベル17Exp
    private int playerLevel18Exp = 1580;//プレイヤーレベル18Exp
    private int playerLevel19Exp = 1700;//プレイヤーレベル19Exp
    private int playerLevel20Exp = 1820;//プレイヤーレベル20Exp
    private bool isGround;//着地しているかどうかの判定
    private bool playerMoveBool = true;//プレイヤームーブ判定
    private bool playerLevelUpCallOne = true;//プレイヤーレベルアップを一回のみ呼び出す用
    private bool playerRetryBool = false;//プレイヤーリトライ用
    private Vector3 movingDirecion;//プレイヤー移動時の制御に使用
    private Rigidbody playerRigidbody;//リジッドボディを取得するための変数
    private GameManager gameManager;


    private void Start()
    {
        gameManager = GameManager.GameManagerGetInstance();
        playerRigidbody = GetComponent<Rigidbody>();
        playerAnimator = GetComponent<Animator>();
        playerJumpPowerSave = playerJumpPower;
        DamageEffectPoolSearch();
        DamageEffectInstantiate();
        PlayerAvatarSaving();
        playerRetryBool = true;
    }

    /// <summary>
    /// オブジェクトアクティブ時に呼び出される
    /// </summary>
    private void OnEnable()
    {
        AudioSource.PlayClipAtPoint(spawnSound, transform.position);

        if (playerRetryBool)
            PlayerRetry();
    }

    private void FixedUpdate()
    {
        PlayerMove();
        PlayerRotate();
        PlyerJumpingMotion();
    }

    /// <summary>
    /// オブジェクト非アクティブ時に呼び出される
    /// </summary>
    private void OnDisable()
    {
        //DamageEffectDestroy();
    }

    /// <summary>
    /// 接触判定
    /// </summary>
    /// <param name="other"></param>
    private void OnCollisionEnter(Collision other)
    {
        //押し出される方向を求める(プレイヤーから触れたものの方向)
        Vector3 toVec = GetAngleVector(this.gameObject, other.gameObject);


        //Groundタグでの指示
        if (other.gameObject.tag == "FieldOutErea")
        {
            playerFieldOutBool = true;
            playerHp = 0;
            PlayerHpEmpty();
        }

        //Groundタグでの指示
        if (other.gameObject.tag == "Ground")
        {
            isGround = true;
            playerAnimator.SetBool("Landing", isGround);
        }

        //MoveWallタグでの指示
        if (other.gameObject.tag == "MoveWall")
        {
            isGround = true;
            playerAnimator.SetBool("Landing", isGround);
            PlayerJumpPowerAdjustment();
        }

        // 敵と接触時にダメージを受け吹き飛ばされる
        if (other.gameObject.tag == "Enemy")
        {
            EnemyCollideDamage();

            playerAnimator.SetBool("Attack", false);
            //プレイヤーアニメーター動作/DamageHit
            playerAnimator.SetTrigger("DamageHit");

            //ダメージを受けた方向に押し出される(空中時は押し出される力を弱くする)
            if (isGround == false)
            {
                playerRigidbody.AddForce(toVec * -enemyAttackAway * 1.5f, ForceMode.VelocityChange);
            }

            //ダメージを受けた方向に押し出される
            else if (isGround == true)
            {

                playerRigidbody.AddForce(toVec * -enemyAttackAway * 3.5f, ForceMode.VelocityChange);
            }

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;
            EngineerPointPlusDamage();
            PlayerHpEmpty();
        }

        // 敵と接触時にダメージを受け吹き飛ばされる
        if (other.gameObject.tag == "Boss" || other.gameObject.tag == "BossGuard")
        {
            EnemyCollideDamage();

            playerAnimator.SetBool("Attack", false);
            //プレイヤーアニメーター動作/DamageHit
            playerAnimator.SetTrigger("DamageHit");

            //ダメージを受けた方向に押し出される(空中時は押し出される力を弱くする)
            if (isGround == false)
            {
                playerRigidbody.AddForce(toVec * -enemyAttackAway * 3.5f, ForceMode.VelocityChange);
            }

            //ダメージを受けた方向に押し出される
            else if (isGround == true)
            {

                playerRigidbody.AddForce(toVec * -enemyAttackAway * 5.5f, ForceMode.VelocityChange);
            }

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;

            EngineerPointPlusDamage();
            PlayerHpEmpty();
        }
    }

    /// <summary>
    /// 接触判定(接触が離れたら呼び出される)
    /// </summary>
    /// <param name="other"></param>
    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag == "MoveWall")
            isGround = true;
    }

    /// <summary>
    /// 接触判定(接触が離れたら呼び出される)
    /// </summary>
    /// <param name="other"></param>
    private void OnCollisionExit(Collision other)
    {
        //MoveWallタグのオブジェクトに離れたとき
        if (other.gameObject.tag == "MoveWall")
        {
            ConstrainsFreezeRotationOn();
            //ジャンプパワーを戻す
            playerJumpPower = playerJumpPowerSave;
            playerAnimator.SetBool("Landing", false);
        }

        if (other.gameObject.tag == "Ground")
        {
            playerAnimator.SetBool("Landing", false);
        }

    }

    /// <summary>
    /// 接触判定（トリガー）
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {

        //押し出される方向を求める(プレイヤーから触れたものの方向)
        Vector3 toVec = GetAngleVector(this.gameObject, other.gameObject);


        //アイテム接触時
        if (other.gameObject.tag == "Item")
        {
            GetItem();

            //アイテムオブジェクト破棄
            Destroy(other.gameObject);
            PlayerLevelCalledOne();
        }

        //アイテムインスタンス接触時にゲートを削除
        if (other.gameObject.tag == "ItemPosition")
        {
            Destroy(other.gameObject);
        }

        /*
        // 敵と接触時にダメージを受け吹き飛ばされる
        if (other.gameObject.tag == "Enemy")
        {

            EnemyCollideDamage();

            playerAnimator.SetBool("Attack", false);
            //プレイヤーアニメーター動作/DamageHit
            playerAnimator.SetTrigger("DamageHit");

            //ダメージを受けた方向に押し出される(空中時は押し出される力を弱くする)
            if (isGround == false)
            {
                playerRigidbody.AddForce(toVec * -enemyAttackAway * 1.7f, ForceMode.VelocityChange);
            }

            //ダメージを受けた方向に押し出される
            else if (isGround == true)
            {

                playerRigidbody.AddForce(toVec * -enemyAttackAway * 3.5f, ForceMode.VelocityChange);
            }

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;

            EngineerPointPlusDamage();
            PlayerHpEmpty();
        }

        // 敵と接触時にダメージを受け吹き飛ばされる
        if (other.gameObject.tag == "Boss" || other.gameObject.tag == "BossGuard")
        {
            EnemyCollideDamage();

            playerAnimator.SetBool("Attack", false);
            //プレイヤーアニメーター動作/DamageHit
            playerAnimator.SetTrigger("DamageHit");

            //ダメージを受けた方向に押し出される(空中時は押し出される力を弱くする)
            if (isGround == false)
            {
                playerRigidbody.AddForce(toVec * -enemyAttackAway * 3.5f, ForceMode.VelocityChange);
            }

            //ダメージを受けた方向に押し出される
            else if (isGround == true)
            {

                playerRigidbody.AddForce(toVec * -enemyAttackAway * 5.5f, ForceMode.VelocityChange);
            }

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;

            EngineerPointPlusDamage();
            PlayerHpEmpty();
        }
        */

        // 敵の攻撃接触時
        if (other.gameObject.tag == "EnemyDamage")
        {
            EnemyAttackDamage1();

            playerAnimator.SetBool("Attack", false);
            //プレイヤーアニメーター動作/DamageHit
            playerAnimator.SetTrigger("DamageHit");

            //ダメージを受けた方向に押し出される(空中時は押し出される力を弱くする)
            if (isGround == false)
            {
                playerRigidbody.AddForce(toVec * -enemyAttackAway * 0.5f, ForceMode.VelocityChange);
            }

            //ダメージを受けた方向に押し出される
            else if (isGround == true)
            {

                playerRigidbody.AddForce(toVec * -enemyAttackAway, ForceMode.VelocityChange);
            }

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;

            // ぶつかってきた相手（敵の攻撃）を非アクティブに
            other.gameObject.SetActive(false);

            EngineerPointPlusDamage();
            PlayerHpEmpty();
        }

        // ボスの通常攻撃接触時
        if (other.gameObject.tag == "BossAttackShell")
        {
            BossAttackDamage1();

            playerAnimator.SetBool("Attack", false);
            //プレイヤーアニメーター動作/DamageHit
            playerAnimator.SetTrigger("DamageHit");

            //ダメージを受けた方向に押し出される(空中時は押し出される力を弱くする)
            if (isGround == false)
            {
                playerRigidbody.AddForce(toVec * -enemyAttackAway * 0.75f, ForceMode.VelocityChange);
            }

            //ダメージを受けた方向に押し出される
            else if (isGround == true)
            {

                playerRigidbody.AddForce(toVec * -enemyAttackAway * 1.5f, ForceMode.VelocityChange);
            }

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;

            // ぶつかってきた相手（敵の攻撃）を非アクティブに
            other.gameObject.SetActive(false);

            EngineerPointPlusDamage();
            PlayerHpEmpty();
        }

        // ボスのライン攻撃接触時
        if (other.gameObject.tag == "BossAttackLineShell")
        {
            BossAttackDamage2();

            playerAnimator.SetBool("Attack", false);
            //プレイヤーアニメーター動作/DamageHit
            playerAnimator.SetTrigger("DamageHit");

            //ダメージを受けた方向に押し出される(空中時は押し出される力を弱くする)
            if (isGround == false)
            {
                playerRigidbody.AddForce(toVec * -enemyAttackAway, ForceMode.VelocityChange);
            }

            //ダメージを受けた方向に押し出される
            else if (isGround == true)
            {

                playerRigidbody.AddForce(toVec * -enemyAttackAway * 2.0f, ForceMode.VelocityChange);
            }

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;

            // ぶつかってきた相手（敵の攻撃）を非アクティブに
            other.gameObject.SetActive(false);

            EngineerPointPlusDamage();
            PlayerHpEmpty();
        }

    }

    /// <summary>
    /// DamageEffectPoolタグ検索し取得
    /// </summary>
    private void DamageEffectPoolSearch()
    {
        damageEffectPool = GameObject.FindGameObjectWithTag("DamageEffectPool");
    }

    /// <summary>
    /// DamageEffectを生成
    /// </summary>
    private void DamageEffectInstantiate()
    {
        for (int i = 0; i < 30; i++)
        {
            damageEffact = Instantiate(damageEffactPrefab, transform.position, Quaternion.identity, damageEffectPool.transform);
            damageEffact.SetActive(false);
            listOfDamageEffact.Add(damageEffact);
        }
    }

    /*
    /// <summary>
    /// DamageEffectを削除
    /// </summary>
    private void DamageEffectDestroy()
    {
        for (int i = 0; i < listOfDamageEffact.Count; i++)
        {
            Destroy(listOfDamageEffact[i]);
        }

    }
    */

    /// <summary>
    /// 0から順に非アクティブならアクティブにする処理
    /// </summary>
    /// <returns></returns>
    private GameObject GetDamageEffect()
    {
        for (int i = 0; i < listOfDamageEffact.Count; i++)
        {
            if (listOfDamageEffact[i].activeInHierarchy == false)
                return listOfDamageEffact[i];
        }

        return null;
    }

    /// <summary>
    /// DamageEffectをアクティブに
    /// </summary>
    private void DamageEffectActive()
    {
        damageEffact = GetDamageEffect();

        if (damageEffact == null)
            return;

        damageEffact.transform.position = this.transform.position;
        damageEffact.transform.rotation = this.transform.rotation;

        damageEffact.SetActive(true);
    }

    /// <summary>
    /// プレイヤーレベルアップ（敵撃破時またはアイテム取得時）
    /// </summary>
    public void PlayerLevelCalledOne()
    {
        if (playerLevelUpCallOne && playerLevel2Exp <= playerGetExp && playerLevel3Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = false;

        }

        else if (!playerLevelUpCallOne && playerLevel3Exp <= playerGetExp && playerLevel4Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = true;
        }

        else if (playerLevelUpCallOne && playerLevel4Exp <= playerGetExp && playerLevel5Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = false;
        }

        else if (!playerLevelUpCallOne && playerLevel5Exp <= playerGetExp && playerLevel6Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = true;
        }

        else if (playerLevelUpCallOne && playerLevel6Exp <= playerGetExp && playerLevel7Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = false;
        }

        else if (!playerLevelUpCallOne && playerLevel7Exp <= playerGetExp && playerLevel8Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = true;
        }

        else if (playerLevelUpCallOne && playerLevel8Exp <= playerGetExp && playerLevel9Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = false;
        }

        else if (!playerLevelUpCallOne && playerLevel9Exp <= playerGetExp && playerLevel10Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = true;
        }

        else if (playerLevelUpCallOne && playerLevel10Exp <= playerGetExp && playerLevel11Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = false;
        }

        else if (!playerLevelUpCallOne && playerLevel11Exp <= playerGetExp && playerLevel12Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = true;
        }

        else if (playerLevelUpCallOne && playerLevel12Exp <= playerGetExp && playerLevel13Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = false;
        }

        else if (!playerLevelUpCallOne && playerLevel13Exp <= playerGetExp && playerLevel14Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = true;
        }

        else if (playerLevelUpCallOne && playerLevel14Exp <= playerGetExp && playerLevel15Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = false;
        }

        else if (!playerLevelUpCallOne && playerLevel15Exp <= playerGetExp && playerLevel16Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = true;
        }

        else if (playerLevelUpCallOne && playerLevel16Exp <= playerGetExp && playerLevel17Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = false;
        }

        else if (!playerLevelUpCallOne && playerLevel17Exp <= playerGetExp && playerLevel18Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = true;
        }

        else if (playerLevelUpCallOne && playerLevel18Exp <= playerGetExp && playerLevel19Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = false;
        }

        else if (!playerLevelUpCallOne && playerLevel19Exp <= playerGetExp && playerLevel20Exp >= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = true;
        }

        else if (playerLevelUpCallOne && playerLevel20Exp <= playerGetExp)
        {
            //レベルアップ効果音
            AudioSource.PlayClipAtPoint(levelUpSound, transform.position);
            playerLevel++;
            gameManager.PlayerLevelUpTextOn();
            //レベルアップエフェクト
            Instantiate(levelUpEffact, transform.position, Quaternion.identity);
            Invoke("UserTextEmptyAnimation", 1.5f);
            Invoke("UserTextEmpty", 1.8f);
            SkillPointPlus();
            this.GetComponent<PlayerStatusController>().PlayerSkillPointText();
            playerLevelUpCallOne = false;
        }
    }

    /// <summary>
    /// スキルポイントを加算する
    /// </summary>
    private void SkillPointPlus()
    {
        this.GetComponent<PlayerStatusController>().skillPoint += 3;
    }

    /// <summary>
    /// ジョイスティックでプレイヤー移動
    /// </summary>
    private void PlayerMove()
    {
        if (playerMoveBool)
        {
            float x = joystick.Horizontal;
            float z = joystick.Vertical;
            float speedX = Mathf.Abs(this.playerRigidbody.velocity.x);
            float speedZ = Mathf.Abs(this.playerRigidbody.velocity.z);
            //horizontalRotationはカメラから見た角度を代入している（移動や方向転換と掛けるとカメラ視点操作基準になる）
            var horizontalRotation = Quaternion.AngleAxis(Camera.main.transform.eulerAngles.y, Vector3.up);
            if (speedX < playerMaxMoveSpeed)
                if (speedZ < playerMaxMoveSpeed)
                {
                    movingDirecion = new Vector3(x, 0, z);
                    movingDirecion.Normalize();
                    movingDirecion = horizontalRotation * movingDirecion * playerMoveSpeed;
                    playerRigidbody.velocity = new Vector3(movingDirecion.x, playerRigidbody.velocity.y, movingDirecion.z);
                    playerCurrentMovingSpeed = movingDirecion.magnitude * 2;
                    playerAnimator.SetBool("Attack", false);
                    if (isGround)
                        playerAnimator.SetFloat("Run", playerCurrentMovingSpeed, 0.1f, Time.deltaTime);
                }
        }

        else playerAnimator.SetFloat("Run", 0, 0.1f, Time.deltaTime); ;
    }

    /// <summary>
    /// ジョイスティックでプレイヤー向きを操作
    /// </summary>
    private void PlayerRotate()
    {
        if (playerMoveBool)
        {
            float x = joystick.Horizontal;
            float z = joystick.Vertical;
            //敵へのロックオンを解除
            playerAttackPoint.GetComponent<PlayerAttackController>().LinkParticleOff();

            //horizontalRotationはカメラから見た角度を代入している（移動や方向転換と掛けるとカメラ視点操作基準になる）
            var horizontalRotation = Quaternion.AngleAxis(Camera.main.transform.eulerAngles.y, Vector3.up);

            //プレイヤー向き指定
            if (x != 0 || z != 0)
            {
                var direction = new Vector3(x, 0, z);
                transform.localRotation = Quaternion.LookRotation(horizontalRotation * direction);

            }
            else
            {
                if (EnemyController.currentTarget == false && BossController.currentTarget == false && BossGuardController.currentTarget == false && goalTarget.activeSelf == false)
                    return;

                else if (EnemyController.currentTarget == true && EnemyController.currentTarget == enemyCurrentTarget)
                {
                    EnemyTargetLockOn();
                }

                else if (BossController.currentTarget == true && BossController.currentTarget == enemyCurrentTarget)
                {
                    BossTargetLockOn();
                }

                else if (BossGuardController.currentTarget == true && BossGuardController.currentTarget == enemyCurrentTarget)
                {
                    BossGuardTargetLockOn();
                }

                else if (goalTarget.activeSelf == true && goalTarget == enemyCurrentTarget && goalTarget.GetComponent<GoalController>().goalTargetBool)
                {
                    GoalTargetLockOn();
                }
            }
        }

        else GoalTargetLockOn();

    }

    /// <summary>
    /// プレイヤージャンプ(ボタン割り当て)
    /// </summary>
    public void PlayerJumpButton()
    {
        ConstrainsFreezeRotationOn();
        //着地しているとき(連続ジャンプ無効)
        if (isGround)
        {
            playerRigidbody.AddForce(transform.up * playerJumpPower, ForceMode.Impulse);

            playerAnimator.SetBool("Attack", false);
            //プレイヤーアニメーター動作/jump
            playerAnimator.SetTrigger("Jump");

            //ジャンプ効果音
            AudioSource.PlayClipAtPoint(jumpSound, transform.position);

            isGround = false;
        }
    }

    /// <summary>
    /// プレイヤージャンプ中/落下中モーションの条件指定
    /// </summary>
    private void PlyerJumpingMotion()
    {
        playerAnimator.SetFloat("Jumping", playerRigidbody.velocity.y);
    }
    /// <summary>
    /// Playerのジャンプ力を半分にする(ムーブウォール上で適応)
    /// </summary>
    private void PlayerJumpPowerAdjustment()
    {
        playerJumpPower /= playerMoveWallJumpPowerDown;
    }

    /// <summary>
    /// エネミーと接触するとHpを減少
    /// </summary>
    private void EnemyCollideDamage()
    {
        // エネミーの攻撃が当たるとHPを減少させる
        playerHp -= enemyAttackDamage * 1.5f;


        //ダメージ受けると効果音
        AudioSource.PlayClipAtPoint(damageSound, transform.position);
    }

    /// <summary>
    /// エネミーの攻撃1を受けるとHpを減少
    /// </summary>
    private void EnemyAttackDamage1()
    {
        // エネミーの攻撃が当たるとHPを減少させる
        playerHp -= enemyAttackDamage;



        //ダメージ受けると効果音
        AudioSource.PlayClipAtPoint(damageSound, transform.position);
    }

    /// <summary>
    /// ボスの攻撃1を受けるとHpを減少
    /// </summary>
    private void BossAttackDamage1()
    {
        // エネミーの攻撃が当たるとHPを減少させる
        playerHp -= enemyAttackDamage * 1.5f;

        //ダメージ受けると効果音
        AudioSource.PlayClipAtPoint(damageSound, transform.position);
    }

    /// <summary>
    /// ボスの攻撃2を受けるとHpを減少
    /// </summary>
    private void BossAttackDamage2()
    {
        // エネミーの攻撃が当たるとHPを減少させる
        playerHp -= enemyAttackDamage * 2;

        //ダメージ受けると効果音
        AudioSource.PlayClipAtPoint(damageSound, transform.position);
    }

    /// <summary>
    /// 現在のエンジニアポイントを取得する（このメソッド使用はダメージを受ける・与える時に呼び出されるようにされている）
    /// </summary>
    private void SliderCurrentEngneerPointCall()
    {
        engineerGauge.GetComponent<EngineerGaugeController>().SliderCurrentEngneerPoint();
    }

    /// <summary>
    /// エンジニアポイントがMAXになった時のメソッド呼び出し（このメソッド使用はダメージを受ける・与える時に呼び出されるようにされている）
    /// </summary>
    private void EngineerPointMaxActionCall()
    {
        engineerGauge.GetComponent<EngineerGaugeController>().EngineerPointMaxAction();
    }

    /// <summary>
    /// ダメージを受けるとエンジニアポイントを加算する
    /// </summary>
    private void EngineerPointPlusDamage()
    {
        if (engineerGauge && engineerPoint < engineerPointMax && playerHp >= 30)
        {
            engineerPoint += 1.5f;

            SliderCurrentEngneerPointCall();
            EngineerPointMaxActionCall();
        }

        else if (engineerGauge && engineerPoint < engineerPointMax && playerHp <= 30)
        {
            engineerPoint += 1.5f * 2;

            SliderCurrentEngneerPointCall();
            EngineerPointMaxActionCall();
        }

        else return;
    }

    /// <summary>
    /// エンジニアポイントを加算する(敵攻撃接触時呼び出し)
    /// </summary>
    public void EngineerPointPlusNomal()
    {
        if (engineerGauge && engineerPoint < engineerPointMax && playerHp >= 30)
        {
            engineerPoint += 1.0f;

            SliderCurrentEngneerPointCall();
            EngineerPointMaxActionCall();
        }

        else if (engineerGauge && engineerPoint < engineerPointMax && playerHp <= 30)
        {
            engineerPoint += 1.0f * 2;

            SliderCurrentEngneerPointCall();
            EngineerPointMaxActionCall();
        }

        else return;
    }

    /// <summary>
    /// エンジニアポイントを加算する(敵攻撃接触時呼び出し)
    /// </summary>
    public void EngineerPointPlusCharge1()
    {
        if (engineerGauge && engineerPoint < engineerPointMax && playerHp >= 30)
        {
            engineerPoint += 0.35f;

            SliderCurrentEngneerPointCall();
            EngineerPointMaxActionCall();
        }

        else if (engineerGauge && engineerPoint < engineerPointMax && playerHp <= 30)
        {
            engineerPoint += 0.35f * 2;

            SliderCurrentEngneerPointCall();
            EngineerPointMaxActionCall();
        }

        else return;
    }

    /// <summary>
    /// エンジニアポイントを加算する(敵攻撃接触時呼び出し)
    /// </summary>
    public void EngineerPointPlusCharge2()
    {
        if (engineerGauge && engineerPoint < engineerPointMax && playerHp >= 30)
        {
            engineerPoint += 2.5f;

            SliderCurrentEngneerPointCall();
            EngineerPointMaxActionCall();
        }

        else if (engineerGauge && engineerPoint < engineerPointMax && playerHp <= 30)
        {
            engineerPoint += 2.5f * 2;

            SliderCurrentEngneerPointCall();
            EngineerPointMaxActionCall();
        }

        else return;
    }

    /// <summary>
    /// エンジニアポイントを加算する(敵攻撃接触時呼び出し)
    /// </summary>
    public void EngineerPointPlusCharge3()
    {
        if (engineerGauge && engineerPoint < engineerPointMax && playerHp >= 30)
        {
            engineerPoint += 4.5f;

            SliderCurrentEngneerPointCall();
            EngineerPointMaxActionCall();
        }

        else if (engineerGauge && engineerPoint < engineerPointMax && playerHp <= 30)
        {
            engineerPoint += 4.5f * 2;

            SliderCurrentEngneerPointCall();
            EngineerPointMaxActionCall();
        }

        else return;
    }

    /// <summary>
    /// 自信と当たってきたオブジェクトの位置を格納し位置を1のベクトルで返す
    /// ベクトルが0の場合は1にならずに0で返す
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <returns></returns>
    private Vector3 GetAngleVector(GameObject from, GameObject to)
    {
        Vector3 fromVec = new Vector3(from.transform.position.x, 0, from.transform.position.z);
        Vector3 toVec = new Vector3(to.transform.position.x, 0, to.transform.position.z);

        return Vector3.Normalize(toVec - fromVec);
    }


    /// <summary>
    /// プレイヤーリトライボタンをアクティブに
    /// PlayerActiveOff()にて呼び出し
    /// </summary>
    private void PlayerRetryButtonActiveOnInvoke()
    {
        playerRetryButton.SetActive(true);
        playerRetryButton.transform.DOScale(new Vector3(1, 1, 1), 0.5f)
            .SetLoops(1)
            .Play();
    }

    /// <summary>
    /// プレイヤーリトライボタンを比アクティブに
    /// </summary>
    private void PlayerRetryButtonActiveOff()
    {
        playerRetryButton.SetActive(false);
    }

    /// <summary>
    /// リトライボタンを押すとプレイヤーがアクティブになる
    /// </summary>
    public void PlayerActiveOnButton()
    {
        this.gameObject.SetActive(true);
        playerRetryButton.transform.DOScale(new Vector3(0, 0, 0), 0.5f)
            .SetLoops(1)
            .Play();
        Invoke("PlayerRetryButtonActiveOff", 1f);
    }

    /// <summary>
    /// プレイヤーのリトライした時の指示
    /// </summary>
    private void PlayerRetry()
    {
        playerHp = 100;
        engineerPoint = 100;
        SliderCurrentEngneerPointCall();
        EngineerPointMaxActionCall();
        this.playerRigidbody.isKinematic = false;
    }

    /// <summary>
    /// HPが0になったらPlayerをアクティブOffにする
    /// </summary>
    public void PlayerHpEmpty()
    {
        if (playerHp <= 0)
        {
            playerDeadCount += 1;
            Invoke("PlayerActiveOff", 0.02f);
        }
    }

    /// <summary>
    /// プレイヤー自信を非アクティブに
    /// </summary>
    private void PlayerActiveOff()
    {
        this.playerRigidbody.isKinematic = true;
        limitOffSlider.GetComponent<LimitOffSliderController>().LimitTimeEndActionPublic();
        //敵へのロックオンを解除
        playerAttackPoint.GetComponent<PlayerAttackController>().LinkParticleOffDestroyCall();
        playerAttackPoint.GetComponent<PlayerAttackController>().PlayerAttackPointRotate();
        playerAttackPoint.GetComponent<PlayerAttackController>().AttackPointTargetReset();
        playerAttackPoint.GetComponent<PlayerAttackController>().LookOnMakerActiveOff();
        enemyCurrentTarget = null;
        Instantiate(playerDeadEffect, transform.position, Quaternion.identity);
        AudioSource.PlayClipAtPoint(playerDeadSound, transform.position);
        Invoke("PlayerRetryButtonActiveOnInvoke", 2.0f);
        Debug.Log("PlayerDead");
        this.gameObject.SetActive(false);
    }

    /// <summary>
    /// エネミーをロックオン
    /// </summary>
    public void EnemyTargetLockOn()
    {
        enemyCurrentTarget = EnemyController.currentTarget;
        this.transform.LookAt(enemyCurrentTarget.transform.position);

        //プレイヤーアニメーター動作/Attack
        playerAnimator.SetBool("Attack", true);

        //下記は近距離時に敵をロックオンするとプレイヤーの向き（回転）がおかしな事になるので調整する指示

        //現在のローテーションをcurrentRotation_yに入れる為の変数
        float currentRotation_y;
        //現在のインスペクター表示のy座標を変数yに代入
        currentRotation_y = transform.localEulerAngles.y;
        //y以外は回転を0にしyだけ数値を適応する
        this.transform.rotation = Quaternion.Euler(0.0f, currentRotation_y, 0.0f);
    }

    /// <summary>
    /// ボスをロックオン
    /// </summary>
    public void BossTargetLockOn()
    {
        enemyCurrentTarget = BossController.currentTarget;
        this.transform.LookAt(enemyCurrentTarget.transform.position);

        //プレイヤーアニメーター動作/Attack
        playerAnimator.SetBool("Attack", true);

        //下記は近距離時に敵をロックオンするとプレイヤーの向き（回転）がおかしな事になるので調整する指示

        //現在のローテーションをcurrentRotation_yに入れる為の変数
        float currentRotation_y;
        //現在のインスペクター表示のy座標を変数yに代入
        currentRotation_y = transform.localEulerAngles.y;
        //y以外は回転を0にしyだけ数値を適応する
        this.transform.rotation = Quaternion.Euler(0.0f, currentRotation_y, 0.0f);
    }

    /// <summary>
    /// ボスガードをロックオン
    /// </summary>
    public void BossGuardTargetLockOn()
    {
        enemyCurrentTarget = BossGuardController.currentTarget;
        this.transform.LookAt(enemyCurrentTarget.transform.position);

        //プレイヤーアニメーター動作/Attack
        playerAnimator.SetBool("Attack", true);

        //下記は近距離時に敵をロックオンするとプレイヤーの向き（回転）がおかしな事になるので調整する指示

        //現在のローテーションをcurrentRotation_yに入れる為の変数
        float currentRotation_y;
        //現在のインスペクター表示のy座標を変数yに代入
        currentRotation_y = transform.localEulerAngles.y;
        //y以外は回転を0にしyだけ数値を適応する
        this.transform.rotation = Quaternion.Euler(0.0f, currentRotation_y, 0.0f);
    }

    /// <summary>
    /// ゴールをロックオン
    /// </summary>
    public void GoalTargetLockOn()
    {
        if (playerMoveBool)
        {
            playerMoveBool = false;
            enemyCurrentTarget = goalTarget;
            this.transform.LookAt(enemyCurrentTarget.transform.position);

            //プレイヤーアニメーター動作/Attack
            playerAnimator.SetBool("Attack", true);

            //下記は近距離時に敵をロックオンするとプレイヤーの向き（回転）がおかしな事になるので調整する指示

            //現在のローテーションをcurrentRotation_yに入れる為の変数
            float currentRotation_y;
            //現在のインスペクター表示のy座標を変数yに代入
            currentRotation_y = transform.localEulerAngles.y;
            //y以外は回転を0にしyだけ数値を適応する
            this.transform.rotation = Quaternion.Euler(0.0f, currentRotation_y, 0.0f);
        }

        else return;

    }

    /// <summary>
    /// ロックオン効果音
    /// </summary>
    public void TargetLockOnSound()
    {
        AudioSource.PlayClipAtPoint(lockOnSound, transform.position);
    }

    /// <summary>
    /// アイテム取得時経験値追加/HP回復(HPMAX時は回復しない)
    /// </summary>
    private void GetItem()
    {
        playerGetExp += 2;

        //アイテム取得効果音
        AudioSource.PlayClipAtPoint(getItemSound, transform.position);

        if (playerHp < playerMaxHp)
        {
            playerHp += 1.0f;
        }
    }

    /// <summary>
    /// コンストレインツフリーズローテションだけを固定する
    /// </summary>
    private void ConstrainsFreezeRotationOn()
    {
        playerRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
    }

    /// <summary>
    /// ムーブウォールに乗った時にのコンストレインツフリーズポジションY軸を固定する
    /// </summary>
    public void MoveWallOnLock()
    {
        playerRigidbody.constraints = RigidbodyConstraints.FreezePositionY
        | RigidbodyConstraints.FreezeRotation;
    }

    /// <summary>
    /// ユーザーテキストを空白にするアニメーション
    /// </summary>
    private void UserTextEmptyAnimation()
    {
        gameManager.PlayerLevelUpTextEmpty();
    }

    /// <summary>
    /// ユーザーテキストを空欄にする
    /// </summary>
    private void UserTextEmpty()
    {
        gameManager.UserTextEmpty();
    }

    /// <summary>
    /// 現在のプレイヤーアバターをplayerAnimatorSaveに保存
    /// </summary>
    private void PlayerAvatarSaving()
    {
        playerAnimatorSave.avatar = playerAnimator.avatar;
    }

    /// <summary>
    /// プレイヤーのアバターをバスターシンクロ様に変更（PlayerStatusController呼び出し）
    /// </summary>
    public void BusterSynchroAvatarChange()
    {
        playerAnimator.avatar = BusterSynchroAnimator.avatar;
    }

    /// <summary>
    /// プレイヤーアバターを元に戻す（PlayerStatusController呼び出し）
    /// </summary>
    public void PlayerAvatarChangeBack()
    {
        playerAnimator.avatar = playerAnimatorSave.avatar;
    }

    /// <summary>
    /// バスターシンクロアニメーション（PlayerStatusController呼び出し）
    /// </summary>
    public void BusterSynchroAnimation()
    {
        //バスターシンクロ動作/BusterChange
        playerAnimator.SetTrigger("BusterChange");
    }

    /// <summary>
    /// ウィザードエンジンアニメーション（PlayerAttackController呼び出し）
    /// </summary>
    public void WizardEngineAnimation()
    {
        //ウィザードエンジン動作/WizardChange
        playerAnimator.SetTrigger("WizardChange");
    }
}
