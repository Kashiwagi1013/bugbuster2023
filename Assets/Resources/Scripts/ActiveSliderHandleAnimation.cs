using UnityEngine;
using DG.Tweening;

public class ActiveSliderHandleAnimation : MonoBehaviour
{

    private void Start()
    {
        HandleRotateAnimation();
        HandleScaleAnimation();
    }

    /// <summary>
    /// ハンドルを回転させるアニメーション
    /// </summary>
    private void HandleRotateAnimation()
    {
        this.transform.DORotate(new Vector3(0, 0,-180),1)
            .SetLoops(-1)
            .Play();
    }

    /// <summary>
    /// ハンドルを拡大縮小させるアニメーション
    /// </summary>
    private void HandleScaleAnimation()
    {
        this.transform.DOScale(new Vector3(1, 1, 1), 1)
            .SetLoops(-1, LoopType.Yoyo)
            .Play();
    }
}
