using UnityEngine;

public class AttackCustomManager : MonoBehaviour
{
    [SerializeField] private GameObject gameCustomManager;//ゲームカスタムマネージャー
    [SerializeField] private int chargeAttackNumber;//チャージスキルの種類1〜3のどれか
    [SerializeField] private int chargeAttackSkillNumber;//チャージスキルの攻撃適用ナンバー指定

    public void ChargeAttackSkillSet()
    {
        if(chargeAttackNumber == 1)
            gameCustomManager.GetComponent<GameCustomManager>().chargeAttack1Number = chargeAttackSkillNumber;

        else if (chargeAttackNumber == 2)
            gameCustomManager.GetComponent<GameCustomManager>().chargeAttack2Number = chargeAttackSkillNumber;

        else if (chargeAttackNumber == 3)
            gameCustomManager.GetComponent<GameCustomManager>().chargeAttack3Number = chargeAttackSkillNumber;
    }
}
