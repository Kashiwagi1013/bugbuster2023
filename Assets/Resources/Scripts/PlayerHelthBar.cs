using UnityEngine;
using UnityEngine.UI;

public class PlayerHelthBar : MonoBehaviour
{
    [SerializeField] GameObject player;//プレイヤー
    [SerializeField] Slider playerHpSlider;//HPスライダー
    [SerializeField] Image playerHpSliderColor;//HPバー色変更用
    [SerializeField] Image playerHpSliderColorReset;//HPバー色を元の色へ戻す用

    private float currentHp;//現在HP管理用


    private void Start()
    {
       playerHpSlider = GetComponent<Slider>();
    }

    private void FixedUpdate()
    {
        SliderCurrentHP();
    }

    /// <summary>
    /// プレイヤーのHPをスライダーへ反映
    /// </summary>
    private void SliderCurrentHP()
    {
        currentHp = player.GetComponent<PlayerController>().playerHp;
        playerHpSlider.value = currentHp;
        SliderCurrentHpCrisis();
        SliderCurrentHpColorReset();
    }

    /// <summary>
    /// プレイヤーHPが30以下になるとHPカラーを赤にする
    /// </summary>
    private void SliderCurrentHpCrisis()
    {
        if (playerHpSlider.value <= 30)
            playerHpSliderColor.color = Color.red;
    }

    /// <summary>
    /// プレイヤーHPが30以上になるとHPカラーを元の色にもどす
    /// </summary>
    private void SliderCurrentHpColorReset()
    {
        if (playerHpSlider.value >= 30)
            playerHpSliderColor.color = playerHpSliderColorReset.color;
    }
}
