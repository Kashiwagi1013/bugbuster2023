using UnityEngine;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine.UI;
//using DG.Tweening;

public class EnemyController : MonoBehaviour
{

    [SerializeField] GameObject enemyShellPrefab;//攻撃のPrefab
    [SerializeField] GameObject sliderColider;//スライダー表示の為GameObject取得用
    [SerializeField] GameObject enemyDestroyEffect;//エネミー撃破時エフェクト
    [SerializeField] GameObject damageEffactPrefab;//ダメージエフェクト
    [SerializeField] GameObject ColliderEffact;//コライダーエフェクト
    [SerializeField] GameObject enemySpawnEffect;//エネミー出現エフェクト
    [SerializeField] GameObject enemyCubeAttackEffect;//エネミーキューブ型アタックエフェクト
    [SerializeField] GameObject enemyHpSlider;//エネミーHP用スライダー
    [SerializeField] GameObject coliderTextObject;//コライダーテキストオブジェクト
    [SerializeField] float enemyNumber;//エネミー識別ナンバー
    [SerializeField] float attackSpeed;//エネミー攻撃スピード
    [SerializeField] float shotIntervalPlus;//エネミー攻撃間隔
    [SerializeField] float attackPositionRange;//エネミー攻撃範囲
    [SerializeField] int playerGetExpPoint;//プレイヤーの獲得経験値
    [SerializeField] AudioClip shotSound;//攻撃効果音
    [SerializeField] AudioClip enemyDestroySound;//エネミー撃破効果音
    [SerializeField] AudioClip enemyColiderSound;//トリガー解除効果音
    [SerializeField] AudioClip damageSound;//ダメージ受ける効果音
    //[SerializeField] Collider enemyCollider;//Collider切り替え用
    //[SerializeField] Collider enemyAttakCollider;//接触時攻撃用Collider
    //[SerializeField] Slider enemySliderValue;//スライダーバリューの取得用
    [SerializeField] Slider enemyHpSliderValue;//エネミーHP用スライダーバリュー
    //[SerializeField] Text coliderText;//コライダーテキスト

    static public GameObject currentTarget;//現在選択しているエネミー

    public float enemyHp;//敵ヒットポイント

    private GameObject target;//エネミーのターゲット指定
    private GameObject playerAttackPoint;//プレイヤーのアタックポイント向き指定用
    private GameObject boss;//ボスオブジェクト
    private GameObject damageEffact;//ダメージエフェクト代入用
    private GameObject enemyShell;//エネミーShell代入用
    private GameObject shellPool;//Shellオブジェクト格納
    private GameObject damageEffectPool;//DamageEffectオブジェクト格納
    private List<GameObject> listOfEnemyShell = new List<GameObject>();//エネミーShellを格納する用
    private List<GameObject> listOfDamageEffact = new List<GameObject>();//DamageEffectを格納する用
    private Vector3 enemyStartPosition;//ボスの初期設定ポジション
    private float enemyMoveSpeed = 0.2f;//エネミーの移動速度
    private float movePosition;//エネミーとプレイヤーの距離差異を保存
    private float movePositionRange = 150.0f;//初期位置へもどる距離
    private float attackPosition;//エネミーとプレイヤーの距離差異を保存
    private float shotInterval;//エネミー攻撃間隔
    private float damageInterval;//ダメージ呼び出し間隔
    private float damageIntervalPlus;//ダメージ呼び出し間隔
    private bool enemyCurrentTargetHpOn;//現在の選択中敵のみをHP表示する為のBool
    private bool enemyCurrentTargetHpOff;//現在の選択中ではない敵のHP非表示する為のBool
    //private bool enemyCurrentTargetActiveSliderOn;//現在の選択中敵のみをアクティブスライダー表示する為のBool
    //private bool enemyCurrentTargetActiveSliderOff;//現在の選択中ではない敵のアクティブスライダー非表示する為のBool
    private bool enemyDestroyEffectBool = true;//エネミー撃破時のエフェクト1回呼び出し用
    private bool enemyDestroyCallMethodsBool = true;//エネミー撃破時のメソッド1回呼び出し用
    private Rigidbody myRigidbody;//エネミーキューブ用リジッドボディ
    private NavMeshAgent agent;//敵AI
    private GameManager gameManager;

    private void Awake()
    {
        gameManager = GameManager.GameManagerGetInstance();
        PlayerSearch();
        PlayerAttackPointSearch();
        BossSearch();
        B_EnemySearch();
        ShellPoolSearch();
        DamageEffectPoolSearch();
        GetAgent();
        enemyStartPosition = this.transform.localPosition;
        EnemyShellInstantiate();
        DamageEffectInstantiate();
        //enemyCollider = GetComponent<Collider>();
        EnemyCubeRigidbody();
        //エネミー撃破時エフェクト
        Instantiate(enemySpawnEffect, transform.position, Quaternion.Euler(0.0f,this.transform.localEulerAngles.y,0.0f));
    }

    private void FixedUpdate()
    {
        AttakPointRange();
        EnemyTargetRotate();
        MovePositionRange();
        PlayerAttackEnemyTargetLockOnContinuation();
        //現在選択中のエネミーのHPゲージを表示
        CurrentEnemyHPSliderActiveOn();
        //現在選択中のエネミーでなければHPゲージを非表示
        CurrentNotEnemyHPSliderActiveOff();
        //現在選択中のエネミーのアクティブスライダーゲージを表示
        //EnemyCurrentActiveSliderOn();
        //現在選択中のエネミーでなければアクティブスライダーゲージを非表示
        //EnemyCurrentActiveSliderOff();
        EnemyLockOn();
    }

    /// <summary>
    /// エネミーオブジェクトタッチでロックオン
    /// </summary>
    private void EnemyLockOn()
    {
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; ++i)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began)
                {
                    //スクリーンタッチをしたらRayをMainCameraから出し
                    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                    RaycastHit hit;

                    //ヒットしたオブジェクトがこのオブジェクトだったら命令を作動
                    if (Physics.Raycast(ray, out hit))
                    {
                        if (hit.collider.gameObject == this.gameObject)
                        {
                            enemyCurrentTargetHpOn = true;
                            enemyCurrentTargetHpOff = true;
                            //enemyCurrentTargetActiveSliderOn = true;
                            //enemyCurrentTargetActiveSliderOff = true;

                            //現在選択しているエネミーを一旦解除
                            currentTarget = null;
                            BossController.currentTarget = null;
                            BossGuardController.currentTarget = null;
                            //タッチされたエネミーを選択
                            currentTarget = this.gameObject;
                            //現在選択しているGameObjectを取得し、ログに表示
                            Debug.Log(currentTarget);

                            //EnemyActiveSliderOn();

                            //プレイヤーの向きを対象に向ける
                            target.GetComponent<PlayerController>().EnemyTargetLockOn();

                            //ロックオン効果音
                            target.GetComponent<PlayerController>().TargetLockOnSound();

                            //プレイヤーアタックポイントの向きを対象に向ける
                            playerAttackPoint.GetComponent<PlayerAttackController>().EnemyTargetLockOn();

                            //他のHPゲージを非表示
                            BossHPSliderActiveOff();
                            BossGuardHPSliderActiveOff();

                            //他のアクティブスライダーを非表示
                            //BossActiveSliderOff();
                            //BossGuardActiveSliderOff();
                        }

                    }
                }
            }
        }

        else return;

    }

    /// <summary>
    /// エネミーオブジェクトタッチでロックオン
    /// </summary>
    public void OnMouseDown()
    {
        enemyCurrentTargetHpOn = true;
        enemyCurrentTargetHpOff = true;
        //enemyCurrentTargetActiveSliderOn = true;
        //enemyCurrentTargetActiveSliderOff = true;

        //現在選択しているエネミーを一旦解除
        currentTarget = null;
        BossController.currentTarget = null;
        BossGuardController.currentTarget = null;
        //タッチされたエネミーを選択
        currentTarget = this.gameObject;
        //現在選択しているGameObjectを取得し、ログに表示
        Debug.Log(currentTarget);

        //EnemyActiveSliderOn();

        //プレイヤーの向きを対象に向ける
        target.GetComponent<PlayerController>().EnemyTargetLockOn();

        //ロックオン効果音
        target.GetComponent<PlayerController>().TargetLockOnSound();

        //プレイヤーアタックポイントの向きを対象に向ける
        playerAttackPoint.GetComponent<PlayerAttackController>().EnemyTargetLockOn();

        //他のHPゲージを非表示
        BossHPSliderActiveOff();
        BossGuardHPSliderActiveOff();

        //他のアクティブスライダーを非表示
        //BossActiveSliderOff();
        //BossGuardActiveSliderOff();
    }

    /// <summary>
    /// 接触判定（トリガー）
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        //プレイヤーの通常攻撃接触時
        if (other.gameObject.tag == "PlayerDamage")
        {
            EnemyDamage();

            PlayerEngineerPointPlusNomal();

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;

            // ぶつかってきた相手（敵の攻撃）を非アクティブに
            other.gameObject.SetActive(false);
        }

        //プレイヤーチャージ攻撃2接触時
        if (other.gameObject.tag == "ChargeAttack2")
        {
            EnemyChargeDamage2();

            PlayerEngineerPointPlusCharge2();

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;
        }  

        //プレイヤーチャージ攻撃3接触時
        if (other.gameObject.tag == "ChargeAttack3")
        {
            EnemyChargeDamage3();

            PlayerEngineerPointPlusCharge3();

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;
        }

        EnemyDestroyCallMethods();
    }

    /// <summary>
    /// 接触判定（トリガー）通っている間呼ばれ続ける
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay(Collider other)
    {
        //プレイヤーチャージ攻撃1接触時
        if (other.gameObject.tag == "ChargeAttack1")
        {
            EnemyChargeDamage1();

            PlayerEngineerPointPlusCharge1();

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;
        }

        EnemyDestroyCallMethods();
    }

    /// <summary>
    /// プレイヤータグ検索し取得
    /// </summary>
    private void PlayerSearch()
    {
        target = GameObject.FindGameObjectWithTag("Player");
        this.transform.LookAt(target.transform.position);
    }

    /// <summary>
    /// プレイヤーアタックポイントタグ検索し取得
    /// </summary>
    private void PlayerAttackPointSearch()
    {
        playerAttackPoint = GameObject.FindGameObjectWithTag("PlayerAttackPoint");
    }

    /// <summary>
    /// ボスタグ検索し取得
    /// </summary>
    public void BossSearch()
    {
        boss = GameObject.FindGameObjectWithTag("Boss");
    }

    /// <summary>
    /// ボスのエネミータグ検索し取得
    /// </summary>
    private void B_EnemySearch()
    {
        if (boss == null)
            return;
        else
            boss.GetComponent<BossController>().EnemySearch();
    }

    /// <summary>
    /// ShellPoolタグ検索し取得
    /// </summary>
    private void ShellPoolSearch()
    {
        shellPool = GameObject.FindGameObjectWithTag("ShellPool");
    }

    /// <summary>
    /// DamageEffectPoolタグ検索し取得
    /// </summary>
    private void DamageEffectPoolSearch()
    {
        damageEffectPool = GameObject.FindGameObjectWithTag("DamageEffectPool");
    }

    /// <summary>
    /// エネミーNo.0だったらNavMeshAgentを取得
    /// </summary>
    private void GetAgent()
    {
        if (enemyNumber == 0)
            agent = GetComponent<NavMeshAgent>();

        else return;
    }

    /// <summary>
    /// 各エネミーがターゲットの位置を目的地に設定し移動する
    /// </summary>
    private void EnemyTargetMove()
    {
        if (target.gameObject.activeSelf == true)
        {
            //NavMeshAgent使用(エネミーNo.0)
            if (enemyNumber == 0 && target.gameObject.activeSelf == true)
                agent.destination = target.transform.position;    

            //Vector3.MoveTowards使用(エネミーNo.1)
            else if (enemyNumber == 1)
            {
                //エネミーがプレイヤーに近づく範囲設定
                if (attackPosition < attackPositionRange / 1.2)
                    return;

                else if(enemyNumber == 1 && target.gameObject.activeSelf == true)
                    transform.position = Vector3.MoveTowards(transform.position, target.transform.position, enemyMoveSpeed);
            }
        }

        else if (enemyNumber == 0 && target.gameObject.activeSelf == false)            
            EnemyPositionReset();
               
        else if (enemyNumber == 1 && target.gameObject.activeSelf == false)
            transform.position = Vector3.MoveTowards(transform.position, enemyStartPosition, enemyMoveSpeed);

        else return;

    }

    /// <summary>
    /// Enemyの移動する範囲
    /// </summary>
    private void MovePositionRange()
    {
        movePosition = Vector3.Distance(transform.position, target.transform.position);

        if (movePosition < movePositionRange)
            EnemyTargetMove();

        else if (enemyNumber == 0 && movePosition > movePositionRange)
            EnemyPositionReset();

        else if (enemyNumber == 1 && movePosition > movePositionRange)
            transform.position = Vector3.MoveTowards(transform.position, enemyStartPosition, enemyMoveSpeed);

        else return;
    }

    /// <summary>
    /// Enemyを初期出現位置まで移動させる。
    /// </summary>
    private void EnemyPositionReset()
    {
        if (this.transform.localPosition != enemyStartPosition)
            agent.destination = enemyStartPosition;

        else if (this.transform.localPosition == enemyStartPosition)
            return;
    }

    /// <summary>
    /// ターゲットを目標にし正面を向く
    /// </summary>
    private void EnemyTargetRotate()
    {
        this.transform.LookAt(target.transform.position);
    }

    /// <summary>
    /// DamageEffectを生成
    /// </summary>
    private void DamageEffectInstantiate()
    {
        for (int i = 0; i < 60; i++)
        {
            damageEffact = Instantiate(damageEffactPrefab, transform.position, Quaternion.identity, damageEffectPool.transform);
            damageEffact.SetActive(false);
            listOfDamageEffact.Add(damageEffact);
        }
    }

    /// <summary>
    /// DamageEffectを削除
    /// </summary>
    private void DamageEffectDestroy()
    {
        for (int i = 0; i < listOfDamageEffact.Count; i++)
        {
            Destroy(listOfDamageEffact[i]);
        }

    }

    /// <summary>
    /// 0から順に非アクティブならアクティブにする処理
    /// </summary>
    /// <returns></returns>
    private GameObject GetDamageEffect()
    {
        for (int i = 0; i < listOfDamageEffact.Count; i++)
        {
            if (listOfDamageEffact[i].activeInHierarchy == false)
                return listOfDamageEffact[i];
        }

        return null;
    }

    /// <summary>
    /// DamageEffectをアクティブに
    /// </summary>
    private void DamageEffectActive()
    {
        damageEffact = GetDamageEffect();

        if (damageEffact == null)
            return;

        damageEffact.transform.position = this.transform.position;
        damageEffact.transform.rotation = this.transform.rotation;

        damageEffact.SetActive(true);
    }

    /// <summary>
    /// 攻撃用のShellを生成
    /// </summary>
    private void EnemyShellInstantiate()
    {
        if(enemyNumber == 0)
        {
            for (int i = 0; i < 15; i++)
            {
                enemyShell = Instantiate(enemyShellPrefab, transform.position, Quaternion.identity, shellPool.transform);
                enemyShell.SetActive(false);
                listOfEnemyShell.Add(enemyShell);
            }
        }
        return;
    }

    /// <summary>
    /// 攻撃用のShellを削除
    /// </summary>
    private void EnemyShellDestroy()
    {
        if (enemyNumber == 0)
        {
            for (int i = 0; i < listOfEnemyShell.Count; i++)
            {
                Destroy(listOfEnemyShell[i]);
            }
        }
        return;
    }

    /// <summary>
    /// 0から順に非アクティブならアクティブにする処理
    /// </summary>
    /// <returns></returns>
    private GameObject GetEnemyShell()
    {
        for(int i = 0; i < listOfEnemyShell.Count; i++)
        {
            if (listOfEnemyShell[i].activeInHierarchy == false)
                return listOfEnemyShell[i];
        }

        return null;
    }

    /// <summary>
    /// 各エネミー攻撃動作（間隔）
    /// </summary>
    private void EnemyAttack()
    {
        if (target.gameObject.activeSelf == true)
        {
            //スフィア型エネミーの攻撃
            if (enemyNumber == 0)
            {
                //プラス加算する
                shotInterval += shotIntervalPlus;

                //加算の値に達すると1度攻撃する
                if (shotInterval % 100 <= 0)
                {
                    enemyShell = GetEnemyShell();

                    if (enemyShell == null)
                        return;

                    enemyShell.transform.position = this.transform.position;
                    enemyShell.transform.rotation = this.transform.rotation;

                    enemyShell.SetActive(true);

                    AudioSource.PlayClipAtPoint(shotSound, transform.position);

                }
            }

            //キューブ型エネミーの攻撃
            else if (enemyNumber == 1)
            {
                //プラス加算する
                shotInterval += shotIntervalPlus;

                //加算の値に達すると1度攻撃する
                //エネミーアクティブなら物理演算をベロシティチェンジで計算
                if (shotInterval % 120 <= 0)
                {
                    myRigidbody.AddForce(transform.forward * attackSpeed, ForceMode.VelocityChange);
                    EnemyCubeAttackEfect();
                    AudioSource.PlayClipAtPoint(shotSound, transform.position);
                }
            }
        }

        else if(target.gameObject.activeSelf == false)
            return;

    }

    /// <summary>
    /// エネミーキューブ型アタックエフェクト
    /// </summary>
    private void EnemyCubeAttackEfect()
    {
        float pos_x = this.transform.position.x;
        float pos_y = this.transform.position.y;
        float pos_z = this.transform.position.z;

        float rot_y = transform.localEulerAngles.y;


       GameObject attackEfect = Instantiate(enemyCubeAttackEffect, new Vector3(pos_x, pos_y+0.5f, pos_z), Quaternion.Euler(0,rot_y+90,90));
       attackEfect.transform.Translate(Vector3.up * 5);



    }

    /*
    /// <summary>
    /// エネミーキネマティックONインヴォーク呼び出し（EnemyAttack）
    /// </summary>
    private void EnemyKinematicTrueInvoke()
    {
        myRigidbody.isKinematic = true;
    }
    */

    /// <summary>
    /// エネミーキューブ用リジッドボディ取得
    /// </summary>
    private void EnemyCubeRigidbody()
    {
        if (enemyNumber == 1)
            myRigidbody = this.gameObject.GetComponent<Rigidbody>();

        else return;
    }

    /// <summary>
    /// エネミーの攻撃範囲
    /// </summary>
    private void AttakPointRange()
    {
        attackPosition = Vector3.Distance(transform.position, target.transform.position);

        if (attackPosition < attackPositionRange)
            EnemyAttack();
    }

    /// <summary>
    /// エネミーがプレイヤーから受ける通常ダメージ
    /// </summary>
    private void EnemyDamage()
    {
        // HPを減少させる
        enemyHp -= playerAttackPoint.GetComponent<PlayerAttackController>().nomalDamage;

        //ダメージ受けると効果音
        AudioSource.PlayClipAtPoint(damageSound, transform.position);

    }

    /// <summary>
    /// エネミーがプレイやから受けるチャージ攻撃1のダメージ
    /// </summary>
    private void EnemyChargeDamage1()
    {
        //プラス加算する
        damageInterval += damageIntervalPlus * Time.deltaTime;

        //加算の値に達すると1度攻撃する
        if (damageInterval % 100 <= 0)
        {
            // HPを減少させる
            enemyHp -= playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage1;

            //ダメージ受けると効果音
            AudioSource.PlayClipAtPoint(damageSound, transform.position);
        }

    }

    /// <summary>
    /// エネミーがプレイやから受けるチャージ攻撃2のダメージ
    /// </summary>
    private void EnemyChargeDamage2()
    {
        // HPを減少させる
        enemyHp -= playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage2;

        //ダメージ受けると効果音
        AudioSource.PlayClipAtPoint(damageSound, transform.position);

    }

    /// <summary>
    /// エネミーがプレイやから受けるチャージ攻撃3のダメージ
    /// </summary>
    private void EnemyChargeDamage3()
    {
        // HPを減少させる
        enemyHp -= playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage3;

        //ダメージ受けると効果音
        AudioSource.PlayClipAtPoint(damageSound, transform.position);

    }

    /// <summary>
    /// エンジニアポイントを加算する
    /// </summary>
    private void PlayerEngineerPointPlusNomal()
    {
        target.GetComponent<PlayerController>().EngineerPointPlusNomal();
    }

    /// <summary>
    /// エンジニアポイントを加算する
    /// </summary>
    private void PlayerEngineerPointPlusCharge1()
    {
        target.GetComponent<PlayerController>().EngineerPointPlusCharge1();
    }

    /// <summary>
    /// エンジニアポイントを加算する
    /// </summary>
    private void PlayerEngineerPointPlusCharge2()
    {
        target.GetComponent<PlayerController>().EngineerPointPlusCharge2();
    }

    /// <summary>
    /// エンジニアポイントを加算する
    /// </summary>
    private void PlayerEngineerPointPlusCharge3()
    {
        target.GetComponent<PlayerController>().EngineerPointPlusCharge3();
    }

    /// <summary>
    /// プレイヤーアタックポイントがエネミーをターゲットし続ける他のターゲットをタップすると切り替わる
    /// </summary>
    private void PlayerAttackEnemyTargetLockOnContinuation()
    {
        if(playerAttackPoint.GetComponent<PlayerAttackController>().enemyCurrentTarget == this.gameObject)
        {
            //プレイヤーアタックポイントの向きを対象に向ける
            playerAttackPoint.GetComponent<PlayerAttackController>().EnemyTargetLockOn();

        }
    }

    /*
    /// <summary>
    /// スライダーバリュー100でトリガー状態解除
    /// </summary>
    public void EnemyColiderValue()
    {
        if (enemySliderValue.value == 100)
        {
            //トリガー解除効果音
            AudioSource.PlayClipAtPoint(enemyColiderSound, transform.position);
            
            enemyCollider.isTrigger = false;
            GetComponent<Renderer>().material.color = Color.cyan;
            //コライダーエフェクト
            Instantiate(ColliderEffact, transform.position, Quaternion.identity);
            //EnemyColiderCompleteTextOn();
            Invoke("EnemyColiderCompleteTextOffInvoke", 0.6f);
        }
    }

    /// <summary>
    /// スライダーバリュー100コンプリートテキストオン
    /// </summary>
    private void EnemyColiderCompleteTextOn()
    {
        coliderTextObject.SetActive(true);
        coliderText.DOText("Complete", 0.5f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);
    }

    /// <summary>
    /// スライダーバリュー100コンプリートテキストオフ（インヴォーク）
    /// </summary>
    private void EnemyColiderCompleteTextOffInvoke()
    {
        coliderText.DOText("        ", 0.1f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);
        Invoke("CompleteTextAciveOffInvoke", 0.1f);
    }

    /// <summary>
    /// コライダー時のコンプリートテキストをオフにする（インヴォーク呼び出し）
    /// </summary>
    private void CompleteTextAciveOffInvoke()
    {
        coliderTextObject.SetActive(false);
    }
    */

    /// <summary>
    /// エネミーのHPゲージをアクティブに
    /// </summary>
    private void EnemyHPSliderActive()
    {
        enemyHpSlider.SetActive(true);
    }

    /// <summary>
    /// エネミーのHPゲージを非アクティブに
    /// </summary>
    private void EnemyHPSliderActiveOff()
    {
        enemyHpSlider.SetActive(false);
    }

    /// <summary>
    /// 現在選択中のエネミーHPゲージをアクティブに
    /// </summary>
    private void CurrentEnemyHPSliderActiveOn()
    {
        if (currentTarget == this.gameObject && enemyCurrentTargetHpOn)
        {
            EnemyHPSliderActive();
            enemyCurrentTargetHpOn = false;
        }

        else return;
    }

    /// <summary>
    /// 現在選択中のエネミーでなければHPゲージを非アクティブに
    /// </summary>
    private void CurrentNotEnemyHPSliderActiveOff()
    {
        if (playerAttackPoint.GetComponent<PlayerAttackController>().enemyCurrentTarget != this.gameObject && enemyCurrentTargetHpOff)
            {
            EnemyHPSliderActiveOff();
            enemyCurrentTargetHpOff = false;
            }

            else return;
    }

    /// <summary>
    /// ボスのHPゲージを非アクティブに
    /// </summary>
    private void BossHPSliderActiveOff()
    {
        if (boss == null)
            return;
        else
            boss.GetComponent<BossController>().BossHPSliderActiveOff();
    }

    /// <summary>
    /// ボスガードのHPゲージを非アクティブに
    /// </summary>
    private void BossGuardHPSliderActiveOff()
    {
        if (boss == null)
            return;
        else
        {
            boss.GetComponent<BossController>().BossGuardForwardHPSliderActiveOff();
            boss.GetComponent<BossController>().BossGuardLeftHPSliderActiveOff();
            boss.GetComponent<BossController>().BossGuardRightHPSliderActiveOff();
            boss.GetComponent<BossController>().BossGuardBackHPSliderActiveOff();
        }

    }

    /*
    /// <summary>
    /// エネミーのアクティブスライダーをOn
    /// </summary>
    private void EnemyActiveSliderOn()
    {
        if (enemyCollider.isTrigger == false)
            return;
        else
            sliderColider.SetActive(true);
    }
    */

    /*
    /// <summary>
    /// エネミーのアクティブスライダーをOff
    /// </summary>
    private void EnemyActiveSliderOff()
    {
        sliderColider.SetActive(false);
    }
    */

    /*
    /// <summary>
    /// 現在選択中のエネミーのアクティブスライダーゲージを表示
    /// </summary>
    private void EnemyCurrentActiveSliderOn()
    {
        if(currentTarget == this.gameObject && enemyCurrentTargetActiveSliderOn)
        {
            EnemyActiveSliderOn();
            enemyCurrentTargetActiveSliderOn = false;
        }
    }

    /// <summary>
    /// 現在選択中のエネミーでなければアクティブスライダーゲージを非表示
    /// </summary>
    private void EnemyCurrentActiveSliderOff()
    {
        if(currentTarget != this.gameObject && enemyCurrentTargetActiveSliderOff)
        {
            EnemyActiveSliderOff();
            enemyCurrentTargetActiveSliderOff = false;
        }
    }

    /// <summary>
    /// ボスのHPゲージを非アクティブに
    /// </summary>
    private void BossActiveSliderOff()
    {
        if (boss == null)
            return;
        else
            boss.GetComponent<BossController>().BossActiveSliderOff();
    }

    /// <summary>
    /// ボスガードのHPゲージを非アクティブに
    /// </summary>
    private void BossGuardActiveSliderOff()
    {
        if (boss == null)
            return;
        else
        {
            boss.GetComponent<BossController>().BossGuardForwardActiveSliderOff();
            boss.GetComponent<BossController>().BossGuardLeftActiveSliderOff();
            boss.GetComponent<BossController>().BossGuardRightActiveSliderOff();
            boss.GetComponent<BossController>().BossGuardBackActiveSliderOff();
        }

    }
    */

    /// <summary>
    /// エネミーのHPが0になったら関数を呼びだす
    /// </summary>
    private void EnemyDestroyCallMethods()
    {
        //HPが0になったら関数を呼び出す
        if (enemyDestroyCallMethodsBool && enemyHp <= 0)
        {
            //撃破効果音
            AudioSource.PlayClipAtPoint(enemyDestroySound, transform.position);
            //エネミー撃破時にエネミーカウントを+1する
            gameManager.EnemyDestroyCountPlus();
            //敵の出現時に生成した攻撃用Shellを破棄する
            EnemyShellDestroy();
            //敵の出現時に生成したDmageEffectを破棄する
            DamageEffectDestroy();
            //敵のHpスライダーを消す
            EnemyHPSliderActiveOff();
            //敵撃破時にプレイヤーに規定値の経験値を与える
            target.GetComponent<PlayerController>().playerGetExp += playerGetExpPoint;
            //プレイヤーの経験値が規定値だったらレベルアップさせる
            target.GetComponent<PlayerController>().PlayerLevelCalledOne();
            //敵消滅時にリンクパーティクルをオフにする
            playerAttackPoint.GetComponent<PlayerAttackController>().LinkParticleOffDestroyCall();
            //ロックオンマーカーをオフにする
            playerAttackPoint.GetComponent<PlayerAttackController>().LookOnMakerActiveOff();
            //HP0なら敵（自信）を削除する
            EnemyHpOutDestroy();
            enemyDestroyCallMethodsBool = false;
        }
    }

    /// <summary>
    /// エネミーのHPが0になったらエネミー自信を消滅させる
    /// </summary>
    private void EnemyHpOutDestroy()
    {

        //HPが0になったら自信を破棄する
        if (enemyDestroyEffectBool && enemyHp <= 0)
        {
            //エネミー撃破時エフェクト
            Instantiate(enemyDestroyEffect, transform.position, Quaternion.identity);
            Destroy(this.gameObject);
            enemyDestroyEffectBool = false;
        }
    }
}
