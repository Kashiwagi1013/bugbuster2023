using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using NCMB;

public class TimeScoreLoad : MonoBehaviour
{
    [SerializeField] private Text rankingText;
    [SerializeField] private Text timeScoreText;

    private float scoreClearTime = 0;
    private float currentClearTime = 0;
    private GameManager gameManager;

    private void Awake()
    {
        gameManager = GameManager.GameManagerGetInstance();
    }

    /// <summary>
    /// タイムスコアをニフティクラウドのデータベースよりデータを取得する（ボタン呼び出し）
    /// </summary>
    public void LoadTimeScoreRanking()
    {
        int rankCount = 0;
        string timeScoreView = "";
        string rankingView = "";
        NCMBQuery<NCMBObject> scoreClass = new NCMBQuery<NCMBObject>("TimeScore");
        scoreClass.OrderByAscending("ClearTime");
        scoreClass.Limit = 10;
        scoreClass.FindAsync((List<NCMBObject> objList, NCMBException saveTimeScore) =>
        {
            if (saveTimeScore != null)
            {
                Debug.LogWarning("error: " + saveTimeScore.ErrorMessage);
            }
            else
            {
                Debug.Log("ランキング取得成功");

                foreach (NCMBObject obj in objList)
                {
                    rankCount++;
                    scoreClearTime = (float)System.Convert.ToDouble(obj["ClearTime"]);
                    Debug.Log("scoreClearTime" + scoreClearTime);
                    Debug.Log("currentClearTime" + currentClearTime);
                    if (scoreClearTime == currentClearTime)
                    {
                        rankingView += "<color=#ff0000>\nRank : " + rankCount.ToString() + "\n\n</color>";
                        timeScoreView += "<color=#ff0000>\nName : " + obj["UserName"] + "\nClearTime : " + obj["ClearTimeReview"] + "\n</color>";
                    }
                    else
                    {
                        rankingView += "\nRank : " + rankCount.ToString() + "\n\n";
                        timeScoreView += "\nName : " + obj["UserName"] + "\nClearTime : " + obj["ClearTimeReview"] + "\n";
                    }

                    Debug.Log("Rank : " + rankCount.ToString() + "  UserName : " + obj["UserName"] + " : ClearTime : " + obj["ClearTimeReview"] + "\n");
                }
                rankingText.text = rankingView;
                timeScoreText.text = timeScoreView;
            }
        });
    }

    /// <summary>
    /// ゲームクリア時のランキングスコア取得
    /// </summary>
    public void GameClearLoadTimeScoreRankingInvoke()
    {
        currentClearTime = gameManager.clearResultTime;
        Invoke("LoadTimeScoreRanking", 2.0f);

    }

}