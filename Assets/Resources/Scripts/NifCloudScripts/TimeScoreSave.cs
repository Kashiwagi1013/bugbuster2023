using UnityEngine;
using UnityEngine.UI;
using NCMB;

public class TimeScoreSave : MonoBehaviour
{
    [SerializeField] private Text userName;
    [SerializeField] private Text clearTimeText;


    private bool saveTimeScoreOneCall = true;//タイムスコアの記録を一回のみ送るため


    private float clearTime;
    private GameManager gameManager;//ゲームマネージャー


    private void Awake()
    {
        gameManager = GameManager.GameManagerGetInstance();
    }

    public void SaveTimeScore()
    {
        if (saveTimeScoreOneCall)
        {
            NCMBObject scoreClass = new NCMBObject("TimeScore");
            
            clearTime = gameManager.clearResultTime;
            clearTimeText.text = clearTime.ToString("F2");
            scoreClass["ClearTime"] = clearTime;
            scoreClass["ClearTimeReview"] = clearTimeText.text;
            scoreClass["UserName"] = userName.text;
            scoreClass.SaveAsync((NCMBException saveTimeScore) =>
            {
                if (saveTimeScore != null)
                {
                    Debug.Log("Error: " + saveTimeScore.ErrorMessage);
                }
                else
                {
                    Debug.Log("success");
                }
            });

            saveTimeScoreOneCall = false;
        }

    }
}
