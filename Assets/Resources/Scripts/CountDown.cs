using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CountDown : MonoBehaviour
{
    [SerializeField] GameObject[] uiObject;//UI関連
    [SerializeField] GameObject timer;//タイマー
    [SerializeField] Text userText;//カウントダウンテキストを表示する用
    [SerializeField] AudioClip gameStartSound;//ゲームスタート効果音
    [SerializeField] AudioClip countDownSound;//カウントダウン効果音

    private float startCountTime = 5.5f;//開始カウントダウン用
    private bool gameStartSoundOneCall = true;
    private AudioSource audioSource;//オーディオ取得用


    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();

        userText.DOText("Ready?", 0.7f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);
        Invoke("CountDownSE", 4f);
        Invoke("CountDownSE", 3f);
        Invoke("CountDownSE", 2f);
    }

    private void FixedUpdate()
    {
        GameStartCount();
    }

    /// <summary>
    /// ゲーム開始時間カウント/表示
    /// </summary>
    private void GameStartCount()
    {
        startCountTime -= Time.deltaTime;

        if (startCountTime <= 3.2f)
            userText.GetComponent<Text>().text = startCountTime.ToString("F0");


        if (startCountTime <= 1)
        {
            userText.text = "START!!";
            GameStartSound();
            UIActive();
        }

        if (startCountTime <= -1)
        {
            userText.text = "";
            timer.SetActive(true);
            Destroy(this.gameObject,0.2f);
        }
    }

    /// <summary>
    /// ゲーム開始時間カウント後に表示
    /// </summary>
    private void UIActive()
    {
        for (int i = 0; i <= 17; i++)
        {
            uiObject[i].SetActive(true);
        }
    }

    /// <summary>
    /// カウントダウン効果音
    /// </summary>
    private void CountDownSE()
    {
        audioSource.PlayOneShot(countDownSound);
    }

    /// <summary>
    /// ゲームスタート効果音
    /// </summary>
    private void GameStartSound()
    {
        if (gameStartSoundOneCall)
        {
            audioSource.PlayOneShot(gameStartSound);
            gameStartSoundOneCall = false;
        }
    }

}
