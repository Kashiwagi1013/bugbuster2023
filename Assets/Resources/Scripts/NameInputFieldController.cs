using UnityEngine;
using DG.Tweening;

public class NameInputFieldController : MonoBehaviour
{

    [SerializeField] Transform nameInputFieldTransform;//ネームインプットフィールドトランスフォーム


    
    private void Awake()
    {
        TransformScaleActiveAnimation();
    }

    /// <summary>
    /// スケールを元のサイズに戻しアニメーションする
    /// </summary>
    private void TransformScaleActiveAnimation()
    {
        nameInputFieldTransform.DOScale(new Vector3(1, 1, 1), duration: 0.5f)
            .SetLoops(1, loopType: LoopType.Yoyo)
            .Play();
    }
}
