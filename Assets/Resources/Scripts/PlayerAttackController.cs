using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerAttackController : MonoBehaviour
{
    [SerializeField] GameObject player;//プレイヤーオブジェクト
    [SerializeField] GameObject lookOnMakerEfect;//ロックオンマーカー
    [SerializeField] GameObject playerAttackShellPrefab;//通常攻撃のオブジェクト
    [SerializeField] GameObject[] playerAttackShell;//playerAttackShellPrefab代入用
    [SerializeField] GameObject playerChargeAttack1ShellPrefab;//チャージ攻撃1のオブジェクト
    [SerializeField] GameObject[] playerChargeAttack1Shell;//playerChargeAttack1ShellPrefab代入用
    [SerializeField] GameObject playerChargeAttack2ShellPrefab;//チャージ攻撃2のオブジェクト
    [SerializeField] GameObject[] playerChargeAttack2Shell;//playerChargeAttack2ShellPrefab代入用
    [SerializeField] GameObject playerChargeAttack3ShellPrefab;//playerChargeAttack3ShellPrefab代入用
    [SerializeField] GameObject[] playerChargeAttack3Shell;//チャージ攻撃3のオブジェクト
    [SerializeField] GameObject goalTarget;//ゴールのターゲット指定
    [SerializeField] GameObject[] attackChargeEffect;//アタックチャージエフェクト
    [SerializeField] GameObject[] targetLockOnParticle;//ターゲットロックオン用エフェクト
    [SerializeField] GameObject[] playerAttackPoint;//ウィザードエンジンのアタックポイント
    [SerializeField] float playerChargeShotSpeed;//プレイヤーチャージ攻撃スピード
    [SerializeField] AudioClip shotSound;//攻撃効果音
    [SerializeField] AudioClip attackChargeStartSE;//攻撃のチャージ開始効果音
    [SerializeField] AudioClip attackChargeingSE;//攻撃のチャージ効果音
    [SerializeField] AudioClip chargeAttack1SE;//チャージ攻撃1効果音
    [SerializeField] AudioClip chargeAttack2SE;//チャージ攻撃2効果音
    [SerializeField] AudioClip chargeAttack3SE;//チャージ攻撃3効果音
    [SerializeField] FixedJoystick joystick;//プレイヤー移動用コントローラー
    [SerializeField] Slider chargeAttackSlider;//チャージアタックゲージ
    [SerializeField] Image chargeAttackSliderColor;//チャージアタックスライダー色変更用
    [SerializeField] Image chargeAttackSliderColorReset;//チャージアタックスライダー色リセット用


    public GameObject enemyCurrentTarget;//現在のエネミー（ターゲット）
    public float chargeAttackTime = 0.0f;//チャージアタックタイムカウント用
    public float chargeSpeed;//チャージスピード
    public float nomalDamage;//通常攻撃のダメージ量
    public float chargeDamage1;//チャージ攻撃1のダメージ量
    public float chargeDamage2;//チャージ攻撃2のダメージ量
    public float chargeDamage3;//チャージ攻撃3のダメージ量
    public float charge3ExplosionScale;//チャージ攻撃3の爆発範囲
    public float charge2DamagePlus;//チャージ攻撃2の追加ダメージ加算
    public float charge3DamagePlus;//チャージ攻撃3の追加ダメージ加算


    private GameObject shellPool;//Shellオブジェクト格納
    private List<GameObject> listOfPlayerShell = new List<GameObject>();//プレイヤーShellを格納する用
    private List<GameObject> listOfChargeShell1 = new List<GameObject>();//プレイヤーChargeShellを1格納する用
    private List<GameObject> listOfChargeShell2 = new List<GameObject>();//プレイヤーChargeShell2を格納する用
    private float chargeAttackValue1 = 1.0f;//チャージアタックタイム指示1値
    private float chargeAttackValue2 = 2.0f;//チャージアタックタイム指示2値
    private float chargeAttackValue3 = 3.0f;//チャージアタックタイム指示3値
    private float timeReset = 0.0f;//チャージアタックタイムカウントをリセット用
    private bool chargeAttackJudge = false;//ボタンが押されるとチャージをする様に動作呼び出す指示
    private bool chargeAttackSoundOne = true;//チャージ音を呼び続けない為一度ずつ呼び出し用
    private bool wizardEngineBool = false;//ウィザードエンジン攻撃時の切り替え用


    private void Start()
    {
        ShellPoolSearch();
        PlayerAttackShellInstantiate();
        ChargeAttackShell1Instantiate();
        ChargeAttackShell2Instantiate();
    }

    /// <summary>
    /// 毎回アクティブになると呼び出される
    /// </summary>
    private void OnEnable()
    {
        timeReset = 0.0f;
        chargeAttackSlider.value = 0.0f;
    }

    private void FixedUpdate()
    {
        PlayerChargeAttackCountStart();
        AttackChargeSound();
    }

    /// <summary>
    /// ShellPoolタグ検索し取得
    /// </summary>
    private void ShellPoolSearch()
    {
        shellPool = GameObject.FindGameObjectWithTag("ShellPool");
    }

    /// <summary>
    /// 攻撃用のShellを生成
    /// </summary>
    private void PlayerAttackShellInstantiate()
    {

        for (int i = 0; i < 30; i++)
        {
            for (int a = 0; a < 4; a++)
            {
                playerAttackShell[a] = Instantiate(playerAttackShellPrefab, transform.position, Quaternion.identity, shellPool.transform);
                playerAttackShell[a].SetActive(false);

                listOfPlayerShell.Add(playerAttackShell[a]);
            }
        }

    }

    /// <summary>
    /// 0から順に非アクティブならアクティブにする処理
    /// </summary>
    /// <returns></returns>
    private GameObject GetPlayerShell()
    {
        for (int i = 0; i < listOfPlayerShell.Count; i++)
        {
            if (listOfPlayerShell[i].activeInHierarchy == false)
                return listOfPlayerShell[i];
        }

        return null;
    }

    /// <summary>
    /// チャージ攻撃用のShellを生成
    /// </summary>
    private void ChargeAttackShell1Instantiate()
    {

        for (int i = 0; i < 25; i++)
        {
            for (int a = 0; a < 4; a++)
            {
                playerChargeAttack1Shell[a] = Instantiate(playerChargeAttack1ShellPrefab, transform.position, Quaternion.identity, shellPool.transform);
                playerChargeAttack1Shell[a].SetActive(false);
                listOfChargeShell1.Add(playerChargeAttack1Shell[a]);
            }
        }

    }

    /// <summary>
    /// 0から順に非アクティブならアクティブにする処理
    /// </summary>
    /// <returns></returns>
    private GameObject GetChargeAttackShell1()
    {
        for (int i = 0; i < listOfChargeShell1.Count; i++)
        {
            if (listOfChargeShell1[i].activeInHierarchy == false)
                return listOfChargeShell1[i];
        }

        return null;
    }

    /// <summary>
    /// チャージ攻撃用のShellを生成
    /// </summary> 
    private void ChargeAttackShell2Instantiate()
    {

        for (int i = 0; i < 30; i++)
        {
            for (int a = 0; a < 4; a++)
            {
                playerChargeAttack2Shell[a] = Instantiate(playerChargeAttack2ShellPrefab, transform.position, Quaternion.identity, shellPool.transform);
                playerChargeAttack2Shell[a].SetActive(false);
                listOfChargeShell2.Add(playerChargeAttack2Shell[a]);
            }
        }

    }

    /// <summary>
    /// 0から順に非アクティブならアクティブにする処理
    /// </summary>
    /// <returns></returns>
    private GameObject GetChargeAttackShell2()
    {
        for (int i = 0; i < listOfChargeShell2.Count; i++)
        {
            if (listOfChargeShell2[i].activeInHierarchy == false)
                return listOfChargeShell2[i];
        }

        return null;
    }

    /// <summary>
    /// プレイヤー攻撃（ボタン割り当て）
    /// </summary>
    public void PlayerAttackButton()
    {

        playerAttackShell[0] = GetPlayerShell();

        if (playerAttackShell == null) return;

        if (!wizardEngineBool)
            NomalAttack();
        else if (wizardEngineBool)
            WizardEngineNomalAttack();

        //攻撃効果音
        AudioSource.PlayClipAtPoint(shotSound, transform.position);

    }

    /// <summary>
    /// プレイヤーチャージ攻撃（ボタン割り当て）
    /// </summary>
    public void PlayerChargeAttackButton()
    {
        if (chargeAttackTime >= chargeAttackValue1 && chargeAttackTime <= chargeAttackValue2 && player.gameObject.activeSelf == true)
        {

            PlayerChargeAttack1();
            Debug.Log("Time:" + chargeAttackTime);
            PlayerChargeAttackReset();
        }

        else if (chargeAttackTime >= chargeAttackValue1 && chargeAttackTime <= chargeAttackValue2 && player.gameObject.activeSelf == false)
            return;


        if (chargeAttackTime >= chargeAttackValue2 && chargeAttackTime <= chargeAttackValue3 && player.gameObject.activeSelf == true)
        {
            PlayerChargeAttack2();
            Debug.Log("Time:" + chargeAttackTime);
            PlayerChargeAttackReset();
        }

        else if (chargeAttackTime >= chargeAttackValue2 && chargeAttackTime <= chargeAttackValue3 && player.gameObject.activeSelf == false)
            return;


        if (chargeAttackTime >= chargeAttackValue3 && player.gameObject.activeSelf == true)
        {
            if (!wizardEngineBool)
            {
                PlayerChargeAttack3();
                Debug.Log("Time:" + chargeAttackTime);
                PlayerChargeAttackReset();
            }

            else if (wizardEngineBool)
            {
                WizardEngineChargeAttack3();
                PlayerChargeAttackReset();
            }

        }

        else if (chargeAttackTime >= chargeAttackValue3 && player.gameObject.activeSelf == false)
            return;
    }

    /// <summary>
    /// ノーマル攻撃
    /// </summary>
    private void NomalAttack()
    {
        playerAttackShell[0].transform.position = playerAttackPoint[0].transform.position;
        playerAttackShell[0].transform.rotation = playerAttackPoint[0].transform.rotation;
        playerAttackShell[0].SetActive(true);
    }

    /// <summary>
    /// ウィザードエンジン時ノーマル攻撃
    /// </summary>
    private void WizardEngineNomalAttack()
    {
        for (int i = 0; i < 4; i++)
        {
            playerAttackShell[i] = GetPlayerShell();
            playerAttackShell[i].transform.position = playerAttackPoint[i].transform.position;
            playerAttackShell[i].transform.rotation = playerAttackPoint[i].transform.rotation;
            playerAttackShell[i].SetActive(true);
        }
    }

    /// <summary>
    /// プレイヤーチャージ1攻撃
    /// </summary>
    private void PlayerChargeAttack1()
    {
        if (!wizardEngineBool)
            StartCoroutine("ChangeAttackShell1Coroutine");
        else if (wizardEngineBool)
            StartCoroutine("WizardEngineAttackShell1Coroutine");

    }

    /// <summary>
    /// チャージ攻撃Shell1をインスタンスする
    /// </summary>
    private void PlayerChargeShell1Instans()
    {
        float z = Random.Range(0, 179);

        playerChargeAttack1Shell[0] = GetChargeAttackShell1();

        if (playerChargeAttack1Shell == null)
            return;

        playerChargeAttack1Shell[0].transform.position = playerAttackPoint[0].transform.position;
        playerChargeAttack1Shell[0].transform.rotation = playerAttackPoint[0].transform.rotation;

        playerChargeAttack1Shell[0].SetActive(true);



        //エネミーが存在しない場合攻撃は向いている方向に（ｚ軸をランダムで回転させている）
        //エネミーが存在する場合は選択中のエネミーを向く
        if (EnemyController.currentTarget == false && BossController.currentTarget == false && BossGuardController.currentTarget == false)
        {
            playerChargeAttack1Shell[0].transform.localRotation = player.transform.localRotation;
            playerChargeAttack1Shell[0].transform.Translate(Vector3.forward * 2);
            playerChargeAttack1Shell[0].transform.Rotate(Vector3.forward * z);
        }
        else if (EnemyController.currentTarget == true)
        {
            playerChargeAttack1Shell[0].transform.LookAt(EnemyController.currentTarget.transform.position);
            playerChargeAttack1Shell[0].transform.Translate(Vector3.forward * 2);
            playerChargeAttack1Shell[0].transform.Rotate(Vector3.forward * z);
        }
        else if (BossController.currentTarget == true)
        {
            playerChargeAttack1Shell[0].transform.LookAt(BossController.currentTarget.transform.position);
            playerChargeAttack1Shell[0].transform.Translate(Vector3.forward * 2);
            playerChargeAttack1Shell[0].transform.Rotate(Vector3.forward * z);
        }
        else if (BossGuardController.currentTarget == true)
        {
            playerChargeAttack1Shell[0].transform.LookAt(BossGuardController.currentTarget.transform.position);
            playerChargeAttack1Shell[0].transform.Translate(Vector3.forward * 2);
            playerChargeAttack1Shell[0].transform.Rotate(Vector3.forward * z);
        }

        //攻撃効果音
        AudioSource.PlayClipAtPoint(chargeAttack1SE, transform.position);


    }

    /// <summary>
    /// ウィザードエンジンチャージ攻撃Shell1をインスタンスする
    /// </summary>
    private void WizardEngineChargeShell1Instans()
    {
        for (int i = 0; i < 4; i++)
        {
            float z = Random.Range(0, 179);

            playerChargeAttack1Shell[i] = GetChargeAttackShell1();

            if (playerChargeAttack1Shell == null)
                return;

            playerChargeAttack1Shell[i].transform.position = playerAttackPoint[i].transform.position;
            playerChargeAttack1Shell[i].transform.rotation = playerAttackPoint[i].transform.rotation;

            playerChargeAttack1Shell[i].SetActive(true);



            //エネミーが存在しない場合攻撃は向いている方向に（ｚ軸をランダムで回転させている）
            //エネミーが存在する場合は選択中のエネミーを向く
            if (EnemyController.currentTarget == false && BossController.currentTarget == false && BossGuardController.currentTarget == false)
            {
                playerChargeAttack1Shell[i].transform.localRotation = player.transform.localRotation;
                playerChargeAttack1Shell[i].transform.Translate(Vector3.forward * 2);
                playerChargeAttack1Shell[i].transform.Rotate(Vector3.forward * z);
            }
            else if (EnemyController.currentTarget == true)
            {
                playerChargeAttack1Shell[i].transform.LookAt(EnemyController.currentTarget.transform.position);
                playerChargeAttack1Shell[i].transform.Translate(Vector3.forward * 2);
                playerChargeAttack1Shell[i].transform.Rotate(Vector3.forward * z);
            }
            else if (BossController.currentTarget == true)
            {
                playerChargeAttack1Shell[i].transform.LookAt(BossController.currentTarget.transform.position);
                playerChargeAttack1Shell[i].transform.Translate(Vector3.forward * 2);
                playerChargeAttack1Shell[i].transform.Rotate(Vector3.forward * z);
            }
            else if (BossGuardController.currentTarget == true)
            {
                playerChargeAttack1Shell[i].transform.LookAt(BossGuardController.currentTarget.transform.position);
                playerChargeAttack1Shell[i].transform.Translate(Vector3.forward * 2);
                playerChargeAttack1Shell[i].transform.Rotate(Vector3.forward * z);
            }

            //攻撃効果音
            AudioSource.PlayClipAtPoint(chargeAttack1SE, transform.position);

        }
    }

    /// <summary>
    /// コルーチン指定数/指定秒数毎に呼び出し
    /// </summary>
    /// <returns></returns>
    private IEnumerator ChangeAttackShell1Coroutine()
    {
        int i = 1;
        int p = 2;

        PlayerChargeShell1Instans();

        while (i <= 10)
        {
            if (player.GetComponent<PlayerStatusController>().charge1SkillPoint >= p)
            {
                yield return new WaitForSeconds(0.3f);
                PlayerChargeShell1Instans();
                i += 1;
                p += 2;
            }

            else i += 10;
        }
    }

    /// <summary>
    /// ウィザードエンジン時コルーチン指定秒数毎に呼び出し
    /// </summary>
    /// <returns></returns>
    private IEnumerator WizardEngineAttackShell1Coroutine()
    {
        int i = 1;
        int p = 2;

        WizardEngineChargeShell1Instans();


        while (i <= 10)
        {
            if (player.GetComponent<PlayerStatusController>().charge1SkillPoint >= p)
            {
                yield return new WaitForSeconds(0.3f);
                WizardEngineChargeShell1Instans();
                i += 1;
                p += 2;
            }

            else i += 10;
        }
    }

    /// <summary>
    /// プレイヤーチャージ2攻撃(コルーチン呼び出し)
    /// </summary>
    private void PlayerChargeAttack2()
    {
        if (!wizardEngineBool && enemyCurrentTarget == true && enemyCurrentTarget.activeInHierarchy == true)
            StartCoroutine("ChangeAttackShell2Coroutine");

        else if (wizardEngineBool && enemyCurrentTarget == true && enemyCurrentTarget.activeInHierarchy == true)
            StartCoroutine("WizardEngineChangeAttackShell2Coroutine");

        else return;
    }

    /// <summary>
    /// チャージ攻撃Shell2をインスタンスする
    /// </summary>
    private void PlayerChargeShell2Instans()
    {
        float x = Random.Range(-30, 30);
        float z = Random.Range(-30, 30);


        if (EnemyController.currentTarget == true || BossController.currentTarget == true || BossGuardController.currentTarget == true)
        {
            playerChargeAttack2Shell[0] = GetChargeAttackShell2();

            if (playerChargeAttack2Shell == null)
                return;

            playerChargeAttack2Shell[0].transform.position = playerAttackPoint[0].transform.position;
            playerChargeAttack2Shell[0].transform.rotation = playerAttackPoint[0].transform.rotation;

            playerChargeAttack2Shell[0].SetActive(true);
            //呼び出したい座標を設定（ｘとｚはランダム数値）
            Vector3 attackStartPosion = new Vector3(x, 20, z);

            //ターゲットしている敵のポジション+指定した座標
            if (EnemyController.currentTarget == true)
                playerChargeAttack2Shell[0].transform.position = EnemyController.currentTarget.transform.position + attackStartPosion;

            else if (BossController.currentTarget == true)
                playerChargeAttack2Shell[0].transform.position = BossController.currentTarget.transform.position + attackStartPosion;

            else if (BossGuardController.currentTarget == true)
                playerChargeAttack2Shell[0].transform.position = BossGuardController.currentTarget.transform.position + attackStartPosion;


            //エネミーが存在しない場合攻撃の向きは回転させない
            //エネミーが存在する場合は選択中のエネミーに向きを向ける
            if (EnemyController.currentTarget == false && BossController.currentTarget == false && BossGuardController.currentTarget == false)
            {
                transform.localRotation = Quaternion.identity;
            }
            else if (EnemyController.currentTarget == true)
            {
                playerChargeAttack2Shell[0].transform.LookAt(EnemyController.currentTarget.transform.position);
            }
            else if (BossController.currentTarget == true)
            {
                playerChargeAttack2Shell[0].transform.LookAt(BossController.currentTarget.transform.position);
            }
            else if (BossGuardController.currentTarget == true)
            {
                playerChargeAttack2Shell[0].transform.LookAt(BossGuardController.currentTarget.transform.position);
            }

            //攻撃効果音
            AudioSource.PlayClipAtPoint(chargeAttack2SE, transform.position);

        }

        else return;

    }

    /// <summary>
    /// ウィザードエンジンチャージ攻撃Shell2をインスタンスする
    /// </summary>
    private void WizardEngineChargeShell2Instans()
    {
        for (int i = 0; i < 4; i++)
        {
            float x = Random.Range(-30, 30);
            float z = Random.Range(-30, 30);


            if (EnemyController.currentTarget == true || BossController.currentTarget == true || BossGuardController.currentTarget == true)
            {
                playerChargeAttack2Shell[i] = GetChargeAttackShell2();

                if (playerChargeAttack2Shell == null)
                    return;

                playerChargeAttack2Shell[i].transform.position = playerAttackPoint[i].transform.position;
                playerChargeAttack2Shell[i].transform.rotation = playerAttackPoint[i].transform.rotation;

                playerChargeAttack2Shell[i].SetActive(true);
                //呼び出したい座標を設定（ｘとｚはランダム数値）
                Vector3 attackStartPosion = new Vector3(x, 20, z);

                //ターゲットしている敵のポジション+指定した座標
                if (EnemyController.currentTarget == true)
                    playerChargeAttack2Shell[i].transform.position = EnemyController.currentTarget.transform.position + attackStartPosion;

                else if (BossController.currentTarget == true)
                    playerChargeAttack2Shell[i].transform.position = BossController.currentTarget.transform.position + attackStartPosion;

                else if (BossGuardController.currentTarget == true)
                    playerChargeAttack2Shell[i].transform.position = BossGuardController.currentTarget.transform.position + attackStartPosion;


                //エネミーが存在しない場合攻撃の向きは回転させない
                //エネミーが存在する場合は選択中のエネミーに向きを向ける
                if (EnemyController.currentTarget == false && BossController.currentTarget == false && BossGuardController.currentTarget == false)
                {
                    transform.localRotation = Quaternion.identity;
                }
                else if (EnemyController.currentTarget == true)
                {
                    playerChargeAttack2Shell[i].transform.LookAt(EnemyController.currentTarget.transform.position);
                }
                else if (BossController.currentTarget == true)
                {
                    playerChargeAttack2Shell[i].transform.LookAt(BossController.currentTarget.transform.position);
                }
                else if (BossGuardController.currentTarget == true)
                {
                    playerChargeAttack2Shell[i].transform.LookAt(BossGuardController.currentTarget.transform.position);
                }

                //攻撃効果音
                AudioSource.PlayClipAtPoint(chargeAttack2SE, transform.position);

            }

            else return;
        }
    }


    /// <summary>
    /// コルーチン指定秒数毎に呼び出し
    /// </summary>
    /// <returns></returns>
    private IEnumerator ChangeAttackShell2Coroutine()
    {
        int i = 2;
        float t = 0.3f;

        PlayerChargeShell2Instans();

        while (i <= 10)
        {
            if (player.GetComponent<PlayerStatusController>().charge2SkillPoint >= i)
            {
                yield return new WaitForSeconds(t);
                PlayerChargeShell2Instans();

                i += 1;

                //ランスの出現する時間を回数によって変動させる
                if (i < 6) t -= 0.05f;
                else t -= 0.025f;
            }

            else i += 10;
        }

    }

    /// <summary>
    /// ウィザードエンジン時コルーチン指定秒数毎に呼び出し
    /// </summary>
    /// <returns></returns>
    private IEnumerator WizardEngineChangeAttackShell2Coroutine()
    {
        int i = 2;
        float t = 0.3f;
        WizardEngineChargeShell2Instans();


        while (i <= 10)
        {
            if (player.GetComponent<PlayerStatusController>().charge2SkillPoint >= i)
            {
                yield return new WaitForSeconds(t);
                WizardEngineChargeShell2Instans();

                i += 1;

                //ランスの出現する時間を回数によって変動させる
                if (i < 6) t -= 0.05f;
                else t -= 0.025f;
            }

            else i += 10;
        }
    }

    /// <summary>
    /// プレイヤーチャージ3攻撃詳細
    /// </summary>
    private void PlayerChargeAttack3()
    {
        //チャージ攻撃を生成
        playerChargeAttack3Shell[0] = Instantiate(playerChargeAttack3ShellPrefab, transform.position, Quaternion.identity);

        //リジッドボディ取得AddForceにて使う為
        Rigidbody playerShellRb = playerChargeAttack3Shell[0].GetComponent<Rigidbody>();

        playerChargeAttack3Shell[0].transform.position = playerAttackPoint[0].transform.position;
        playerChargeAttack3Shell[0].transform.localRotation = player.transform.localRotation;
        playerChargeAttack3Shell[0].transform.Translate(Vector3.forward * 2);

        float shot = 4 * playerChargeShotSpeed;

        //forwardZ軸方向（青軸方向）に力を加える。
        playerShellRb.AddForce(transform.forward * shot);

        //攻撃効果音
        AudioSource.PlayClipAtPoint(chargeAttack3SE, transform.position);

        //3秒で攻撃を破棄
        Destroy(playerChargeAttack3Shell[0], 3.0f);
    }

    /// <summary>
    /// ウィザードエンジンチャージ3攻撃詳細
    /// </summary>
    private void WizardEngineChargeAttack3()
    {
        for (int i = 0; i < 4; i++)
        {

            //チャージ攻撃を生成
            playerChargeAttack3Shell[i] = Instantiate(playerChargeAttack3ShellPrefab, transform.position, Quaternion.identity);

            //リジッドボディ取得AddForceにて使う為
            Rigidbody playerShellRb = playerChargeAttack3Shell[i].GetComponent<Rigidbody>();


            playerChargeAttack3Shell[i].transform.position = playerAttackPoint[i].transform.position;
            playerChargeAttack3Shell[i].transform.localRotation = player.transform.localRotation;
            playerChargeAttack3Shell[i].transform.Translate(Vector3.forward * 2);

            float shot = 4 * playerChargeShotSpeed;

            //forwardZ軸方向（青軸方向）に力を加える。
            playerShellRb.AddForce(transform.forward * shot);

            //攻撃効果音
            AudioSource.PlayClipAtPoint(chargeAttack3SE, transform.position);

            //3秒で攻撃を破棄
            Destroy(playerChargeAttack3Shell[i], 3.0f);
        }
    }

    /// <summary>
    /// プレイヤーチャージ攻撃リセット（ボタン割り当て）
    /// </summary>
    public void PlayerChargeAttackReset()
    {
        chargeAttackTime = timeReset;
        chargeAttackSlider.value = 0.0f;
        chargeAttackSliderColor.color = chargeAttackSliderColorReset.color;
        chargeAttackJudge = false;
        chargeAttackSoundOne = true;

        if (!wizardEngineBool)
        {
            attackChargeEffect[0].SetActive(false);
        }

        else if (wizardEngineBool)
        {
            for (int i = 0; i < 4; i++)
            {
                attackChargeEffect[i].SetActive(false);
            }
        }
    }

    /// <summary>
    /// プレイヤーチャージ攻撃カウント判断（ボタン割り当て）
    /// </summary>
    public void PlayerChargeAttackCountJudge()
    {
        chargeAttackJudge = true;

        if (!wizardEngineBool)
        {
            attackChargeEffect[0].SetActive(true);
        }

        else if (wizardEngineBool)
        {
            for (int i = 0; i < 4; i++)
            {
                attackChargeEffect[i].SetActive(true);
            }
        }
    }

    /// <summary>
    /// プレイヤーチャージ攻撃カウント開始（ボタン割り当て）
    /// </summary>
    private void PlayerChargeAttackCountStart()
    {
        if (chargeAttackJudge)
        {
            chargeAttackTime += Time.deltaTime * chargeSpeed;
            chargeAttackSlider.value = chargeAttackTime;
            ChargeAttackCountSliderColorChange();
        }
    }

    /// <summary>
    /// 攻撃チャージ開始・1・2・3時の効果音
    /// </summary>
    private void AttackChargeSound()
    {
        //チャージ開始SE
        if (chargeAttackSoundOne)
            if (chargeAttackTime >= 0.4f && chargeAttackTime <= chargeAttackValue1)
            {
                AudioSource.PlayClipAtPoint(attackChargeStartSE, transform.position);
                chargeAttackSoundOne = false;
            }

        //チャージ1SE
        if (!chargeAttackSoundOne)
            if (chargeAttackTime >= chargeAttackValue1 && chargeAttackTime <= chargeAttackValue2)
            {
                AudioSource.PlayClipAtPoint(attackChargeingSE, transform.position);
                chargeAttackSoundOne = true;
            }

        //チャージ2SE
        if (chargeAttackSoundOne)
            if (chargeAttackTime >= chargeAttackValue2 && chargeAttackTime <= chargeAttackValue3)
            {
                AudioSource.PlayClipAtPoint(attackChargeingSE, transform.position);
                chargeAttackSoundOne = false;
            }

        //チャージ3SE
        if (!chargeAttackSoundOne)
            if (chargeAttackTime >= chargeAttackValue3)
            {
                AudioSource.PlayClipAtPoint(attackChargeingSE, transform.position);
                chargeAttackSoundOne = true;
            }

    }

    /// <summary>
    /// チャージ時間によってスライダーの色を変更
    /// </summary>
    private void ChargeAttackCountSliderColorChange()
    {
        if (chargeAttackSlider.value >= chargeAttackValue1)
            chargeAttackSliderColor.color = Color.green;
        if (chargeAttackSlider.value >= chargeAttackValue2)
            chargeAttackSliderColor.color = Color.yellow;
        if (chargeAttackSlider.value >= chargeAttackValue3)
            chargeAttackSliderColor.color = Color.red;
    }

    /// <summary>
    /// ロックオンマーカーをアクティブにする
    /// </summary>
    private void LookOnMakerAction()
    {
        Vector3 makerPosion = new Vector3(0, 0, 0);

        if (EnemyController.currentTarget == true)
        {
            lookOnMakerEfect.transform.localScale = new Vector3(2, 2, 2);
            lookOnMakerEfect.transform.position = EnemyController.currentTarget.transform.position + makerPosion;
        }

        if (BossController.currentTarget == true)
        {
            lookOnMakerEfect.transform.localScale = new Vector3(7, 7, 7);
            lookOnMakerEfect.transform.position = BossController.currentTarget.transform.position + makerPosion;
        }

        if (BossGuardController.currentTarget == true)
        {
            lookOnMakerEfect.transform.localScale = new Vector3(4, 4, 4);

            lookOnMakerEfect.transform.position = BossGuardController.currentTarget.transform.position + makerPosion;
        }

        if (goalTarget.activeSelf == true && goalTarget.GetComponent<GoalController>().goalTargetBool)
        {
            lookOnMakerEfect.transform.localScale = new Vector3(4, 4, 4);

            lookOnMakerEfect.transform.position = goalTarget.transform.position + makerPosion;
        }

        lookOnMakerEfect.SetActive(true);

    }

    /// <summary>
    /// ロックオンマーカーエフェクトを非アクティブに（敵撃破呼び出し）
    /// </summary>
    public void LookOnMakerActiveOff()
    {
        lookOnMakerEfect.SetActive(false);
    }

    /// <summary>
    /// エネミーをロックオン
    /// </summary>
    public void EnemyTargetLockOn()
    {
        LookOnMakerAction();
        if (!wizardEngineBool)
        {
            enemyCurrentTarget = EnemyController.currentTarget;
            //ロックオンパーティクル
            LinkParticleOn();
            playerAttackPoint[0].transform.LookAt(enemyCurrentTarget.transform.position);
        }

        else if (wizardEngineBool)
        {
            for (int i = 0; i < 4; i++)
            {
                enemyCurrentTarget = EnemyController.currentTarget;
                //ロックオンパーティクル
                LinkParticleOn();
                playerAttackPoint[i].transform.LookAt(enemyCurrentTarget.transform.position);
            }
        }

    }

    /// <summary>
    /// ボスをロックオン
    /// </summary>
    public void BossTargetLockOn()
    {
        LookOnMakerAction();
        if (!wizardEngineBool)
        {
            enemyCurrentTarget = BossController.currentTarget;
            //ロックオンパーティクル
            LinkParticleOn();
            playerAttackPoint[0].transform.LookAt(enemyCurrentTarget.transform.position);
        }

        else if (wizardEngineBool)
        {
            for (int i = 0; i < 4; i++)
            {
                enemyCurrentTarget = BossController.currentTarget;
                //ロックオンパーティクル
                LinkParticleOn();
                playerAttackPoint[i].transform.LookAt(enemyCurrentTarget.transform.position);
            }
        }

    }

    /// <summary>
    /// ガードをロックオン
    /// </summary>
    public void GuardTargetLockOn()
    {
        LookOnMakerAction();
        if (!wizardEngineBool)
        {
            enemyCurrentTarget = BossGuardController.currentTarget;
            //ロックオンパーティクル
            LinkParticleOn();
            playerAttackPoint[0].transform.LookAt(enemyCurrentTarget.transform.position);
        }

        else if (wizardEngineBool)
        {
            for (int i = 0; i < 4; i++)
            {
                enemyCurrentTarget = BossGuardController.currentTarget;
                //ロックオンパーティクル
                LinkParticleOn();
                playerAttackPoint[i].transform.LookAt(enemyCurrentTarget.transform.position);
            }
        }

    }

    /// <summary>
    /// ゴールをロックオン
    /// </summary>
    public void GoalTargetLockOn()
    {
        LookOnMakerAction();
        if (!wizardEngineBool)
        {
            enemyCurrentTarget = goalTarget;
            LinkParticleOn();
            playerAttackPoint[0].transform.LookAt(enemyCurrentTarget.transform.position);
        }

        else if (wizardEngineBool)
        {
            for (int i = 0; i < 4; i++)
            {
                enemyCurrentTarget = goalTarget;
                LinkParticleOn();
                playerAttackPoint[i].transform.LookAt(enemyCurrentTarget.transform.position);
            }
        }

    }

    /// <summary>
    /// Linkパーティクルをアクティブ
    /// </summary>
    private void LinkParticleOn()
    {
        if (!wizardEngineBool)
        {
            targetLockOnParticle[0].SetActive(true);
        }

        else if (wizardEngineBool)
        {
            for (int i = 0; i < 4; i++)
            {
                targetLockOnParticle[i].SetActive(true);
            }
        }

    }

    /// <summary>
    /// Linkパーティクルを非アクティブ（ジョイスティックに連動）
    /// </summary>
    public void LinkParticleOff()
    {
        float x = joystick.Horizontal;
        float z = joystick.Vertical;

        //ジョイスティックが動いていて
        //ターゲットが消滅していたら
        if (x != 0 || z != 0)
            if (EnemyController.currentTarget == false && BossController.currentTarget == false && BossGuardController.currentTarget == false)
            {
                LinkParticleOffDestroyCall();
                PlayerAttackPointRotate();
            }
    }

    /// <summary>
    /// アタックポイントの向き
    /// </summary>
    public void PlayerAttackPointRotate()
    {
        if (!wizardEngineBool)
        {
            playerAttackPoint[0].transform.localRotation = Quaternion.identity;
        }

        else if (wizardEngineBool)
        {
            for (int i = 0; i < 4; i++)
            {
                playerAttackPoint[i].transform.localRotation = Quaternion.identity;
            }
        }
    }

    /// <summary>
    /// Linkパーティクルを非アクティブ（敵消滅時）
    /// </summary>
    public void LinkParticleOffDestroyCall()
    {
        if (!wizardEngineBool)
        {
            targetLockOnParticle[0].SetActive(false);
        }

        else if (wizardEngineBool)
        {
            for (int i = 0; i < 4; i++)
            {
                targetLockOnParticle[i].SetActive(false);
            }
        }

    }

    /// <summary>
    /// アタックポイントターゲットリセット
    /// </summary>
    public void AttackPointTargetReset()
    {
        enemyCurrentTarget = null;
    }

    /// <summary>
    /// ウィザードエンジン開始
    /// </summary>
    public void WizardEngineStart()
    {
        wizardEngineBool = true;
        player.GetComponent<PlayerController>().WizardEngineAnimation();
        WizardEngineActive();
        PlayerChargeAttackCountJudge();
    }

    /// <summary>
    /// ウィザードエンジン終了
    /// </summary>
    public void WizardEngineEnd()
    {
        WizardEngineActiveOff();
        wizardEngineBool = false;
    }

    /// <summary>
    /// ウィザードエンジンをアクティブに
    /// </summary>
    private void WizardEngineActive()
    {
        for (int i = 1; i <= 3; i++)
        {
            playerAttackPoint[i].SetActive(true);
        }
    }

    /// <summary>
    /// ウィザードエンジンを非アクティブに
    /// </summary>
    private void WizardEngineActiveOff()
    {
        for (int i = 1; i <= 3; i++)
        {
            playerAttackPoint[i].SetActive(false);
        }
    }
}
