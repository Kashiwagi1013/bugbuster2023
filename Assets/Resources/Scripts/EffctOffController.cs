using UnityEngine;

public class EffctOffController : MonoBehaviour
{
    [SerializeField] float destroyTime;

    private void OnEnable()
    {
        Invoke("EffectDestroy", destroyTime);
    }

    private void EffectDestroy()
    {
        this.gameObject.SetActive(false);
    }
}
