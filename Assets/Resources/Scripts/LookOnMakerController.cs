using UnityEngine;

public class LookOnMakerController : MonoBehaviour
{

    private void FixedUpdate()
    {
        MakerRotation();
    }

    /// <summary>
    /// マーカーオブジェクト自信を回転
    /// </summary>
    private void MakerRotation()
    {
        this.transform.Rotate(1,1,1);
    }
}
