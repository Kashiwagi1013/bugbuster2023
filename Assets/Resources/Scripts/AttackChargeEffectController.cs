using UnityEngine;
using UnityEngine.UI;

public class AttackChargeEffectController : MonoBehaviour
{
    [SerializeField] GameObject playerAttackPoint;//playerのアタックポイント
    [SerializeField] Slider chargeAttackSlider;//チャージアタックゲージ
    [SerializeField] Image particleResetColor;//パーティクルカラーリセット用

    private float chargeAttackValue1 = 1.0f;//チャージアタックタイム指示1値
    private float chargeAttackValue2 = 2.0f;//チャージアタックタイム指示2値
    private float chargeAttackValue3 = 3.0f;//チャージアタックタイム指示3値


    private void FixedUpdate()
    {
        ChangeEffectColorChange();
    }

    /// <summary>
    /// オブジェクト非アクティブ時に呼び出される
    /// </summary>
    private void OnDisable()
    {
        ChangeEffectColorReset();
        ChangeEffectRateReset();
        PlayerChargeTimekReset();
    }

    /// <summary>
    /// チャージ時間によるエフェクトの色を変更/時間のカウント
    /// </summary>
    private void ChangeEffectColorChange()
    {

        chargeAttackSlider.value = playerAttackPoint.GetComponent<PlayerAttackController>().chargeAttackTime;
        ChargeAttackCountEfectColorChange();
    }

    /// <summary>
    /// チャージ時間によってエフェクトの色を変更
    /// </summary>
    private void ChargeAttackCountEfectColorChange()
    {

        ParticleSystem.MainModule particle_m = GetComponent<ParticleSystem>().main;

        //1秒間隔にパーティクルrateを増加変更
        ParticleSystem.EmissionModule particle_e = GetComponent<ParticleSystem>().emission;


        if (chargeAttackSlider.value >= chargeAttackValue1)
        {
            particle_e.rateOverTime = 30;
            particle_m.startColor = Color.green;
        }

        if (chargeAttackSlider.value >= chargeAttackValue2)
        {
            particle_e.rateOverTime = 40;
            particle_m.startColor = Color.yellow;
        }

        if (chargeAttackSlider.value >= chargeAttackValue3)
        {
            particle_e.rateOverTime = 50;
            particle_m.startColor = Color.red;
        }

    }

    /// <summary>
    /// チャージ時間/スライダーのバリューリセット（ボタン割り当て）
    /// </summary>
    private void PlayerChargeTimekReset()
    {
        chargeAttackSlider.value = 0.0f;
    }

    /// <summary>
    /// スタートカラーを元の色へ変更
    /// </summary>
    private void ChangeEffectColorReset()
    {
        ParticleSystem.MainModule particle_m = GetComponent<ParticleSystem>().main;
        particle_m.startColor = particleResetColor.color;
    }

    /// <summary>
    /// パーティクルrateの数を初期値へ変更
    /// </summary>
    private void ChangeEffectRateReset()
    {
        ParticleSystem.EmissionModule particle_e = GetComponent<ParticleSystem>().emission;
        particle_e.rateOverTime = 20;
    }
}
