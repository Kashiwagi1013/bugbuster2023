using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class EngineerGaugeController : MonoBehaviour
{
    [SerializeField] GameObject player;//プレイヤー
    [SerializeField] GameObject limitOffButton;//リミットオフボタン
    [SerializeField] Transform engineerGaugeTransform;//エンジニアゲージトランスフォーム
    [SerializeField] Slider engineerGaugeSlider;//エンジニアゲージ

    private float currentEngieerPoint;//現在エンジニアポイント
    private bool engineerGaugeAnimationBool = false;//初回ゲージ表示をアニメーションなし用


    private void Start()
    {
        engineerGaugeSlider = GetComponent<Slider>();
        engineerGaugeAnimationBool = true;
    }

    /// <summary>
    /// 毎回アクティブになると呼び出される
    /// </summary>
    private void OnEnable()
    {
        TransformScaleZero();
        TransformScaleActiveAnimation();
        player.GetComponent<PlayerController>().engineerPoint = 0.0f;
        SliderCurrentEngneerPoint();
    }

    /// <summary>
    ///　スケールを0にする
    /// </summary>
    private void TransformScaleZero()
    {
        if(engineerGaugeAnimationBool)
            engineerGaugeTransform.DOScale(new Vector3(0, 0, 0), duration: 0);
    }
    /// <summary>
    /// スケールを元のサイズに戻しアニメーションする
    /// </summary>
    private void TransformScaleActiveAnimation()
    {
        engineerGaugeTransform.DOScale(new Vector3(1, 1, 1), duration: 0.5f)
            .SetLoops(1, loopType: LoopType.Yoyo)
            .Play();
    }

    /// <summary>
    /// プレイヤーのエンジニアポイントをスライダーへ反映（Playerから呼び出し）
    /// </summary>
    public void SliderCurrentEngneerPoint()
    {
        currentEngieerPoint = player.GetComponent<PlayerController>().engineerPoint;
        engineerGaugeSlider.value = currentEngieerPoint;
    }

    /// <summary>
    /// エンジニアポイントが100まで到達したらリミットオフボタンをActiveに（Playerから呼び出し）
    /// EngineerGaugeを非アクティブに
    /// </summary>
    public void EngineerPointMaxAction()
    {
        if (engineerGaugeSlider.value == 100)
        {
            limitOffButton.SetActive(true);
            this.gameObject.SetActive(false);
        }

        else return;
    }

}
