using UnityEngine;
using UnityEngine.UI;

public class BossHelthBar : MonoBehaviour
{
    [SerializeField] GameObject boss;//ボス
    [SerializeField] Slider bossHpSlider;//HPスライダー

    private float currentHp;//現在HP管理用


    private void Start()
    {
        bossHpSlider = GetComponent<Slider>();
    }

    private void FixedUpdate()
    {
        BossCurrentHP();
    }

    /// <summary>
    /// ボスのHPをスライダーへ反映
    /// </summary>
    private void BossCurrentHP()
    {
        currentHp = boss.GetComponent<BossController>().bossHp;
        bossHpSlider.value = currentHp;
    }
}
