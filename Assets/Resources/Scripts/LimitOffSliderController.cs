using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LimitOffSliderController : MonoBehaviour
{
    [SerializeField] GameObject player;//プレイヤーオブジェクト
    [SerializeField] GameObject playerAttackPoint;//playerのアタックポイント
    [SerializeField] GameObject engineerGauge;//エンジニアゲージ
    [SerializeField] GameObject limitOffButton;//リミットオフボタン
    [SerializeField] GameObject limitOffSliderHandle;//リミットオフスライダーハンドル
    [SerializeField] GameObject limitOffSliderBusterText;//バスターシンクロテキスト
    [SerializeField] GameObject limitOffSliderWiazrdText;//ウィザードエンジンテキスト
    [SerializeField] Transform limitOffSliderTransform;//リミットオフスライダートランスフォーム
    [SerializeField] AudioClip limitOffButtonSE;//リミットオフボタン効果音
    [SerializeField] Slider limitOffSlider;//リミットオフスライダー
    [SerializeField] Image limitOffSliderBackground;//リミットオフスライダーバックグラウンド
    [SerializeField] Image limitOffTimeBackground;//リミットオフタイム中カラー
    [SerializeField] Image limitOffSliderFillColor;//リミットオフタイム中カラー
    [SerializeField] Image busterSyncroSliderColor;//バスターシンクロ用バックグラウンドカラー指定
    [SerializeField] Image wizardEngineSliderColor;//ウィザードエンジン用バックグラウンドカラー指定


    private AudioSource audioSource;//オーディオ取得用
    private bool limitChange = true;//リミットオフスライダーからリミットタイムへ切り替え


    private void Awake()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    /// <summary>
    /// 毎回アクティブになると呼び出される
    /// </summary>
    private void OnEnable()
    {
        TransformScaleZero();
        TransformScaleActiveAnimation();
        limitOffSlider.interactable = true;
        audioSource.PlayOneShot(limitOffButtonSE);
        LimitOffSliderValueReset();
        LimitOffSliderColorReset();
    }

    private void FixedUpdate()
    {
        Invoke("LimitTimeInvoke", 1.5f);
        LimitTimeEndAction();
    }

    /// <summary>
    ///　スケールを0にする
    /// </summary>
    private void TransformScaleZero()
    {
        limitOffSliderTransform.DOScale(new Vector3(0, 0, 0), duration: 0);
    }

    /// <summary>
    /// スケールを元のサイズに戻しアニメーションする
    /// </summary>
    private void TransformScaleActiveAnimation()
    {
        limitOffSliderTransform.DOScale(new Vector3(1, 1, 1), duration: 0.5f)
            .SetLoops(1, loopType: LoopType.Yoyo)
            .Play();
    }

    /// <summary>
    ///　リミットスライダーパンチスケールアニメーション
    /// </summary>
    private void limitSliderPunchScaleAnimation()
    {
        limitOffSliderTransform.DOPunchScale(new Vector3(1, 1, 1), 0.5f)
            .Play();
    }

    /// <summary>
    /// maxvalueが100の時にValueが100になったら
    /// バスターシンクロを実行
    /// </summary>
    public void BusterSyncroActionValue()
    {
        if(limitChange && limitOffSlider.maxValue == 100 && limitOffSlider.value == 100)
        {
            limitOffSliderHandle.SetActive(false);
            limitOffSlider.maxValue = 15.0f;
            limitOffSlider.value = 15.0f;
            limitOffSlider.interactable = false;
            BusterSyncroColorChange();
            limitSliderPunchScaleAnimation();
            player.GetComponent<PlayerStatusController>().BusterSynchroStart();
            limitOffSliderWiazrdText.SetActive(false);
            limitChange = false;
        }
    }

    /// <summary>
    /// maxvalueが100の時にValueが0になったら
    /// ウィザードエンジンを実行
    /// </summary>
    public void WizardEngineActionValue()
    {
        if (limitChange && limitOffSlider.maxValue == 100 && limitOffSlider.value == 0)
        {
            limitOffSliderHandle.SetActive(false);
            limitOffSlider.maxValue = 15.0f;
            limitOffSlider.value = 15.0f;
            limitOffSlider.interactable = false;
            WizardEngineColorChange();
            limitSliderPunchScaleAnimation();
            player.GetComponent<PlayerStatusController>().WizardEngineStart();
            playerAttackPoint.GetComponent<PlayerAttackController>().WizardEngineStart();
            limitOffSliderBusterText.SetActive(false);
            limitChange = false;
        }
    }

    /// <summary>
    /// バスターシンクロまたはウィザードエンジン使用後インヴォークでカウント開始
    /// 15秒カウント後LimitTimeEndAction()へ繋がる
    /// </summary>
    private void LimitTimeInvoke()
    {
        if (!limitChange && limitOffSlider.maxValue == 15.0f && limitOffSlider.value > 0)
        {
            limitOffSlider.value -=  Time.deltaTime;
            if (limitOffSlider.value == 0)
                limitChange = true;
        }

        else return;
    }

    /// <summary>
    /// リミットタイムが0になったらエンジニアゲージをアクティブに
    /// リミットオフスライダーを非アクティブに
    /// </summary>
    private void LimitTimeEndAction()
    {
        if (limitChange && limitOffSlider.maxValue == 15.0f && limitOffSlider.value == 0)
        {
            engineerGauge.SetActive(true);
            player.GetComponent<PlayerStatusController>().BusterSynchroEnd();
            playerAttackPoint.GetComponent<PlayerAttackController>().WizardEngineEnd();
            player.GetComponent<PlayerStatusController>().WizardEngineEnd();
            player.GetComponent<PlayerStatusController>().PlayerAttackPointPositionCurrentBack();
            player.GetComponent<PlayerStatusController>().LimitTimeEndSound();
            limitOffButton.SetActive(false);
            this.gameObject.SetActive(false);
        }

        else return;
    }

    /// <summary>
    /// リミットタイムが0になったらエンジニアゲージをアクティブに
    /// リミットオフスライダーを非アクティブに
    /// PlayerController呼び出し
    /// </summary>
    public void LimitTimeEndActionPublic()
    {
        player.GetComponent<PlayerStatusController>().BusterSynchroEnd();
        playerAttackPoint.GetComponent<PlayerAttackController>().WizardEngineEnd();
        player.GetComponent<PlayerStatusController>().WizardEngineEnd();
        player.GetComponent<PlayerStatusController>().PlayerAttackPointPositionCurrentBack();
        player.GetComponent<PlayerStatusController>().LimitTimeEndSound();
        limitChange = true;
        engineerGauge.SetActive(true);
        limitOffButton.SetActive(false);
        this.gameObject.SetActive(false);
    }

    /// <summary>
    /// Fillカラーをバスターカラーに変更
    /// limitSliderのBackカラーを黒系に変更（オブジェクト指定）
    /// </summary>
    private void BusterSyncroColorChange()
    {
        limitOffSliderFillColor.color = busterSyncroSliderColor.color;
        limitOffSliderBackground.color = limitOffTimeBackground.color;
    }

    /// <summary>
    /// Fillカラーをウィザードカラーに変更
    /// limitSliderのBackカラーを黒系に変更（オブジェクト指定）
    /// </summary>
    private void WizardEngineColorChange()
    {
        limitOffSliderFillColor.color = wizardEngineSliderColor.color;
        limitOffSliderBackground.color = limitOffTimeBackground.color;
    }

    /// <summary>
    /// limitSliderをFillとBack初期カラーに戻す
    /// </summary>
    private void LimitOffSliderColorReset()
    {
        limitOffSliderFillColor.color = busterSyncroSliderColor.color;
        limitOffSliderBackground.color = wizardEngineSliderColor.color;
    }

    /// <summary>
    /// リミットオフスライダーをリセット
    /// </summary>
    private void LimitOffSliderValueReset()
    {
        limitOffSliderHandle.SetActive(true);
        limitOffSlider.maxValue = 100.0f;
        limitOffSlider.value = 50.0f;
        limitOffSliderBusterText.SetActive(true);
        limitOffSliderWiazrdText.SetActive(true);
    }
}
