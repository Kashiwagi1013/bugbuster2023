using UnityEngine;
using UnityEngine.UI;

public class VirtualCameraController : MonoBehaviour
{
    [SerializeField] GameObject player;//プレイヤー
    [SerializeField] FloatingJoystick joystick;//カメラ回転用コントローラー
    [SerializeField] float rotateSpeed;//回転させるスピード
    [SerializeField] Image cameraStickImage;

    private void FixedUpdate()
    {
        CameraRotate();
    }

    /// <summary>
    /// ジョイスティックでカメラを回転操作
    /// </summary>
    private void CameraRotate()
    {
        float y = joystick.Horizontal;

        //回転させる入力値*スピード
        float angle = y * rotateSpeed;

        //プレイヤー位置情報
        Vector3 playerPos = player.transform.position;

        //カメラを回転させる
        transform.RotateAround(playerPos, Vector3.up, angle);
    }

    public void CameraImageActiveOn()
    {
        cameraStickImage.enabled = true;
    }

    public void CameraImageActiveOff()
    {
        cameraStickImage.enabled = false;
    }

}
