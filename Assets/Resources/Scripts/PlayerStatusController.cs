using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PlayerStatusController : MonoBehaviour
{
    [SerializeField] GameObject playerAttackPoint;//playerのアタックポイント
    [SerializeField] GameObject nomalSkin;//通常プレイ時のノーマルスキン
    [SerializeField] GameObject busterSynchroSkin;//バスターシンクロ時のスキン
    [SerializeField] GameObject[] statusUpEffect;//バスターシンクロ時のエフェクト
    [SerializeField] GameObject busterSynchroEffect;//バスターシンクロ時のエフェクト
    [SerializeField] GameObject[] wizardEngineEffect;//ウィザードエンジン時のエフェクト
    [SerializeField] GameObject[] limitTimeEndEffect;//リミットタイムエンドのエフェクト
    [SerializeField] MeshRenderer playerAttackPointMesh;//プレイヤーアタックポイントメッシュ
    [SerializeField] Transform moveButtonTransform;//ムーブボタントランスフォーム
    [SerializeField] Transform attackSpeedButtonTransform;//スピードボタントランスフォーム
    [SerializeField] Transform attackPowerButtonTransform;//パワーボタントランスフォーム
    [SerializeField] Transform charge1SkillButtonTransform;//チャージ1ボタントランスフォーム
    [SerializeField] Transform charge2SkillButtonTransform;//チャージ2ボタントランスフォーム
    [SerializeField] Transform charge3SkillButtonTransform;//チャージ3ボタントランスフォーム
    [SerializeField] AudioClip busterSynchroSE;//バスターシンクロSE
    [SerializeField] AudioClip wizardEngineSE;//ウィザードエンジンSE
    [SerializeField] AudioClip limitTimeEndSE;//リミットタイムエンドSE
    [SerializeField] AudioClip statusUpSE;//ステータスUP効果音
    [SerializeField] Text skillPointText;//スキルポイントテキスト
    [SerializeField] Text moveSpeedPointText;//ムーブスピードポイントテキスト
    [SerializeField] Text attackSpeedPointText;//アタックスピードポイントテキスト
    [SerializeField] Text attackPowerPointText;//アタックパワーポイントテキスト
    [SerializeField] Text charge1SkillPointText;//チャージスキル1ポイントテキスト
    [SerializeField] Text charge2SkillPointText;//チャージスキル2ポイントテキスト
    [SerializeField] Text charge3SkillPointText;//チャージスキル3ポイントテキスト

    public int skillPoint = 3;//プレイヤースキルポイント（プレイヤー）
    public int moveSpeedPoint = 1;//ムーブスピードポイント（プレイヤー）
    public int attackSpeedPoint = 1;//アタックスピードポイント（アタックポイント）
    public int attackPowerPoint = 1;//アタックパワーポイント（アタックポイント）
    public int charge1SkillPoint = 1;//チャージ1スキルポイント（アタックポイント）
    public int charge2SkillPoint = 1;//チャージ2スキルポイント（アタックポイント）
    public int charge3SkillPoint = 1;//チャージ3スキルポイント（アタックポイント）

    private float ps_ChargeDamage2;//チャージスキル2のレベルアップ時に呼び出すダメージ量置き換え用
    private float ps_ChargeDamage3;//チャージスキル3のレベルアップ時に呼び出すダメージ量置き換え用
    private int busterSynchroMaxPoint = 10;//バスターシンクロ時のステータスポイントMAX置き換え用
    private int currentMoveSpeedPointSave;//現在のムーブスピードポイント保存用
    private int currentAttackSpeedPointSave;//現在のアタックスピードポイント保存用
    private int currentAttackPowerPointSave;//現在のアタックパワーポイント保存用
    private int currentCharge1SkillPointSave;//現在のチャージ1スキルポイント保存用
    private int currentCharge2SkillPointSave;//現在のチャージ2スキルポイント保存用
    private int currentCharge3SkillPointSave;//現在のチャージ3スキルポイント保存用
    private Vector3 playerAttackPointCurrentPosition;//playerのアタックポイントの初期設定ポジション
    private AudioSource audioSource;//オーディオ取得用
    private bool busterSynchroBool = false;//バスターシンクロ判断用（バスターシンクロエンドで使用）
    private bool wizardEngineBool = false;//ウィザードエンジン判断用（ウィザードエンジンエンドで使用）

    private void Start()
    {
        PlayerSkillPointText();
        MoveSpeedUpStatus();
        AttackPowerUpStatus();
        AttackSpeedUpStatus();
        Charge2SkillUpStatus();
        Charge3SkillUpStatus();
        PlayerAttackPointPositionCurrentSaving();
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    /// <summary>
    /// プレイヤースキルポイントを1消費する
    /// </summary>
    private void PlayerSkillMinus()
    {
        if (skillPoint > 0)
        {
            skillPoint -= 1;
            PlayerSkillPointText();
        }

        else return;
    }

    /// <summary>
    /// スキルポイントをテキストに反映する（プレイヤーレベルUP時も呼び出す）
    /// </summary>
    public void PlayerSkillPointText()
    {
        skillPointText.text = skillPoint.ToString();
    }

    /// <summary>
    /// ステータスUP時の効果音
    /// </summary>
    private void PlayerStatusUpSE()
    {
        audioSource.PlayOneShot(statusUpSE);
    }



    /// <summary>
    /// ムーブスピードポイントを1加算する
    /// </summary>
    private void MoveSpeedPointPlus()
    {
        if (moveSpeedPoint < 10 && skillPoint > 0)
        {
            moveSpeedPoint += 1;
            MoveSpeedPointPlusText();
            PlayerSkillMinus();
            PlayerStatusUpSE();
            MoveButtonShakeScaleAnimation();

            for (int i = 0; i < 2; i++)
            {
                statusUpEffect[i].SetActive(true);
                statusUpEffect[i].GetComponent<StatusUpEffectController>().MoveUpEffect();
            }
        }

        else return;
    }

    /// <summary>
    ///　ムーブボタンシェイクスケールアニメーション
    /// </summary>
    private void MoveButtonShakeScaleAnimation()
    {
        moveButtonTransform.DOShakeScale(0.5f, 0.5f)
            .Play();
        Invoke("MoveButtonShakeScaleReset", 0.6f);
    }

    /// <summary>
    ///　ムーブボタンシェイクスケールリセット
    /// </summary>
    private void MoveButtonShakeScaleReset()
    {
        moveButtonTransform.DOScale(new Vector3(1, 1, 1), 0.1f)
            .Play();
    }

    /// <summary>
    /// 加算のポイントをテキストに反映する
    /// </summary>
    private void MoveSpeedPointPlusText()
    {
        moveSpeedPointText.text = moveSpeedPoint.ToString();
        if (moveSpeedPoint == 10) moveSpeedPointText.color = Color.red;
        else moveSpeedPointText.color = Color.white;
    }

    /// <summary>
    /// ボタンを押すとムーブスピードポイントを加算しステータスに反映する
    /// </summary>
    public void MoveSpeedUpButtonDown()
    {
        MoveSpeedPointPlus();
        MoveSpeedUpStatus();
    }

    /// <summary>
    /// ムーブスピードステータスの基盤
    /// </summary>
    /// <param name="p_Speed"></param>
    /// <param name="p_MaxSpeed"></param>
    /// <param name="p_AnimationSpeed"></param>
    private void MoveSpeedUpParameter(float p_Speed, float p_MaxSpeed)
    {
        this.GetComponent<PlayerController>().playerMoveSpeed = p_Speed;
        this.GetComponent<PlayerController>().playerMaxMoveSpeed = p_MaxSpeed;
    }

    /// <summary>
    /// ムーブスピードアップステータス（プレイヤー）
    /// </summary>
    private void MoveSpeedUpStatus()
    {
        int i = moveSpeedPoint;
        float startSpeed = 11f;
        float startMaxSpeed = 5f;
        float speedPlusP = 1.0f;
        float maxSeedPlusP = 1.0f;

        if (moveSpeedPoint == 1) MoveSpeedUpParameter(startSpeed + speedPlusP, startMaxSpeed + maxSeedPlusP);

        else if (moveSpeedPoint == i) MoveSpeedUpParameter(startSpeed + speedPlusP * i, startMaxSpeed + maxSeedPlusP * i);

    }



    /// <summary>
    /// アタックスピードポイントを1加算する
    /// </summary>
    private void AttackSpeedPointPlus()
    {
        if (attackSpeedPoint < 10 && skillPoint > 0)
        {
            attackSpeedPoint += 1;
            AttackSpeedPointPlusText();
            PlayerSkillMinus();
            PlayerStatusUpSE();
            AttackSpeedButtonShakeScaleAnimation();

            for (int i = 0; i < 2; i++)
            {
                statusUpEffect[i].GetComponent<StatusUpEffectController>().SpeedUpEffect();
            }


        }

        else return;
    }

    /// <summary>
    ///　アタックスピードボタンシェイクスケールアニメーション
    /// </summary>
    private void AttackSpeedButtonShakeScaleAnimation()
    {
        attackSpeedButtonTransform.DOShakeScale(0.5f, 0.5f)
            .Play();
        Invoke("AttackSpeedButtonShakeScaleReset", 0.6f);
    }

    /// <summary>
    ///　アタックスピードボタンシェイクスケールリセット
    /// </summary>
    private void AttackSpeedButtonShakeScaleReset()
    {
        attackSpeedButtonTransform.DOScale(new Vector3(1, 1, 1), 0.1f)
            .Play();
    }

    /// <summary>
    /// 加算のポイントをテキストに反映する
    /// </summary>
    private void AttackSpeedPointPlusText()
    {
        attackSpeedPointText.text = attackSpeedPoint.ToString();
        if (attackSpeedPoint == 10) attackSpeedPointText.color = Color.red;
        else attackSpeedPointText.color = Color.white;
    }

    /// <summary>
    /// ボタンを押すとアタックスピードポイントを加算しステータスに反映する
    /// </summary>
    public void AttackSpeedUpButtonDown()
    {
        AttackSpeedPointPlus();
        AttackSpeedUpStatus();
    }

    /// <summary>
    /// アタックスピードステータスの基盤
    /// </summary>
    /// <param name="pA_chargeSpeed"></param>
    private void AttackSpeedUpParameter(float pA_chargeSpeed)
    {
        playerAttackPoint.GetComponent<PlayerAttackController>().chargeSpeed = pA_chargeSpeed;
    }

    /// <summary>
    /// アタックスピードアップステータス（アタックポイント）
    /// </summary>
    private void AttackSpeedUpStatus()
    {
        int i = attackSpeedPoint;
        float startP = 0.5f;
        float plusP = 0.1f;

        if (attackSpeedPoint == 1) AttackSpeedUpParameter(startP + plusP);

        else if (attackSpeedPoint == i) AttackSpeedUpParameter((startP) + plusP * i);


    }



    /// <summary>
    /// アタックパワーポイントを1加算する
    /// </summary>
    private void AttackPowerPointPlus()
    {
        if (attackPowerPoint < 10 && skillPoint > 0)
        {
            attackPowerPoint += 1;
            AttackPowerPointPlusText();
            PlayerSkillMinus();
            PlayerStatusUpSE();
            AttackPowerButtonShakeScaleAnimation();

            for (int i = 0; i < 2; i++)
            {
                statusUpEffect[i].GetComponent<StatusUpEffectController>().PowerUpEffect();
            }
        }

        else return;
    }

    /// <summary>
    ///　アタックスパワーボタンシェイクスケールアニメーション
    /// </summary>
    private void AttackPowerButtonShakeScaleAnimation()
    {
        attackPowerButtonTransform.DOShakeScale(0.5f, 0.5f)
            .Play();

        Invoke("AttackPowerButtonShakeScaleReset", 0.6f);
    }

    /// <summary>
    ///　アタックパワーボタンシェイクスケールリセット
    /// </summary>
    private void AttackPowerButtonShakeScaleReset()
    {
        attackPowerButtonTransform.DOScale(new Vector3(1, 1, 1), 0.1f)
            .Play();
    }

    /// <summary>
    /// 加算のポイントをテキストに反映する
    /// </summary>
    private void AttackPowerPointPlusText()
    {
        attackPowerPointText.text = attackPowerPoint.ToString();
        if (attackPowerPoint == 10) attackPowerPointText.color = Color.red;
        else attackPowerPointText.color = Color.white;
    }

    /// <summary>
    /// ボタンを押すとアタックパワーポイントを加算しステータスに反映する
    /// </summary>
    public void AttackPowerUpButtonDown()
    {
        AttackPowerPointPlus();
        AttackPowerUpStatus();
    }

    /// <summary>
    /// アタックパワーステータスの基盤
    /// </summary>
    /// <param name="pA_damage"></param>
    /// <param name="pA_Charge1"></param>
    /// <param name="pA_Charge2"></param>
    /// <param name="pA_Charge3"></param>
    private void AttackPowerUpParameter(float pA_damage, float pA_Charge1, float pA_Charge2, float pA_Charge3)
    {
        playerAttackPoint.GetComponent<PlayerAttackController>().nomalDamage = pA_damage;
        playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage1 = pA_Charge1;
        playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage2 = pA_Charge2;
        playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage3 = pA_Charge3;

        //チャージ2スキルアップ時に呼び出す為代入し保存する
        ps_ChargeDamage2 = pA_Charge2;

        //チャージ3スキルアップ時に呼び出す為代入し保存する
        ps_ChargeDamage3 = pA_Charge3;

        //チャージ2攻撃には追加ダメージを加算
        playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage2 += playerAttackPoint.GetComponent<PlayerAttackController>().charge2DamagePlus;

        //チャージ3攻撃には追加ダメージを加算
        playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage3 += playerAttackPoint.GetComponent<PlayerAttackController>().charge3DamagePlus;
    }

    /// <summary>
    /// アタックパワーアップステータス（アタックポイント）
    /// </summary>
    private void AttackPowerUpStatus()
    {
        int i = attackPowerPoint;
        float nomalD = 2.5f;
        float charge1D = 1.5f;
        float charge2D = 5f;
        float charge3D = 50f;
        float nomalPlusD = 0.25f;
        float charge1plusD = 0.25f;
        float charge2plusD = 2f;
        float charge3plusD = 10f;


        if (attackPowerPoint == 1) AttackPowerUpParameter(nomalD + nomalPlusD, charge1D + charge1plusD, charge2D + charge2plusD, charge3D + charge3plusD);

        else if (attackPowerPoint == i) AttackPowerUpParameter(nomalD + nomalPlusD * i, charge1D + charge1plusD * i, charge2D + charge2plusD * i, charge3D + charge3plusD * i);

    }



    /// <summary>
    /// チャージ1スキルポイントを1加算する
    /// </summary>
    private void Charge1SkillPointPlus()
    {
        if (charge1SkillPoint < 10 && skillPoint > 0)
        {
            charge1SkillPoint += 1;
            Charge1SkillPointPlusText();
            PlayerSkillMinus();
            PlayerStatusUpSE();
            Charge1SkillButtonShakeScaleAnimation();

            for (int i = 0; i < 2; i++)
            {
                statusUpEffect[i].GetComponent<StatusUpEffectController>().Charge1UpEffect();
            }
        }

        else return;
    }

    /// <summary>
    /// 加算のポイントをテキストに反映する
    /// </summary>
    private void Charge1SkillPointPlusText()
    {
        charge1SkillPointText.text = charge1SkillPoint.ToString();
        if (charge1SkillPoint == 10) charge1SkillPointText.color = Color.red;
        else charge1SkillPointText.color = Color.white;
    }

    /// <summary>
    ///　チャージ1スキルボタンシェイクスケールアニメーション
    /// </summary>
    private void Charge1SkillButtonShakeScaleAnimation()
    {
        charge1SkillButtonTransform.DOShakeScale(0.5f, 0.5f)
             .Play();

        Invoke("Charge1SkillButtonShakeScaleReset", 0.6f);
    }

    /// <summary>
    ///　チャージ1スキルボタンシェイクスケールリセット
    /// </summary>
    private void Charge1SkillButtonShakeScaleReset()
    {
        charge1SkillButtonTransform.DOScale(new Vector3(1, 1, 1), 0.1f)
            .Play();
    }

    /// <summary>
    /// ボタンを押すとチャージ1スキルポイントを加算しステータスに反映する
    /// </summary>
    public void Charge1SkillUpButtonDown()
    {
        Charge1SkillPointPlus();
        Charge1SkillUpStatus();
    }

    /// <summary>
    /// チャージ1スキルステータスの基盤
    /// </summary>
    /// <param name="charge1"></param>
    private void Charge1SkillUpParameter(int charge1)
    {
        charge1SkillPoint = charge1;
    }

    /// <summary>
    /// チャージ1スキルステータス（アタックポイント）
    /// </summary>
    private void Charge1SkillUpStatus()
    {
        int i = charge1SkillPoint;

        int charge1P = 0;
        int charge1plusP = 1;

        if (charge1SkillPoint == 1) Charge1SkillUpParameter(charge1P + charge1plusP);

        else if (charge1SkillPoint == i) Charge1SkillUpParameter(charge1P + charge1plusP * i);

    }



    /// <summary>
    /// チャージ2スキルポイントを1加算する
    /// </summary>
    private void Charge2SkillPointPlus()
    {
        if (charge2SkillPoint < 10 && skillPoint > 0)
        {
            charge2SkillPoint += 1;
            Charge2SkillPointPlusText();
            PlayerSkillMinus();
            PlayerStatusUpSE();
            Charge2SkillButtonShakeScaleAnimation();

            for (int i = 0; i < 2; i++)
            {
                statusUpEffect[i].GetComponent<StatusUpEffectController>().Charge2UpEffect();
            }
        }

        else return;
    }

    /// <summary>
    ///　チャージ2スキルボタンシェイクスケールアニメーション
    /// </summary>
    private void Charge2SkillButtonShakeScaleAnimation()
    {
        charge2SkillButtonTransform.DOShakeScale(0.5f, 0.5f)
             .Play();

        Invoke("Charge2SkillButtonShakeScaleReset", 0.6f);
    }

    /// <summary>
    ///　チャージ2スキルボタンシェイクスケールリセット
    /// </summary>
    private void Charge2SkillButtonShakeScaleReset()
    {
        charge2SkillButtonTransform.DOScale(new Vector3(1, 1, 1), 0.1f)
            .Play();
    }

    /// <summary>
    /// 加算のポイントをテキストに反映する
    /// </summary>
    private void Charge2SkillPointPlusText()
    {
        charge2SkillPointText.text = charge2SkillPoint.ToString();
        if (charge2SkillPoint == 10) charge2SkillPointText.color = Color.red;
        else charge2SkillPointText.color = Color.white;
    }

    /// <summary>
    /// ボタンを押すとチャージ2スキルポイントを加算しステータスに反映する
    /// </summary>
    public void Charge2SkillUpButtonDown()
    {
        Charge2SkillPointPlus();
        Charge2SkillUpStatus();
    }

    /// <summary>
    /// チャージ2スキルステータスの基盤
    /// </summary>
    /// <param name="charge2"></param>
    private void Charge2SkillUpParameter(int charge2, float pA_charge2DamagePlus)
    {
        charge2SkillPoint = charge2;
        playerAttackPoint.GetComponent<PlayerAttackController>().charge2DamagePlus = pA_charge2DamagePlus;

        //チャージ2攻撃には追加ダメージを加算
        playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage2 = ps_ChargeDamage2;
        playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage2 += playerAttackPoint.GetComponent<PlayerAttackController>().charge2DamagePlus;
    }

    /// <summary>
    /// チャージ2スキルステータス（アタックポイント）
    /// </summary>
    private void Charge2SkillUpStatus()
    {
        int i = charge2SkillPoint;

        int charge2P = 0;
        float charge2D = 0;
        int charge2plusP = 1;
        float charge2plusD = 2.5f;


        if (charge2SkillPoint == 1) Charge2SkillUpParameter(charge2P + charge2plusP, charge2D + charge2plusD);

        else if (charge2SkillPoint == i) Charge2SkillUpParameter(charge2P + charge2plusP * i, charge2D + charge2plusD * i);

    }



    /// <summary>
    /// チャージ3スキルポイントを1加算する
    /// </summary>
    private void Charge3SkillPointPlus()
    {
        if (charge3SkillPoint < 10 && skillPoint > 0)
        {
            charge3SkillPoint += 1;
            Charge3SkillPointPlusText();
            PlayerSkillMinus();
            PlayerStatusUpSE();
            Charge3SkillButtonShakeScaleAnimation();

            for (int i = 0; i < 2; i++)
            {
                statusUpEffect[i].GetComponent<StatusUpEffectController>().Charge3UpEffect();
            }
        }

        else return;
    }

    /// <summary>
    ///　チャージ3スキルボタンシェイクスケールアニメーション
    /// </summary>
    private void Charge3SkillButtonShakeScaleAnimation()
    {
        charge3SkillButtonTransform.DOShakeScale(0.5f, 0.5f)
             .Play();
        Invoke("Charge3SkillButtonShakeScaleReset", 0.6f);
    }

    /// <summary>
    ///　チャージ3スキルボタンシェイクスケールリセット
    /// </summary>
    private void Charge3SkillButtonShakeScaleReset()
    {
        charge3SkillButtonTransform.DOScale(new Vector3(1, 1, 1), 0.1f)
            .Play();
    }

    /// <summary>
    /// 加算のポイントをテキストに反映する
    /// </summary>
    private void Charge3SkillPointPlusText()
    {
        charge3SkillPointText.text = charge3SkillPoint.ToString();
        if (charge3SkillPoint == 10) charge3SkillPointText.color = Color.red;
        else charge3SkillPointText.color = Color.white;
    }

    /// <summary>
    /// ボタンを押すとチャージ3スキルポイントを加算しステータスに反映する
    /// </summary>
    public void Charge3SkillUpButtonDown()
    {
        Charge3SkillPointPlus();
        Charge3SkillUpStatus();
    }

    /// <summary>
    /// チャージ3スキルステータスの基盤
    /// </summary>
    /// <param name="pA_charge3ExpoScale"></param>
    /// <param name="pA_charge3DamagePlus"></param>
    private void Charge3SkillUpParameter(float pA_charge3ExpoScale, float pA_charge3DamagePlus)
    {
        playerAttackPoint.GetComponent<PlayerAttackController>().charge3ExplosionScale = pA_charge3ExpoScale;
        playerAttackPoint.GetComponent<PlayerAttackController>().charge3DamagePlus = pA_charge3DamagePlus;

        //チャージ3攻撃には追加ダメージを加算
        playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage3 = ps_ChargeDamage3;
        playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage3 += playerAttackPoint.GetComponent<PlayerAttackController>().charge3DamagePlus;
    }

    /// <summary>
    /// チャージ3スキルステータス（アタックポイント）
    /// </summary>
    private void Charge3SkillUpStatus()
    {
        int i = charge3SkillPoint;

        float charge3S = 0.75f;
        float charge3D = 5f;
        float charge3plusS = 0.25f;
        float charge3plusD = 5f;


        if (charge3SkillPoint == 1) Charge3SkillUpParameter(charge3S + charge3plusS, charge3D + charge3plusD);

        else if (charge3SkillPoint == i) Charge3SkillUpParameter(charge3S + charge3plusS * i, charge3D + charge3plusD * i);

    }



    /// <summary>
    /// 現在のステータスを記憶する
    /// </summary>
    private void CurrentStatusSaving()
    {
        currentMoveSpeedPointSave = moveSpeedPoint;
        currentAttackSpeedPointSave = attackSpeedPoint;
        currentAttackPowerPointSave = attackPowerPoint;
        currentCharge1SkillPointSave = charge1SkillPoint;
        currentCharge2SkillPointSave = charge2SkillPoint;
        currentCharge3SkillPointSave = charge3SkillPoint;
    }

    /// <summary>
    /// 通常時のステータスをバスターシンクロ時のステータスに上げる基盤
    /// </summary>
    private void BusterSynchroStatusUp()
    {
        moveSpeedPoint = busterSynchroMaxPoint;
        attackSpeedPoint = busterSynchroMaxPoint;
        attackPowerPoint = busterSynchroMaxPoint;
        charge1SkillPoint = busterSynchroMaxPoint;
        charge2SkillPoint = busterSynchroMaxPoint;
        charge3SkillPoint = busterSynchroMaxPoint;
    }

    /// <summary>
    /// バスターシンクロ時のステータスを通常のステータスに戻す
    /// </summary>
    private void BusterSynchroStatusCurrentBack()
    {
        moveSpeedPoint = currentMoveSpeedPointSave;
        attackSpeedPoint = currentAttackSpeedPointSave;
        attackPowerPoint = currentAttackPowerPointSave;
        charge1SkillPoint = currentCharge1SkillPointSave;
        charge2SkillPoint = currentCharge2SkillPointSave;
        charge3SkillPoint = currentCharge3SkillPointSave;
    }

    /// <summary>
    /// バスターシンクロ開始（リミットオフスライダー呼び出し）
    /// </summary>
    public void BusterSynchroStart()
    {

        busterSynchroBool = true;
        CurrentStatusSaving();
        BusterSynchroStatusUp();
        //ステータスを反映
        MoveSpeedUpStatus();
        AttackSpeedUpStatus();
        AttackPowerUpStatus();
        Charge1SkillUpStatus();
        Charge2SkillUpStatus();
        Charge3SkillUpStatus();
        //ボタンテキストに反映
        BusterSynchroButtonsShakeScaleAnimation();
        MoveSpeedPointPlusText();
        AttackSpeedPointPlusText();
        AttackPowerPointPlusText();
        Charge1SkillPointPlusText();
        Charge2SkillPointPlusText();
        Charge3SkillPointPlusText();
        //スキンを反映
        this.GetComponent<PlayerController>().BusterSynchroAvatarChange();
        this.GetComponent<PlayerController>().BusterSynchroAnimation();
        BusterSynchroEffectActive();
        NomalSynchroSkinActiveOff();
        BusterSynchroSkinActiveOn();
        BusterSynchroPlayerAttackPointMeshActiveOff();
        BusterSynchroPlayerAttackPointPosition();
        BusterSynchroStartSound();
    }

    /// <summary>
    /// バスターシンクロ終了(ステータスを戻す)（リミットオフスライダー制限時間後呼び出し）
    /// </summary>
    public void BusterSynchroEnd()
    {
        if (busterSynchroBool)
        {
            busterSynchroBool = false;
            BusterSynchroStatusCurrentBack();
            //ステータスを反映
            MoveSpeedUpStatus();
            AttackSpeedUpStatus();
            AttackPowerUpStatus();
            Charge1SkillUpStatus();
            Charge2SkillUpStatus();
            Charge3SkillUpStatus();
            //ボタンテキストに反映
            BusterSynchroButtonsShakeScaleAnimation();
            MoveSpeedPointPlusText();
            AttackSpeedPointPlusText();
            AttackPowerPointPlusText();
            Charge1SkillPointPlusText();
            Charge2SkillPointPlusText();
            Charge3SkillPointPlusText();
            //スキンを反映
            BusterSynchroEndEffect();
            BusterSynchroSkinActiveOff();
            NomalSynchroSkinActiveOn();
            this.GetComponent<PlayerController>().PlayerAvatarChangeBack();
            BusterSynchroPlayerAttackPointMeshActiveOn();
        }
        else return;
    }

    /// <summary>
    /// バスターシンクロスタートSE
    /// </summary>
    private void BusterSynchroStartSound()
    {
        audioSource.PlayOneShot(busterSynchroSE);
    }

    /// <summary>
    /// 通常操作のノーマルスキンをアクティブに
    /// </summary>
    private void NomalSynchroSkinActiveOn()
    {
        nomalSkin.SetActive(true);
    }

    /// <summary>
    /// 通常操作のノーマルスキンを非アクティブに
    /// </summary>
    private void NomalSynchroSkinActiveOff()
    {
        nomalSkin.SetActive(false);
    }

    /// <summary>
    /// バスターシンクロ時のスキンをアクティブに
    /// </summary>
    private void BusterSynchroSkinActiveOn()
    {
        busterSynchroSkin.SetActive(true);
    }

    /// <summary>
    /// バスターシンクロエフェウトをアクティブに
    /// </summary>
    private void BusterSynchroEffectActive()
    {
        busterSynchroEffect.SetActive(true);
    }

    /// <summary>
    /// バスターシンクロ終了エフェウトをアクティブに
    /// </summary>
    private void BusterSynchroEndEffect()
    {
        limitTimeEndEffect[0].SetActive(true);
    }

    /// <summary>
    /// バスターシンクロ時のスキンを非アクティブに
    /// </summary>
    private void BusterSynchroSkinActiveOff()
    {
        busterSynchroSkin.SetActive(false);
    }

    /// <summary>
    /// 現在のアタックポイントの位置を記憶する
    /// </summary>
    private void PlayerAttackPointPositionCurrentSaving()
    {
        playerAttackPointCurrentPosition = playerAttackPoint.transform.localPosition;
    }

    /// <summary>
    /// バスターシンクロ時アタックポイントのメッシュレンダラーを非アクティブに
    /// </summary>
    private void BusterSynchroPlayerAttackPointMeshActiveOff()
    {
        playerAttackPointMesh.enabled = false;
    }

    /// <summary>
    /// バスターシンクロ時アタックポイントのメッシュレンダラーをアクティブに
    /// </summary>
    private void BusterSynchroPlayerAttackPointMeshActiveOn()
    {
        playerAttackPointMesh.enabled = true;
    }

    /// <summary>
    /// バスターシンクロ時アタックポイントの位置を変更する
    /// </summary>
    private void BusterSynchroPlayerAttackPointPosition()
    {
        Vector3 busterPos = new Vector3(-0.1f, 3, 2.5f);
        playerAttackPoint.transform.localPosition = busterPos;
    }

    /// <summary>
    ///　バスターシンクロ時ステータスアップボタンをシェイクスケールアニメーション
    /// </summary>
    private void BusterSynchroButtonsShakeScaleAnimation()
    {
        MoveButtonShakeScaleAnimation();
        AttackSpeedButtonShakeScaleAnimation();
        AttackPowerButtonShakeScaleAnimation();
        Charge1SkillButtonShakeScaleAnimation();
        Charge2SkillButtonShakeScaleAnimation();
        Charge3SkillButtonShakeScaleAnimation();
    }

    /// <summary>
    /// ウィザードエンジン開始（リミットオフスライダー呼び出し）
    /// </summary>
    public void WizardEngineStart()
    {
        wizardEngineBool = true;
        WizardEnginePlayerAttackPointPosition();
        WizardEngineEffectActive();
        WizardEngineStartSound();
    }

    /// <summary>
    /// ウィザードエンジン終了（リミットオフスライダー制限時間後呼び出し）
    /// </summary>
    public void WizardEngineEnd()
    {
        if (wizardEngineBool)
        {
            wizardEngineBool = false;
            WizardEngineEndEffect();

        }
        else return;
    }

    /// <summary>
    /// ウィザードエンジンエフェクトをアクティブに
    /// </summary>
    private void WizardEngineEffectActive()
    {
        for (int i = 0; i < 4; i++)
        {
            wizardEngineEffect[i].SetActive(true);
        }
    }

    /// <summary>
    /// ウィザードエンジン終了エフェクトをアクティブに
    /// </summary>
    private void WizardEngineEndEffect()
    {
        for (int i = 1; i < 5; i++)
        {
            limitTimeEndEffect[i].SetActive(true);
        }
    }

    /// <summary>
    /// ウィザードエンジンスタートSE
    /// </summary>
    private void WizardEngineStartSound()
    {
        audioSource.PlayOneShot(wizardEngineSE);
    }

    /// <summary>
    /// ウィザードエンジン時アタックポイントの位置を変更する
    /// </summary>
    private void WizardEnginePlayerAttackPointPosition()
    {
        Vector3 attackPos = new Vector3(-2, 5, 0.4f);
        playerAttackPoint.transform.localPosition = attackPos;
    }

    /// <summary>
    /// リミットタイムエンドSE（バスター・ウィザード終了時）（リミットオフスライダー制限時間後呼び出し）
    /// </summary>
    public void LimitTimeEndSound()
    {
        audioSource.PlayOneShot(limitTimeEndSE);
    }

    /// <summary>
    /// アタックポイントの位置を初期位置へ戻す（リミットオフスライダー制限時間後呼び出し）
    /// </summary>
    public void PlayerAttackPointPositionCurrentBack()
    {
        playerAttackPoint.transform.localPosition = playerAttackPointCurrentPosition;
    }

}
