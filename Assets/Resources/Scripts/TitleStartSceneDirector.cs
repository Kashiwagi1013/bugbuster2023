using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class TitleStartSceneDirector : MonoBehaviour
{
    [SerializeField] GameObject titleScreen;//タイトルスクリーン
    [SerializeField] GameObject titleUiTexts;//タイトルのUIテキスト関連
    [SerializeField] GameObject menuScreen;//メニュースクリーン
    [SerializeField] GameObject buttonView;//ボタンビュー
    [SerializeField] GameObject manualView;//マニュアルビュー
    [SerializeField] GameObject rankingView;//ランキングビュー
    [SerializeField] GameObject attackCustomView;//アタックカスタムビュー
    [SerializeField] GameObject creditView;//クレジットビュー
    [SerializeField] Image sceneChangeBackImage;//シーンチェンジ時のバックイメージ
    [SerializeField] Text titleText;//タイトルボタンテキスト
    [SerializeField] Text titleButtonText;//タイトルボタンテキスト
    [SerializeField] AudioClip gameStartButtonSE;//ゲームスタートボタン用SE
    [SerializeField] AudioClip ViewOpenButtonSE;//マニュアル・ランキングオープン用SE
    [SerializeField] AudioClip ViewCloseButtonSE;//マニュアル・ランキングクローズ用SE

    private GameObject CustomManager;//カスタムマネージャー
    private bool buttonBool = false;//ボタン一回呼び出し
    private AudioSource audioSource;//オーディオ取得用



    private void Start()
    {
        sceneChangeBackImage.enabled = true;
        CustomManager = GameObject.FindGameObjectWithTag("CustomManager");
        audioSource = gameObject.GetComponent<AudioSource>();
        StartAnimationTitle();
        SceneChangeBackImageAnimationOff();
        Invoke("SceneChangeBackImageSetActiveOffInvoke", 1.5f);
    }

    /// <summary>
    /// タイトルからメニュー画面を表示（ボタン割り当て）
    /// </summary>
    public void OnClickTitleActiveOff()
    {
        if (!buttonBool)
        {
            audioSource.PlayOneShot(ViewOpenButtonSE);
            Invoke("TitleTextActiveOffAnimation", 0.2f);
            Invoke("TitleButtonTextActiveOffAnimation", 0.2f);
            MenuScreenActiveOn();
            MenuScreenActiveOnAnimation();
            buttonView.SetActive(true);
            Invoke("ButtonViewActiveAnimation", 0.5f);
            Invoke("TitleUiTextsActiveOff", 1f);
            buttonBool = true;
        }
    }

    /// <summary>
    /// ゲーム画面へシーン遷移（ボタン割り当て）
    /// </summary>
    public void OnClickTutorialStartButton()
    {
        if (buttonBool)
        {
            audioSource.PlayOneShot(gameStartButtonSE);
            Invoke("TutorialStart", 1.5f);
            buttonBool = false;
        }
    }

    /// <summary>
    /// ゲーム画面へシーン遷移（ボタン割り当て）
    /// </summary>
    public void OnClickGameStartButton()
    {
        if (buttonBool)
        {
            audioSource.PlayOneShot(gameStartButtonSE);
            Invoke("GameStart", 1.5f);
            buttonBool = false;
        }
    }

    /// <summary>
    /// マニュアルビュー表示（ボタン割り当て）
    /// </summary>
    public void OnClickManualViewButton()
    {
        if (buttonBool)
        {
            audioSource.PlayOneShot(ViewOpenButtonSE);
            ManualViewActiveAnimation();
            manualView.SetActive(true);
            ButtonViewActiveOffAnimation();
            Invoke("ButtonViewActiveOffInvoke",0.5f);
            buttonBool = false;

        }

    }

    /// <summary>
    /// マニュアルビューを閉じる（ボタン割り当て）
    /// </summary>
    public void OnClickManualViewBackButton()
    {
        if (!buttonBool)
        {
            audioSource.PlayOneShot(ViewCloseButtonSE);
            buttonView.SetActive(true);
            ButtonViewActiveAnimation();
            ManualViewActiveOffAnimation();
            Invoke("ManualViewActiveOffInvoke", 0.5f);
            buttonBool = true;
        }
    }

    /// <summary>
    /// ランキングビュー表示/ランキングデータ取得（ボタン割り当て）
    /// </summary>
    public void OnClickRankingViewButton()
    {
        if (buttonBool)
        {
            audioSource.PlayOneShot(ViewOpenButtonSE);
            rankingView.SetActive(true);
            RankingViewActiveAnimation();
            ButtonViewActiveOffAnimation();
            Invoke("ButtonViewActiveOffInvoke", 0.5f);
            buttonBool = false;
        }
    }

    /// <summary>
    /// ランキングビューを閉じる（ボタン割り当て）
    /// </summary>
    public void OnClickRankingViewBackButton()
    {
        if (!buttonBool)
        {
            audioSource.PlayOneShot(ViewCloseButtonSE);
            buttonView.SetActive(true);
            ButtonViewActiveAnimation();
            RankingViewActiveOffAnimation();
            Invoke("RankingViewActiveOffInvoke", 0.5f);
            buttonBool = true;
        }
    }

    /// <summary>
    /// アタックカスタムビュー表示（ボタン割り当て）
    /// </summary>
    public void OnClickAttackCustomViewButton()
    {
        if (buttonBool)
        {
            audioSource.PlayOneShot(ViewOpenButtonSE);
            attackCustomView.SetActive(true);
            AttackCustomViewActiveAnimation();
            ButtonViewActiveOffAnimation();
            Invoke("ButtonViewActiveOffInvoke", 0.5f);
            buttonBool = false;
        }
    }

    /// <summary>
    /// アタックカスタムビューを閉じる（ボタン割り当て）
    /// </summary>
    public void OnClickAttackCustomViewBackButton()
    {
        if (!buttonBool)
        {
            audioSource.PlayOneShot(ViewCloseButtonSE);
            buttonView.SetActive(true);
            ButtonViewActiveAnimation();
            AttackCustomActiveOffAnimation();
            Invoke("AttackCustomActiveOffInvoke", 0.5f);
            buttonBool = true;
        }
    }

    /// <summary>
    /// クレジットビュー表示（ボタン割り当て）
    /// </summary>
    public void OnClickCreditViewButton()
    {
        if (buttonBool)
        {
            audioSource.PlayOneShot(ViewOpenButtonSE);
            creditView.SetActive(true);
            CreditViewActiveAnimation();
            ButtonViewActiveOffAnimation();
            Invoke("ButtonViewActiveOffInvoke", 0.5f);
            buttonBool = false;
        }
    }

    /// <summary>
    ///クレジットビューを閉じる（ボタン割り当て）
    /// </summary>
    public void OnClickCreditViewBackButton()
    {
        if (!buttonBool)
        {
            audioSource.PlayOneShot(ViewCloseButtonSE);
            buttonView.SetActive(true);
            ButtonViewActiveAnimation();
            CreditViewActiveOffAnimation();
            Invoke("CreditViewActiveOffInvoke", 0.5f);
            buttonBool = true;
        }
    }

    /// <summary>
    /// シーンチェンジ用バックイラストを透明にするアニメーション
    /// </summary>
    private void SceneChangeBackImageAnimationOff()
    {
        sceneChangeBackImage.DOFade(0f, 2f);
    }

    /// <summary>
    /// シーンチェンジ用バックイラストを非アクティブにする（インヴォーク）
    /// </summary>
    private void SceneChangeBackImageSetActiveOffInvoke()
    {
        sceneChangeBackImage.enabled = false;
    }

    /// <summary>
    /// 初回時のスタート時にタイトルを表示する以降は省略する
    /// </summary>
    private void StartAnimationTitle()
    {
        if (CustomManager.GetComponent<GameCustomManager>().movieCallOne)
        {
            TitleTouchStartButtonAnimation();
        }

        else
        {
            buttonBool = true;
            MenuScreenActiveOn();
            MenuScreenActiveOnAnimation();
            TitleUiTextsActiveOff();
            buttonView.SetActive(true);
            Invoke("ButtonViewActiveAnimation", 0.5f);
        }

    }

    /// <summary>
    /// タイトル画面の-TOUCH START-ボタンのアニメーション
    /// </summary>
    private void TitleTouchStartButtonAnimation()
    {
        titleButtonText.DOFade(0.0f, 1.0f)
            .SetLoops(-1, LoopType.Yoyo)
            .Play();
    }

    /// <summary>
    /// タイトルのテキストを非表示にするアニメーション
    /// </summary>
    private void TitleTextActiveOffAnimation()
    {
        titleText.transform.DOScale(new Vector3(0, 0, 0), 0.7f)
            .SetLoops(1)
            .Play();
    }

    /// <summary>
    /// タイトルのボタンテキストを非表示にするアニメーション
    /// </summary>
    private void TitleButtonTextActiveOffAnimation()
    {
        titleButtonText.transform.DOScale(new Vector3(0, 0, 0), 0.7f)
            .SetLoops(1)
            .Play();
    }

    /// <summary>
    /// タイトルで使っていたUIオブジェクトを非アクティブにする（インヴォーク）
    /// </summary>
    private void TitleUiTextsActiveOff()
    {
        titleUiTexts.SetActive(false);
    }

    /// <summary>
    /// メニュー画面をアクティブに
    /// </summary>
    private void MenuScreenActiveOn()
    {
        menuScreen.SetActive(true);
    }

    /// <summary>
    /// メニュー画面を表示するアニメーション
    /// </summary>
    private void MenuScreenActiveOnAnimation()
    {
        menuScreen.transform.DOScale(new Vector3(1, 1, 1),0.5f)
            .SetLoops(1)
            .Play();
    }

    /// <summary>
    /// ボタンUI関連が表示されるアニメーション
    /// </summary>
    private void ButtonViewActiveAnimation()
    {
        buttonView.transform.DOScale(new Vector3(1, 1, 1), 0.5f)
            .SetLoops(1)
            .Play();
    }

    /// <summary>
    /// ボタンUI関連が非表示になるアニメーション
    /// </summary>
    private void ButtonViewActiveOffAnimation()
    {
        buttonView.transform.DOScale(new Vector3(0, 0, 0), 0.5f)
            .SetLoops(1)
            .Play();
    }

    /// <summary>
    /// ボタンUI関連オブジェクトが非アクティブに
    /// </summary>
    private void ButtonViewActiveOffInvoke()
    {
        buttonView.SetActive(false);
    }

    /// <summary>
    /// マニュアルが表示されるアニメーション
    /// </summary>
    private void ManualViewActiveAnimation()
    {
        manualView.transform.localScale = new Vector3(0, 0, 0);
        manualView.transform.DOScale(new Vector3(1, 1, 1), 0.5f)
            .SetLoops(1)
            .Play();
    }

    /// <summary>
    /// ランキングが表示されるアニメーション
    /// </summary>
    private void RankingViewActiveAnimation()
    {
        rankingView.transform.localScale = new Vector3(0,0,0);
        rankingView.transform.DOScale(new Vector3(1, 1, 1), 0.5f)
            .SetLoops(1)
            .Play();
    }

    /// <summary>
    /// アタックカスタムが表示されるアニメーション
    /// </summary>
    private void AttackCustomViewActiveAnimation()
    {
        attackCustomView.transform.localScale = new Vector3(0, 0, 0);
        attackCustomView.transform.DOScale(new Vector3(1, 1, 1), 0.5f)
            .SetLoops(1)
            .Play();
    }

    /// <summary>
    /// クレジットが表示されるアニメーション
    /// </summary>
    private void CreditViewActiveAnimation()
    {
        creditView.transform.localScale = new Vector3(0, 0, 0);
        creditView.transform.DOScale(new Vector3(1, 1, 1), 0.5f)
            .SetLoops(1)
            .Play();
    }

    /// <summary>
    /// マニュアルが非表示になるアニメーション
    /// </summary>
    private void ManualViewActiveOffAnimation()
    {
        manualView.transform.DOScale(new Vector3(0, 0, 0), 0.5f)
            .SetLoops(1)
            .Play();
    }

    /// <summary>
    /// ランキングが非表示になるアニメーション
    /// </summary>
    private void RankingViewActiveOffAnimation()
    {
        rankingView.transform.DOScale(new Vector3(0, 0, 0), 0.5f)
            .SetLoops(1)
            .Play();
    }

    /// <summary>
    /// アタックカスタムが非表示になるアニメーション
    /// </summary>
    private void AttackCustomActiveOffAnimation()
    {
        attackCustomView.transform.DOScale(new Vector3(0, 0, 0), 0.5f)
            .SetLoops(1)
            .Play();
    }

    /// <summary>
    /// クレジットが非表示になるアニメーション
    /// </summary>
    private void CreditViewActiveOffAnimation()
    {
        creditView.transform.DOScale(new Vector3(0, 0, 0), 0.5f)
            .SetLoops(1)
            .Play();
    }

    /// <summary>
    /// マニュアルのオブジェクトが非アクティブにする（インヴォーク）
    /// </summary>
    private void ManualViewActiveOffInvoke()
    {
        manualView.SetActive(false);
    }

    /// <summary>
    /// ランキングのオブジェクトが非アクティブにする（インヴォーク）
    /// </summary>
    private void RankingViewActiveOffInvoke()
    {
        rankingView.SetActive(false);
    }

    /// <summary>
    /// アタックカスタムのオブジェクトが非アクティブにする（インヴォーク）
    /// </summary>
    private void AttackCustomActiveOffInvoke()
    {
        attackCustomView.SetActive(false);
    }

    /// <summary>
    /// クレジットのオブジェクトが非アクティブにする（インヴォーク） 
    /// </summary>
    private void CreditViewActiveOffInvoke()
    {
        creditView.SetActive(false);
    }

    /// <summary>
    /// ゲームスタート（インヴォーク呼び出し用）
    /// </summary>
    private void GameStart()
    {
        SceneManager.LoadScene("GameScene");
    }


    /// <summary>
    /// ゲームスタート（インヴォーク呼び出し用）
    /// </summary>
    private void TutorialStart()
    {
        SceneManager.LoadScene("TutorialScene");
    }


}