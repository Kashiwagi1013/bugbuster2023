using UnityEngine;

public class ChargeAttackShell2Controller : MonoBehaviour
{

    [SerializeField] float shellSpeed;//shell攻撃速度（飛んでいく速さ）

    private GameObject firstTarget;//攻撃開始時の最初のターゲットを識別
    private float shotLifeTime = 0.0f;//Shellの生存時間


    /// <summary>
    /// 毎回アクティブになると呼び出される
    /// </summary>
    private void OnEnable()
    {
        TargetMemory();
        ChargeAttackRotate();
    }

    private void FixedUpdate()
    {
        ChargeAttackShot();
        ShellActiveOff();
    }

    /// <summary>
    /// 非アクティブになるたびに呼ばれる
    /// </summary>
    private void OnDisable()
    {
        ShotLifeTimeReset();
        TagetMemoryReset();
    }

    /// <summary>
    /// 攻撃始動時の現在の敵を記憶
    /// </summary>
    private void TargetMemory()
    {
        if (EnemyController.currentTarget == true)
        {
            firstTarget = EnemyController.currentTarget;
        }
        else if (BossController.currentTarget == true)
        {
            firstTarget = BossController.currentTarget;
        }
        else if (BossGuardController.currentTarget == true)
        {
            firstTarget = BossGuardController.currentTarget;
        }
        else return;
    }

    private void TagetMemoryReset()
    {
        firstTarget = null;
    }

    /// <summary>
    /// エネミーの方向を向く
    /// </summary>
    private void ChargeAttackRotate()
    {
        if (EnemyController.currentTarget == true)
        {
            this.transform.LookAt(firstTarget.transform.position);
        }
        else if (BossController.currentTarget == true)
        {
            this.transform.LookAt(firstTarget.transform.position);
        }
        else if (BossGuardController.currentTarget == true)
        {
            this.transform.LookAt(firstTarget.transform.position);
        }
        else return;
    }

    /// <summary>
    /// 現在敵が存在し、攻撃始動時に選択していたエネミーだったら攻撃（自信）が移動する
    /// エネミーが途中で変わる、または消滅したら移動しない
    /// </summary>
    private void ChargeAttackShot()
    {
        if (EnemyController.currentTarget == true && EnemyController.currentTarget == firstTarget)
            transform.position = Vector3.MoveTowards(transform.position, firstTarget.transform.position, shellSpeed);

        else if(BossController.currentTarget == true && BossController.currentTarget == firstTarget)
            transform.position = Vector3.MoveTowards(transform.position, firstTarget.transform.position, shellSpeed);

        else if (BossGuardController.currentTarget == true && BossGuardController.currentTarget == firstTarget)
            transform.position = Vector3.MoveTowards(transform.position, firstTarget.transform.position, shellSpeed);

        else return;
    }

    /// <summary>
    ///　ショット生存時間のリセット
    /// </summary>
    private void ShotLifeTimeReset()
    {
        shotLifeTime = 0.0f;
    }

    /// <summary>
    /// エネミーShell自身を非アクティブに
    /// </summary>
    private void ShellActiveOff()
    {
        shotLifeTime = shotLifeTime + Time.deltaTime;

        if (shotLifeTime >= 1.0f)
            this.gameObject.SetActive(false);

        else return;
    }
}
