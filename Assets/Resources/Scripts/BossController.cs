using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.AI;
using DG.Tweening;

public class BossController : MonoBehaviour
{

    [SerializeField] GameObject sliderColider;//スライダー表示の為GameObject取得用
    //[SerializeField] GameObject coliderTextObject;//コライダーテキストオブジェクト
    [SerializeField] GameObject bossLineAttackShell;//ラインアタックオブジェクト
    [SerializeField] GameObject bossDestroyEffect1;//ボス撃破時エフェクト1
    [SerializeField] GameObject bossDestroyEffect2;//ボス撃破時エフェクト2
    [SerializeField] GameObject damageEffactPrefab;//ダメージエフェクト
    [SerializeField] GameObject invincibleDamageEffactPrefab;//無敵状態ダメージエフェクト
    //[SerializeField] GameObject ColliderEffact;//コライダーエフェクト
    [SerializeField] GameObject bossSpawnEffect;//ボス出現エフェクト
    [SerializeField] GameObject bossGuardSpawnEffect;//ボスガード出現エフェクト
    [SerializeField] GameObject[] breakDownEfect;//故障エフェクト
    [SerializeField] GameObject bossHpSlider;//ボスHP用スライダー
    [SerializeField] GameObject enemy;//エネミーオブジェクト
    [SerializeField] GameObject attackPoint;//アタックポイント
    [SerializeField] GameObject bossGuardForward;//ボスガードフォワード
    [SerializeField] GameObject bossGuardLeft;//ボスガードレフト
    [SerializeField] GameObject bossGuardRight;//ボスガードライト
    [SerializeField] GameObject bossGuardBack;//ボスガードバック
    [SerializeField] GameObject bossMoveArea;//ボスムーブエリア
    [SerializeField] float bossGuardRadiusInterval;//ボスとガードとの半径距離
    [SerializeField] float bossGuardRadiusSpeed;//ボスガードの回転スピード
    [SerializeField] AudioClip shotSound;//攻撃効果音
    [SerializeField] AudioClip bossDestroySound;//ボス撃破効果音
    [SerializeField] AudioClip bossColiderSound;//トリガー解除効果音
    [SerializeField] AudioClip damageSound;//ダメージ受ける効果音
    [SerializeField] AudioClip invincibleDamageSound;//無敵状態で攻撃を受ける効果音
    //[SerializeField] Collider bossCollider;//Collider切り替え用
    //[SerializeField] Collider bossAttakCollider;//接触時攻撃用Collider
    //[SerializeField] Slider bossSliderValue;//スライダーバリューの取得用
    [SerializeField] Slider bossHpSliderValue;//ボスHP用スライダーバリュー
    //[SerializeField] Text coliderText;//コライダーテキスト


    static public GameObject currentTarget;//現在選択しているエネミー


    public int bossGuardCurrentCount;//ボスガードの生存数
    public float bossHp;//敵ヒットポイント
    public GameObject target;//ボスのターゲット指定


    private GameObject playerAttackPoint;//プレイヤーのアタックポイント向き指定用
    private GameObject mainSoundManager;//BGM切り替え用
    private GameObject bossLineShell;//エネミーShell代入用
    private GameObject damageEffact;//ダメージエフェクト代入用
    private GameObject invincibleDamageEffact;//無敵状態ダメージエフェクト代入用
    private GameObject shellPool;//Shellオブジェクト格納
    private GameObject damageEffectPool;//DamageEffectオブジェクト格納
    private List<GameObject> listOfBossLineShell = new List<GameObject>();//エネミーShellを格納する用
    private List<GameObject> listOfDamageEffact = new List<GameObject>();//DamageEffectを格納する用
    private List<GameObject> listOfInvincibleDamageEffact = new List<GameObject>();//invincibleDamageEffectを格納する用
    private Vector3 bossStartPosition;//ボスの初期設定ポジション
    private float bossMaxHp;//敵最大ヒットポイント
    private float movePosition;//ボスとプレイヤーの距離差異を保存
    private float movePositionRange = 100.0f;//初期位置へもどる距離
    private float shotIntervalLeft;//ライン攻撃間隔レフト
    private float shotIntervalPlus = 1.0f;//ライン攻撃間隔の値に加算する値
    private float damageInterval;//ダメージ呼び出し間隔
    private float damageIntervalPlus;//ダメージ呼び出し間隔
    private float shotIntervalRight;//ライン攻撃間隔ライト
    private float shotIntervalStandard = 100.0f;//攻撃感覚の基準値
    private float bossPatternCurrentHp1 = 0.8f;//ボスパターン指定の現在HP80%（ボスマックスHPに掛ける）
    private float bossPatternCurrentHp2 = 0.6f;//ボスパターン指定の現在HP60%（ボスマックスHPに掛ける）
    private float bossPatternCurrentHp3 = 0.4f;//ボスパターン指定の現在HP40%（ボスマックスHPに掛ける）
    private float bossPatternCurrentHp4 = 0.2f;//ボスパターン指定の現在HP20%（ボスマックスHPに掛ける）
    private float bossPatternCurrentHp5 = 0.10f;//ボスパターン指定の現在HP10%（ボスマックスHPに掛ける）
    private bool onMouseDownBool = false;//ボス出現後経過時間後に触れる様にする為
    private bool bossPatternOneCall = true;//ボスパターン一回呼び出し用
    private bool bossDestroyEffectBool = true;//ボス撃破時のエフェクト1回呼び出し用
    private bool bossDestroyCallMethodsBool = true;//ボス撃破時のメソッド1回呼び出し用
    private bool bossDestroyCallMethodsBool2 = true;//ボス撃破時のメソッド1回呼び出し用
    private NavMeshAgent agent;//敵AI
    private GameManager gameManager;//ゲームマネージャー


    private void Awake()
    {
        gameManager = GameManager.GameManagerGetInstance();
        PlayerSearch();
        PlayerAttackPointSearch();
        EnemySearch();
        E_BossSearch();
        ShellPoolSearch();
        DamageEffectPoolSearch();
        BossAnimation();
        DamageEffectInstantiate();
        InvincibleDamageEffectInstantiate();
        BossLineShellInstantiate();
        //bossCollider = GetComponent<Collider>();
        agent = GetComponent<NavMeshAgent>();
        bossStartPosition = this.transform.localPosition;
        //ボス出現時エフェクト
        Instantiate(bossSpawnEffect, transform.position, Quaternion.Euler(0.0f, this.transform.localEulerAngles.y, 0.0f));
        BossBattleBGM();
        shotIntervalStandard = 100.0f;
        bossMaxHp = bossHp;
        Invoke("OnMouseDownBoolOnInvoke", 6f);
    }

    private void FixedUpdate()
    {
        MovePositionRange();
        BossActionPattern();
        BossActionPatternPlus();
        BossTargetLockOnContinuation();
        BossLockOn();
    }

    /// <summary>
    /// ボスオブジェクトタッチでスライダーを表示
    /// </summary>
    private void BossLockOn()
    {
        if (onMouseDownBool)
        {
            if (Input.touchCount > 0)
            {
                for (int i = 0; i < Input.touchCount; ++i)
                {
                    if (Input.GetTouch(i).phase == TouchPhase.Began)
                    {
                        //スクリーンタッチをしたらRayをMainCameraから出し
                        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                        RaycastHit hit;

                        //ヒットしたオブジェクトがこのオブジェクトだったら命令を作動
                        if (Physics.Raycast(ray, out hit))
                        {
                            if (hit.collider.gameObject == this.gameObject)
                            {

                                //現在選択している敵を一旦解除
                                currentTarget = null;
                                EnemyController.currentTarget = null;
                                BossGuardController.currentTarget = null;
                                //タッチされた敵を選択
                                currentTarget = this.gameObject;
                                //現在選択しているGameObjectを取得し、ログに表示
                                Debug.Log(currentTarget);

                                //BossActiveSliderActiveOn();

                                //プレイヤーの向きを対象に向ける
                                target.GetComponent<PlayerController>().BossTargetLockOn();

                                //ロックオン効果音
                                target.GetComponent<PlayerController>().TargetLockOnSound();

                                //プレイヤーアタックポイントの向きを対象に向ける
                                playerAttackPoint.GetComponent<PlayerAttackController>().BossTargetLockOn();

                                //HPゲージを表示
                                BossHPSliderActive();

                                //他のオブジェクトのHPを非表示
                                BossGuardForwardHPSliderActiveOff();
                                BossGuardLeftHPSliderActiveOff();
                                BossGuardRightHPSliderActiveOff();
                                BossGuardBackHPSliderActiveOff();

                                //他のアクティブスライダーを非表示
                                //BossGuardForwardActiveSliderOff();
                                //BossGuardLeftActiveSliderOff();
                                //BossGuardRightActiveSliderOff();
                                //BossGuardBackActiveSliderOff();
                            }

                        }
                    }
                }
            }
        }
    }




    /// <summary>
    /// ボスオブジェクトタッチでスライダーを表示
    /// </summary>
    public void OnMouseDown()
    {
        if (onMouseDownBool)
        {
            //現在選択している敵を一旦解除
            currentTarget = null;
            EnemyController.currentTarget = null;
            BossGuardController.currentTarget = null;
            //タッチされた敵を選択
            currentTarget = this.gameObject;
            //現在選択しているGameObjectを取得し、ログに表示
            Debug.Log(currentTarget);

            //BossActiveSliderActiveOn();

            //プレイヤーの向きを対象に向ける
            target.GetComponent<PlayerController>().BossTargetLockOn();

            //ロックオン効果音
            target.GetComponent<PlayerController>().TargetLockOnSound();

            //プレイヤーアタックポイントの向きを対象に向ける
            playerAttackPoint.GetComponent<PlayerAttackController>().BossTargetLockOn();

            //HPゲージを表示
            BossHPSliderActive();

            //他のオブジェクトのHPを非表示
            BossGuardForwardHPSliderActiveOff();
            BossGuardLeftHPSliderActiveOff();
            BossGuardRightHPSliderActiveOff();
            BossGuardBackHPSliderActiveOff();

            //他のアクティブスライダーを非表示
            //BossGuardForwardActiveSliderOff();
            //BossGuardLeftActiveSliderOff();
            //BossGuardRightActiveSliderOff();
            //BossGuardBackActiveSliderOff();
        }

    }

    /// <summary>
    /// 接触判定（トリガー）
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {

        //プレイヤーの通常攻撃接触時
        if (other.gameObject.tag == "PlayerDamage")
            if (bossGuardCurrentCount == 0 && bossHp >= bossMaxHp * bossPatternCurrentHp4)
            {
                BossDamage();

                PlayerEngineerPointPlusNomal();

                //ダメージエフェクト
                DamageEffectActive();
                damageEffact.transform.position = other.transform.position;

                // ぶつかってきた相手（敵の攻撃）を非アクティブに
                other.gameObject.SetActive(false);
            }

            else if (bossGuardCurrentCount != 0)
            {
                InvincibleDamageEffectActive();
                invincibleDamageEffact.transform.position = other.transform.position;
                InvincibleDamageEffectRotate();
                AudioSource.PlayClipAtPoint(invincibleDamageSound, transform.position);
                bossHp -= 0;
            }


                else
            {
                BossDamageReduction();

                PlayerEngineerPointPlusNomal();

                //ダメージエフェクト
                DamageEffectActive();
                damageEffact.transform.position = other.transform.position;

                // ぶつかってきた相手（敵の攻撃）を非アクティブに
                other.gameObject.SetActive(false);
            }

        //プレイヤーチャージ攻撃2接触時
        if (other.gameObject.tag == "ChargeAttack2")
            if(bossGuardCurrentCount == 0 && bossHp >= bossMaxHp * bossPatternCurrentHp4)
            {
                BossChargeDamage2();

                PlayerEngineerPointPlusCharge2();

                //ダメージエフェクト
                DamageEffectActive();
                damageEffact.transform.position = other.transform.position;
            }

            else if (bossGuardCurrentCount != 0)
            {
                InvincibleDamageEffectActive();
                invincibleDamageEffact.transform.position = other.transform.position;
                InvincibleDamageEffectRotate();
                AudioSource.PlayClipAtPoint(invincibleDamageSound, transform.position);
                bossHp -= 0;
            }

            else
            {
                BossDamage2Reduction();

                PlayerEngineerPointPlusCharge2();

                //ダメージエフェクト
                DamageEffectActive();
                damageEffact.transform.position = other.transform.position;
                Debug.Log(bossGuardCurrentCount);
            }

        //プレイヤーチャージ攻撃3接触時
        if (other.gameObject.tag == "ChargeAttack3")
            if (bossGuardCurrentCount == 0 && bossHp >= bossMaxHp * bossPatternCurrentHp4)
            {
                BossChargeDamage3();

                PlayerEngineerPointPlusCharge3();

                //ダメージエフェクト
                DamageEffectActive();
                damageEffact.transform.position = other.transform.position;
            }

            else if (bossGuardCurrentCount != 0)
            {
                InvincibleDamageEffectActive();
                invincibleDamageEffact.transform.position = other.transform.position;
                InvincibleDamageEffectRotate();
                AudioSource.PlayClipAtPoint(invincibleDamageSound, transform.position);
                bossHp -= 0;
            }

            else
            {
                BossDamage3Reduction();

                PlayerEngineerPointPlusCharge3();

                //ダメージエフェクト
                DamageEffectActive();
                damageEffact.transform.position = other.transform.position;
            }
        //HPが0になったら関数を呼び出す
        if (bossDestroyCallMethodsBool && bossHp <= 0)
        {
            DestroyAnimation();
            Instantiate(bossDestroyEffect1, transform.position, Quaternion.identity);
            Invoke("BossDestroyCallMethods", 2.8f);
            bossDestroyCallMethodsBool = false;
        }
    }

    /// <summary>
    /// 接触判定（トリガー）通っている間呼ばれ続ける
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay(Collider other)
    {
        //プレイヤーチャージ攻撃1接触時
        if (other.gameObject.tag == "ChargeAttack1")
            if (bossGuardCurrentCount == 0 && bossHp >= bossMaxHp * bossPatternCurrentHp4)
            {
                BossChargeDamage1();

                PlayerEngineerPointPlusCharge1();

                //ダメージエフェクト
                DamageEffectActive();
                damageEffact.transform.position = other.transform.position;
            }

            else if (bossGuardCurrentCount != 0)
            {
                InvincibleDamageEffectActive();
                invincibleDamageEffact.transform.position = other.transform.position;
                InvincibleDamageEffectRotate();
                AudioSource.PlayClipAtPoint(invincibleDamageSound, transform.position);
                bossHp -= 0;
            }

            else
            {
                BossDamage1Reduction();

                PlayerEngineerPointPlusCharge1();

                //ダメージエフェクト
                DamageEffectActive();
                damageEffact.transform.position = other.transform.position;
            }
        //HPが0になったら関数を呼び出す
        if (bossDestroyCallMethodsBool && bossHp <= 0)
        {
            DestroyAnimation();
            Instantiate(bossDestroyEffect1, transform.position, Quaternion.identity);
            Invoke("BossDestroyCallMethods", 2.8f);
            bossDestroyCallMethodsBool = false;
        }
    }

    /// <summary>
    /// OnMouseDownを触れる様にする為のbool（インヴォーク呼び出し）
    /// </summary>
    private void OnMouseDownBoolOnInvoke()
    {
        onMouseDownBool = true;
    }

    /// <summary>
    /// ボス出現時アニメーション
    /// </summary>
    private void BossAnimation()
    {
        gameManager.BossAnimation();
    }

    /// <summary>
    /// プレイヤータグ検索し取得
    /// </summary>
    private void PlayerSearch()
    {
        target = GameObject.FindGameObjectWithTag("Player");
        this.transform.LookAt(target.transform.position);
    }

    /// <summary>
    /// プレイヤーアタックポイントタグ検索し取得
    /// </summary>
    private void PlayerAttackPointSearch()
    {
        playerAttackPoint = GameObject.FindGameObjectWithTag("PlayerAttackPoint");
    }

    /// <summary>
    /// エネミータグ検索し取得
    /// </summary>
    public void EnemySearch()
    {
        enemy = GameObject.FindGameObjectWithTag("Enemy");
    }

    /// <summary>
    /// エネミーのボスタグ検索し取得
    /// </summary>
    private void E_BossSearch()
    {
        if (enemy == null)
            return;
        else
            enemy.GetComponent<EnemyController>().BossSearch();
    }

    /// <summary>
    /// ShellPoolタグ検索し取得
    /// </summary>
    private void ShellPoolSearch()
    {
        shellPool = GameObject.FindGameObjectWithTag("ShellPool");
    }

    /// <summary>
    /// DamageEffectPoolタグ検索し取得
    /// </summary>
    private void DamageEffectPoolSearch()
    {
        damageEffectPool = GameObject.FindGameObjectWithTag("DamageEffectPool");
    }

    /// <summary>
    /// ボスのアクションパターン
    /// </summary>
    private void BossActionPattern()
    {
        //初期HP〜HP80％までの攻撃パターン（アタックポイント）
        if (bossPatternOneCall && bossHp > bossMaxHp * bossPatternCurrentHp1)
        {
            Invoke("AttackPointActiveOn", 2.0f);
            bossPatternOneCall = false;
            Debug.Log("フェーズ0");
        }

        //HP80％〜HP60％までの攻撃パターン1（ボスガード）
        else if (!bossPatternOneCall && bossHp < bossMaxHp * bossPatternCurrentHp1 && bossHp > bossMaxHp * bossPatternCurrentHp2)
        {
            AttackPointActiveOff();
            BossGuardSpawnEfect();
            BossGuardActiveOn();
            BossGuradMoveSetting();
            //BossColiderValueReset();
            bossPatternOneCall = true;
            Debug.Log("フェーズ1");
        }

        //HP60％〜HP40％までの攻撃パターン2（アタックポイント）
        else if (bossPatternOneCall && bossHp < bossMaxHp * bossPatternCurrentHp2 && bossHp > bossMaxHp * bossPatternCurrentHp3)
        {
            AttackPointActiveOn();
            bossPatternOneCall = false;
            Debug.Log("フェーズ2");
        }

        //HP40％〜HP20％までの攻撃パターン3（ボスガード）
        else if (!bossPatternOneCall && bossHp < bossMaxHp * bossPatternCurrentHp3 && bossHp > bossMaxHp * bossPatternCurrentHp4)
        {
            AttackPointActiveOff();
            BossGuardSpawnEfect();
            BossGuardActiveOn();
            BossGuradMoveSetting();
            ShotIntervalStandardUp();
            //BossColiderValueReset();
            BreakDownEfectOn1();
            bossPatternOneCall = true;
            Debug.Log("フェーズ3");
        }

        //HP20％〜HP10％までの攻撃パターン4（アタックポイント）
        else if (bossPatternOneCall && bossHp < bossMaxHp * bossPatternCurrentHp4 && bossHp > bossMaxHp * bossPatternCurrentHp5)
        {
            AttackPointActiveOn();
            BreakDownEfectOn2();
            bossPatternOneCall = false;
            Debug.Log("フェーズ4");
        }

        //HP10％〜HP0までの攻撃パターン5（ムーブポイント削除・故障エフェクトオン）
        else if (!bossPatternOneCall && bossHp < bossMaxHp * bossPatternCurrentHp5)
        {
            BossMoveAreaDestroy();
            BreakDownEfectOn3();
            bossPatternOneCall = true;
            Debug.Log("フェーズ5");
        }

        else if (bossPatternOneCall && bossHp <= 0)
        {
            AttackPointActiveOff();
            bossPatternOneCall = false;
            return;

        }


        else return;
    }
    /// <summary>
    /// ボスのアクションパターン追加
    /// </summary>
    private void BossActionPatternPlus()
    {
        if (target.gameObject.activeSelf == true)
        {
            //HP80％〜HP20％までの攻撃パターン1（ラインショット）
            if (bossHp < bossMaxHp * bossPatternCurrentHp1 && bossHp > bossMaxHp * bossPatternCurrentHp4)
            {
                //HP40％を切ると攻撃間隔が早くなる（ボスパターンの方で発動）
                BossLineAttackShotLeft();
                Debug.Log("フェーズ1ライン攻撃");
            }

            //HP80％〜HP20％までの攻撃パターン2（ラインショット）
            else if (bossHp < bossMaxHp * bossPatternCurrentHp4 && bossHp >= 0)
            {
                //HPが40％を切ると攻撃間隔が早くなる（ボスパターンの方で発動）
                BossLineAttackShotLeft();
                BossLineAttackShotRight();
                Debug.Log("フェーズ4ライン攻撃");
            }

            else if (bossHp <= 0)
                return;
        }

        else if (target.gameObject.activeSelf == false)
            return;

    }

    /// <summary>
    /// DamageEffectを生成
    /// </summary>
    private void DamageEffectInstantiate()
    {
        for (int i = 0; i < 400; i++)
        {
            damageEffact = Instantiate(damageEffactPrefab, transform.position, Quaternion.identity, damageEffectPool.transform);
            damageEffact.SetActive(false);
            listOfDamageEffact.Add(damageEffact);
        }
    }

    /// <summary>
    /// DamageEffectを削除
    /// </summary>
    private void DamageEffectDestroy()
    {
        for (int i = 0; i < listOfDamageEffact.Count; i++)
        {
            Destroy(listOfDamageEffact[i]);
        }

    }

    /// <summary>
    /// DamageEffectを削除
    /// </summary>
    private void DamageEffectDestroyBossGuard()
    {
        bossGuardForward.GetComponent<BossGuardController>().DamageEffectDestroy();
        bossGuardLeft.GetComponent<BossGuardController>().DamageEffectDestroy();
        bossGuardRight.GetComponent<BossGuardController>().DamageEffectDestroy();
        bossGuardBack.GetComponent<BossGuardController>().DamageEffectDestroy();
    }

    /// <summary>
    /// 0から順に非アクティブならアクティブにする処理
    /// </summary>
    /// <returns></returns>
    private GameObject GetDamageEffect()
    {
        for (int i = 0; i < listOfDamageEffact.Count; i++)
        {
            if (listOfDamageEffact[i].activeInHierarchy == false)
                return listOfDamageEffact[i];
        }

        return null;
    }

    /// <summary>
    /// DamageEffectをアクティブに
    /// </summary>
    private void DamageEffectActive()
    {
        damageEffact = GetDamageEffect();

        if (damageEffact == null)
            return;

        damageEffact.transform.position = this.transform.position;
        damageEffact.transform.rotation = this.transform.rotation;

        damageEffact.SetActive(true);
    }

    /// <summary>
    /// invincibleDamageEffectを生成
    /// </summary>
    private void InvincibleDamageEffectInstantiate()
    {
        for (int i = 0; i < 400; i++)
        {
            invincibleDamageEffact = Instantiate(invincibleDamageEffactPrefab, transform.position, Quaternion.identity, damageEffectPool.transform);
            invincibleDamageEffact.SetActive(false);
            listOfInvincibleDamageEffact.Add(invincibleDamageEffact);
        }
    }

    /// <summary>
    /// invincibleDamageEffectを削除
    /// </summary>
    private void InvincibleDamageEffectDestroy()
    {
        for (int i = 0; i < listOfInvincibleDamageEffact.Count; i++)
        {
            Destroy(listOfInvincibleDamageEffact[i]);
        }

    }

    /// <summary>
    /// 0から順に非アクティブならアクティブにする処理
    /// </summary>
    /// <returns></returns>
    private GameObject GetInvincibleDamageEffect()
    {
        for (int i = 0; i < listOfInvincibleDamageEffact.Count; i++)
        {
            if (listOfInvincibleDamageEffact[i].activeInHierarchy == false)
                return listOfInvincibleDamageEffact[i];
        }

        return null;
    }

    /// <summary>
    /// DamageEffectをアクティブに
    /// </summary>
    private void InvincibleDamageEffectActive()
    {
        invincibleDamageEffact = GetInvincibleDamageEffect();

        if (invincibleDamageEffact == null)
            return;

        invincibleDamageEffact.transform.position = this.transform.position;
        invincibleDamageEffact.transform.rotation = this.transform.rotation;

        invincibleDamageEffact.SetActive(true);
    }

    /// <summary>
    /// invincibleDamageEffectの方向を決める指示
    /// </summary>
    private void InvincibleDamageEffectRotate()
    {
        invincibleDamageEffact.transform.LookAt(target.transform.position);

        //現在のローテーションをcurrentRotation_yに入れる為の変数
        float currentRotation_y;

        //現在のインスペクター表示のy座標を変数yに代入
        currentRotation_y = invincibleDamageEffact.transform.localEulerAngles.y;

        invincibleDamageEffact.transform.rotation = Quaternion.Euler(0.0f, currentRotation_y, 0.0f);
    }

    /// <summary>
    /// アタックポイントをアクティブに
    /// </summary>
    private void AttackPointActiveOn()
    {
        attackPoint.SetActive(true);
    }

    /// <summary>
    /// アタックポイントを非アクティブに
    /// </summary>
    private void AttackPointActiveOff()
    {
        attackPoint.SetActive(false);
    }

    /// <summary>
    /// 攻撃用のShellを生成
    /// </summary>
    private void BossLineShellInstantiate()
    {
        for (int i = 0; i < 15; i++)
        {
            bossLineShell = Instantiate(bossLineAttackShell, transform.position, Quaternion.identity, shellPool.transform);
            bossLineShell.SetActive(false);
            listOfBossLineShell.Add(bossLineShell);
        }
    }

    /// <summary>
    /// プレイヤーの方向を向く
    /// </summary>
    private void LineAttackRotate()
    {

        bossLineShell.transform.LookAt(target.transform.position);

        //現在のローテーションをcurrentRotation_yに入れる為の変数
        float currentRotation_y;
        //現在のインスペクター表示のy座標を変数yに代入
        currentRotation_y = bossLineShell.transform.localEulerAngles.y;
        //y以外は回転を0にしyだけ数値を適応する
        bossLineShell.transform.rotation = Quaternion.Euler(0.0f, currentRotation_y, 0.0f);
    }

    /// <summary>
    /// 攻撃用のShellを削除
    /// </summary>
    private void BossLineShellDestroy()
    {
        for (int i = 0; i < listOfBossLineShell.Count; i++)
        {
            Destroy(listOfBossLineShell[i]);
        }

    }

    /// <summary>
    /// 攻撃用のShellを削除
    /// </summary>
    private void PoolShellDestroy()
    {
        BossLineShellDestroy();
        attackPoint.GetComponent<AttackPointController>().ShellDestroy();
        bossGuardForward.GetComponent<BossGuardController>().AttackPointPoolShellDestroy();
        bossGuardLeft.GetComponent<BossGuardController>().AttackPointPoolShellDestroy();
        bossGuardRight.GetComponent<BossGuardController>().AttackPointPoolShellDestroy();
        bossGuardBack.GetComponent<BossGuardController>().AttackPointPoolShellDestroy();
    }

    /// <summary>
    /// 0から順に非アクティブならアクティブにする処理
    /// </summary>
    /// <returns></returns>
    private GameObject GetBossLineShell()
    {
        for (int i = 0; i < listOfBossLineShell.Count; i++)
        {
            if (listOfBossLineShell[i].activeInHierarchy == false)
                return listOfBossLineShell[i];
        }

        return null;
    }

    /// <summary>
    /// Bossの攻撃/プレイヤーの右直線上に攻撃を生成
    /// </summary>
    private void BossLineAttackShellInstansLeft()
    {
        float x = target.transform.position.x;
        float y = target.transform.position.y;
        float z = target.transform.position.z;

        //ライン攻撃をアクティブに
        bossLineShell.SetActive(true);
        Vector3 attackPosition = new Vector3(x - 15, y + 2, z);

        bossLineShell.transform.position = attackPosition;

    }

    /// <summary>
    /// プレイヤーの左直線上に攻撃を生成し直線上に攻撃
    /// </summary>
    private void BossLineAttackShotLeft()
    {
        //プラス1加算する
        shotIntervalLeft += shotIntervalPlus;

        //加算の値に達すると1度攻撃する
        if (shotIntervalLeft % shotIntervalStandard <= 0)
        {
            bossLineShell = GetBossLineShell();

            if (bossLineShell == null)
                return;

            BossLineAttackShellInstansLeft();
            LineAttackRotate();
            AudioSource.PlayClipAtPoint(shotSound, transform.position);
        }
    }

    /// <summary>
    /// Bossの攻撃/プレイヤーの右直線上に攻撃を生成
    /// </summary>
    private void BossLineAttackShellInstansRight()
    {
        float x = target.transform.position.x;
        float y = target.transform.position.y;
        float z = target.transform.position.z;

        //ライン攻撃をアクティブに
        bossLineShell.SetActive(true);
        Vector3 attackPosition = new Vector3(x + 15, y + 2, z);

        bossLineShell.transform.position = attackPosition;
    }


    /// <summary>
    /// プレイヤーの右直線上に攻撃を生成し直線上に攻撃
    /// </summary>
    private void BossLineAttackShotRight()
    {
        //プラス1加算する
        shotIntervalRight += shotIntervalPlus;

        //加算の値に達すると1度攻撃する
        if (shotIntervalRight % shotIntervalStandard <= 0)
        {
            bossLineShell = GetBossLineShell();

            if (bossLineShell == null)
                return;

            BossLineAttackShellInstansRight();
            LineAttackRotate();
            AudioSource.PlayClipAtPoint(shotSound, transform.position);
        }
    }

    /// <summary>
    /// ショットインターバルの間隔基準を狭める（ライン攻撃頻度が早くなる）
    /// </summary>
    private void ShotIntervalStandardUp()
    {
        shotIntervalStandard = 80.0f;
    }

    /// <summary>
    /// Bossの移動行動BossMoveAreaのスクリプトでコライダー接触中呼び出し
    /// </summary>
    public void BossMove()
    {
        if (target.gameObject.activeSelf == true)
            agent.destination = target.transform.position;

        else if (target.gameObject.activeSelf == false)
            BossPositionReset();

        else return;
    }

    /// <summary>
    /// Bossの初期位置へもどる範囲
    /// </summary>
    private void MovePositionRange()
    {
        movePosition = Vector3.Distance(transform.position, target.transform.position);

        if (movePosition > movePositionRange)
        {
            BossPositionReset();
        }

        else if (target.gameObject.activeSelf == false)
            BossPositionReset();

        else return;
    }

    /// <summary>
    /// Bossを初期出現位置まで移動させる。
    /// BossMoveAreaController呼び出し
    /// </summary>
    private void BossPositionReset()
    {
        if (this.transform.localPosition != bossStartPosition)
        {
            agent.destination = bossStartPosition;
            Debug.Log("ポジションリセットno.Boss");
        }
            

        else if (this.transform.localPosition == bossStartPosition)
            return;
    }

    /// <summary>
    /// ボスガードを呼び出し
    /// </summary>
    private void BossGuardActiveOn()
    {
        bossGuardForward.SetActive(true);
        bossGuardLeft.SetActive(true);
        bossGuardRight.SetActive(true);
        bossGuardBack.SetActive(true);
    }

    /// <summary>
    /// ボスの受けるダメージが減少する
    /// </summary>
    private void BossDamageReduction()
    {
        bossHp -= playerAttackPoint.GetComponent<PlayerAttackController>().nomalDamage / 5f;
    }

    /// <summary>
    /// ボスの受けるダメージが減少する
    /// </summary>
    private void BossDamage1Reduction()
    {
        bossHp -= playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage1 / 5f;
    }

    /// <summary>
    /// ボスの受けるダメージが減少する
    /// </summary>
    private void BossDamage2Reduction()
    {
        bossHp -= playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage2 / 5f;
    }

    /// <summary>
    /// ボスの受けるダメージが減少する
    /// </summary>
    private void BossDamage3Reduction()
    {
        bossHp -= playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage3 / 5f;
    }

    /// <summary>
    /// BossGuardとBossとの初期距離設定
    /// </summary>
    private void BossGuradMoveSetting()
    {
        // RotateAround は現在の距離が半径として計算されるので、予め対象半径の位置に移動する 各ガードの距離設定を行う
        bossGuardForward.transform.position = bossGuardRadiusInterval * (bossGuardForward.transform.position - this.transform.position).normalized + this.transform.position;
        bossGuardLeft.transform.position = bossGuardRadiusInterval * (bossGuardLeft.transform.position - this.transform.position).normalized + this.transform.position;
        bossGuardRight.transform.position = bossGuardRadiusInterval * (bossGuardRight.transform.position - this.transform.position).normalized + this.transform.position;
        bossGuardBack.transform.position = bossGuardRadiusInterval * (bossGuardBack.transform.position - this.transform.position).normalized + this.transform.position;
    }

    /// <summary>
    /// BossGuard移動行動X軸に回転
    /// </summary>
    public void BossGuradMove()
    {
        StartCoroutine("BossGuradMoveCoroutine");
    }

    /// <summary>
    /// コルーチン指定秒数毎に呼び出し
    /// </summary>
    /// <returns></returns>
    private IEnumerator BossGuradMoveCoroutine()
    {
        yield return new WaitForSeconds(1.0f);
        bossGuardForward.transform.RotateAround(this.transform.position, bossGuardLeft.transform.up, bossGuardRadiusSpeed);
        bossGuardLeft.transform.RotateAround(this.transform.position, bossGuardLeft.transform.up, bossGuardRadiusSpeed);
        bossGuardRight.transform.RotateAround(this.transform.position, bossGuardRight.transform.up, bossGuardRadiusSpeed);
        bossGuardBack.transform.RotateAround(this.transform.position, bossGuardLeft.transform.up, bossGuardRadiusSpeed);
    }

    /// <summary>
    /// ボスがプレイヤーから受ける通常ダメージ
    /// </summary>
    private void BossDamage()
    {
        // HPを減少させる
        bossHp -= playerAttackPoint.GetComponent<PlayerAttackController>().nomalDamage;
        //ダメージ受けると効果音
        AudioSource.PlayClipAtPoint(damageSound, transform.position);

    }

    /// <summary>
    /// ボスがプレイヤーから受けるチャージ攻撃1のダメージ
    /// </summary>
    private void BossChargeDamage1()
    {
        //プラス加算する
        damageInterval += damageIntervalPlus * Time.deltaTime;

        //加算の値に達すると1度攻撃する
        if (damageInterval % 100 <= 0)
        {
            // HPを減少させる
            bossHp -= playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage1;

            //ダメージ受けると効果音
            AudioSource.PlayClipAtPoint(damageSound, transform.position);
        }

    }

    /// <summary>
    /// ボスがプレイヤーから受けるチャージ攻撃2のダメージ
    /// </summary>
    private void BossChargeDamage2()
    {
        // HPを減少させる
        bossHp -= playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage2;

        //ダメージ受けると効果音
        AudioSource.PlayClipAtPoint(damageSound, transform.position);

    }

    /// <summary>
    /// ボスがプレイヤーから受けるチャージ攻撃3のダメージ
    /// </summary>
    private void BossChargeDamage3()
    {
        // HPを減少させる
        bossHp -= playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage3;

        //ダメージ受けると効果音
        AudioSource.PlayClipAtPoint(damageSound, transform.position);

    }

    /// <summary>
    /// エンジニアポイントを加算する
    /// </summary>
    private void PlayerEngineerPointPlusNomal()
    {
        target.GetComponent<PlayerController>().EngineerPointPlusNomal();
    }

    /// <summary>
    /// エンジニアポイントを加算する
    /// </summary>
    private void PlayerEngineerPointPlusCharge1()
    {
        target.GetComponent<PlayerController>().EngineerPointPlusCharge1();
    }

    /// <summary>
    /// エンジニアポイントを加算する
    /// </summary>
    private void PlayerEngineerPointPlusCharge2()
    {
        target.GetComponent<PlayerController>().EngineerPointPlusCharge2();
    }

    /// <summary>
    /// エンジニアポイントを加算する
    /// </summary>
    private void PlayerEngineerPointPlusCharge3()
    {
        target.GetComponent<PlayerController>().EngineerPointPlusCharge3();
    }

    /// <summary>
    /// プレイヤーアタックポイントがボスをターゲットし続ける他のターゲットをタップすると切り替わる
    /// </summary>
    private void BossTargetLockOnContinuation()
    {
        if (playerAttackPoint.GetComponent<PlayerAttackController>().enemyCurrentTarget == this.gameObject)
        {
            //プレイヤーアタックポイントの向きを対象に向ける
            playerAttackPoint.GetComponent<PlayerAttackController>().BossTargetLockOn();

        }
    }

    /*
    /// <summary>
    /// スライダーバリュー100でトリガー状態解除
    /// </summary>
    public void BossColiderValue()
    {
        if (bossSliderValue.value == 100)
        {
            //トリガー解除効果音
            AudioSource.PlayClipAtPoint(bossColiderSound, transform.position);

            bossCollider.isTrigger = false;
            GetComponent<Renderer>().material.color = Color.blue;
            //コライダーエフェクト
            Instantiate(ColliderEffact, transform.position, Quaternion.identity);
            BossColiderCompleteTextOn();
            Invoke("BossColiderCompleteTextOffInvoke", 0.6f);
        }
    }

    /// <summary>
    /// スライダーバリュー100コンプリートテキストオン
    /// </summary>
    private void BossColiderCompleteTextOn()
    {
        coliderTextObject.SetActive(true);
        coliderText.DOText("Complete", 0.5f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);
    }

    /// <summary>
    /// スライダーバリュー100コンプリートテキストオフ（インヴォーク）
    /// </summary>
    private void BossColiderCompleteTextOffInvoke()
    {
        coliderText.DOText("        ", 0.1f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);
        Invoke("CompleteTextAciveOffInvoke", 0.1f);
    }

    /// <summary>
    /// コライダー時のコンプリートテキストをオフにする（インヴォーク呼び出し）
    /// </summary>
    private void CompleteTextAciveOffInvoke()
    {
        coliderTextObject.SetActive(false);
    }

    /// <summary>
    /// スライダーバリューをリセット
    /// </summary>
    private void BossColiderValueReset()
    {
        bossSliderValue.value = 0;

        bossCollider.isTrigger = true;
        GetComponent<Renderer>().material.color = Color.red;
    }
    */

    /// <summary>
    /// ボスのHPゲージをアクティブに
    /// </summary>
    private void BossHPSliderActive()
    {
        bossHpSlider.SetActive(true);
    }

    /// <summary>
    /// ボスのHPゲージを非アクティブに
    /// </summary>
    public void BossHPSliderActiveOff()
    {
        bossHpSlider.SetActive(false);
    }

    /// <summary>
    /// ボスガードフォワードのHPゲージを非アクティブに
    /// </summary>
    public void BossGuardForwardHPSliderActiveOff()
    {
        bossGuardForward.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
    }

    /// <summary>
    /// ボスガードレフトのHPゲージを非アクティブに
    /// </summary>
    public void BossGuardLeftHPSliderActiveOff()
    {
        bossGuardLeft.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
    }

    /// <summary>
    /// ボスガードライトのHPゲージを非アクティブに
    /// </summary>
    public void BossGuardRightHPSliderActiveOff()
    {
        bossGuardRight.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
    }

    /// <summary>
    /// ボスガードバックのHPゲージを非アクティブに
    /// </summary>
    public void BossGuardBackHPSliderActiveOff()
    {
        bossGuardBack.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
    }

    /*
    /// <summary>
    /// ボスのアクティブスライダーをOn
    /// </summary>
    private void BossActiveSliderActiveOn()
    {
        if (bossCollider.isTrigger == false)
            return;
        else
            sliderColider.SetActive(true);
    }

    /// <summary>
    /// ボスのアクティブスライダーをOff
    /// </summary>
    public void BossActiveSliderOff()
    {
        sliderColider.SetActive(false);
    }

    /// <summary>
    /// ボスガードフォワードのアクティブスライダーをOff
    /// </summary>
    public void BossGuardForwardActiveSliderOff()
    {
        bossGuardForward.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
    }

    /// <summary>
    /// ボスガードレフトのアクティブスライダーをOff
    /// </summary>
    public void BossGuardLeftActiveSliderOff()
    {
        bossGuardLeft.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
    }

    /// <summary>
    /// ボスガードライトのアクティブスライダーをOff
    /// </summary>
    public void BossGuardRightActiveSliderOff()
    {
        bossGuardRight.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
    }

    /// <summary>
    /// ボスガードバックのアクティブスライダーをOff
    /// </summary>
    public void BossGuardBackActiveSliderOff()
    {
        bossGuardBack.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
    }
    */

    /// <summary>
    /// ボスのHPが0になったら関数を呼びだす
    /// </summary>
    private void BossDestroyCallMethods()
    {
        //HPが0になったら関数を呼び出す
        if (bossDestroyCallMethodsBool2 && bossHp <= 0)
        {
            //撃破効果音
            AudioSource.PlayClipAtPoint(bossDestroySound, transform.position);
            //ボス撃破時にゴールをアクティブに
            gameManager.GoalActive();
            //ボスのHpスライダーを消す
            BossHPSliderActiveOff();
            //ボスの出現時に生成した攻撃用Shellを破棄する
            PoolShellDestroy();
            //ボスの出現時に生成したDmageEffectを破棄する
            DamageEffectDestroy();
            //ボスの出現時に生成したinvincibleDmageEffectを破棄する
            InvincibleDamageEffectDestroy();
            //ボスの出現時に生成した攻撃用Shellを破棄する
            DamageEffectDestroyBossGuard();
            //ボス撃破時にゲームクリアBGMを再生する
            GameClearBGM();
            //敵消滅時にリンクパーティクルをオフにする
            playerAttackPoint.GetComponent<PlayerAttackController>().LinkParticleOffDestroyCall();
            //ロックオンマーカーをオフにする
            playerAttackPoint.GetComponent<PlayerAttackController>().LookOnMakerActiveOff();
            //HP0なら敵（自信）を削除する
            BossHpOutDestroy();
            bossDestroyCallMethodsBool = false;
        }
    }

    /// <summary>
    /// ボス撃破時のアニメーション（シェイク）
    /// </summary>
    private void DestroyAnimation()
    {
        transform.DOShakePosition(3.5f,2.5f);
    }

    /// <summary>
    /// ボスのHPが0になったらエネミー自信を消滅させる
    /// </summary>
    private void BossHpOutDestroy()
    {
        //HPが0になったら自信を破棄する
        if (bossDestroyEffectBool && bossHp <= 0)
        {
            //ボス撃破時エフェクト
            Instantiate(bossDestroyEffect2, transform.position, Quaternion.identity);
            Destroy(this.gameObject);
            bossDestroyEffectBool = false;
        }
    }

    /// <summary>
    /// ボスムーブエリアを消滅(BossMoveAreaControllerのOnTriggerStayで呼び出し)
    /// </summary>
    public void BossMoveAreaDestroy()
    {
        Destroy(bossMoveArea);
    }

    /// <summary>
    /// ボスガード出現時に衝撃はエフェクトを出す
    /// </summary>
    private void BossGuardSpawnEfect()
    {
        float x = this.transform.position.x;
        float y = this.transform.position.y;
        float z = this.transform.position.z;

        Instantiate(bossGuardSpawnEffect, new Vector3(x + 0, y + -7, z + 0), Quaternion.identity);
    }

    /// <summary>
    /// 故障エフェクト1をアクティブ
    /// </summary>
    private void BreakDownEfectOn1()
    {
        breakDownEfect[0].SetActive(true);
    }

    /// <summary>
    /// 故障エフェクト1をアクティブ
    /// </summary>
    private void BreakDownEfectOn2()
    {
        breakDownEfect[1].SetActive(true);
    }

    /// <summary>
    /// 故障エフェクト1をアクティブ
    /// </summary>
    private void BreakDownEfectOn3()
    {
        breakDownEfect[2].SetActive(true);
    }

    /// <summary>
    /// ボスがアクティブになったらBGMを変更する
    /// </summary>
    private void BossBattleBGM()
    {
        mainSoundManager = GameObject.FindGameObjectWithTag("MainSound");
        mainSoundManager.GetComponent<MainSoundManager>().BossBattleBGMActive();
    }

    /// <summary>
    /// ボスが撃破されたらBGMを変更する
    /// </summary>
    private void GameClearBGM()
    {
        mainSoundManager.GetComponent<MainSoundManager>().GameClearBGMActive();
    }
}
