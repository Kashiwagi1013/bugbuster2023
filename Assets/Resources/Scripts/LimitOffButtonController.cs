using UnityEngine;
using DG.Tweening;

public class LimitOffButtonController : MonoBehaviour
{

    [SerializeField] GameObject limitOffSlider;//リミットオフスライダー
    [SerializeField] AudioClip limitGaugeMaxSE;//リミットゲージMAX時効果音
    [SerializeField] Transform limitButtonTransform;//リミットボタントランスフォーム

    private AudioSource audioSource;//オーディオ取得用



    private void Awake()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        TransformScaleZero();
        TransformScaleActiveAnimation();
        audioSource.PlayOneShot(limitGaugeMaxSE);
    }

    /// <summary>
    ///　スケールを0にする
    /// </summary>
    private void TransformScaleZero()
    {
        limitButtonTransform.DOScale(new Vector3(0, 0, 0), duration: 0);
    }

    /// <summary>
    /// スケールを元のサイズに戻しアニメーションする
    /// </summary>
    private void TransformScaleActiveAnimation()
    {
        limitButtonTransform.DOScale(new Vector3(1,1,1), duration: 0.5f)
            .SetLoops(1,loopType:LoopType.Yoyo)
            .Play();
    }

    /// <summary>
    /// ボタン割り当てリミットスライダーをActiveに
    /// リミットオフボタンを非アクティブに
    /// </summary>
    public void LimitOffSliderActiveButton()
    {
        limitOffSlider.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
