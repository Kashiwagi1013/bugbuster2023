using UnityEngine;
//using UnityEngine.UI;
//using DG.Tweening;
using System.Collections.Generic;

public class BossGuardController : MonoBehaviour
{

    [SerializeField] GameObject boss;//ボスオブジェクト
    [SerializeField] GameObject bossGuardForward;//ボスガードフォワード
    [SerializeField] GameObject bossGuardLeft;//ボスガードレフト
    [SerializeField] GameObject bossGuardRight;//ボスガードライト
    [SerializeField] GameObject bossGuardBack;//ボスガードバック
    [SerializeField] GameObject attackPoint;//アタックポイント
    [SerializeField] GameObject damageEffactPrefab;//ダメージエフェクト
    //[SerializeField] GameObject ColliderEffact;//コライダーエフェクト
    [SerializeField] GameObject bossGuardSpawnEffect;//ボスガード出現エフェクト
    [SerializeField] GameObject bossGuardDestroyEffect;//ボスガード撃破時エフェクト
    [SerializeField] GameObject bossGuardHpSlider;//ボスガードHP用スライダー
    //[SerializeField] GameObject sliderColider;//スライダー表示の為GameObject取得用
    //[SerializeField] GameObject coliderTextObject;//コライダーテキストオブジェクト
    //[SerializeField] Collider bossGuardCollider;//Collider切り替え用
    [SerializeField] AudioClip bossGuardDestroySound;//ボスガード撃破効果音
    [SerializeField] AudioClip damageSound;//ダメージ受ける効果音
    //[SerializeField] AudioClip bossColiderSound;//トリガー解除効果音
    [SerializeField] AudioClip spawnSound;//出現時の効果音
    //[SerializeField] Slider bossGuardSliderValue;//スライダーバリューの取得用
    //[SerializeField] Text coliderText;//コライダーテキスト

    static public GameObject currentTarget;//現在選択している標的

    public float bossGuardHp;//敵ヒットポイント

    private GameObject target;//プレイヤーの向き指定用
    private GameObject playerAttackPoint;//プレイヤーのアタックポイント向き指定用
    private GameObject damageEffact;//ダメージエフェクト代入用
    private GameObject damageEffectPool;//DamageEffectオブジェクト格納
    private List<GameObject> listOfDamageEffact = new List<GameObject>();//DamageEffectを格納する用
    private float damageInterval;//ダメージ呼び出し間隔
    private float damageIntervalPlus;//ダメージ呼び出し間隔
    private bool bossGuardDestroyEffectBool = true;//ボスガード撃破時のエフェクト1回呼び出し用
    private bool bossGuardDestroyCallMethodsBool = true;//ボスガード撃破時のメソッド1回呼び出し用

    private void Awake()
    {
        PlayerSearch();
        PlayerAttackPointSearch();
        BossGuradRotateSetting();
        DamageEffectPoolSearch();
        DamageEffectInstantiate();
        AudioSource.PlayClipAtPoint(spawnSound, transform.position);
        //bossGuardCollider = GetComponent<Collider>();
        Invoke("AttackPointActiveOnInvoke", 2.0f);
    }

    /// <summary>
    /// 毎回アクティブになると呼び出される
    /// </summary>
    private void OnEnable()
    {
        BossGuardHpReset();
        BossGuardCountPlus();
        //BossColiderValueReset();
        Instantiate(bossGuardSpawnEffect, transform.position, Quaternion.identity);
        bossGuardDestroyCallMethodsBool = true;
        bossGuardDestroyEffectBool = true;
    }

    private void FixedUpdate()
    {
        GuardTargetLockOnContinuation();
        boss.GetComponent<BossController>().BossGuradMove();
        BossGuardLockOn();
    }

    /// <summary>
    /// ボスガードオブジェクトタッチでスライダーを表示
    /// </summary>
    private void BossGuardLockOn()
    {
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; ++i)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began)
                {
                    //スクリーンタッチをしたらRayをMainCameraから出し
                    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                    RaycastHit hit;

                    //ヒットしたオブジェクトがこのオブジェクトだったら命令を作動
                    if (Physics.Raycast(ray, out hit))
                    {
                        if (hit.collider.gameObject == this.gameObject)
                        {
                            //現在選択している敵を一旦解除
                            currentTarget = null;
                            EnemyController.currentTarget = null;
                            BossController.currentTarget = null;
                            //タッチされた敵を選択
                            currentTarget = this.gameObject;
                            //現在選択しているGameObjectを取得し、ログに表示
                            Debug.Log(currentTarget);

                            //BossGuardActiveSliderActiveOn();

                            //プレイヤーの向きを対象に向ける
                            target.GetComponent<PlayerController>().BossGuardTargetLockOn();

                            //ロックオン効果音
                            target.GetComponent<PlayerController>().TargetLockOnSound();

                            //プレイヤーアタックポイントの向きを対象に向ける
                            playerAttackPoint.GetComponent<PlayerAttackController>().GuardTargetLockOn();

                            //HPゲージを表示
                            BossGuardHPSliderActive();

                            //他のオブジェクトのHPを非表示
                            boss.GetComponent<BossController>().BossHPSliderActiveOff();
                            BossGuardForwardHPSliderActiveOn();
                            BossGuardLeftHPSliderActiveOn();
                            BossGuardRightHPSliderActiveOn();
                            BossGuardBackHPSliderActiveOn();

                            //他のアクティブスライダーを非表示
                            //boss.GetComponent<BossController>().BossActiveSliderOff();
                            //BossGuardForwardActiveSliderActiveOn();
                            //BossGuardLeftActiveSliderActiveOn();
                            //BossGuardRightActiveSliderActiveOn();
                            //BossGuardBackActiveSliderActiveOn();}

                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// ボスガードオブジェクトタッチでスライダーを表示
    /// </summary>
    public void OnMouseDown()
    {
        //現在選択している敵を一旦解除
        currentTarget = null;
        EnemyController.currentTarget = null;
        BossController.currentTarget = null;
        //タッチされた敵を選択
        currentTarget = this.gameObject;
        //現在選択しているGameObjectを取得し、ログに表示
        Debug.Log(currentTarget);

        //BossGuardActiveSliderActiveOn();

        //プレイヤーの向きを対象に向ける
        target.GetComponent<PlayerController>().BossGuardTargetLockOn();

        //ロックオン効果音
        target.GetComponent<PlayerController>().TargetLockOnSound();

        //プレイヤーアタックポイントの向きを対象に向ける
        playerAttackPoint.GetComponent<PlayerAttackController>().GuardTargetLockOn();

        //HPゲージを表示
        BossGuardHPSliderActive();

        //他のオブジェクトのHPを非表示
        boss.GetComponent<BossController>().BossHPSliderActiveOff();
        BossGuardForwardHPSliderActiveOn();
        BossGuardLeftHPSliderActiveOn();
        BossGuardRightHPSliderActiveOn();
        BossGuardBackHPSliderActiveOn();

        //他のアクティブスライダーを非表示
        //boss.GetComponent<BossController>().BossActiveSliderOff();
        //BossGuardForwardActiveSliderActiveOn();
        //BossGuardLeftActiveSliderActiveOn();
        //BossGuardRightActiveSliderActiveOn();
        //BossGuardBackActiveSliderActiveOn();}
    }

    /// <summary>
    /// 接触判定（トリガー）
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        //プレイヤーの通常攻撃接触時
        if (other.gameObject.tag == "PlayerDamage")
        {
            BossGuardDamage();

            PlayerEngineerPointPlusNomal();

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;

            // ぶつかってきた相手（敵の攻撃）を非アクティブに
            other.gameObject.SetActive(false);
        }

        //プレイヤーチャージ攻撃2接触時
        if (other.gameObject.tag == "ChargeAttack2")
        {
            BossGuardChargeDamage2();

            PlayerEngineerPointPlusCharge2();

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;
        }

        //プレイヤーチャージ攻撃3接触時
        if (other.gameObject.tag == "ChargeAttack3")
        {
            BossGuardChargeDamage3();

            PlayerEngineerPointPlusCharge3();

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;
        }

        BossGuardDestroyCallMethods();
    }

    /// <summary>
    /// 接触判定（トリガー）通っている間呼ばれ続ける
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay(Collider other)
    {
        //プレイヤーチャージ攻撃1接触時
        if (other.gameObject.tag == "ChargeAttack1")
        {
            BossGuardChargeDamage1();

            PlayerEngineerPointPlusCharge1();

            //ダメージエフェクト
            DamageEffectActive();
            damageEffact.transform.position = other.transform.position;
        }

        BossGuardDestroyCallMethods();
    }

    /// <summary>
    /// ボスガードの生存数を+1する
    /// </summary>
    private void BossGuardCountPlus()
    {
        boss.GetComponent<BossController>().bossGuardCurrentCount += 1;
    }

    /// <summary>
    /// ボスガードの生存数を-1する
    /// </summary>
    private void BossGuardCountPlusOpposite()
    {
        boss.GetComponent<BossController>().bossGuardCurrentCount -= 1;
    }

    /// <summary>
    /// Guardの向きをBossに向ける
    /// </summary>
    private void BossGuradRotateSetting()
    {
        this.transform.LookAt(boss.transform.position);
    }

    /// <summary>
    /// プレイヤータグ検索し取得
    /// </summary>
    private void PlayerSearch()
    {
        target = boss.GetComponent<BossController>().target;
    }

    /// <summary>
    /// プレイヤーアタックポイントタグ検索し取得
    /// </summary>
    private void PlayerAttackPointSearch()
    {
        playerAttackPoint = GameObject.FindGameObjectWithTag("PlayerAttackPoint");
    }

    /// <summary>
    /// DamageEffectPoolタグ検索し取得
    /// </summary>
    private void DamageEffectPoolSearch()
    {
        damageEffectPool = GameObject.FindGameObjectWithTag("DamageEffectPool");
    }

    /// <summary>
    /// 攻撃用のShellを削除（BossContorollerで呼び出し）
    /// </summary>
    public void AttackPointPoolShellDestroy()
    {
        attackPoint.GetComponent<AttackPointController>().ShellDestroy();
    }


    /// <summary>
    /// DamageEffectを生成
    /// </summary>
    private void DamageEffectInstantiate()
    {
        for (int i = 0; i < 120; i++)
        {
            damageEffact = Instantiate(damageEffactPrefab, transform.position, Quaternion.identity, damageEffectPool.transform);
            damageEffact.SetActive(false);
            listOfDamageEffact.Add(damageEffact);
        }
    }

    /// <summary>
    /// DamageEffectを削除（BossController呼び出し）
    /// </summary>
    public void DamageEffectDestroy()
    {
        for (int i = 0; i < listOfDamageEffact.Count; i++)
        {
            Destroy(listOfDamageEffact[i]);
        }

    }

    /// <summary>
    /// 0から順に非アクティブならアクティブにする処理
    /// </summary>
    /// <returns></returns>
    private GameObject GetDamageEffect()
    {
        for (int i = 0; i < listOfDamageEffact.Count; i++)
        {
            if (listOfDamageEffact[i].activeInHierarchy == false)
                return listOfDamageEffact[i];
        }

        return null;
    }

    /// <summary>
    /// DamageEffectをアクティブに
    /// </summary>
    private void DamageEffectActive()
    {
        damageEffact = GetDamageEffect();

        if (damageEffact == null)
            return;

        damageEffact.transform.position = this.transform.position;
        damageEffact.transform.rotation = this.transform.rotation;

        damageEffact.SetActive(true);
    }

    /*
    /// <summary>
    /// スライダーバリュー100でトリガー状態解除
    /// </summary>
    public void BossGuardColiderValue()
    {
        if (bossGuardSliderValue.value == 100)
        {
            //トリガー解除効果音
            AudioSource.PlayClipAtPoint(bossColiderSound, transform.position);

            bossGuardCollider.isTrigger = false;
            GetComponent<Renderer>().material.color = Color.blue;
            //コライダーエフェクト
            Instantiate(ColliderEffact, transform.position, Quaternion.identity);
            BossGuardColiderCompleteTextOn();
            Invoke("BossGuardColiderCompleteTextOffInvoke", 0.6f);
        }
    }

    /// <summary>
    /// スライダーバリュー100コンプリートテキストオン
    /// </summary>
    private void BossGuardColiderCompleteTextOn()
    {
        coliderTextObject.SetActive(true);
        coliderText.DOText("Complete", 0.5f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);
    }

    /// <summary>
    /// スライダーバリュー100コンプリートテキストオフ（インヴォーク）
    /// </summary>
    private void BossGuardColiderCompleteTextOffInvoke()
    {
        coliderText.DOText("        ", 0.1f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);
        Invoke("CompleteTextAciveOffInvoke", 0.1f);
    }

    /// <summary>
    /// コライダー時のコンプリートテキストをオフにする（インヴォーク呼び出し）
    /// </summary>
    private void CompleteTextAciveOffInvoke()
    {
        coliderTextObject.SetActive(false);
    }

    /// <summary>
    /// スライダーバリューをリセット
    /// </summary>
    private void BossColiderValueReset()
    {
        bossGuardSliderValue.value = 0;

        bossGuardCollider.isTrigger = true;
        GetComponent<Renderer>().material.color = Color.red;
    }
    */

    /// <summary>
    /// ボスガードのHPゲージをアクティブに
    /// </summary>
    private void BossGuardHPSliderActive()
    {
        bossGuardHpSlider.SetActive(true);
    }

    /// <summary>
    /// エネミーのHPゲージを非アクティブに
    /// </summary>
    public void BossGuardHPSliderActiveOff()
    {
        bossGuardHpSlider.SetActive(false);
    }

    private void BossGuardHpReset()
    {
        bossGuardHp = 300f;
    }

    /// <summary>
    /// ボスガードフォワードのHPゲージ以外を非アクティブに
    /// </summary>
    private void BossGuardForwardHPSliderActiveOn()
    {
        if (playerAttackPoint.GetComponent<PlayerAttackController>().enemyCurrentTarget == bossGuardForward)
        {
            bossGuardLeft.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
            bossGuardRight.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
            bossGuardBack.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
        }

        else return;
    }

    /// <summary>
    /// ボスガードレフトのHPゲージ以外を非アクティブに
    /// </summary>
    private void BossGuardLeftHPSliderActiveOn()
    {
        if (playerAttackPoint.GetComponent<PlayerAttackController>().enemyCurrentTarget == bossGuardLeft)
        {
            bossGuardForward.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
            bossGuardRight.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
            bossGuardBack.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
        }
        else return;
    }

    /// <summary>
    /// ボスガードライトのHPゲージ以外を非アクティブに
    /// </summary>
    private void BossGuardRightHPSliderActiveOn()
    {
        if (playerAttackPoint.GetComponent<PlayerAttackController>().enemyCurrentTarget == bossGuardRight)
        {
            bossGuardForward.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
            bossGuardLeft.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
            bossGuardBack.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
        }
        else return;
    }

    /// <summary>
    /// ボスガードバックのHPゲージ以外を非アクティブに
    /// </summary>
    private void BossGuardBackHPSliderActiveOn()
    {
        if (playerAttackPoint.GetComponent<PlayerAttackController>().enemyCurrentTarget == bossGuardBack)
        {
            bossGuardForward.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
            bossGuardLeft.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
            bossGuardRight.GetComponent<BossGuardController>().BossGuardHPSliderActiveOff();
        }
        else return;
    }

    /*
    /// <summary>
    /// ボスガードのアクティブスライダーをON
    /// </summary>
    private void BossGuardActiveSliderActiveOn()
    {
        if (bossGuardCollider.isTrigger == false)
            return;
        else
            sliderColider.SetActive(true);
    }

    /// <summary>
    /// ボスガードのHPゲージを非アクティブに
    /// </summary>
    public void BossGuardActiveSliderActiveOff()
    {
        sliderColider.SetActive(false);
    }

    /// <summary>
    /// ボスガードフォワードのHPゲージ以外を非アクティブに
    /// </summary>
    private void BossGuardForwardActiveSliderActiveOn()
    {
        if (currentTarget == bossGuardForward)
        {
            bossGuardLeft.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
            bossGuardRight.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
            bossGuardBack.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
        }

        else return;
    }

    /// <summary>
    /// ボスガードレフト以外のHPゲージを非アクティブに
    /// </summary>
    private void BossGuardLeftActiveSliderActiveOn()
    {
        if (currentTarget == bossGuardLeft)
        {
            bossGuardForward.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
            bossGuardRight.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
            bossGuardBack.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
        }

        else return;
    }

    /// <summary>
    /// ボスガードライト以外のアクティブスライダーを非アクティブに
    /// </summary>
    private void BossGuardRightActiveSliderActiveOn()
    {
        if (currentTarget == bossGuardRight)
        {
            bossGuardForward.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
            bossGuardLeft.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
            bossGuardBack.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
        }

        else return;
    }

    /// <summary>
    /// ボスガードバック以外のアクティブスライダーを非アクティブに
    /// </summary>
    private void BossGuardBackActiveSliderActiveOn()
    {
        if (currentTarget == bossGuardBack)
        {
            bossGuardForward.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
            bossGuardLeft.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
            bossGuardRight.GetComponent<BossGuardController>().BossGuardActiveSliderActiveOff();
        }
        else return;
    }
    */

    /// <summary>
    /// アタックポイントをアクティブにインヴォーク呼び出し
    /// </summary>
    private void AttackPointActiveOnInvoke()
    {
        attackPoint.SetActive(true);
    }

    /// <summary>
    /// ボスガードがプレイヤーから受ける通常ダメージ
    /// </summary>
    private void BossGuardDamage()
    {
        // HPを減少させる
        bossGuardHp -= playerAttackPoint.GetComponent<PlayerAttackController>().nomalDamage; ;
        //ダメージ受けると効果音
        AudioSource.PlayClipAtPoint(damageSound, transform.position);

    }

    /// <summary>
    /// ボスガードがプレイヤーから受けるチャージ攻撃1のダメージ
    /// </summary>
    private void BossGuardChargeDamage1()
    {
        //プラス加算する
        damageInterval += damageIntervalPlus * Time.deltaTime;

        //加算の値に達すると1度攻撃する
        if (damageInterval % 100 <= 0)
        {
            // HPを減少させる
            bossGuardHp -= playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage1;

            //ダメージ受けると効果音
            AudioSource.PlayClipAtPoint(damageSound, transform.position);
        }

    }

    /// <summary>
    /// ボスガードがプレイヤーから受けるチャージ攻撃2のダメージ
    /// </summary>
    private void BossGuardChargeDamage2()
    {
        // HPを減少させる
        bossGuardHp -= playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage2;

        //ダメージ受けると効果音
        AudioSource.PlayClipAtPoint(damageSound, transform.position);

    }

    /// <summary>
    /// ボスガードがプレイヤーから受けるチャージ攻撃3のダメージ
    /// </summary>
    private void BossGuardChargeDamage3()
    {
        // HPを減少させる
        bossGuardHp -= playerAttackPoint.GetComponent<PlayerAttackController>().chargeDamage3;

        //ダメージ受けると効果音
        AudioSource.PlayClipAtPoint(damageSound, transform.position);

    }

    /// <summary>
    /// エンジニアポイントを加算する
    /// </summary>
    private void PlayerEngineerPointPlusNomal()
    {
        target.GetComponent<PlayerController>().EngineerPointPlusNomal();
    }

    /// <summary>
    /// エンジニアポイントを加算する
    /// </summary>
    private void PlayerEngineerPointPlusCharge1()
    {
        target.GetComponent<PlayerController>().EngineerPointPlusCharge1();
    }

    /// <summary>
    /// エンジニアポイントを加算する
    /// </summary>
    private void PlayerEngineerPointPlusCharge2()
    {
        target.GetComponent<PlayerController>().EngineerPointPlusCharge2();
    }

    /// <summary>
    /// エンジニアポイントを加算する
    /// </summary>
    private void PlayerEngineerPointPlusCharge3()
    {
        target.GetComponent<PlayerController>().EngineerPointPlusCharge3();
    }

    /// <summary>
    /// ボスガードをターゲットし続ける、他のターゲットをタップすると切り替わる
    /// </summary>
    private void GuardTargetLockOnContinuation()
    {
        if (playerAttackPoint.GetComponent<PlayerAttackController>().enemyCurrentTarget == this.gameObject)
        {
            //プレイヤーアタックポイントの向きを対象に向ける
            playerAttackPoint.GetComponent<PlayerAttackController>().GuardTargetLockOn();

        }
    }

    /// <summary>
    /// ボスガードのHPが0になったら関数を呼びだす
    /// </summary>
    private void BossGuardDestroyCallMethods()
    {
        //HPが0になったら関数を呼び出す
        if (bossGuardDestroyCallMethodsBool && bossGuardHp <= 0)
        {
            //撃破効果音
            AudioSource.PlayClipAtPoint(bossGuardDestroySound, transform.position);
            //ボスガードのHpスライダーを消す
            BossGuardHPSliderActiveOff();
            //ボスガード撃破時にBossContorollerのボスガードの現在数を-1する
            BossGuardCountPlusOpposite();
            //敵消滅時にリンクパーティクルをオフにする
            playerAttackPoint.GetComponent<PlayerAttackController>().LinkParticleOffDestroyCall();
            //ロックオンマーカーをオフにする
            playerAttackPoint.GetComponent<PlayerAttackController>().LookOnMakerActiveOff();
            //HP0なら敵（自信）を非アクティブにする
            BossGuardHpOutDestroy();
            bossGuardDestroyCallMethodsBool = false;
        }
    }

    /// <summary>
    /// ボスガードのHPが0になったら自信を消滅させる
    /// </summary>
    private void BossGuardHpOutDestroy()
    {
        //HPが0になったら自信を破棄する
        if (bossGuardDestroyEffectBool && bossGuardHp <= 0)
        {
            //ボスガード撃破時エフェクト
            Instantiate(bossGuardDestroyEffect, transform.position, Quaternion.identity);
            this.gameObject.SetActive(false);
            bossGuardDestroyEffectBool = false;
        }
    }
}
