using UnityEngine;

public class BossMoveAreaController : MonoBehaviour
{
    [SerializeField] GameObject boss;

    private bool bossMoveStopCallOne = true;


    /// <summary>
    /// 接触判定（トリガー）通っている間呼ばれ続ける
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay(Collider other)
    {
        //プレイヤーがエリアに入っている間はBossは移動する
        if (other.gameObject.tag == "Player" && boss != null)
            if (bossMoveStopCallOne)
            {
                Invoke("BossMoveInvoke", 6f);
            }

            else if (!bossMoveStopCallOne)
                BossMoveInvoke();

            else return;

        else return;
    }

    /// <summary>
    /// ボスムーブ呼び出し（インヴォークでアニメーションタイミングと合わせ）
    /// </summary>
    private void BossMoveInvoke()
    {
        boss.GetComponent<BossController>().BossMove();
        bossMoveStopCallOne = false;

    }
}
