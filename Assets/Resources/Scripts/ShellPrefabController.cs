using UnityEngine;

public class ShellPrefabController : MonoBehaviour
{
    [SerializeField] float shellSpeed;//shell攻撃速度（飛んでいく速さ）

    private float shotLifeTime = 0.0f;//Shellの生存時間


    /// <summary>
    /// アクティブになるたびに呼び出される
    /// </summary>
    private void OnEnable()
    {
        AttackShot();
    }

    private void FixedUpdate()
    {
        ShellActiveOff();
    }

    /// <summary>
    /// 接触判定（トリガー）
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        //床接触時には自信を破棄
        if (other.gameObject.tag == "Ground")
            ShellActiveOffWithGround();
    }

    /// <summary>
    /// 非アクティブになるたびに呼ばれる
    /// </summary>
    private void OnDisable()
    {
        ShotLifeTimeReset();
    }

    /// <summary>
    /// エネミーShell自信をZ軸方向に飛ばす
    /// </summary>
    private void AttackShot()
    {
        Vector3 pos = Vector3.zero;

        Rigidbody ShellRb = this.gameObject.GetComponent<Rigidbody>();
        ShellRb.velocity = pos;
        // forwardはZ軸方向に力を加える。
        ShellRb.AddForce(this.transform.forward * shellSpeed);
    }

    /// <summary>
    ///　ショット生存時間のリセット
    /// </summary>
    private void ShotLifeTimeReset()
    {
        shotLifeTime = 0.0f;
    }

    /// <summary>
    /// エネミーShell自身を非アクティブに
    /// </summary>
    private void ShellActiveOff()
    {
        shotLifeTime = shotLifeTime + Time.deltaTime;

        if (shotLifeTime >= 4.5f)
            this.gameObject.SetActive(false);

        else return;
    }

    /// <summary>
    /// エネミーShell自身が床に接触したらショット生存時間が4秒以下なら4秒にし0.5病後に消える様にする
    /// </summary>
    private void ShellActiveOffWithGround()
    {
        if(shotLifeTime <= 4.0f)
            shotLifeTime = 4.0f;
    }


}
