using UnityEngine;

public class GameCustomManager : MonoBehaviour
{
    public bool movieCallOne = true;//ステージ開始ムービー1回のみ呼びだし用

    public int chargeAttack1Number;
    public int chargeAttack2Number;
    public int chargeAttack3Number;

    private void Awake()
    {
        CustomManagerSystem();
    }

    /// <summary>
    /// シーン遷移時にFindで呼び出しているオブジェクトを破棄させない/1以上存在もさせない
    /// </summary>
    private void CustomManagerSystem()
    {
        int customObj = FindObjectsOfType<MainSoundManager>().Length;
        if (customObj > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    public void ChargeAttackSkill1SetUp()
    {
        PlayerPrefs.SetInt("ChargeAttackSkill1", chargeAttack1Number);
    }

    public void ChargeAttackSkill2SetUp()
    {
        PlayerPrefs.SetInt("ChargeAttackSkill2", chargeAttack2Number);
    }

    public void ChargeAttackSkill3SetUp()
    {
        PlayerPrefs.SetInt("ChargeAttackSkill3", chargeAttack3Number);
    }
}
