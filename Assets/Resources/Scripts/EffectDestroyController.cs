using UnityEngine;

public class EffectDestroyController : MonoBehaviour
{
    [SerializeField] float destroyTime;

    private void Awake()
    {
        Invoke("EffectDestroy", destroyTime);
    }

    private void EffectDestroy()
    {
        Destroy(this.gameObject);
    }
}
