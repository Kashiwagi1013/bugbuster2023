using UnityEngine;

public class BossLineAttackShellController : MonoBehaviour
{

    [SerializeField] float shellShotSpeed;//Shell移動スピード

    private Rigidbody shellRigidbody;//リジッドボディ
    private float shotLifeTime = 0.0f;//Shellの生存時間


    private void Awake()
    {
        shellRigidbody = this.gameObject.GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        VelocityReset();
    }

    private void FixedUpdate()
    {
        LineAttackShellShot();
        ShellActiveOff();
    }

    /// <summary>
    /// 非アクティブになるたびに呼ばれる
    /// </summary>
    private void OnDisable()
    {
        ShotLifeTimeReset();
    }

    /// <summary>
    /// リジッドボディのVelocityをリセット
    /// </summary>
    private void VelocityReset()
    {
        Vector3 pos = Vector3.zero;

        shellRigidbody.velocity = pos;
    }

    /// <summary>
    /// Shell移動指示(攻撃)
    /// </summary>
    private void LineAttackShellShot()
    {
        if (shotLifeTime >= 1.5f)
        {
            //forwardZ軸方向（青軸方向）に力を加える。
            shellRigidbody.AddForce(transform.forward * shellShotSpeed);
        }

        else return;

    }

    /// <summary>
    /// エネミーShell自身を非アクティブに
    /// </summary>
    private void ShellActiveOff()
    {
        shotLifeTime = shotLifeTime + Time.deltaTime;

        if (shotLifeTime >= 4.5f)
            this.gameObject.SetActive(false);

        else return;
    }

    /// <summary>
    ///　ショット生存時間のリセット
    /// </summary>
    private void ShotLifeTimeReset()
    {
        shotLifeTime = 0.0f;
    }
}
