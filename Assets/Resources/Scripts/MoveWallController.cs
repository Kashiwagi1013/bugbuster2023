using UnityEngine;

public class MoveWallController : MonoBehaviour
{
    [SerializeField] GameObject player;//プレイヤー
    [SerializeField] float moveWallSpeed;//動く床のスピード
    [SerializeField] float moveWallSpeedMax;//動く床のスピード上限値
    [SerializeField] float moveWallPositionMax;//動く床のポジション上限値

    private Vector3 moveWallStartPosition;//移動床の初期設定ポジション
    private float moveWallSpeedMemory;//スピードを0にした後にリセットする為
    private Rigidbody moveWallRigidbody;//moveWallのリジッドボディを取得するため




    private void Start()
    {
        //ムーブウォールリジッドボディ・コンストレインツでフリーズ使用でロックする為（ポジションMAX時）
        moveWallRigidbody = GetComponent<Rigidbody>();
        //ムーブウォールスピードを記憶
        moveWallSpeedMemory = moveWallSpeed;
        //ムーブウォールの初期位置を記憶
        moveWallStartPosition = this.transform.position;
    }

    /// <summary>
    /// /// 接触判定(触れている間は呼び出される)
    /// /// </summary>
    /// <param name="other"></param>
    private void OnCollisionStay(Collision other)
    {
        //Playerタグのオブジェクトに触れたとき
        if (other.gameObject.tag == "Player")
        {
            MoveWallStart();
            MoveWallPositionMaxLock();
            //プレイヤー移動時に滑るのを防止する為グラビティをOFFにする
            UseGravityOff();
        }
    }

    /// <summary>
    /// 接触判定(接触が離れたら呼び出される)
    /// </summary>
    /// <param name="other"></param>
    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            MoveWallReset();
            Invoke("MoveWallIsKinematicOff", 0.1f);
            //バウンド防止の為グラビティONにする
            UseGravityON();
        }
    }

    /// <summary>
    /// ムーブウォール始動スピードMAXを超えない様に動く指定頂点までいくとムーブスピードを0にする
    /// </summary>
    private void MoveWallStart()
    {

        float speedY = Mathf.Abs(this.moveWallRigidbody.velocity.y);

        if (speedY < moveWallSpeedMax)
            moveWallRigidbody.AddForce(transform.up * moveWallSpeed, ForceMode.VelocityChange);
        
        if (this.gameObject.transform.position.y >= moveWallPositionMax)
            moveWallSpeed = 0;
       
    }

    /// <summary>
    /// ムーブウォールのポジションが上限まで達したら固定する・キャラもY軸とローテションを固定
    /// </summary>
    private void MoveWallPositionMaxLock()
    {
        if (this.gameObject.transform.position.y >= moveWallPositionMax)
        {
            moveWallRigidbody.constraints = RigidbodyConstraints.FreezeAll;
            player.GetComponent<PlayerController>().MoveWallOnLock();
        }
    }

    /// <summary>
    /// 動く床が初期位置へ戻る
    /// </summary>
    private void MoveWallReset()
    {
        //バウンドを防ぐ為にイズキネマティックを使用
        moveWallRigidbody.isKinematic = true;
        moveWallSpeed = 0;
        //ムーブウォールポジションを初期位置へ移動
        this.transform.position = moveWallStartPosition;
        moveWallSpeed = moveWallSpeedMemory;
        MoveWallRigidReset();
        Debug.Log("MoveWallReset");
    }

    /// <summary>
    /// IsKinematicをfalseでOFFする
    /// </summary>
    private void MoveWallIsKinematicOff()
    {
        moveWallRigidbody.isKinematic = false;
    }

    /// <summary>
    /// リジッドボディのコンストレインツフリーズポジション・ローテションを固定
    /// </summary>
    private void MoveWallRigidReset()
    {
        moveWallRigidbody.constraints = RigidbodyConstraints.FreezePositionX
        | RigidbodyConstraints.FreezePositionZ
        | RigidbodyConstraints.FreezeRotation;
    }

    /// <summary>
    /// ムーブウォールのグラビティをOFFにする
    /// </summary>
    private void UseGravityOff()
    {
        moveWallRigidbody.useGravity = false;
    }

    /// <summary>
    /// ムーブウォールのグラビティをONにする
    /// </summary>
    private void UseGravityON()
    {
        moveWallRigidbody.useGravity = true;
    }
}
