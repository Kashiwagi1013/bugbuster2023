using UnityEngine;
using UnityEngine.SceneManagement;

public class MainSoundManager : MonoBehaviour
{

    [SerializeField] AudioSource titleBGM;//タイトルBGM
    [SerializeField] AudioSource gamePlayBGM;//ゲームシーンBGM
    [SerializeField] AudioSource tutorialBGM;//チュートリアルBGM
    [SerializeField] AudioSource bossBattleBGM;//ボスバトルBGM
    [SerializeField] AudioSource gameClearBGM;//ゲームクリアBGM

    private string bgm_StageSlect = "";//現在のシーンをテキストで記憶する用


    private void Awake()
    {
        SoundManagerSystem();
    }

    private void Start()
    {
        //シーンが切り替わった時にシーン毎にBGMを再生/停止する
        SceneManager.activeSceneChanged += OnActiveSceneChanged;
    }

    //シーンが切り替わった時に条件に応じてBGMを変更し再生/停止する
    private void OnActiveSceneChanged(Scene prevScene, Scene nextScene)
    {


        //シーンがどう変わったかで判定

        //他のシーンからタイトルシーンへ
        if (bgm_StageSlect != "TitleScene" && nextScene.name == "TitleScene")
        {
            gamePlayBGM.Stop();
            bossBattleBGM.Stop();
            gameClearBGM.Stop();
            tutorialBGM.Stop();
            titleBGM.Play();
        }

        //タイトルシーンからチュートリアルシーンへ
        if (bgm_StageSlect != "TutorialScene" && nextScene.name == "TutorialScene")
        {
            titleBGM.Stop();
            tutorialBGM.Play();
        }

        //タイトルシーンからゲームシーンへ
        if (bgm_StageSlect != "GameScene" && nextScene.name == "GameScene")
        {
            titleBGM.Stop();
            gamePlayBGM.Play();
        }

        //ゲームシーンリセット時
        if (bgm_StageSlect == "GameScene" && nextScene.name == "GameScene")
        {
            bossBattleBGM.Stop();
            gameClearBGM.Stop();
            gamePlayBGM.Play();
        }



        //遷移後のシーン名を「１つ前のシーン名」として保持
        bgm_StageSlect = nextScene.name;
    }

    /// <summary>
    /// シーン遷移時にFindで呼び出しているオブジェクトを破棄させない/1以上存在もさせない
    /// </summary>
    private void SoundManagerSystem()
    {
        int numMusicPlayers = FindObjectsOfType<MainSoundManager>().Length;
        if (numMusicPlayers > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    /// <summary>
    /// ボスバトルBGMをONに（BOSS出現時に鳴る）
    /// </summary>
    public void BossBattleBGMActive()
    {
        gamePlayBGM.Stop();
        bossBattleBGM.Play();
    }

    /// <summary>
    /// ワーニングゾーン侵入時にBGMを止める。
    /// </summary>
    public void GameBGMStop()
    {
        gamePlayBGM.Stop();
    }

    /// <summary>
    /// ゲームクリアBGMをONに（BOSS撃破時に鳴る）
    /// </summary>
    public void GameClearBGMActive()
    {
        bossBattleBGM.Stop();
        gameClearBGM.Play();
    }
}
