using UnityEngine;

public class ObjectInstanceController : MonoBehaviour
{
    [SerializeField] GameObject objectInstancePrefab;//アイテムフィールドプレファブ


    /// <summary>
    /// 接触判定（トリガー）
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        //プレイヤー接触時にアイテムフィールドを生成（接触後自信を破棄）
        if (other.gameObject.tag == "Player")
        {
            ObjectInstans();
            Destroy(this.gameObject);
        }

    }

        //オブジェクトインスタンス用
        private void ObjectInstans()
    {
        Instantiate(objectInstancePrefab);
    }
}
