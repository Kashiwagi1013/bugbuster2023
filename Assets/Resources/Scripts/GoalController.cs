using UnityEngine;
using UnityEngine.UI;


public class GoalController : MonoBehaviour
{
    [SerializeField] GameObject player;//プレイヤー指定
    [SerializeField] GameObject playerAttackPoint;//プレイヤーのアタックポイント向き指定用
    [SerializeField] GameObject sliderColider;//スライダー表示の為GameObject取得用
    [SerializeField] GameObject timer;//タイマー
    [SerializeField] GameObject ColliderEffact;//コライダーエフェクト
    [SerializeField] GameObject[] uiObject;//UI関連
    [SerializeField] Slider goalSliderValue;//スライダーバリューの取得用


    public bool goalTargetBool = false;//ゴールをターゲットとする用


    private bool sliderColledOne = true;//スライダー1度だけ呼び出し用
    private GameManager gameManager;//ゲームマネージャー



    private void Awake()
    {
        gameManager = GameManager.GameManagerGetInstance();
    }

    private void FixedUpdate()
    {
        GoalRotation();
        GoalTargetLockOnContinuation();
        GoalLockOn();
    }

    /// <summary>
    /// ゴールオブジェクトタッチでスライダーを表示
    /// </summary>
    private void GoalLockOn()
    {
        if (!sliderColledOne)
        {
            if (Input.touchCount > 0)
            {
                for (int i = 0; i < Input.touchCount; ++i)
                {
                    if (Input.GetTouch(i).phase == TouchPhase.Began)
                    {
                        //スクリーンタッチをしたらRayをMainCameraから出し
                        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                        RaycastHit hit;

                        //ヒットしたオブジェクトがこのオブジェクトだったら命令を作動
                        if (Physics.Raycast(ray, out hit))
                        {
                            if (hit.collider.gameObject == this.gameObject)
                            {
                                gameManager.UIActiveOff();
                                sliderColider.SetActive(true);
                                sliderColledOne = true;

                                //プレイヤーの向きを対象に向ける
                                player.GetComponent<PlayerController>().GoalTargetLockOn();

                                //プレイヤーアタックポイントの向きを対象に向ける
                                playerAttackPoint.GetComponent<PlayerAttackController>().GoalTargetLockOn();

                                //ロックオン効果音
                                player.GetComponent<PlayerController>().TargetLockOnSound();

                                goalTargetBool = true;
                            }
                        }
                    }
                }
            }
        }
        else return;

    }

    /// <summary>
    /// ゴールオブジェクトタッチでスライダーを表示
    /// </summary>
    public void OnMouseDown()
    {
        if (!sliderColledOne)
        {
            gameManager.UIActiveOff();
            sliderColider.SetActive(true);
            sliderColledOne = true;

            //プレイヤーの向きを対象に向ける
            player.GetComponent<PlayerController>().GoalTargetLockOn();

            //プレイヤーアタックポイントの向きを対象に向ける
            playerAttackPoint.GetComponent<PlayerAttackController>().GoalTargetLockOn();

            //ロックオン効果音
            player.GetComponent<PlayerController>().TargetLockOnSound();

            goalTargetBool = true;
        }

    }

    /// <summary>
    /// 非アクティブになる度に呼び出される
    /// </summary>
    private void OnDisable()
    {
        sliderColledOne = false;
    }

    /// <summary>
    /// プレイヤーアタックポイントがボスをターゲットし続ける他のターゲットをタップすると切り替わる
    /// </summary>
    private void GoalTargetLockOnContinuation()
    {
        if (playerAttackPoint.GetComponent<PlayerAttackController>().enemyCurrentTarget == this.gameObject && goalTargetBool)
        {
            //プレイヤーアタックポイントの向きを対象に向ける
            playerAttackPoint.GetComponent<PlayerAttackController>().GoalTargetLockOn();

        }
    }

    /// <summary>
    /// ゴールオブジェクト自身を回転
    /// </summary>
    private void GoalRotation()
    {
        this.transform.Rotate(-0.5f, -0.5f, -0.5f);
    }

    /// <summary>
    /// ゲーム開始時間カウント/表示
    /// </summary>
    private void UIActiveOff()
    {
        for (int i = 0; i <= 19; i++)
        {
            uiObject[i].SetActive(false);
        }
    }

    /// <summary>
    /// スライダーバリュー100でゲームクリア
    /// </summary>
    public void GoalColiderValue()
    {
        if (goalSliderValue.value == 100)
        {
            //ゲームクリアの指示
            gameManager.GameClear();
            //コライダーエフェクト
            Instantiate(ColliderEffact, transform.position, Quaternion.identity);
            timer.SetActive(false);
            UIActiveOff();
            PlayerActiveOff();
        }
    }

    /// <summary>
    /// プレイヤーをアクティブオフに
    /// </summary>
    private void PlayerActiveOff()
    {
        player.SetActive(false);
    }
}
