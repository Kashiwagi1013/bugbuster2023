using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] Text timeCountText;//時間をテキストに表示する用
    [SerializeField] Text clearTimeText;//クリアタイムをテキストに表示する用

    static public float time;//時間カウント用


    private void FixedUpdate()
    {
        //タイマーカウント用
        TimeCount();
        //クリアタイム表示用
        ClearTimeTextActive();
    }

    /// <summary>
    /// 時間カウント/表示
    /// </summary>
    private void TimeCount()
    {
        time += Time.deltaTime;
        timeCountText.GetComponent<Text>().text = "Time " + time.ToString("F2");
    }

    private void ClearTimeTextActive()
    {
        clearTimeText.GetComponent<Text>().text = time.ToString("F2");
    }

}
