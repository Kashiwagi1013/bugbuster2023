using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class WarningUIActiveController : MonoBehaviour
{
    [SerializeField] GameObject[] warningUI;//デンジャー表示UI関連
    [SerializeField] AudioClip[] warningSE;//Warning（警告）ボス戦前
    [SerializeField] Text[] warningText;//デンジャーテキスト
    [SerializeField] Image[] warningImage;//デンジャーバックイメージ
    [SerializeField] Light[] lightColor;//ライトのカラー
    [SerializeField] Light lightResetColor;//ライトのリセットカラー

    private GameObject mainSoundManager;//BGM切り替え用

    private AudioSource audioSource;//オーディオ取得用
    private bool warningCallOne = true;//ワーニング一回呼び出し



    private void Start()
    {
        warningCallOne = true;
        audioSource = gameObject.GetComponent<AudioSource>();
        mainSoundManager = GameObject.FindGameObjectWithTag("MainSound");
        lightResetColor.color = lightColor[0].color;
    }

    /// <summary>
    /// 接触判定（トリガー）
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            WarningExpression();
        }
    }

    /// <summary>
    /// ワーニングUIの表示/削除
    /// </summary>
    private void WarningUIActiveSeting()
    {
        for (int i = 0; i <= 3; i++)
        {
            warningUI[i].SetActive(true);
            Destroy(warningUI[i], 5.0f);
        }
    }

    /// <summary>
    /// ワーニングテキスト表示
    /// </summary>
    private void WarningTextOn()
    {
        for (int i = 0; i <= 1; i++)
        {
            warningText[i].DOFade(0.0f, 1.0f)
            .SetLoops(5, LoopType.Yoyo)
            .Play();
        }
    }

    /// <summary>
    /// ボス戦前のワーニングバックイメージ表示
    /// </summary>
    private void WarningBackImageOn()
    {
        warningImage[0].DOFade(0.0f, 1.0f)
            .SetLoops(5, LoopType.Yoyo)
            .Play();

        warningImage[1].DOFade(0.0f, 1.0f)
            .SetLoops(5, LoopType.Yoyo)
            .Play();
    }

    /// <summary>
    /// ワーニングライトのカラー変更
    /// </summary>
    private void WarningLightColorChange()
    {
        for (int i = 0; i <= 3; i++)
        {
            lightColor[i].DOColor(Color.red,1f)
            .SetLoops(5, LoopType.Yoyo)
            .Play();
        }
    }

    /// <summary>
    /// ワーニングライトのカラーを初期のカラーへ戻す(インヴォーク呼び出し)
    /// </summary>
    private void WarningLightColorResetInvoke()
    {
        for (int i = 0; i <= 3; i++)
        {
            lightColor[i].color = lightResetColor.color;
        }
    }

    /// <summary>
    /// ワーニングSEを鳴らす
    /// </summary>
    private void WarningSoundCall()
    {
        audioSource.PlayOneShot(warningSE[0]);
        audioSource.PlayOneShot(warningSE[1]);
    }

    /// <summary>
    /// エネミー撃破数に応じてムーブウォールが全てアクティブになれば通常BGMを止める
    /// </summary>
    private void WarningActionBGMStop()
    {
        mainSoundManager = GameObject.FindGameObjectWithTag("MainSound");
        mainSoundManager.GetComponent<MainSoundManager>().GameBGMStop();
    }

    /// <summary>
    /// エネミー撃破数に応じてムーブウォールが全てアクティブになればワーニングの指示を出す
    /// </summary>
    private void WarningExpression()
    {
        if (warningCallOne)
        {
            WarningActionBGMStop();
            WarningSoundCall();
            WarningUIActiveSeting();
            WarningTextOn();
            WarningBackImageOn();
            WarningLightColorChange();
            Invoke("WarningLightColorResetInvoke", 5.1f);
            Invoke("DestroyInvoke", 6f);
            warningCallOne = false;
        }
    }

    /// <summary>
    /// 自信のオブジェクトを削除（インヴォーク）
    /// </summary>
    private void DestroyInvoke()
    {
        Destroy(this.gameObject);
    }
}
