using UnityEngine;

public class ChargeAttackShell3Controller : MonoBehaviour
{

    [SerializeField] GameObject chargeAttackExplosion;//爆発エフェクト
    [SerializeField] AudioClip chargeAttackExplosionSE;//爆発音


    private GameObject playerAttackPoint;//playerのアタックポイント
    private bool instantiateCallOne = true;//爆発を一回呼び出す用


    
    /// <summary>
    /// 接触判定（トリガー）
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "PlayerAttackPoint")
            if (other.gameObject.tag != "Player")
                if (other.gameObject.tag != "Item")
                    if (other.gameObject.tag != "ItemPosition")
                        if (other.gameObject.tag != "PlayerDamage")
                            if (other.gameObject.tag != "EnemyDamage")
                                if (other.gameObject.tag != "PlsayerChargeAttackShell")
                                    if (other.gameObject.tag != "BossAttackShell")
                                        if (other.gameObject.tag != "BossAttackLineShell")
                                            if (other.gameObject.tag != "ChargeAttack3")
                                            {
                                                playerAttackPoint = GameObject.FindGameObjectWithTag("PlayerAttackPoint");
                                                ChargeAttackExplosionScale();
                                                ChargeAttackExplosionInstantiate();
                                                Destroy(this.gameObject);
                                            }
    }

    /// <summary>
    /// チャージアタック3の爆発を生成し
    /// </summary>
    private void ChargeAttackExplosionInstantiate()
    {
        if (instantiateCallOne)
        {
            Instantiate(chargeAttackExplosion, transform.position, Quaternion.identity);
            AudioSource.PlayClipAtPoint(chargeAttackExplosionSE, transform.position);
            instantiateCallOne = false;
        }

    }

    /// <summary>
    /// チャージアタック3の爆発範囲をスキルポイントの分大きく変更
    /// </summary>
    private void ChargeAttackExplosionScale()
    {
        if (playerAttackPoint.gameObject.activeInHierarchy == true)
        {
            chargeAttackExplosion.transform.localScale = new Vector3(playerAttackPoint.GetComponent<PlayerAttackController>().charge3ExplosionScale,
                playerAttackPoint.GetComponent<PlayerAttackController>().charge3ExplosionScale, playerAttackPoint.GetComponent<PlayerAttackController>().charge3ExplosionScale);
        }

        else return;
    }

}
