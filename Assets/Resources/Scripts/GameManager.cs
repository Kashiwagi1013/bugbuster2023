using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using Cinemachine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject player;//プレイヤー
    [SerializeField] GameObject playerStartLookPoint;//playerのスタート時のルックポイント
    [SerializeField] GameObject playerCameraStartLookPoint;//playerカメラのスタート時のルックポイント
    [SerializeField] GameObject[] playerSpawnPoint;//プレイヤースポーンポイント
    [SerializeField] GameObject startCinemachineAnimationCameras;//アニメーションカメラ
    [SerializeField] GameObject bossCinemachineAnimationCameras;//アニメーションカメラ
    [SerializeField] GameObject countDownObject;//カウントダウンオブジェクト
    [SerializeField] GameObject goal;//ゴール
    [SerializeField] GameObject menuButton;//メニューボタン
    [SerializeField] GameObject menuSubButton;//メニューサブボタン
    [SerializeField] GameObject retryButton;//リトライボタン
    [SerializeField] GameObject[] menuSelectButtons;//メニューセレクトボタン
    [SerializeField] GameObject[] uiObject;//UI関連
    [SerializeField] GameObject nameInputField;//ネームインプット用
    [SerializeField] GameObject resultBackImage;//リザルトバックイメージ
    [SerializeField] GameObject rankingView;//ランキングビュー
    [SerializeField] GameObject stageClearUI;//ステージククリアUI
    [SerializeField] Canvas currentUI;//現在のUIを表示/非表示命令用
    [SerializeField] CinemachineVirtualCamera vCam2;//バーチャルカメラ2
    [SerializeField] CinemachineVirtualCamera vCam3;//バーチャルカメラ3
    [SerializeField] CinemachineVirtualCamera vCam4;//バーチャルカメラ4
    [SerializeField] CinemachineVirtualCamera vCam5;//バーチャルカメラ5
    [SerializeField] CinemachineVirtualCamera vCam6;//バーチャルカメラ6
    [SerializeField] CinemachineVirtualCamera vCam7;//バーチャルカメラ7
    [SerializeField] CinemachineVirtualCamera stageClearResultvCam;//ステージクリア時のリザルト表示のバーチャルカメラ
    [SerializeField] Transform vCam2Transform;//バーチャルカメラ2トランスフォーム
    [SerializeField] Transform vCam3Transform;//バーチャルカメラ3トランスフォーム
    [SerializeField] Image sceneChangeBackImage;//シーンチェンジ時のバックイメージ
    [SerializeField] Text expCountText;//獲得経験値をテキストに表示する用
    [SerializeField] Text playerLevelText;//プレイヤーレベルをテキストに表示する用
    [SerializeField] Text clearResultText;//クリアリザルトを表示するテキスト
    [SerializeField] Text clearResultTextPoint;//クリアリザルトのポイン表示するテキスト
    [SerializeField] Text scoreResultText;//リザルトタイムを表示するテキスト
    [SerializeField] Text scoreResultTextPoint;//リザルトタイムのポイント表示するテキスト
    [SerializeField] Text clearTimeText;//ステージクリア時のタイムを表示するテキスト
    [SerializeField] Text totalTimeText;//集計後のタイムを表示するテキスト
    [SerializeField] AudioClip gameClearSound;//ゲームクリア効果音
    [SerializeField] AudioClip gameClearCompleteButtonSE;//ゲームクリア時に鳴るSE
    [SerializeField] AudioClip ViewOpenButtonSE;//メニューオープン用SE
    [SerializeField] AudioClip ViewCloseButtonSE;//キャンセルボタンSE
    

    public float clearResultTime;//クリア時のリザルトを合計TIME
    public Text userText;//プレイヤーレベルアップをテキストに表示する用


    private static GameManager instance;//シングルトンパターン用変数


    private GameObject CustomManager;//カスタムマネージャー
    private float playerCurrentHp;//プレイヤー現在HPゲームオーバー用
    private float resultTime;//リザルトタイム(集計用)
    private int enemyDestroyCount;//エネミー撃破数
    private int gm_playerGetExp;//UI表示用プレイヤー獲得Exp
    private int gm_playerCurrentLevel;//UI表示用プレイヤーレベル
    private int playerDeadCountPoint;//プレイヤーの死亡回数をポイントにする用
    private bool playerDeadCallOne;//プレイヤー死亡時を1度だけ呼び出し用
    private bool buttonOneCall = true;//ゲームクリア時のバックタイトルボタン一回のみ呼び出しの為
    private AudioSource audioSource;//オーディオ取得用


    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        sceneChangeBackImage.enabled = true;

        CustomManager = GameObject.FindGameObjectWithTag("CustomManager");

        audioSource = gameObject.GetComponent<AudioSource>();

        playerDeadCallOne = false;

        StartAnimationMovie();
        SceneChangeBackImageAnimationOff();
        PlayerSpawnPoint();
        PlayerStartLook();
        PlayerCameraStartLook();
        Invoke("SceneChangeBackImageSetActiveOffInvoke", 3.0f);      
    }

    private void FixedUpdate()
    {

        //プレイヤー落下でゲームシーン再読み込み
        PlayerFieldOut();

        //プレイヤーHP0になった時の指示
        PlayerDeadAction();

        //プレイヤーの獲得EXPをテキストへ反映
        GetExpText();

        //プレイヤーの現在レベル表示
        PlayerCurrentLevelText();
    }

    /// <summary>
    /// プレイヤーオブジェクトをアクティブに
    /// </summary>
    private void PlayerActive()
    {
        player.SetActive(true);
    }

    /// <summary>
    /// 初回時のゲームスタート時にムービーを再生する以降は省略する
    /// </summary>
    private void StartAnimationMovie()
    {
        if(CustomManager.GetComponent<GameCustomManager>().movieCallOne)
        {
            StartCoroutine("StartVCameraAnimationCoroutine");
            CustomManager.GetComponent<GameCustomManager>().movieCallOne = false;
        }

        else
        {
            VCamera4PriorityDown();
            VCamera3PriorityDown();
            VCamera2PriorityDown();
            GoalActiveOff();
            PlayerActive();
            StartCinemachineAnimationCameraActiveOff();
            CountDownObjectActive();
        }

    }

    /// <summary>
    /// シーンチェンジ用バックイラストをアクティブにするアニメーション
    /// </summary>
    private void SceneChangeBackImageAnimationOn()
    {
        sceneChangeBackImage.DOFade(0.9f, 3.0f);
    }

    /// <summary>
    /// シーンチェンジ用バックイラストを真っ暗にするアニメーション
    /// </summary>
    private void SceneChangeBackImageFullAnimationOn()
    {
        sceneChangeBackImage.DOFade(1.0f, 2.0f);
    }

    /// <summary>
    /// シーンチェンジ用バックイラストを透明にするアニメーション
    /// </summary>
    private void SceneChangeBackImageAnimationOff()
    {
        sceneChangeBackImage.DOFade(0.0f,3.0f);
    }

    /// <summary>
    /// シーンチェンジ用バックイラストをアクティブにする
    /// </summary>
    private void SceneChangeBackImageSetActiveOn()
    {
        sceneChangeBackImage.enabled = true;
    }

    /// <summary>
    /// シーンチェンジ用バックイラストを非アクティブにする（インヴォーク）
    /// Start()　PlayerDeadAction()にて呼び出し
    /// </summary>
    private void SceneChangeBackImageSetActiveOffInvoke()
    {
        sceneChangeBackImage.enabled = false;
    }

    /// <summary>
    /// ボスアニメーションをBossContorollerボス出現時に呼び出し
    /// </summary>
    public void BossAnimation()
    {
        StartCoroutine("BossVCameraAnimationCoroutine");
    }

    /// <summary>
    /// ゲームスタート時のムービーアニメーションコルーチン呼び出しにて調整
    /// </summary>
    /// <returns></returns>
    private IEnumerator StartVCameraAnimationCoroutine()
    {
        yield return new WaitForSeconds(2.5f);
        VCamera4PriorityDown();
        VCamera3Rotate();
        yield return new WaitForSeconds(5.0f);
        VCamera3Move();
        yield return new WaitForSeconds(2.5f);
        VCamera3PriorityDown();
        yield return new WaitForSeconds(2.5f);
        VCamera2PriorityDown();
        yield return new WaitForSeconds(2.0f);
        GoalActiveOff();
        PlayerActive();
        StartCinemachineAnimationCameraActiveOff();
        CountDownObjectActive();
    }

    /// <summary>
    /// ボス出現時コルーチン呼びだしにてアニメーション等
    /// </summary>
    /// <returns></returns>
    private IEnumerator BossVCameraAnimationCoroutine()
    {
        currentUI.enabled = false;
        VCamera5PriorityUp();
        yield return new WaitForSeconds(2.0f);
        VCamera6PriorityUp();
        VCamera5PriorityDown();
        yield return new WaitForSeconds(2.0f);
        VCamera7PriorityUp();
        VCamera6PriorityDown();
        yield return new WaitForSeconds(2.0f);
        VCamera7PriorityDown();
        currentUI.enabled = true;
        BossCinemachineAnimationCameraActiveOff();
    }

    /// <summary>
    /// バーチャルカメラ3のカメラディスタンスを変更
    /// </summary>
    private void VCamera3Move()
    {
        CinemachineComponentBase componentBase = vCam3.GetCinemachineComponent(CinemachineCore.Stage.Body);
        if (componentBase is CinemachineFramingTransposer)
        {
            (componentBase as CinemachineFramingTransposer).m_CameraDistance = 40;
        }
    }

    /// <summary>
    /// バーチャルカメラ3のカ回転を変更
    /// </summary>
    private void VCamera3Rotate()
    {
        vCam3Transform.DORotateQuaternion(Quaternion.AngleAxis(100f, new Vector3(100f, 100f)), 1.0f);
        vCam3Transform.DOLookAt(goal.transform.localPosition, 1.0f);
    }

    /// <summary>
    /// バーチャルカメラ3のプリオリティを0に
    /// </summary>
    private void VCamera2PriorityDown()
    {
        vCam2.Priority = 0;
    }

    /// <summary>
    /// バーチャルカメラ3のプリオリティを0に
    /// </summary>
    private void VCamera3PriorityDown()
    {
        vCam3.Priority = 0;
    }

    /// <summary>
    /// バーチャルカメラ4のプリオリティを0に
    /// </summary>
    private void VCamera4PriorityDown()
    {
        vCam4.Priority = 0;
    }

    /// <summary>
    /// バーチャルカメラ5のプリオリティを0に
    /// </summary>
    private void VCamera5PriorityDown()
    {
        vCam5.Priority = 0;
    }

    /// <summary>
    /// バーチャルカメラ6のプリオリティを0に
    /// </summary>
    private void VCamera6PriorityDown()
    {
        vCam6.Priority = 0;
    }

    /// <summary>
    /// バーチャルカメラ7のプリオリティを0に
    /// </summary>
    private void VCamera7PriorityDown()
    {
        vCam7.Priority = 0;
    }

    /// <summary>
    /// バーチャルカメラ5のプリオリティを50に
    /// </summary>
    private void VCamera5PriorityUp()
    {
        vCam5.Priority = 50;
    }

    /// <summary>
    /// バーチャルカメラ6のプリオリティを50に
    /// </summary>
    private void VCamera6PriorityUp()
    {
        vCam6.Priority = 60;
    }

    /// <summary>
    /// バーチャルカメラ7のプリオリティを50に
    /// </summary>
    private void VCamera7PriorityUp()
    {
        vCam7.Priority = 60;
    }

    /// <summary>
    /// バーチャルカメラ2・3・4をアクティブオフに
    /// </summary>
    private void StartCinemachineAnimationCameraActiveOff()
    {
        startCinemachineAnimationCameras.SetActive(false);
    }

    /// <summary>
    /// バーチャルカメラ5・6・7をアクティブオフに
    /// </summary>
    private void BossCinemachineAnimationCameraActiveOff()
    {
        bossCinemachineAnimationCameras.SetActive(false);
    }

    /// <summary>
    /// カウントダウンオブジェクトをアクティブに
    /// </summary>
    private void CountDownObjectActive()
    {
        countDownObject.SetActive(true);
    }

    /// <summary>
    /// プレイヤーのスポーンポイントをランダムで選定
    /// </summary>
    private void PlayerSpawnPoint()
    {
        int p = Random.Range(0, 5);
        Vector3 startPosion = new Vector3(0, 0.25f, 0);
        player.transform.position = playerSpawnPoint[p].transform.position + startPosion;
    }

    /// <summary>
    /// プレイヤーのスタート時の向きをスタート真正面に向くようにする。
    /// </summary>
    private void PlayerStartLook()
    {
        player.transform.LookAt(playerStartLookPoint.transform.position);

        //現在のローテーションをcurrentRotation_yに入れる為の変数
        float currentRotation_y;
        //現在のインスペクター表示のy座標を変数yに代入
        currentRotation_y = player.transform.localEulerAngles.y;
        //y以外は回転を0にしyだけ数値を適応する
        player.transform.rotation = Quaternion.Euler(0.0f, currentRotation_y, 0.0f);
    }

    /// <summary>
    /// プレイヤーカメラのスタート時にプレイヤーと同じ方向を向くようにする。（プレイヤーのｙ軸だけ代入）
    /// </summary>
    private void PlayerCameraStartLook()
    {
        float x = playerCameraStartLookPoint.transform.localEulerAngles.x;
        float y = player.transform.localEulerAngles.y;
        float z = playerCameraStartLookPoint.transform.localEulerAngles.z;
        playerCameraStartLookPoint.transform.localRotation = Quaternion.Euler(x, y, z);
    }

    /// <summary>
    /// PlayerControllerより呼び出し条件付けてボタン割り当て
    /// </summary>
    public void PlayerActiveButton()
    {
        if (menuSubButton.activeSelf == false)
            player.GetComponent<PlayerController>().PlayerActiveOnButton();

        else return;
    }

    /// <summary>
    /// プレイヤーHPが0になったらリトライボタンを表示する（他のUIは触れない）
    /// </summary>
    private void PlayerDeadAction()
    {
        playerCurrentHp = player.GetComponent<PlayerController>().playerHp;

        if (playerCurrentHp <= 0 && !playerDeadCallOne)
        {
            SceneChangeBackImageSetActiveOn();
            SceneChangeBackImageAnimationOn();
            playerDeadCallOne = true;
        }

        else if (playerDeadCallOne && player.activeSelf == true)
        {
            playerDeadCallOne = false;
            SceneChangeBackImageAnimationOff();
            Invoke("SceneChangeBackImageSetActiveOffInvoke", 0.5f);
        }

        else return;
    }

    /// <summary>
    /// プレイヤーが落下したらゲーム開始ポイントへ戻る
    /// </summary>
    private void PlayerFieldOut()
    {
        if (player.GetComponent<PlayerController>().playerFieldOutBool)
        {
            Invoke("PlayerSpawnPoint", 1.0f);
            Invoke("PlayerStartLook", 1.0f);
            Invoke("PlayerCameraStartLook", 1.0f);
            player.GetComponent<PlayerController>().playerFieldOutBool = false;
        }

        else return;

    }

    /// <summary>
    /// プレイヤーレベル/表示
    /// </summary>
    private void PlayerCurrentLevelText()
    {
        if(player.GetComponent<PlayerController>().playerLevel == 20)
            playerLevelText.GetComponent<Text>().text = "Lv. MAX";

        else
        {
            gm_playerCurrentLevel = player.GetComponent<PlayerController>().playerLevel;
            playerLevelText.GetComponent<Text>().text = "Lv. " + gm_playerCurrentLevel;
        }
    }

    /// <summary>
    /// シングルトンパターン 渡し
    /// </summary>
    public static GameManager GameManagerGetInstance()
    {
        return instance;
    }

    /// <summary>
    /// プレイヤーレベルアップテキスト表示
    /// </summary>
    public void PlayerLevelUpTextOn()
    {
        userText.DOText("LevelUp",0.7f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);
    }

    /// <summary>
    /// ユーザーテキストを空白にするアニメーション
    /// </summary>
    public void PlayerLevelUpTextEmpty()
    {
        userText.DOText("       ", 0.2f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);
    }

    /// <summary>
    /// ユーザーテキストを空欄にする
    /// </summary>
    public void UserTextEmpty()
    {
        userText.text = "";
    }

    /// <summary>
    /// 獲得経験値/表示
    /// </summary>
    private void GetExpText()
    {
        gm_playerGetExp = player.GetComponent<PlayerController>().playerGetExp;
        expCountText.GetComponent<Text>().text = gm_playerGetExp.ToString() + " Exp";
    }

    /// <summary>
    /// ゲームシーン再読み込み
    /// </summary>
    private void GameSceneLoadActionInvoke()
    {
        enemyDestroyCount = 0;
        TimerCountReset();
        SceneManager.LoadScene("GameScene");
    }

    /// <summary>
    /// チュートリアルシーン再読み込み
    /// </summary>
    private void TutorialSceneLoad()
    {
        enemyDestroyCount = 0;
        TimerCountReset();
        SceneManager.LoadScene("TutorialScene");
    }

    /// <summary>
    /// エネミー撃破数カウント
    /// </summary>
    public void EnemyDestroyCountPlus()
    {
        enemyDestroyCount++;
        Debug.Log(enemyDestroyCount);
    }

    /// <summary>
    /// ゴールをアクティブ（Boss撃破で呼び出しに）
    /// </summary>
    public void GoalActive()
    {
        goal.SetActive(true);
    }

    /// <summary>
    /// ゴールを非アクティブに
    /// </summary>
    private void GoalActiveOff()
    {
        goal.SetActive(false);
    }

    /// <summary>
    /// プレイヤーレベルアップテキスト表示
    /// </summary>
    private void StageClearTextOn()
    {
        userText.DOText("STAGE CLEAR!!", 1.0f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);
    }

    /// <summary>
    /// プレイヤーリトライ回数を表示し/リトライの回数分に応じたタイムへの影響を設定
    /// </summary>
    private void PlayerDeadCountResult()
    {
        playerDeadCountPoint = player.GetComponent<PlayerController>().playerDeadCount;

        if (playerDeadCountPoint == 0)
            resultTime = -30;

        else if (playerDeadCountPoint > 0)
        {
            resultTime = +10;
            resultTime *= playerDeadCountPoint;
        }

        clearResultText.enabled = true;
        clearResultText.DOText("Retry :",1.0f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);

        clearResultTextPoint.enabled = true;
        clearResultTextPoint.DOText(playerDeadCountPoint.ToString("F0"), 1.0f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);
    }

    /// <summary>
    /// リザルトタイムテキストにScoreとして計算されるポイントを表示
    /// </summary>
    private void ClearScoreResult()
    {
        scoreResultText.enabled = true;
        scoreResultText.DOText("BonusTime :", 1.0f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);

        scoreResultTextPoint.enabled = true;
        scoreResultTextPoint.DOText(resultTime.ToString("F0"), 1.0f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);
    }

    /// <summary>
    /// リザルトに応じてポイントのカラーを変更する
    /// </summary>
    private void PointColorChange()
    {

        if (playerDeadCountPoint == 0)
            clearResultTextPoint.DOColor(Color.blue, 1f)
                 .SetLoops(-1, LoopType.Yoyo)
                 .Play();

        else if (playerDeadCountPoint > 0)
            clearResultTextPoint.DOColor(Color.red, 1f)
                .SetLoops(-1, LoopType.Yoyo)
                .Play();

        if (playerDeadCountPoint == 0)
            scoreResultTextPoint.DOColor(Color.blue, 1f)
                 .SetLoops(-1, LoopType.Yoyo)
                 .Play();

        else if (playerDeadCountPoint > 0)
            scoreResultTextPoint.DOColor(Color.red, 1f)
                .SetLoops(-1, LoopType.Yoyo)
                .Play();
    }

    /// <summary>
    /// クリアタイムテキストにクリア時のタイムを表示
    /// </summary>
    private void ClearTimeResult()
    {
        clearTimeText.enabled = true;
        clearTimeText.DOText("ClearTime : " + clearResultTime.ToString("F2"), 1.0f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);
    }

    /// <summary>
    /// トータルタイムテキストにスコアとボーナスを集計したタイムを表示
    /// </summary>
    private void TotalTimeResult()
    {
        clearResultTime += resultTime;
        totalTimeText.enabled = true;
        totalTimeText.DOText("TotalTime\n" + clearResultTime.ToString("F2"), 1.0f, scrambleMode: ScrambleMode.All).SetEase(Ease.Linear);
    }

    /// <summary>
    /// トータルタイムテキストにアニメーション
    /// </summary>
    private void TotalTimeResultAnimation()
    {
        totalTimeText.DOColor(Color.yellow, 1f)
             .SetLoops(-1, LoopType.Yoyo)
             .Play();
    }

    /// <summary>
    /// ネームインプットフィールドアクティブ
    /// </summary>
    private void NameInputFieldActive()
    {
        nameInputField.SetActive(true);
    }

    /// <summary>
    /// リザルトバックイメージアクティブ
    /// </summary>
    private void ResultBackImageActive()
    {
        resultBackImage.SetActive(true);
        resultBackImage.transform.localScale = new Vector3(0, 0, 0);
        resultBackImage.transform.DOScale(new Vector3(1, 1, 1),0.7f);
    }

    /// <summary>
    /// ステージクリアのリザルトをコルーチン呼びだし
    /// </summary>
    /// <returns></returns>
    private IEnumerator StageClearCoroutine()
    {
        stageClearResultvCam.Priority = 60;
        clearResultTime = Timer.time;
        audioSource.PlayOneShot(gameClearSound);
        yield return new WaitForSeconds(2.2f);
        ResultBackImageActive();
        StageClearTextOn();
        yield return new WaitForSeconds(2.0f);
        ClearTimeResult();
        yield return new WaitForSeconds(0.5f);
        PlayerDeadCountResult();
        yield return new WaitForSeconds(0.5f);
        ClearScoreResult();
        PointColorChange();
        TotalTimeResultAnimation();
        yield return new WaitForSeconds(0.5f);
        TotalTimeResult();
        yield return new WaitForSeconds(0.5f);
        NameInputFieldActive();
    }

    /// <summary>
    /// ゲームクリアテキスト表示
    /// </summary>
    public void GameClear()
    {
        StartCoroutine("StageClearCoroutine");
    }

    /// <summary>
    /// メニューボタンの子をアクティブに
    /// </summary>
    public void MenuButtonsActive()
    {
        if (menuSubButton.activeSelf)
        {
            Time.timeScale = 1;
            menuSubButton.SetActive(false);
            audioSource.PlayOneShot(ViewCloseButtonSE);
            if (retryButton.activeSelf == false)
            {
                SceneChangeBackImageSetActiveOffInvoke();
            }
        }

        else if (menuSubButton.activeSelf == false)
        {
            SceneChangeBackImageSetActiveOn();
            SceneChangeBackImageAnimationOn();
            menuSubButton.SetActive(true);
            audioSource.PlayOneShot(ViewOpenButtonSE);
            Time.timeScale = 0;
        }

        else return;
            
    }

    private void StageClearUiOff()
    {
        stageClearUI.SetActive(false);
    }

    private void StageClearUIActiveOffAnimation()
    {
        stageClearUI.transform.DOScale(new Vector3(0, 0, 0), 0.5f);
    }

    /// <summary>
    /// ランキングビューのアニメーション
    /// </summary>
    public void RankingViewAnimation()
    {
        if (buttonOneCall)
        {
            rankingView.SetActive(true);
            audioSource.PlayOneShot(gameClearCompleteButtonSE);
            StageClearUIActiveOffAnimation();
            Invoke("StageClearUiOff", 0.5f);
            userText.enabled = false;
            rankingView.transform.DOShakeScale(duration: 0.3f, strength: 1.5f);
            buttonOneCall = false;
        }
    }

    /// <summary>
    /// ゲームリセットボタンをアクティブに
    /// </summary>
    public void MenuSelectGameResetButtonActive()
    {
        if (buttonOneCall)
        {
            menuButton.SetActive(false);
            menuSelectButtons[1].SetActive(true);
            menuSelectButtons[2].SetActive(true);
            audioSource.PlayOneShot(ViewOpenButtonSE);
            buttonOneCall = false;
        }

    }

    /// <summary>
    /// タイトルバックボタンをアクティブに
    /// </summary>
    public void MenuSelectTitleBackButtonActive()
    {
        if (buttonOneCall)
        {
            menuButton.SetActive(false);
            menuSelectButtons[0].SetActive(true);
            menuSelectButtons[2].SetActive(true);
            audioSource.PlayOneShot(ViewOpenButtonSE);
            buttonOneCall = false;
        }

    }

    /// <summary>
    /// メニューセレクトボタンを非アクティブに
    /// </summary>
    public void MenuSelectButtonsActiveOff()
    {
        if (!buttonOneCall)
        {
            for (int i = 0; i < 3; i++)
            {
                menuSelectButtons[i].SetActive(false);
            }
            menuButton.SetActive(true);
            audioSource.PlayOneShot(ViewCloseButtonSE);
            buttonOneCall = true;
        }

    }

    /// <summary>
    /// タイトル画面へ戻る
    /// </summary>
    private void BackTitleActionInvoke()
    {
        TimerCountReset();
        SceneManager.LoadScene("TitleScene");
    }

    /// <summary>
    /// ゲームのリセット
    /// </summary>
    public void ResetGameButton()
    {
        if (!buttonOneCall)
        {
            audioSource.PlayOneShot(gameClearCompleteButtonSE);
            SceneChangeBackImageFullAnimationOn();
            Invoke("GameSceneLoadActionInvoke",1.0f);
            for (int i = 0; i < 3; i++)
            {
                menuSelectButtons[i].SetActive(false);
            }
            Time.timeScale = 1;
        }
    }

    /// <summary>
    /// チュートリアルのリセット
    /// </summary>
    public void ResetTutorialButton()
    {
        TutorialSceneLoad();
        TimerCountReset();
    }

    /// <summary>
    /// タイトル画面へ戻る
    /// </summary>
    public void GameBackTitleButton()
    {
        if (!buttonOneCall)
        {
            audioSource.PlayOneShot(gameClearCompleteButtonSE);
            SceneChangeBackImageFullAnimationOn();
            enemyDestroyCount = 0;
            rankingView.transform.DOShakeScale(duration: 0.5f, strength: 2.5f);
            rankingView.transform.DOScale(new Vector3(0, 0, 0), 0.5f);
            for (int i = 0; i < 3; i++)
            {
                menuSelectButtons[i].SetActive(false);
            }
            Invoke("BackTitleActionInvoke", 1.0f);
            Time.timeScale = 1;
        }
    }

    /// <summary>
    /// タイマーをリセット
    /// </summary>
    public void TimerCountReset()
    {
        Timer.time = 0;
    }

    /// <summary>
    /// UIを非表示に
    /// </summary>
    public void UIActiveOff()
    {
        for (int i = 0; i <= 19; i++)
        {
            uiObject[i].SetActive(false);
        }
    }

}
