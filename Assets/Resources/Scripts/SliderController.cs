using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SliderController : MonoBehaviour
{
    [SerializeField] GameObject enemySliderObject;//スライダー表示の為GameObject取得用
    [SerializeField] Slider enemySlider;//スライダーバリューの取得用
    [SerializeField] Transform activeSliderTransform;//アクティブスライダートランスフォーム


    private void Awake()
    {
        enemySlider = GetComponent<Slider>();
    }

    private void OnEnable()
    {
        TransformScaleActiveAnimation();
    }

    /// <summary>
    /// スライダーの操作/Valueが100になると削除
    /// </summary>
    public void OnDrag()
    {
        if (enemySlider.value == 100)
        {
            this.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// スケールをシェイクするアニメーション
    /// </summary>
    private void TransformScaleActiveAnimation()
    {
        activeSliderTransform.DOShakeScale(duration:0.3f,strength:1.5f)
            .Play();
    }
}
