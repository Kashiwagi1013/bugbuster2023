using UnityEngine;
using UnityEngine.UI;

public class EnemyHelthBar : MonoBehaviour
{
    [SerializeField] GameObject enemy;//エネミー
    [SerializeField] Slider enemyHpSlider;//HPスライダー

    private float currentHp;//現在HP管理用


    private void Start()
    {
        enemyHpSlider = GetComponent<Slider>();
    }

    private void FixedUpdate()
    {
        EnemyCurrentHP();
    }

    /// <summary>
    /// エネミーのHPをスライダーへ反映
    /// </summary>
    private void EnemyCurrentHP()
    {
        currentHp = enemy.GetComponent<EnemyController>().enemyHp;
        enemyHpSlider.value = currentHp;
    }
}
