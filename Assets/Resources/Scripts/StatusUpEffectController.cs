using UnityEngine;
using UnityEngine.UI;

public class StatusUpEffectController : MonoBehaviour
{
    [SerializeField] Image[] statusUpEffectMain;//エフェクトの親オブジェクト

    private float effectActiveTime = 0.0f;




    /// <summary>
    /// 毎回アクティブになると呼び出される
    /// </summary>
    private void OnEnable()
    {
        EffectActiveTimeReset();
    }

    private void FixedUpdate()
    {
        StatusUpEffectActiveOff();
    }

    /// <summary>
    /// オブジェクト非アクティブ時に呼び出される
    /// </summary>
    private void OnDisable()
    {
        this.gameObject.SetActive(false); 
    }

    /// <summary>
    /// エフェクトアクティブタイムをリセット
    /// </summary>
    private void EffectActiveTimeReset()
    {
        effectActiveTime = 0.0f;
    }

    /// <summary>
    /// 2秒を過ぎたらエフェクトを非表示にする
    /// </summary>
    private void StatusUpEffectActiveOff()
    {
        effectActiveTime += Time.deltaTime;

        if (effectActiveTime >= 2)
        {
            for (int i = 0; i < 2; i++)
            {
                this.gameObject.SetActive(false);
            }
        }

    }

    /// <summary>
    /// ムーブアップボタンが押されるとエフェクトカラーを変更し呼び出す
    /// </summary>
    public void MoveUpEffect()
    {
        this.gameObject.SetActive(false);
        EffectActiveTimeReset();
        ParticleSystem.MainModule particle_m = GetComponent<ParticleSystem>().main;

        particle_m.startColor = statusUpEffectMain[0].color;

        this.gameObject.SetActive(true);
    }

    /// <summary>
    /// スピードアップボタンが押されるとエフェクトカラーを変更し呼び出す
    /// </summary>
    public void SpeedUpEffect()
    {

        this.gameObject.SetActive(false);
        EffectActiveTimeReset();
        ParticleSystem.MainModule particle_m = GetComponent<ParticleSystem>().main;

        particle_m.startColor = statusUpEffectMain[1].color;

        this.gameObject.SetActive(true);
    }

    /// <summary>
    /// パワーアップボタンが押されるとエフェクトカラーを変更し呼び出す
    /// </summary>
    public void PowerUpEffect()
    {

        this.gameObject.SetActive(false);
        EffectActiveTimeReset();
        ParticleSystem.MainModule particle_m = GetComponent<ParticleSystem>().main;

        particle_m.startColor = statusUpEffectMain[2].color;

        this.gameObject.SetActive(true);
    }

    /// <summary>
    /// チャージ1アップボタンが押されるとエフェクトカラーを変更し呼び出す
    /// </summary>
    public void Charge1UpEffect()
    {

        this.gameObject.SetActive(false);
        EffectActiveTimeReset();
        ParticleSystem.MainModule particle_m = GetComponent<ParticleSystem>().main;

        particle_m.startColor = statusUpEffectMain[3].color;

        this.gameObject.SetActive(true);
    }

    /// <summary>
    /// チャージ2アップボタンが押されるとエフェクトカラーを変更し呼び出す
    /// </summary>
    public void Charge2UpEffect()
    {

        this.gameObject.SetActive(false);
        EffectActiveTimeReset();
        ParticleSystem.MainModule particle_m = GetComponent<ParticleSystem>().main;

        particle_m.startColor = statusUpEffectMain[4].color;

        this.gameObject.SetActive(true);
    }

    /// <summary>
    /// チャージ3アップボタンが押されるとエフェクトカラーを変更し呼び出す
    /// </summary>
    public void Charge3UpEffect()
    {

        this.gameObject.SetActive(false);
        EffectActiveTimeReset();
        ParticleSystem.MainModule particle_m = GetComponent<ParticleSystem>().main;

        particle_m.startColor = statusUpEffectMain[5].color;

        this.gameObject.SetActive(true);
    }
}
