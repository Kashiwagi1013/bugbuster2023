<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Messageboard Model
 *
 * @method \App\Model\Entity\Messageboard newEmptyEntity()
 * @method \App\Model\Entity\Messageboard newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Messageboard[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Messageboard get($primaryKey, $options = [])
 * @method \App\Model\Entity\Messageboard findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Messageboard patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Messageboard[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Messageboard|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Messageboard saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Messageboard[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Messageboard[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Messageboard[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Messageboard[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MessageboardTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('messageboard');
        $this->setDisplayField('Id');
        $this->setPrimaryKey('Id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('Id')
            ->allowEmptyString('Id', null, 'create');

        $validator
            ->scalar('Name')
            ->maxLength('Name', 30)
            ->requirePresence('Name', 'create')
            ->notEmptyString('Name');

        $validator
            ->scalar('Message')
            ->maxLength('Message', 128)
            ->requirePresence('Message', 'create')
            ->notEmptyString('Message');

        $validator
            ->dateTime('Data')
            ->notEmptyDateTime('Data');

        return $validator;
    }
}
