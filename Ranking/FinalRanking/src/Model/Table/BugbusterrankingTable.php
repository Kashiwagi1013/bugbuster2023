<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bugbusterranking Model
 *
 * @method \App\Model\Entity\Bugbusterranking newEmptyEntity()
 * @method \App\Model\Entity\Bugbusterranking newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Bugbusterranking[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Bugbusterranking get($primaryKey, $options = [])
 * @method \App\Model\Entity\Bugbusterranking findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Bugbusterranking patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Bugbusterranking[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Bugbusterranking|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bugbusterranking saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bugbusterranking[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Bugbusterranking[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Bugbusterranking[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Bugbusterranking[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class BugbusterrankingTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('bugbusterranking');
        $this->setDisplayField('No');
        $this->setPrimaryKey('No');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('Name')
            ->maxLength('Name', 30)
            ->requirePresence('Name', 'create')
            ->notEmptyString('Name');

        $validator
            ->scalar('Time')
            ->maxLength('Time', 30)
            ->requirePresence('Time', 'create')
            ->notEmptyString('Time');

        $validator
            ->dateTime('Date')
            ->notEmptyDateTime('Date');

        $validator
            ->integer('No')
            ->allowEmptyString('No', null, 'create');

        return $validator;
    }
}
