<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Bugbusterranking Controller
 *
 * @property \App\Model\Table\BugbusterrankingTable $Bugbusterranking
 * @method \App\Model\Entity\Bugbusterranking[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BugbusterrankingController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $bugbusterranking = $this->paginate($this->Bugbusterranking);

        $this->set(compact('bugbusterranking'));
    }

    /**
     * View method
     *
     * @param string|null $id Bugbusterranking id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bugbusterranking = $this->Bugbusterranking->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('bugbusterranking'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bugbusterranking = $this->Bugbusterranking->newEmptyEntity();
        if ($this->request->is('post')) {
            $bugbusterranking = $this->Bugbusterranking->patchEntity($bugbusterranking, $this->request->getData());
            if ($this->Bugbusterranking->save($bugbusterranking)) {
                $this->Flash->success(__('The bugbusterranking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bugbusterranking could not be saved. Please, try again.'));
        }
        $this->set(compact('bugbusterranking'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Bugbusterranking id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bugbusterranking = $this->Bugbusterranking->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bugbusterranking = $this->Bugbusterranking->patchEntity($bugbusterranking, $this->request->getData());
            if ($this->Bugbusterranking->save($bugbusterranking)) {
                $this->Flash->success(__('The bugbusterranking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bugbusterranking could not be saved. Please, try again.'));
        }
        $this->set(compact('bugbusterranking'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Bugbusterranking id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bugbusterranking = $this->Bugbusterranking->get($id);
        if ($this->Bugbusterranking->delete($bugbusterranking)) {
            $this->Flash->success(__('The bugbusterranking has been deleted.'));
        } else {
            $this->Flash->error(__('The bugbusterranking could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
     /*
    リクエストURL
    http://localhost/rankproject/Ranktable/getRankings
    リクエストパラメータ
    無し
    レスポンスコード
    ランキングデータリスト
    [{"id":2,"name":"\u7530\u4e2d","score":200,"date":"2019-03-20T18:47:48+00:00"},{"id":1,"name":"\u4f50\u85e4","score":100,"date":"2019-03-20T18:47:48+00:00"}]
    */
	//ランキングデータリストを取得する。
	public function getRanking()
	{
		$this->autoRender	= false;
		
		//テーブルからランキングリストをとってくる
        $query	= $this->Bugbusterranking->find("all");

        //クエリー処理を行う。
        $query->order(['Time'=>'ASC']);   //降順
        $query->limit(3);                  //取得件数を3件までに絞る
		
		//jsonにシリアライズする。
		$json	= json_encode($query);

		//jsonデータを返す。（レスポンスとして表示する。）
		echo $json;
    }

    /*
    リクエストURL
    http://localhost/rankproject/Ranktable/setRanking
    リクエストパラメータ
    name  varchar
    score int
    レスポンスコード
    0:失敗
    1:成功
    */
    //ランキングデータ単体をセットする。
	public function setRanking()
	{
        error_log("setRanking()");
        
		$this->autoRender	= false;


        // POSTデータの受け取り方
		// name,message をPOSTで受け取る。
        
		// $postName		= "";
		// if( isset( $this->getRequest()->data['Name'] ) ){
		// 	$postName	= $this->getRequest()->data['Name'];
		// 	error_log($postName);
		// }
		// $postTime	= "";
		// if( isset( $this->getRequest()->data['Time'] ) ){
		// 	$postTime	= $this->getRequest()->data['Time'];
		// 	error_log($postTime);
		// }


        //POST パラメータを取得
        $postName   = $this->request->getData("Name");
        $postTime  = $this->request->getData("Time");

        //テーブルに追加するレコードを作る
        // $record = array(
        //     "name"=>"鈴木",
        //     "score"=>150,
        //     "date"=>date("Y/m/d H:i:s")
        // );
        $record = array(

            "Name"=>$postName,
            "Time"=>$postTime,
            "Date"=>date("Y/m/d H:i:s")
        );

        //テーブルにレコードを追加
        $bugbusterranking    = $this->Bugbusterranking->newEmptyEntity();
        $bugbusterranking    = $this->Bugbusterranking->patchEntity($bugbusterranking,$record);
        if( $this->Bugbusterranking->save($bugbusterranking) ){

            echo "success";   //成功
        }else{
            echo "failed";   //失敗
        }
	}
}
