<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bugbusterranking $bugbusterranking
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Bugbusterranking'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="bugbusterranking form content">
            <?= $this->Form->create($bugbusterranking) ?>
            <fieldset>
                <legend><?= __('Add Bugbusterranking') ?></legend>
                <?php
                    echo $this->Form->control('Name');
                    echo $this->Form->control('Time');
                    echo $this->Form->control('Date');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
