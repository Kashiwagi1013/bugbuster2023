<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bugbusterranking $bugbusterranking
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Bugbusterranking'), ['action' => 'edit', $bugbusterranking->No], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Bugbusterranking'), ['action' => 'delete', $bugbusterranking->No], ['confirm' => __('Are you sure you want to delete # {0}?', $bugbusterranking->No), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Bugbusterranking'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Bugbusterranking'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="bugbusterranking view content">
            <h3><?= h($bugbusterranking->No) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($bugbusterranking->Name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Time') ?></th>
                    <td><?= h($bugbusterranking->Time) ?></td>
                </tr>
                <tr>
                    <th><?= __('No') ?></th>
                    <td><?= $this->Number->format($bugbusterranking->No) ?></td>
                </tr>
                <tr>
                    <th><?= __('Date') ?></th>
                    <td><?= h($bugbusterranking->Date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
