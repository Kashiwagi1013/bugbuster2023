<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bugbusterranking[]|\Cake\Collection\CollectionInterface $bugbusterranking
 */
?>
<div class="bugbusterranking index content">
    <?= $this->Html->link(__('New Bugbusterranking'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Bugbusterranking') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('Name') ?></th>
                    <th><?= $this->Paginator->sort('Time') ?></th>
                    <th><?= $this->Paginator->sort('Date') ?></th>
                    <th><?= $this->Paginator->sort('No') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($bugbusterranking as $bugbusterranking): ?>
                <tr>
                    <td><?= h($bugbusterranking->Name) ?></td>
                    <td><?= h($bugbusterranking->Time) ?></td>
                    <td><?= h($bugbusterranking->Date) ?></td>
                    <td><?= $this->Number->format($bugbusterranking->No) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $bugbusterranking->No]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $bugbusterranking->No]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bugbusterranking->No], ['confirm' => __('Are you sure you want to delete # {0}?', $bugbusterranking->No)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
