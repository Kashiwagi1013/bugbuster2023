<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bugbusterranking $bugbusterranking
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $bugbusterranking->No],
                ['confirm' => __('Are you sure you want to delete # {0}?', $bugbusterranking->No), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Bugbusterranking'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="bugbusterranking form content">
            <?= $this->Form->create($bugbusterranking) ?>
            <fieldset>
                <legend><?= __('Edit Bugbusterranking') ?></legend>
                <?php
                    echo $this->Form->control('Name');
                    echo $this->Form->control('Time');
                    echo $this->Form->control('Date');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
