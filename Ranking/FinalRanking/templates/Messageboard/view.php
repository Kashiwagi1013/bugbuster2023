<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Messageboard $messageboard
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Messageboard'), ['action' => 'edit', $messageboard->Id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Messageboard'), ['action' => 'delete', $messageboard->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $messageboard->Id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Messageboard'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Messageboard'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="messageboard view content">
            <h3><?= h($messageboard->Id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($messageboard->Name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Message') ?></th>
                    <td><?= h($messageboard->Message) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($messageboard->Id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Data') ?></th>
                    <td><?= h($messageboard->Data) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
