<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BugbusterrankingFixture
 */
class BugbusterrankingFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'bugbusterranking';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'Name' => 'Lorem ipsum dolor sit amet',
                'Time' => 'Lorem ipsum dolor sit amet',
                'Date' => 1648993558,
                'No' => 1,
            ],
        ];
        parent::init();
    }
}
