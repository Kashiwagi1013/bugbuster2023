<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MessageboardFixture
 */
class MessageboardFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'messageboard';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'Id' => 1,
                'Name' => 'Lorem ipsum dolor sit amet',
                'Message' => 'Lorem ipsum dolor sit amet',
                'Data' => 1648951635,
            ],
        ];
        parent::init();
    }
}
