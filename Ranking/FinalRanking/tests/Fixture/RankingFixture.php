<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RankingFixture
 */
class RankingFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'ranking';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'Name' => 'Lorem ipsum dolor sit amet',
                'Time' => 1,
                'Date' => 1648888811,
            ],
        ];
        parent::init();
    }
}
