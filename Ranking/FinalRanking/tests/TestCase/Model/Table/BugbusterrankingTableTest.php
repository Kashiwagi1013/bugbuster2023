<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BugbusterrankingTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BugbusterrankingTable Test Case
 */
class BugbusterrankingTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\BugbusterrankingTable
     */
    protected $Bugbusterranking;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Bugbusterranking',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Bugbusterranking') ? [] : ['className' => BugbusterrankingTable::class];
        $this->Bugbusterranking = $this->getTableLocator()->get('Bugbusterranking', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Bugbusterranking);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\BugbusterrankingTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
