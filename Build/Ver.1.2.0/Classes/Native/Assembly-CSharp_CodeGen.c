﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* ExecuteInvoke_HandlerCallback_mE2BC7E5E60255E8A4AC9D08FA99C7F91A67B697B_RuntimeMethod_var;



// 0x00000001 System.Void JoystickPlayerExample::FixedUpdate()
extern void JoystickPlayerExample_FixedUpdate_m9AEDBA111F95D67A006A5D3821956048224541B7 (void);
// 0x00000002 System.Void JoystickPlayerExample::.ctor()
extern void JoystickPlayerExample__ctor_m702422E0AE29402330CF41FDDBEE76F0506342E2 (void);
// 0x00000003 System.Void JoystickSetterExample::ModeChanged(System.Int32)
extern void JoystickSetterExample_ModeChanged_m35AF30EE3E6C8CEBF064A7AB80F5795E9AF06D23 (void);
// 0x00000004 System.Void JoystickSetterExample::AxisChanged(System.Int32)
extern void JoystickSetterExample_AxisChanged_m5CA220FEA14E06BD8A445E31C5B66E4601C5E404 (void);
// 0x00000005 System.Void JoystickSetterExample::SnapX(System.Boolean)
extern void JoystickSetterExample_SnapX_m25A77C3DE4C6DBBE3B4A58E2DE8CD44B1773D6A1 (void);
// 0x00000006 System.Void JoystickSetterExample::SnapY(System.Boolean)
extern void JoystickSetterExample_SnapY_m54FE8DCB2CE8D2BF5D2CDF84C68DE263F0E25B1B (void);
// 0x00000007 System.Void JoystickSetterExample::Update()
extern void JoystickSetterExample_Update_m99B2432D22FE669B4DC3209696AD4A62096C7D41 (void);
// 0x00000008 System.Void JoystickSetterExample::.ctor()
extern void JoystickSetterExample__ctor_m2A3D66E05BCDF78D0F116348094717BEBA73EC91 (void);
// 0x00000009 System.Single Joystick::get_Horizontal()
extern void Joystick_get_Horizontal_m78CF4472B86063E54254AC8AE0A52126E4008AFA (void);
// 0x0000000A System.Single Joystick::get_Vertical()
extern void Joystick_get_Vertical_mA2B0917896CF9CE47A6D342D1734E43441C3D4BE (void);
// 0x0000000B UnityEngine.Vector2 Joystick::get_Direction()
extern void Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC (void);
// 0x0000000C System.Single Joystick::get_HandleRange()
extern void Joystick_get_HandleRange_mB38F0D3B6ADE2D1557D7A3D6759C469F17509D77 (void);
// 0x0000000D System.Void Joystick::set_HandleRange(System.Single)
extern void Joystick_set_HandleRange_m686B579A1F02EFCD4878BEA27EA606FC23CD2505 (void);
// 0x0000000E System.Single Joystick::get_DeadZone()
extern void Joystick_get_DeadZone_mCE52B635A8CF24F6DD867C14E34094515DE6AEFC (void);
// 0x0000000F System.Void Joystick::set_DeadZone(System.Single)
extern void Joystick_set_DeadZone_mD5699A14E5284026F303C8AF8D3457DFA9920F19 (void);
// 0x00000010 AxisOptions Joystick::get_AxisOptions()
extern void Joystick_get_AxisOptions_mA74F5FEE31C158E5179F0B108272ED28A661E388 (void);
// 0x00000011 System.Void Joystick::set_AxisOptions(AxisOptions)
extern void Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6 (void);
// 0x00000012 System.Boolean Joystick::get_SnapX()
extern void Joystick_get_SnapX_m51CAFDCC399606BA82986908700AAA45668A0F40 (void);
// 0x00000013 System.Void Joystick::set_SnapX(System.Boolean)
extern void Joystick_set_SnapX_mB2090989F6AC933B30823751D74E799FC8D9B87A (void);
// 0x00000014 System.Boolean Joystick::get_SnapY()
extern void Joystick_get_SnapY_m35AFC1AD1DF5EDE5FCE8BAFEBE91AD51D7451613 (void);
// 0x00000015 System.Void Joystick::set_SnapY(System.Boolean)
extern void Joystick_set_SnapY_m7419D5EB972285A9E5E446CD668BEC266D11CD86 (void);
// 0x00000016 System.Void Joystick::Start()
extern void Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C (void);
// 0x00000017 System.Void Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6 (void);
// 0x00000018 System.Void Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnDrag_m39E69636AEDC0E471EAD1371A99F4694ECDBA1E9 (void);
// 0x00000019 System.Void Joystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void Joystick_HandleInput_m15A4E86369A1AF0A4A5727DEC0FD93212A99901C (void);
// 0x0000001A System.Void Joystick::FormatInput()
extern void Joystick_FormatInput_mDDF7AF40138CF227F0106811C8749180FBF45342 (void);
// 0x0000001B System.Single Joystick::SnapFloat(System.Single,AxisOptions)
extern void Joystick_SnapFloat_mADE5AF21C67A2298A08CD12F9A8ED73AFA866987 (void);
// 0x0000001C System.Void Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C (void);
// 0x0000001D UnityEngine.Vector2 Joystick::ScreenPointToAnchoredPosition(UnityEngine.Vector2)
extern void Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024 (void);
// 0x0000001E System.Void Joystick::.ctor()
extern void Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B (void);
// 0x0000001F System.Single DynamicJoystick::get_MoveThreshold()
extern void DynamicJoystick_get_MoveThreshold_m16C670C1DA0A45E83F6F87C4304F459EDDEEDD5A (void);
// 0x00000020 System.Void DynamicJoystick::set_MoveThreshold(System.Single)
extern void DynamicJoystick_set_MoveThreshold_m000C1AE325C0B9C33172E4202F2AFB59820517F9 (void);
// 0x00000021 System.Void DynamicJoystick::Start()
extern void DynamicJoystick_Start_mFE16C6CE0B753F08E79A2AEC75782DEEF3B96F72 (void);
// 0x00000022 System.Void DynamicJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void DynamicJoystick_OnPointerDown_mBFA3026A0DA4A6B53C0E747A1E892CBC7F43E136 (void);
// 0x00000023 System.Void DynamicJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void DynamicJoystick_OnPointerUp_m10389907A9D3362F6B75FDC5F35AF37A5DD5AE7C (void);
// 0x00000024 System.Void DynamicJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void DynamicJoystick_HandleInput_m3F157F4825BE6682228C8E135581C6404D268506 (void);
// 0x00000025 System.Void DynamicJoystick::.ctor()
extern void DynamicJoystick__ctor_m9DDA6263314BD7B97679DF14A4664358BD3E58CB (void);
// 0x00000026 System.Void FixedJoystick::.ctor()
extern void FixedJoystick__ctor_m8C8BB74E5EA8CA2C3DD2AE084301EC91F519AD24 (void);
// 0x00000027 System.Void FloatingJoystick::Start()
extern void FloatingJoystick_Start_mB22059CD82AF77A8F94AC72E81A8BAE969399E81 (void);
// 0x00000028 System.Void FloatingJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void FloatingJoystick_OnPointerDown_mFE5B4F54D5BBCA72F9729AB64765F558FA5C7A54 (void);
// 0x00000029 System.Void FloatingJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void FloatingJoystick_OnPointerUp_m80ABA9C914E1953F91DBF74853CE84879352280D (void);
// 0x0000002A System.Void FloatingJoystick::.ctor()
extern void FloatingJoystick__ctor_m6B72425996D46B025F9E9D22121E9D01BEC6BD3C (void);
// 0x0000002B System.Single VariableJoystick::get_MoveThreshold()
extern void VariableJoystick_get_MoveThreshold_m8C9D3A63DB3B6CF1F0139C3504EC2E7AC4E7CF99 (void);
// 0x0000002C System.Void VariableJoystick::set_MoveThreshold(System.Single)
extern void VariableJoystick_set_MoveThreshold_m23DC4187B405EB690D297379E2113568B40C705A (void);
// 0x0000002D System.Void VariableJoystick::SetMode(JoystickType)
extern void VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE (void);
// 0x0000002E System.Void VariableJoystick::Start()
extern void VariableJoystick_Start_m21743760641EA0317ACCD95949B99825446FE74D (void);
// 0x0000002F System.Void VariableJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void VariableJoystick_OnPointerDown_m8ABE5C8EFBC8DB3A2EE135FFF3C0F67C533AF4B5 (void);
// 0x00000030 System.Void VariableJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void VariableJoystick_OnPointerUp_m65296D82A6C2E1BDC2D622B9C922FAE8E4544526 (void);
// 0x00000031 System.Void VariableJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void VariableJoystick_HandleInput_mD1BCF9A068737A9C057EE8CEB7E6DEB682CC03AB (void);
// 0x00000032 System.Void VariableJoystick::.ctor()
extern void VariableJoystick__ctor_m6C7B41973BE20A94F16DB5DCC9AA805C5D8DF852 (void);
// 0x00000033 System.Void ActiveSliderHandleAnimation::Start()
extern void ActiveSliderHandleAnimation_Start_mB740F795BF77D8902D11EB82EE795D33F2704E47 (void);
// 0x00000034 System.Void ActiveSliderHandleAnimation::HandleRotateAnimation()
extern void ActiveSliderHandleAnimation_HandleRotateAnimation_mB30864719A6BCE6E19F395B7F58D94ECFE708979 (void);
// 0x00000035 System.Void ActiveSliderHandleAnimation::HandleScaleAnimation()
extern void ActiveSliderHandleAnimation_HandleScaleAnimation_m7A1E89C592FA9483C1F1C777F3D5114668206CDA (void);
// 0x00000036 System.Void ActiveSliderHandleAnimation::.ctor()
extern void ActiveSliderHandleAnimation__ctor_m1E65A669B3A94950A2A09200D5153DDB1AFACE61 (void);
// 0x00000037 System.Void AttackChargeEffectController::FixedUpdate()
extern void AttackChargeEffectController_FixedUpdate_m062E03EDA04C50D5D9BB6304D9426637DFD2CE30 (void);
// 0x00000038 System.Void AttackChargeEffectController::OnDisable()
extern void AttackChargeEffectController_OnDisable_m8E241DF56DDC5C3267710F49A4E17CCB74CEAF3E (void);
// 0x00000039 System.Void AttackChargeEffectController::ChangeEffectColorChange()
extern void AttackChargeEffectController_ChangeEffectColorChange_mF264E9F92807203B998E096A858D921A05306F88 (void);
// 0x0000003A System.Void AttackChargeEffectController::ChargeAttackCountEfectColorChange()
extern void AttackChargeEffectController_ChargeAttackCountEfectColorChange_m00EA5B0A53D6DECB1773F0809F2565BD26B766E8 (void);
// 0x0000003B System.Void AttackChargeEffectController::PlayerChargeTimekReset()
extern void AttackChargeEffectController_PlayerChargeTimekReset_mB9A7690D4243D404B8D36A9B29C5C122FDE9C6D0 (void);
// 0x0000003C System.Void AttackChargeEffectController::ChangeEffectColorReset()
extern void AttackChargeEffectController_ChangeEffectColorReset_mE0D02E082BCED94424723E311AA4586B5DD23200 (void);
// 0x0000003D System.Void AttackChargeEffectController::ChangeEffectRateReset()
extern void AttackChargeEffectController_ChangeEffectRateReset_m0406F63E4757C9EC053DCE911115CA240986C70C (void);
// 0x0000003E System.Void AttackChargeEffectController::.ctor()
extern void AttackChargeEffectController__ctor_mBC60AAE1E9A758E1494FF6B07041DD2F3C762A26 (void);
// 0x0000003F System.Void AttackPointController::Awake()
extern void AttackPointController_Awake_mB5D9DDFD0B5773983469658C8395B4E2BC6C1ED3 (void);
// 0x00000040 System.Void AttackPointController::FixedUpdate()
extern void AttackPointController_FixedUpdate_mC2631CABA16F6C38A26798861C4E97388BF3D85B (void);
// 0x00000041 System.Void AttackPointController::PlayerSearch()
extern void AttackPointController_PlayerSearch_m09AC1F38193ECECE66377BBB087B6AD44DA87E32 (void);
// 0x00000042 System.Void AttackPointController::ShellPoolSearch()
extern void AttackPointController_ShellPoolSearch_m123DCC0E0E2C7AF4E37ACF5FAE0D96CE0B1D1BF1 (void);
// 0x00000043 System.Void AttackPointController::PlayerTargetLockOn()
extern void AttackPointController_PlayerTargetLockOn_m7FE99704FA08886746F227BCB74769B323C3FCEC (void);
// 0x00000044 System.Void AttackPointController::AttakPointRange()
extern void AttackPointController_AttakPointRange_mCC0F0DC69C3B4A48B9A19CF84F4E372AB29708F4 (void);
// 0x00000045 System.Void AttackPointController::BossShellInstantiate()
extern void AttackPointController_BossShellInstantiate_mE8430CF2CE38F6427778197AF40E8C2B2B8E65EE (void);
// 0x00000046 System.Void AttackPointController::ShellDestroy()
extern void AttackPointController_ShellDestroy_mD19C3C33BE7936D4142384900652D96FBD4C58FC (void);
// 0x00000047 UnityEngine.GameObject AttackPointController::GetBossShell()
extern void AttackPointController_GetBossShell_mFA725EBF88517B80CEDC7759A136FEB02AA7EBFC (void);
// 0x00000048 System.Void AttackPointController::BossAttack()
extern void AttackPointController_BossAttack_m83B4EF8C409AA62034A8C4D6B438E13E429795B2 (void);
// 0x00000049 System.Void AttackPointController::.ctor()
extern void AttackPointController__ctor_m749097EFADD67D3BF1E2AE10309E0144D7CD863E (void);
// 0x0000004A System.Void BossController::Awake()
extern void BossController_Awake_m5839437A38C7F49F5BC738F8869C3C167A0EBB7E (void);
// 0x0000004B System.Void BossController::FixedUpdate()
extern void BossController_FixedUpdate_mB7A8B8400438A551C04C0B24F2991A79E47B50BB (void);
// 0x0000004C System.Void BossController::OnMouseDown()
extern void BossController_OnMouseDown_m6BEC0F244A92BCD0B2A74AD90E360F1074AFBA41 (void);
// 0x0000004D System.Void BossController::OnTriggerEnter(UnityEngine.Collider)
extern void BossController_OnTriggerEnter_m6BA80DA8F2CCD89B6C4EB7DC80EF3FEA348DF319 (void);
// 0x0000004E System.Void BossController::OnTriggerStay(UnityEngine.Collider)
extern void BossController_OnTriggerStay_m631031F4D748345016FFE3E5AA60163D2447EAC9 (void);
// 0x0000004F System.Void BossController::OnMouseDownBoolOnInvoke()
extern void BossController_OnMouseDownBoolOnInvoke_mDCD769582A809A09C8CA9AE81BCCAE183908F078 (void);
// 0x00000050 System.Void BossController::BossAnimation()
extern void BossController_BossAnimation_mA3F5935BAF461DC8BB1A82867C5E6133804D4B48 (void);
// 0x00000051 System.Void BossController::PlayerSearch()
extern void BossController_PlayerSearch_m7EF01C131B8601AE319A26A69E81E5FF1D342DCB (void);
// 0x00000052 System.Void BossController::PlayerAttackPointSearch()
extern void BossController_PlayerAttackPointSearch_m9210CAAF206AB47631637C49A12C2B32E9106531 (void);
// 0x00000053 System.Void BossController::EnemySearch()
extern void BossController_EnemySearch_mB7D016E0422E8AEBDF86E448EA7438F8FE010A86 (void);
// 0x00000054 System.Void BossController::E_BossSearch()
extern void BossController_E_BossSearch_mD19B456E801F56B9BDBA835F013B06E8C93A8FE3 (void);
// 0x00000055 System.Void BossController::ShellPoolSearch()
extern void BossController_ShellPoolSearch_m9BD2323F336FAA328DBB48C63A509003455E54E8 (void);
// 0x00000056 System.Void BossController::DamageEffectPoolSearch()
extern void BossController_DamageEffectPoolSearch_m0A5A76D6DC35D0DAFFF1DFF2630AD12E6DD3CCD1 (void);
// 0x00000057 System.Void BossController::GameManagerSearch()
extern void BossController_GameManagerSearch_mA4EFADDA270B01AE965E9DF330FF26705CA69F21 (void);
// 0x00000058 System.Void BossController::BossActionPattern()
extern void BossController_BossActionPattern_m9D1EA7D9288304FBAD544A938D366D5FBA3B7D0D (void);
// 0x00000059 System.Void BossController::BossActionPatternPlus()
extern void BossController_BossActionPatternPlus_m0365F7D98558DEF4A1638EBB91E6EF8147556364 (void);
// 0x0000005A System.Void BossController::DamageEffectInstantiate()
extern void BossController_DamageEffectInstantiate_m338FC7CA0B063034305253EBBC58DF1E36970782 (void);
// 0x0000005B System.Void BossController::DamageEffectDestroy()
extern void BossController_DamageEffectDestroy_m43CA8A135057A9F8C9F1F2E98E5A393D16CCDE50 (void);
// 0x0000005C System.Void BossController::DamageEffectDestroyBossGuard()
extern void BossController_DamageEffectDestroyBossGuard_mB5F746E7D3247FC22E693D2BD5E31E5E6B9CFF03 (void);
// 0x0000005D UnityEngine.GameObject BossController::GetDamageEffect()
extern void BossController_GetDamageEffect_m844ACC82742FF3EA3C188462C04D3FC7E219253D (void);
// 0x0000005E System.Void BossController::DamageEffectActive()
extern void BossController_DamageEffectActive_m32445C5FEA5A809C0F29747445DE3D690DDA3792 (void);
// 0x0000005F System.Void BossController::AttackPointActiveOn()
extern void BossController_AttackPointActiveOn_mA27B19FE5D544AF91AC5135F6FEF8AF4FBF88809 (void);
// 0x00000060 System.Void BossController::AttackPointActiveOff()
extern void BossController_AttackPointActiveOff_m59D635C40F0BBA42EE06B7586BE7355E8BBD55BF (void);
// 0x00000061 System.Void BossController::BossLineShellInstantiate()
extern void BossController_BossLineShellInstantiate_mCA1C2271D750E2EE5A8EB2624653D32EA3E5CBDC (void);
// 0x00000062 System.Void BossController::LineAttackRotate()
extern void BossController_LineAttackRotate_m49D83F89E5313D36B15EF9ED64B023EC4F9F245D (void);
// 0x00000063 System.Void BossController::BossLineShellDestroy()
extern void BossController_BossLineShellDestroy_m46B9368065F0F6953950F05DBB02447B447D490F (void);
// 0x00000064 System.Void BossController::PoolShellDestroy()
extern void BossController_PoolShellDestroy_m8DAC33558DEADA4BFE2A25A039928523FDAB0C16 (void);
// 0x00000065 UnityEngine.GameObject BossController::GetBossLineShell()
extern void BossController_GetBossLineShell_m391752E659A9FC38B211DA6A84AF4A4AD8FA45F0 (void);
// 0x00000066 System.Void BossController::BossLineAttackShellInstansLeft()
extern void BossController_BossLineAttackShellInstansLeft_m65F41C365FB0EDE9C5258A2944AEF9245E8FC27C (void);
// 0x00000067 System.Void BossController::BossLineAttackShotLeft()
extern void BossController_BossLineAttackShotLeft_mF37552156098CF243F7B2BAED6727B3B2702A3E2 (void);
// 0x00000068 System.Void BossController::BossLineAttackShellInstansRight()
extern void BossController_BossLineAttackShellInstansRight_mB75D06D09B244119A5F85DE080AA1928BC868133 (void);
// 0x00000069 System.Void BossController::BossLineAttackShotRight()
extern void BossController_BossLineAttackShotRight_m9B9D36069EC1E5440C2FD730A6D4EC24E84785ED (void);
// 0x0000006A System.Void BossController::ShotIntervalStandardUp()
extern void BossController_ShotIntervalStandardUp_mC6AC462A63368112B5547691868ABC47DF4D0F2E (void);
// 0x0000006B System.Void BossController::BossMove()
extern void BossController_BossMove_mC1769BA06503B2EBD5FDAEE531B7889A1C313F38 (void);
// 0x0000006C System.Void BossController::MovePositionRange()
extern void BossController_MovePositionRange_m1790C9E2B4C79BED17FE5127240793DAB38ED11A (void);
// 0x0000006D System.Void BossController::BossPositionReset()
extern void BossController_BossPositionReset_mE5B945F0FD29ACA0C4BE4DCCD622CA187D97547D (void);
// 0x0000006E System.Void BossController::BossGuardActiveOn()
extern void BossController_BossGuardActiveOn_m8528118E744E672A80E6E15E3B9CD7DC159C7408 (void);
// 0x0000006F System.Void BossController::BossDamageReduction()
extern void BossController_BossDamageReduction_mAF009E741B6721CF2A338B345F321CBE751D8C51 (void);
// 0x00000070 System.Void BossController::BossDamage1Reduction()
extern void BossController_BossDamage1Reduction_m39092D046FF75265D95F61B3FB0875EC7253A37C (void);
// 0x00000071 System.Void BossController::BossDamage2Reduction()
extern void BossController_BossDamage2Reduction_mE5FC58067797B3707D7570FD8FDA219091322F31 (void);
// 0x00000072 System.Void BossController::BossDamage3Reduction()
extern void BossController_BossDamage3Reduction_mD664FBB89BD852A02FD5D0C88D2CB38EF92EE9F8 (void);
// 0x00000073 System.Void BossController::BossGuradMoveSetting()
extern void BossController_BossGuradMoveSetting_mA877730399AE34C0C1389D3786C29CC0BA4230CB (void);
// 0x00000074 System.Void BossController::BossGuradMove()
extern void BossController_BossGuradMove_m390702D77667ADC242703EFB2B63D2386E5D10B0 (void);
// 0x00000075 System.Collections.IEnumerator BossController::BossGuradMoveCoroutine()
extern void BossController_BossGuradMoveCoroutine_m13EA63B4C08947F4A46F5A2736C4567C891B3F92 (void);
// 0x00000076 System.Void BossController::BossDamage()
extern void BossController_BossDamage_mC606A208B28E3EF0F09252AC04D971FD5668665A (void);
// 0x00000077 System.Void BossController::BossChargeDamage1()
extern void BossController_BossChargeDamage1_mF496D343C91FC56CD8E80B8AD4B93445136AB255 (void);
// 0x00000078 System.Void BossController::BossChargeDamage2()
extern void BossController_BossChargeDamage2_m85995E6DFAC3BFB29067B92503895635177E3032 (void);
// 0x00000079 System.Void BossController::BossChargeDamage3()
extern void BossController_BossChargeDamage3_mBA203E23059096D19924E487FE8D193A7F73019C (void);
// 0x0000007A System.Void BossController::PlayerEngineerPointPlusNomal()
extern void BossController_PlayerEngineerPointPlusNomal_m42C2F5C292D31C99A4ADA768BB5DAD2447E398A2 (void);
// 0x0000007B System.Void BossController::PlayerEngineerPointPlusCharge1()
extern void BossController_PlayerEngineerPointPlusCharge1_m1C856F9E46FAB43E480333FC4839BD0ABFD33A7D (void);
// 0x0000007C System.Void BossController::PlayerEngineerPointPlusCharge2()
extern void BossController_PlayerEngineerPointPlusCharge2_m3F89DE5572F59161F474A22FE129AD986D08EA94 (void);
// 0x0000007D System.Void BossController::PlayerEngineerPointPlusCharge3()
extern void BossController_PlayerEngineerPointPlusCharge3_m06DFF9BCAA9910C02B8FB8BBE630DC527DA03E48 (void);
// 0x0000007E System.Void BossController::BossTargetLockOnContinuation()
extern void BossController_BossTargetLockOnContinuation_m97193D6967E32C9D2BF0B75309E778F3A885E3AF (void);
// 0x0000007F System.Void BossController::BossHPSliderActive()
extern void BossController_BossHPSliderActive_m49E0A731943E2815366D91933C460DDC82E77D21 (void);
// 0x00000080 System.Void BossController::BossHPSliderActiveOff()
extern void BossController_BossHPSliderActiveOff_m50D6AAD8897ECCB08ED9DF5ECC7E521635AD120B (void);
// 0x00000081 System.Void BossController::BossGuardForwardHPSliderActiveOff()
extern void BossController_BossGuardForwardHPSliderActiveOff_mC0FAB83DE4625197DC068CEEBEEFC5756C835B92 (void);
// 0x00000082 System.Void BossController::BossGuardLeftHPSliderActiveOff()
extern void BossController_BossGuardLeftHPSliderActiveOff_m9BDD8F3B764E64C05E5B77FA926ABD69631EA5E8 (void);
// 0x00000083 System.Void BossController::BossGuardRightHPSliderActiveOff()
extern void BossController_BossGuardRightHPSliderActiveOff_mCF1416A7E7800D4725FAED9C382FD7931964B765 (void);
// 0x00000084 System.Void BossController::BossGuardBackHPSliderActiveOff()
extern void BossController_BossGuardBackHPSliderActiveOff_m8747ED90AB2D2ED2E9B74C40BF2B2E41CF30010D (void);
// 0x00000085 System.Void BossController::BossDestroyCallMethods()
extern void BossController_BossDestroyCallMethods_mB12F36F19168714D1A85A461F122E4EA19EEF481 (void);
// 0x00000086 System.Void BossController::DestroyAnimation()
extern void BossController_DestroyAnimation_m19476EED5F67B1564F8343F88FD6316BB299B763 (void);
// 0x00000087 System.Void BossController::BossHpOutDestroy()
extern void BossController_BossHpOutDestroy_m39761386CA0C9F3481D021FE04C0C6431F2A9457 (void);
// 0x00000088 System.Void BossController::BossMoveAreaDestroy()
extern void BossController_BossMoveAreaDestroy_m11BD7A25DB4BDD2D56C8448748F2C0B0F323BABA (void);
// 0x00000089 System.Void BossController::BossGuardSpawnEfect()
extern void BossController_BossGuardSpawnEfect_m465CE04EC585BA85F19082C2D56C59BB2505F9BC (void);
// 0x0000008A System.Void BossController::BreakDownEfectOn1()
extern void BossController_BreakDownEfectOn1_mB2BBBF0CAA061CF5E65037C3B24716A7A8B8DF0A (void);
// 0x0000008B System.Void BossController::BreakDownEfectOn2()
extern void BossController_BreakDownEfectOn2_m406CC2B9B876D227DED59D8F560A08D0D54AA48F (void);
// 0x0000008C System.Void BossController::BreakDownEfectOn3()
extern void BossController_BreakDownEfectOn3_mC2D8C065699718A02F3E6D37F1205DF92DC7D601 (void);
// 0x0000008D System.Void BossController::BossBattleBGM()
extern void BossController_BossBattleBGM_m9089C40D5FF7DA27979888863B4127C88797EDAA (void);
// 0x0000008E System.Void BossController::GameClearBGM()
extern void BossController_GameClearBGM_m3BD1DB2289F258D9FE75C864D7FC6DF3840653DD (void);
// 0x0000008F System.Void BossController::.ctor()
extern void BossController__ctor_mC4C9EEC35858E3E8D6475F9C844D40EF8F2E537A (void);
// 0x00000090 System.Void BossController/<BossGuradMoveCoroutine>d__100::.ctor(System.Int32)
extern void U3CBossGuradMoveCoroutineU3Ed__100__ctor_mBBA2244364FB04EB53AA709ACAD576BEA75C9760 (void);
// 0x00000091 System.Void BossController/<BossGuradMoveCoroutine>d__100::System.IDisposable.Dispose()
extern void U3CBossGuradMoveCoroutineU3Ed__100_System_IDisposable_Dispose_m39D7620484A1AB151FFF9779F3A7B820850545B2 (void);
// 0x00000092 System.Boolean BossController/<BossGuradMoveCoroutine>d__100::MoveNext()
extern void U3CBossGuradMoveCoroutineU3Ed__100_MoveNext_m1B85630DD2B9216756328CB453CFE10167B957DF (void);
// 0x00000093 System.Object BossController/<BossGuradMoveCoroutine>d__100::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBossGuradMoveCoroutineU3Ed__100_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C91675C1150931F6E4FB4AD893481F32F61C529 (void);
// 0x00000094 System.Void BossController/<BossGuradMoveCoroutine>d__100::System.Collections.IEnumerator.Reset()
extern void U3CBossGuradMoveCoroutineU3Ed__100_System_Collections_IEnumerator_Reset_mA65ED59A1D5BC835D4E113FF3C4DAABEBE658D7F (void);
// 0x00000095 System.Object BossController/<BossGuradMoveCoroutine>d__100::System.Collections.IEnumerator.get_Current()
extern void U3CBossGuradMoveCoroutineU3Ed__100_System_Collections_IEnumerator_get_Current_m73113E44C714D3338041C845FBE95CC808484380 (void);
// 0x00000096 System.Void BossGuardController::Awake()
extern void BossGuardController_Awake_mB1E0540C3E6290F1E1A26AD9962F1850FC100863 (void);
// 0x00000097 System.Void BossGuardController::FixedUpdate()
extern void BossGuardController_FixedUpdate_mC606DBB08AC874EE0E4F41F61C68AC4B90CAF67A (void);
// 0x00000098 System.Void BossGuardController::OnEnable()
extern void BossGuardController_OnEnable_mC1CDBAB9D9BCE1A197EE91AD352059A7BB11536B (void);
// 0x00000099 System.Void BossGuardController::OnMouseDown()
extern void BossGuardController_OnMouseDown_m57EAF87A12DEBC39D257F49C1C1C243CA412377E (void);
// 0x0000009A System.Void BossGuardController::OnTriggerEnter(UnityEngine.Collider)
extern void BossGuardController_OnTriggerEnter_m5F1B37C80450292B1CE336E3212AF293A303BDB3 (void);
// 0x0000009B System.Void BossGuardController::OnTriggerStay(UnityEngine.Collider)
extern void BossGuardController_OnTriggerStay_mC76E7D005700497A1D68ED4CB12FC1B73ED16D32 (void);
// 0x0000009C System.Void BossGuardController::BossGuardCountPlus()
extern void BossGuardController_BossGuardCountPlus_m1F444AF1DA9D3B6E71A68EB470F21FC4096C2F75 (void);
// 0x0000009D System.Void BossGuardController::BossGuardCountPlusOpposite()
extern void BossGuardController_BossGuardCountPlusOpposite_m52EA70E9707B47E538F49016CC35C4FC8FC2467C (void);
// 0x0000009E System.Void BossGuardController::BossGuradRotateSetting()
extern void BossGuardController_BossGuradRotateSetting_m0383D28C793A43D59F3E2EFAA63103460DB5045E (void);
// 0x0000009F System.Void BossGuardController::PlayerSearch()
extern void BossGuardController_PlayerSearch_mA6CED10F916A1BC4EC3ADFBEE1E673BEBBBFB59D (void);
// 0x000000A0 System.Void BossGuardController::PlayerAttackPointSearch()
extern void BossGuardController_PlayerAttackPointSearch_mF850A3F187BCC41ADDF4ED1E61BC10D69C06A1DA (void);
// 0x000000A1 System.Void BossGuardController::DamageEffectPoolSearch()
extern void BossGuardController_DamageEffectPoolSearch_m0BE46A39D46C2D5CA59652F10C7FA8842E03444D (void);
// 0x000000A2 System.Void BossGuardController::AttackPointPoolShellDestroy()
extern void BossGuardController_AttackPointPoolShellDestroy_mF1BA60CF3012A0E850D504793097DEADE0AFECD7 (void);
// 0x000000A3 System.Void BossGuardController::DamageEffectInstantiate()
extern void BossGuardController_DamageEffectInstantiate_m0209A04D6DC6AEE16BF41297E2A854849766B8BC (void);
// 0x000000A4 System.Void BossGuardController::DamageEffectDestroy()
extern void BossGuardController_DamageEffectDestroy_m0A9B1F08A6426E4866E1BBEEE5F9F71EDED7AB01 (void);
// 0x000000A5 UnityEngine.GameObject BossGuardController::GetDamageEffect()
extern void BossGuardController_GetDamageEffect_m2C62676E171A835A9F2DB4FE8CCAC2A3DEC607EA (void);
// 0x000000A6 System.Void BossGuardController::DamageEffectActive()
extern void BossGuardController_DamageEffectActive_mF48D741CB5F2C06E6C03AAE9FFE5BFCBFEA976D9 (void);
// 0x000000A7 System.Void BossGuardController::BossGuardHPSliderActive()
extern void BossGuardController_BossGuardHPSliderActive_mA0DD1510B16DDA92B659108903201EEE65A6570C (void);
// 0x000000A8 System.Void BossGuardController::BossGuardHPSliderActiveOff()
extern void BossGuardController_BossGuardHPSliderActiveOff_m797EE816325D2CA58283A9F82D42CEEF222B43CA (void);
// 0x000000A9 System.Void BossGuardController::BossGuardHpReset()
extern void BossGuardController_BossGuardHpReset_m34E56A8455CF65A0D27B509BE89A66EE6E1337C4 (void);
// 0x000000AA System.Void BossGuardController::BossGuardForwardHPSliderActiveOn()
extern void BossGuardController_BossGuardForwardHPSliderActiveOn_m0DB5999CE1F841EF5F51A2BCC1D2668BDB4DB6D2 (void);
// 0x000000AB System.Void BossGuardController::BossGuardLeftHPSliderActiveOn()
extern void BossGuardController_BossGuardLeftHPSliderActiveOn_m9E1D57C276E8FEE47AC4E34B987A14D678B5E355 (void);
// 0x000000AC System.Void BossGuardController::BossGuardRightHPSliderActiveOn()
extern void BossGuardController_BossGuardRightHPSliderActiveOn_m9C122AB7F84850352AEAC7F55E63FD711CA99B97 (void);
// 0x000000AD System.Void BossGuardController::BossGuardBackHPSliderActiveOn()
extern void BossGuardController_BossGuardBackHPSliderActiveOn_mA8DF937037AB3440072D56676741E6214EBD695D (void);
// 0x000000AE System.Void BossGuardController::AttackPointActiveOnInvoke()
extern void BossGuardController_AttackPointActiveOnInvoke_m4F7ECB32EAF30691A194E2CF7120A03E353B6E71 (void);
// 0x000000AF System.Void BossGuardController::BossGuardDamage()
extern void BossGuardController_BossGuardDamage_m09FCC6AA24BB651152C7FFA5EE1B4A99EF5EDFDC (void);
// 0x000000B0 System.Void BossGuardController::BossGuardChargeDamage1()
extern void BossGuardController_BossGuardChargeDamage1_m40643166383EBD0B0E0B51C6A69F848278ACB025 (void);
// 0x000000B1 System.Void BossGuardController::BossGuardChargeDamage2()
extern void BossGuardController_BossGuardChargeDamage2_m6778BD4F2710F63F3137BBED77FF1708E95E70A8 (void);
// 0x000000B2 System.Void BossGuardController::BossGuardChargeDamage3()
extern void BossGuardController_BossGuardChargeDamage3_mBBADA47D125EF6625314EBA41EF195A503C2F0D8 (void);
// 0x000000B3 System.Void BossGuardController::PlayerEngineerPointPlusNomal()
extern void BossGuardController_PlayerEngineerPointPlusNomal_m743A874D97920F7B203380F3213B8029895EBC6B (void);
// 0x000000B4 System.Void BossGuardController::PlayerEngineerPointPlusCharge1()
extern void BossGuardController_PlayerEngineerPointPlusCharge1_m17DDF6E337177657E5DF712F71240400D1BA10B8 (void);
// 0x000000B5 System.Void BossGuardController::PlayerEngineerPointPlusCharge2()
extern void BossGuardController_PlayerEngineerPointPlusCharge2_m4515D565D4360C12729B3A05EC796D4AEAF1CB90 (void);
// 0x000000B6 System.Void BossGuardController::PlayerEngineerPointPlusCharge3()
extern void BossGuardController_PlayerEngineerPointPlusCharge3_mF8F51D1B03DD24B76E3DE96871B44EA66A48E723 (void);
// 0x000000B7 System.Void BossGuardController::GuardTargetLockOnContinuation()
extern void BossGuardController_GuardTargetLockOnContinuation_m6E0C8C613A25BF36186F91E93630898DF9B1A73E (void);
// 0x000000B8 System.Void BossGuardController::BossGuardDestroyCallMethods()
extern void BossGuardController_BossGuardDestroyCallMethods_mA3E408A6CE8F1CE687607A9F8128AAF627B1B185 (void);
// 0x000000B9 System.Void BossGuardController::BossGuardHpOutDestroy()
extern void BossGuardController_BossGuardHpOutDestroy_m0A7257F70DD17AFA9EE7F82911ABC7B9823F0839 (void);
// 0x000000BA System.Void BossGuardController::.ctor()
extern void BossGuardController__ctor_mDB641834D7A6F9BEAB7F299493642B0D6AE6F679 (void);
// 0x000000BB System.Void BossGuardHelthBar::Start()
extern void BossGuardHelthBar_Start_m52C94875FD62FBA9DAF7187495DBBBAFA0465971 (void);
// 0x000000BC System.Void BossGuardHelthBar::FixedUpdate()
extern void BossGuardHelthBar_FixedUpdate_m64D0EADF76836AE3064FE0E4DF51C998F565A2E7 (void);
// 0x000000BD System.Void BossGuardHelthBar::BossCurrentHP()
extern void BossGuardHelthBar_BossCurrentHP_m60E8832BB98B13289164AF7EE4A6430BCC780941 (void);
// 0x000000BE System.Void BossGuardHelthBar::.ctor()
extern void BossGuardHelthBar__ctor_m468760944D7C6417B178FD32FC91C0EFDADAE242 (void);
// 0x000000BF System.Void BossHelthBar::Start()
extern void BossHelthBar_Start_m0C57CD9FC9426A47476E1D667BE1599E3FC7F831 (void);
// 0x000000C0 System.Void BossHelthBar::FixedUpdate()
extern void BossHelthBar_FixedUpdate_mE5734482BC3BF8A66814F3150BB09B73D5D5886A (void);
// 0x000000C1 System.Void BossHelthBar::BossCurrentHP()
extern void BossHelthBar_BossCurrentHP_m53F938597D2D7151EC5E870E2067C074500408B9 (void);
// 0x000000C2 System.Void BossHelthBar::.ctor()
extern void BossHelthBar__ctor_m47C768945400507663A42C8DA447F89B32361C80 (void);
// 0x000000C3 System.Void BossLineAttackShellController::Awake()
extern void BossLineAttackShellController_Awake_mEB2824738FF286568326FDC770851B56525FA1C6 (void);
// 0x000000C4 System.Void BossLineAttackShellController::OnEnable()
extern void BossLineAttackShellController_OnEnable_mA7A3BECC11137B0B2E10F37F1DC9B6BBA8BEFC93 (void);
// 0x000000C5 System.Void BossLineAttackShellController::FixedUpdate()
extern void BossLineAttackShellController_FixedUpdate_m4ECFC7CA6E1DA4AD9781B7168CB47F2F42A42758 (void);
// 0x000000C6 System.Void BossLineAttackShellController::OnDisable()
extern void BossLineAttackShellController_OnDisable_mEA11B520AF6009E3CE5FEDC5E27A797BBE0FE700 (void);
// 0x000000C7 System.Void BossLineAttackShellController::VelocityReset()
extern void BossLineAttackShellController_VelocityReset_m69B5EA1A4866542643746A981C2FDE1DD231EB18 (void);
// 0x000000C8 System.Void BossLineAttackShellController::LineAttackShellShot()
extern void BossLineAttackShellController_LineAttackShellShot_mE3462AA4D516607FABD93E68AEF83A78770CBE94 (void);
// 0x000000C9 System.Void BossLineAttackShellController::ShellActiveOff()
extern void BossLineAttackShellController_ShellActiveOff_m42B15D4CDA9C4B1D77D72E23459F2075CF04026E (void);
// 0x000000CA System.Void BossLineAttackShellController::ShotLifeTimeReset()
extern void BossLineAttackShellController_ShotLifeTimeReset_m22A1C2B94B6E8DC8BE05911838C24D6D0B569EE1 (void);
// 0x000000CB System.Void BossLineAttackShellController::.ctor()
extern void BossLineAttackShellController__ctor_m07EC25ABA21A6672F766FA0BDA3591558445241B (void);
// 0x000000CC System.Void BossMoveAreaController::OnTriggerStay(UnityEngine.Collider)
extern void BossMoveAreaController_OnTriggerStay_mBCA165DDB4D9CF74D0BFC40E2D6A18763C423C2C (void);
// 0x000000CD System.Void BossMoveAreaController::BossMoveInvoke()
extern void BossMoveAreaController_BossMoveInvoke_m3AA206CBA30958F8DAE61A86516C237CED9B3F6F (void);
// 0x000000CE System.Void BossMoveAreaController::.ctor()
extern void BossMoveAreaController__ctor_m6787A76C0E5B4BB43009E0B6E9E34C2BB3630666 (void);
// 0x000000CF System.Void ChargeAttackShell2Controller::OnEnable()
extern void ChargeAttackShell2Controller_OnEnable_m383A7661FEEF846E29EBE1E74099DADF07205268 (void);
// 0x000000D0 System.Void ChargeAttackShell2Controller::FixedUpdate()
extern void ChargeAttackShell2Controller_FixedUpdate_m4003D99FE0F6D94DCD7CD094530848DF3777FAB6 (void);
// 0x000000D1 System.Void ChargeAttackShell2Controller::OnDisable()
extern void ChargeAttackShell2Controller_OnDisable_m8FC81EFC33C9EDC2EF70EA9A5094B39498EFCF5B (void);
// 0x000000D2 System.Void ChargeAttackShell2Controller::TargetMemory()
extern void ChargeAttackShell2Controller_TargetMemory_m005D2253AF75A35D28E32C03AC1E78C627D0AB2B (void);
// 0x000000D3 System.Void ChargeAttackShell2Controller::TagetMemoryReset()
extern void ChargeAttackShell2Controller_TagetMemoryReset_m2A27D92329E895A4533AD0C966FEA6CFFFBDD78D (void);
// 0x000000D4 System.Void ChargeAttackShell2Controller::ChargeAttackRotate()
extern void ChargeAttackShell2Controller_ChargeAttackRotate_m3FA2C62CFF56DBCDAD9F5F56D9233AA6FBEE752C (void);
// 0x000000D5 System.Void ChargeAttackShell2Controller::ChargeAttackShot()
extern void ChargeAttackShell2Controller_ChargeAttackShot_m3F8124BA0CBABED4C14405A7E077DCB0B93946C5 (void);
// 0x000000D6 System.Void ChargeAttackShell2Controller::ShotLifeTimeReset()
extern void ChargeAttackShell2Controller_ShotLifeTimeReset_m8255D006240669BEAD6C83B1FBF4EAE910EBE0E9 (void);
// 0x000000D7 System.Void ChargeAttackShell2Controller::ShellActiveOff()
extern void ChargeAttackShell2Controller_ShellActiveOff_m54D0C4B07B98F133BA514D26BA5FF60944DCC503 (void);
// 0x000000D8 System.Void ChargeAttackShell2Controller::.ctor()
extern void ChargeAttackShell2Controller__ctor_m2FBB72AF5765B5E8A5528DEA0786D04A16B92F18 (void);
// 0x000000D9 System.Void ChargeAttackShell3Controller::OnTriggerEnter(UnityEngine.Collider)
extern void ChargeAttackShell3Controller_OnTriggerEnter_m47392BE9B976ECD987977437BB946D910B39E881 (void);
// 0x000000DA System.Void ChargeAttackShell3Controller::ChargeAttackExplosionInstantiate()
extern void ChargeAttackShell3Controller_ChargeAttackExplosionInstantiate_mD3B9E58F00E33DB104146764EE14F26A53EFCB0F (void);
// 0x000000DB System.Void ChargeAttackShell3Controller::ChargeAttackExplosionScale()
extern void ChargeAttackShell3Controller_ChargeAttackExplosionScale_m1E463DDFFB0FF53331B85FE6A183DB1C5F788453 (void);
// 0x000000DC System.Void ChargeAttackShell3Controller::.ctor()
extern void ChargeAttackShell3Controller__ctor_m59DC513E15E4FD1B93076BE7C748A71063D689B5 (void);
// 0x000000DD System.Void CountDown::Start()
extern void CountDown_Start_m9F5188BA5A447532A1B45D2FD837A73E8BE2F2D5 (void);
// 0x000000DE System.Void CountDown::FixedUpdate()
extern void CountDown_FixedUpdate_m3838C1ACFA068CDF031BF0A1AB168A17CDF24DD5 (void);
// 0x000000DF System.Void CountDown::GameStartCount()
extern void CountDown_GameStartCount_m0DAD45D3EED699334660D0B86E37CEA19E16FA91 (void);
// 0x000000E0 System.Void CountDown::UIActive()
extern void CountDown_UIActive_m1AFDE8CE574E7DC34D62B8539027F076201D0025 (void);
// 0x000000E1 System.Void CountDown::CountDownSE()
extern void CountDown_CountDownSE_mD11A32F2BD05B3C7C258545140D55EA21EF9DFA3 (void);
// 0x000000E2 System.Void CountDown::GameStartSound()
extern void CountDown_GameStartSound_m9EEB9BF4442946DAE1170C467F3DEE59096FA8D6 (void);
// 0x000000E3 System.Void CountDown::.ctor()
extern void CountDown__ctor_m0213629414162CE304073FD33AA08E59413FFD57 (void);
// 0x000000E4 System.Void EffctOffController::OnEnable()
extern void EffctOffController_OnEnable_m67C71DC3347C4C7EBB607169FF443F54DEC9002C (void);
// 0x000000E5 System.Void EffctOffController::EffectDestroy()
extern void EffctOffController_EffectDestroy_mDDD5B46FAE71063370499BD37FCBE1F3D132BC42 (void);
// 0x000000E6 System.Void EffctOffController::.ctor()
extern void EffctOffController__ctor_mFB8048F3D4F5473DA415F61EBE8FBC395811AA8C (void);
// 0x000000E7 System.Void EffectDestroyController::Awake()
extern void EffectDestroyController_Awake_m480B81D25A0EE30E3B55455CE9BC83AB309D28E4 (void);
// 0x000000E8 System.Void EffectDestroyController::EffectDestroy()
extern void EffectDestroyController_EffectDestroy_mAF0855DAA9E6711CB28A73E40D7DFB8DAB50B8E4 (void);
// 0x000000E9 System.Void EffectDestroyController::.ctor()
extern void EffectDestroyController__ctor_mE58999AD2EC33E8E1E07A88AB0B69782BE42E27C (void);
// 0x000000EA System.Void EnemyController::Awake()
extern void EnemyController_Awake_m277E4A36C2A2089AD56826D99D8490FB399BD61B (void);
// 0x000000EB System.Void EnemyController::FixedUpdate()
extern void EnemyController_FixedUpdate_m1A283386230E165353842B2EE804F95A47052449 (void);
// 0x000000EC System.Void EnemyController::OnMouseDown()
extern void EnemyController_OnMouseDown_m45673A6114E6EC2080205285806AA97BB59EAFA0 (void);
// 0x000000ED System.Void EnemyController::OnTriggerEnter(UnityEngine.Collider)
extern void EnemyController_OnTriggerEnter_m2DADB51852D544E1329A118D76BFB058ABBB57EA (void);
// 0x000000EE System.Void EnemyController::OnTriggerStay(UnityEngine.Collider)
extern void EnemyController_OnTriggerStay_m338B2CA7829CE15063E29DD8D67F3410D710BB87 (void);
// 0x000000EF System.Void EnemyController::PlayerSearch()
extern void EnemyController_PlayerSearch_m915599E5555613F13B8DD291CDC5AC824112758E (void);
// 0x000000F0 System.Void EnemyController::PlayerAttackPointSearch()
extern void EnemyController_PlayerAttackPointSearch_m0AF75AF802F162E1BCF07E1E8B7A5734DAAE91DB (void);
// 0x000000F1 System.Void EnemyController::BossSearch()
extern void EnemyController_BossSearch_m0C8BC334805228711BAC552DF43D224D7B5003B5 (void);
// 0x000000F2 System.Void EnemyController::B_EnemySearch()
extern void EnemyController_B_EnemySearch_m30584A01A09CD9751BA16E62BE3480C88C3F316E (void);
// 0x000000F3 System.Void EnemyController::ShellPoolSearch()
extern void EnemyController_ShellPoolSearch_mD67210C6DE2F430D326BCA7AFDE38C8E51C7A337 (void);
// 0x000000F4 System.Void EnemyController::DamageEffectPoolSearch()
extern void EnemyController_DamageEffectPoolSearch_mA3096959C4F930E959D3E3C6FD0B29F0F63E53E9 (void);
// 0x000000F5 System.Void EnemyController::GetAgent()
extern void EnemyController_GetAgent_m01DEB3C399B0763A231713595AE7EFFE49E4D72F (void);
// 0x000000F6 System.Void EnemyController::EnemyTargetMove()
extern void EnemyController_EnemyTargetMove_m59C4536706311F73ABB660D666190B5516BEFFE1 (void);
// 0x000000F7 System.Void EnemyController::MovePositionRange()
extern void EnemyController_MovePositionRange_mC82EB866CD18EEEA02726F0D7407A99C6C693CEC (void);
// 0x000000F8 System.Void EnemyController::EnemyPositionReset()
extern void EnemyController_EnemyPositionReset_m062A894953B3985436D370E4672B992645E9CEBE (void);
// 0x000000F9 System.Void EnemyController::EnemyTargetRotate()
extern void EnemyController_EnemyTargetRotate_m00D2FC0BE8EE78EBC420F26D7E118EA206457CCD (void);
// 0x000000FA System.Void EnemyController::DamageEffectInstantiate()
extern void EnemyController_DamageEffectInstantiate_m32587C85CE4B706D6446E17F15ABFB241A130DAC (void);
// 0x000000FB System.Void EnemyController::DamageEffectDestroy()
extern void EnemyController_DamageEffectDestroy_m493A1EF218EB369DA217A87AB6E8FFBD373C5432 (void);
// 0x000000FC UnityEngine.GameObject EnemyController::GetDamageEffect()
extern void EnemyController_GetDamageEffect_m8B728310A0C8F9ED109223E9BE17735E625C621E (void);
// 0x000000FD System.Void EnemyController::DamageEffectActive()
extern void EnemyController_DamageEffectActive_m5D985FC372DD0B5EB606C2F466FF55C5B6DA42EF (void);
// 0x000000FE System.Void EnemyController::EnemyShellInstantiate()
extern void EnemyController_EnemyShellInstantiate_mB6FC128A5DDFCAF6244E13AB47B4DA50EDCD20AC (void);
// 0x000000FF System.Void EnemyController::EnemyShellDestroy()
extern void EnemyController_EnemyShellDestroy_mAB04D00D8444E1CAEF744E2EAFB1E0B463C0CB3E (void);
// 0x00000100 UnityEngine.GameObject EnemyController::GetEnemyShell()
extern void EnemyController_GetEnemyShell_mF0E0BD57280DB0DDE5193AE5A219BF164AF1B1EF (void);
// 0x00000101 System.Void EnemyController::EnemyAttack()
extern void EnemyController_EnemyAttack_mD3C2EC37BD0097DDE7B83168CEB2829AAED79B56 (void);
// 0x00000102 System.Void EnemyController::EnemyCubeAttackEfect()
extern void EnemyController_EnemyCubeAttackEfect_m5627B270AED6D58EE9F0EEBC060E76DCA2A5EE15 (void);
// 0x00000103 System.Void EnemyController::EnemyCubeRigidbody()
extern void EnemyController_EnemyCubeRigidbody_mECC334AC0B9BF4732BADC8F64F778386350E6695 (void);
// 0x00000104 System.Void EnemyController::AttakPointRange()
extern void EnemyController_AttakPointRange_m27BFF94C28BD4AA063F43839C14134515437868E (void);
// 0x00000105 System.Void EnemyController::EnemyDamage()
extern void EnemyController_EnemyDamage_m54B70F4EF85D2F26B3E09FAC86D86AB2840E23DB (void);
// 0x00000106 System.Void EnemyController::EnemyChargeDamage1()
extern void EnemyController_EnemyChargeDamage1_m2E6BE7E6009F9B4C71FCE401B6C3E8402D9442D6 (void);
// 0x00000107 System.Void EnemyController::EnemyChargeDamage2()
extern void EnemyController_EnemyChargeDamage2_mB0B8A0D7139315646E885FEF394C0CDABE6AD842 (void);
// 0x00000108 System.Void EnemyController::EnemyChargeDamage3()
extern void EnemyController_EnemyChargeDamage3_mA250F4E27425631CD0236A4E78E1BEF06FD4F004 (void);
// 0x00000109 System.Void EnemyController::PlayerEngineerPointPlusNomal()
extern void EnemyController_PlayerEngineerPointPlusNomal_mBEFB47290E255BC72E520DCA946D4FF6316CA0AD (void);
// 0x0000010A System.Void EnemyController::PlayerEngineerPointPlusCharge1()
extern void EnemyController_PlayerEngineerPointPlusCharge1_mFDDB7FDBD5F3ABB7A7610978492FD00C51B56A23 (void);
// 0x0000010B System.Void EnemyController::PlayerEngineerPointPlusCharge2()
extern void EnemyController_PlayerEngineerPointPlusCharge2_m0570F57226975554A33A0A3C5578E2B38E70F735 (void);
// 0x0000010C System.Void EnemyController::PlayerEngineerPointPlusCharge3()
extern void EnemyController_PlayerEngineerPointPlusCharge3_m28EF2A8697127D10D564CF9289A0FF314B22C055 (void);
// 0x0000010D System.Void EnemyController::PlayerAttackEnemyTargetLockOnContinuation()
extern void EnemyController_PlayerAttackEnemyTargetLockOnContinuation_mA0B6CF326C390EBDC4F8190D48DC6FB41866391C (void);
// 0x0000010E System.Void EnemyController::EnemyHPSliderActive()
extern void EnemyController_EnemyHPSliderActive_m184F76585BF138EBAAD39DE5166ED3AE1114367A (void);
// 0x0000010F System.Void EnemyController::EnemyHPSliderActiveOff()
extern void EnemyController_EnemyHPSliderActiveOff_m23D049B3D3621EE0EC30E52A349B20D5AB5A044E (void);
// 0x00000110 System.Void EnemyController::CurrentEnemyHPSliderActiveOn()
extern void EnemyController_CurrentEnemyHPSliderActiveOn_m48153B1571E14B6A206705C8BE5440083431F08C (void);
// 0x00000111 System.Void EnemyController::CurrentNotEnemyHPSliderActiveOff()
extern void EnemyController_CurrentNotEnemyHPSliderActiveOff_m6ED2A00E8D27A6248D0F6FCD6220D848664AAE94 (void);
// 0x00000112 System.Void EnemyController::BossHPSliderActiveOff()
extern void EnemyController_BossHPSliderActiveOff_mEA861320611BC9C8DDCBA397931B821F75F23262 (void);
// 0x00000113 System.Void EnemyController::BossGuardHPSliderActiveOff()
extern void EnemyController_BossGuardHPSliderActiveOff_mE210A7F1F599C690DC1E698B0B582AF49A38DD16 (void);
// 0x00000114 System.Void EnemyController::EnemyDestroyCallMethods()
extern void EnemyController_EnemyDestroyCallMethods_mE08C7E3894B4446D784D0BB868D4B7E7280689A5 (void);
// 0x00000115 System.Void EnemyController::EnemyHpOutDestroy()
extern void EnemyController_EnemyHpOutDestroy_mAB4C378C65A11833CD988442A4BFBB72FA2154CD (void);
// 0x00000116 System.Void EnemyController::.ctor()
extern void EnemyController__ctor_m984FF7EBF9BF2923A09FA943862B8941E9FFECCD (void);
// 0x00000117 System.Void EnemyHelthBar::Start()
extern void EnemyHelthBar_Start_mD1DD1B96C51842F971E264F61C5CF36E26DF0D9C (void);
// 0x00000118 System.Void EnemyHelthBar::FixedUpdate()
extern void EnemyHelthBar_FixedUpdate_mA1F6D5840126DE8289431DA0D5E161352C68727B (void);
// 0x00000119 System.Void EnemyHelthBar::EnemyCurrentHP()
extern void EnemyHelthBar_EnemyCurrentHP_mF3C81F42195BE4EB173D02C17287188B1B65FD00 (void);
// 0x0000011A System.Void EnemyHelthBar::.ctor()
extern void EnemyHelthBar__ctor_mFB12B425888522961438ABE7EB4E8794B5AB55DF (void);
// 0x0000011B System.Void EngineerGaugeController::Start()
extern void EngineerGaugeController_Start_m9C63291705F4639FAE6B913EA6C8A1712F1B467E (void);
// 0x0000011C System.Void EngineerGaugeController::OnEnable()
extern void EngineerGaugeController_OnEnable_m1C22A9881643B031464C7E51B7B51D19765BC2C5 (void);
// 0x0000011D System.Void EngineerGaugeController::TransformScaleZero()
extern void EngineerGaugeController_TransformScaleZero_m60FA82DA1599F54CB9DF1B2D752D12B17C29F40D (void);
// 0x0000011E System.Void EngineerGaugeController::TransformScaleActiveAnimation()
extern void EngineerGaugeController_TransformScaleActiveAnimation_mA91DE1364DE76F0097757D5535BCAA0743F9BAEA (void);
// 0x0000011F System.Void EngineerGaugeController::SliderCurrentEngneerPoint()
extern void EngineerGaugeController_SliderCurrentEngneerPoint_m4FAC67E3DD92ADE9B7291A125F7DCF3F1D6675DD (void);
// 0x00000120 System.Void EngineerGaugeController::EngineerPointMaxAction()
extern void EngineerGaugeController_EngineerPointMaxAction_m61C49549891250857265DC5F55A31BD815CFA164 (void);
// 0x00000121 System.Void EngineerGaugeController::.ctor()
extern void EngineerGaugeController__ctor_mA5327925B1FA820E47635E0FCD6ED12FC448B246 (void);
// 0x00000122 System.Void GameManager::Start()
extern void GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2 (void);
// 0x00000123 System.Void GameManager::FixedUpdate()
extern void GameManager_FixedUpdate_mCBA9C3ED1DE7FC1B9D9609D702B13EFB38245017 (void);
// 0x00000124 System.Void GameManager::PlayerActive()
extern void GameManager_PlayerActive_m249E9947ACA8250CE03BC31C2C565D99CBF204A8 (void);
// 0x00000125 System.Void GameManager::StartAnimationMovie()
extern void GameManager_StartAnimationMovie_m0CCF1EC1466B0546149EEAFEE2E82B68280F54EB (void);
// 0x00000126 System.Void GameManager::SceneChangeBackImageAnimationOn()
extern void GameManager_SceneChangeBackImageAnimationOn_mFB7C1AAFA55EC40209F12D28FB5AEEAD664B1084 (void);
// 0x00000127 System.Void GameManager::SceneChangeBackImageAnimationOff()
extern void GameManager_SceneChangeBackImageAnimationOff_mC266392365515BC1897630E167C995AB7F998574 (void);
// 0x00000128 System.Void GameManager::SceneChangeBackImageSetActiveOn()
extern void GameManager_SceneChangeBackImageSetActiveOn_mF118E8973CCDF46E08BC8BE42EDAB1986CD0BCA4 (void);
// 0x00000129 System.Void GameManager::SceneChangeBackImageSetActiveOffInvoke()
extern void GameManager_SceneChangeBackImageSetActiveOffInvoke_mE58FFC9203C554BBFEE6C7A0A89EFDD070590735 (void);
// 0x0000012A System.Void GameManager::BossAnimation()
extern void GameManager_BossAnimation_m672A141F62E2984EA29AD1B57104450186F09AD9 (void);
// 0x0000012B System.Collections.IEnumerator GameManager::StartVCameraAnimationCoroutine()
extern void GameManager_StartVCameraAnimationCoroutine_m4F40509E0ECC21C10A089DB1B13256681154F8C4 (void);
// 0x0000012C System.Collections.IEnumerator GameManager::BossVCameraAnimationCoroutine()
extern void GameManager_BossVCameraAnimationCoroutine_m9558BB31FCF6EDB732187438FA76538D05D75F5C (void);
// 0x0000012D System.Void GameManager::VCamera3Move()
extern void GameManager_VCamera3Move_mBEA0E4E1ABF054A8B71E48547221434759B511D0 (void);
// 0x0000012E System.Void GameManager::VCamera3Rotate()
extern void GameManager_VCamera3Rotate_m4FD35F62A56591F188EEEE9BEDE9173C1D0E5F15 (void);
// 0x0000012F System.Void GameManager::VCamera2PriorityDown()
extern void GameManager_VCamera2PriorityDown_m35D3F50FFA3CB16688F8FD0C5AC3F9E2F794834F (void);
// 0x00000130 System.Void GameManager::VCamera3PriorityDown()
extern void GameManager_VCamera3PriorityDown_mB525F0850CE9F9D102E8A0599032CC8218146AFB (void);
// 0x00000131 System.Void GameManager::VCamera4PriorityDown()
extern void GameManager_VCamera4PriorityDown_m03D1C411E34B373C09A43665771B96FE96502E45 (void);
// 0x00000132 System.Void GameManager::VCamera5PriorityDown()
extern void GameManager_VCamera5PriorityDown_mC99AA109E6FAE36FCB3B21BFE7E7F6CA1B25F3C0 (void);
// 0x00000133 System.Void GameManager::VCamera6PriorityDown()
extern void GameManager_VCamera6PriorityDown_mA6F56FD847C129FB35F0F30419EE419137094A6F (void);
// 0x00000134 System.Void GameManager::VCamera7PriorityDown()
extern void GameManager_VCamera7PriorityDown_m048F11806108426E6E128CFD86184B5C7F9A4B41 (void);
// 0x00000135 System.Void GameManager::VCamera5PriorityUp()
extern void GameManager_VCamera5PriorityUp_mF76C5CC17A9D6D9E30E1D7C69F6956C6F95A339F (void);
// 0x00000136 System.Void GameManager::VCamera6PriorityUp()
extern void GameManager_VCamera6PriorityUp_mD0569BA7E4CA9347B5A7A429AA053D7E8B334DB1 (void);
// 0x00000137 System.Void GameManager::VCamera7PriorityUp()
extern void GameManager_VCamera7PriorityUp_mE72EB451AAA494E48B5916A48F355F1DBF610B86 (void);
// 0x00000138 System.Void GameManager::StartCinemachineAnimationCameraActiveOff()
extern void GameManager_StartCinemachineAnimationCameraActiveOff_mEFD88C23A68DE8D823F1ACE99F15AE4AC03D96B8 (void);
// 0x00000139 System.Void GameManager::BossCinemachineAnimationCameraActiveOff()
extern void GameManager_BossCinemachineAnimationCameraActiveOff_m0253FB648687197A79931DC0A37C13870658C38C (void);
// 0x0000013A System.Void GameManager::CountDownObjectActive()
extern void GameManager_CountDownObjectActive_m412A303A5BCACB2ACD82A92922B2BA8853286F2C (void);
// 0x0000013B System.Void GameManager::PlayerSpawnPoint()
extern void GameManager_PlayerSpawnPoint_m6F5684B7BD2446935004DC1643A2273C0DB213C0 (void);
// 0x0000013C System.Void GameManager::PlayerStartLook()
extern void GameManager_PlayerStartLook_m4274734D67A44B6BBA9B1DF8C9AACE894302061D (void);
// 0x0000013D System.Void GameManager::PlayerCameraStartLook()
extern void GameManager_PlayerCameraStartLook_mB7BED31454ABBE47AA14529FCCB2E9D33C694367 (void);
// 0x0000013E System.Void GameManager::PlayerDeadAction()
extern void GameManager_PlayerDeadAction_m3509182C38C8CDC07C23B276D57BF749A6C343E8 (void);
// 0x0000013F System.Void GameManager::PlayerFieldOut()
extern void GameManager_PlayerFieldOut_m1C9DF3F712656EBE6D52D438BC1F0C322D982E65 (void);
// 0x00000140 System.Void GameManager::PlayerCurrentLevelText()
extern void GameManager_PlayerCurrentLevelText_mF08FCA777C30760C857F1FA498A886A4B1B68E4E (void);
// 0x00000141 GameManager GameManager::GameManagerSinglton()
extern void GameManager_GameManagerSinglton_m89EAD074AFD033C7D5E9676F2EBDBFF1282156D3 (void);
// 0x00000142 System.Void GameManager::PlayerLevelUpTextOn()
extern void GameManager_PlayerLevelUpTextOn_m4DB704759E96F67C92EBCD0C5CB4F15C50B6693F (void);
// 0x00000143 System.Void GameManager::PlayerLevelUpTextEmpty()
extern void GameManager_PlayerLevelUpTextEmpty_mD11DA5B112AF785CC9432EAC3649D76A2C04CE3E (void);
// 0x00000144 System.Void GameManager::UserTextEmpty()
extern void GameManager_UserTextEmpty_mBB7C454A198D4FB10246B9BCDD1594552F251AB2 (void);
// 0x00000145 System.Void GameManager::GetExpText()
extern void GameManager_GetExpText_m94B174A61E8C97DA42A12024E114D0C589A15DC4 (void);
// 0x00000146 System.Void GameManager::GameSceneLoad()
extern void GameManager_GameSceneLoad_m611595F606A752E5ED401F3A46ED1E40DFB6C2D3 (void);
// 0x00000147 System.Void GameManager::TutorialSceneLoad()
extern void GameManager_TutorialSceneLoad_m3AD5C5168A455DC360D0AE20F77BE9D7CCBA933E (void);
// 0x00000148 System.Void GameManager::EnemyDestroyCountPlus()
extern void GameManager_EnemyDestroyCountPlus_m3BF11E3425DFD8C98AD6D795F0FD5CCB0A994729 (void);
// 0x00000149 System.Void GameManager::GoalActive()
extern void GameManager_GoalActive_m715B1C6C6C1143DB79841899ECBBB9103C7E2B6F (void);
// 0x0000014A System.Void GameManager::GoalActiveOff()
extern void GameManager_GoalActiveOff_mFA54E7993E1D3D3A2D4526F4192ED8CCC6E67D9A (void);
// 0x0000014B System.Void GameManager::GameClear()
extern void GameManager_GameClear_mF5CE946B586CD7FA9FF5EDB85881E458D6149D11 (void);
// 0x0000014C System.Void GameManager::MenuButtonsActive()
extern void GameManager_MenuButtonsActive_m6001800DEE9DD28A7E02C9B93345C9397233447F (void);
// 0x0000014D System.Void GameManager::BackTitleButton()
extern void GameManager_BackTitleButton_m6F887EF4E52D6699F3EF5E3EBD14172463EAD9F8 (void);
// 0x0000014E System.Void GameManager::ResetGameButton()
extern void GameManager_ResetGameButton_mBE21C0DB49682DC6AC91FD10C583A4E5ED06DF0D (void);
// 0x0000014F System.Void GameManager::ResetTutorialButton()
extern void GameManager_ResetTutorialButton_m0207CDB71EB5BE310A2DB65FE013F5D32D54405A (void);
// 0x00000150 System.Void GameManager::GameClearBackTitleButton()
extern void GameManager_GameClearBackTitleButton_m979313047A526E8DC0FDAD5E3C6E58D2E5D83196 (void);
// 0x00000151 System.Void GameManager::TimerCountReset()
extern void GameManager_TimerCountReset_m51655BE1F4C85DC4D1CD0821DDADEEBAE08D9480 (void);
// 0x00000152 System.Void GameManager::UIActiveOff()
extern void GameManager_UIActiveOff_m8669E5EF4898969A38D2C5DB6D4B575409A0C9CC (void);
// 0x00000153 System.Void GameManager::.ctor()
extern void GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368 (void);
// 0x00000154 System.Void GameManager/<StartVCameraAnimationCoroutine>d__44::.ctor(System.Int32)
extern void U3CStartVCameraAnimationCoroutineU3Ed__44__ctor_mA8B16DFA9DBBA0A7F14E50587461644B6FFDCF26 (void);
// 0x00000155 System.Void GameManager/<StartVCameraAnimationCoroutine>d__44::System.IDisposable.Dispose()
extern void U3CStartVCameraAnimationCoroutineU3Ed__44_System_IDisposable_Dispose_m97BE1E39FFE0BE5DB808212C68521BE697CDFB76 (void);
// 0x00000156 System.Boolean GameManager/<StartVCameraAnimationCoroutine>d__44::MoveNext()
extern void U3CStartVCameraAnimationCoroutineU3Ed__44_MoveNext_m7332C7A84F7CF83385666F344B035AD5EEDEAE77 (void);
// 0x00000157 System.Object GameManager/<StartVCameraAnimationCoroutine>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartVCameraAnimationCoroutineU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCADA8309F44BC020F1702B5A1650A078CC35B91 (void);
// 0x00000158 System.Void GameManager/<StartVCameraAnimationCoroutine>d__44::System.Collections.IEnumerator.Reset()
extern void U3CStartVCameraAnimationCoroutineU3Ed__44_System_Collections_IEnumerator_Reset_m8ABAA998E4846E1907C88D56855C5F6A7CD79214 (void);
// 0x00000159 System.Object GameManager/<StartVCameraAnimationCoroutine>d__44::System.Collections.IEnumerator.get_Current()
extern void U3CStartVCameraAnimationCoroutineU3Ed__44_System_Collections_IEnumerator_get_Current_mE55DB54AF1D447447DBF736150A0249C896E141C (void);
// 0x0000015A System.Void GameManager/<BossVCameraAnimationCoroutine>d__45::.ctor(System.Int32)
extern void U3CBossVCameraAnimationCoroutineU3Ed__45__ctor_mC83D3DDC5C37670415672911694DB2F10F7C2EDA (void);
// 0x0000015B System.Void GameManager/<BossVCameraAnimationCoroutine>d__45::System.IDisposable.Dispose()
extern void U3CBossVCameraAnimationCoroutineU3Ed__45_System_IDisposable_Dispose_m702A93A0D3CC4FD59CDA0C2482210C3AA7A3ED5E (void);
// 0x0000015C System.Boolean GameManager/<BossVCameraAnimationCoroutine>d__45::MoveNext()
extern void U3CBossVCameraAnimationCoroutineU3Ed__45_MoveNext_mC1B653BEDF62CFB889B80B821CAD71574D0A304E (void);
// 0x0000015D System.Object GameManager/<BossVCameraAnimationCoroutine>d__45::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBossVCameraAnimationCoroutineU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F4F5E24BF69F400FB893E84B3F443A295C3EEC0 (void);
// 0x0000015E System.Void GameManager/<BossVCameraAnimationCoroutine>d__45::System.Collections.IEnumerator.Reset()
extern void U3CBossVCameraAnimationCoroutineU3Ed__45_System_Collections_IEnumerator_Reset_m3F03D0837EC9560C17B1CECCD7874D321F7FFB6E (void);
// 0x0000015F System.Object GameManager/<BossVCameraAnimationCoroutine>d__45::System.Collections.IEnumerator.get_Current()
extern void U3CBossVCameraAnimationCoroutineU3Ed__45_System_Collections_IEnumerator_get_Current_mD431B289958AE9DC655DCEADD051CFFF43449E04 (void);
// 0x00000160 System.Void GoalController::OnMouseDown()
extern void GoalController_OnMouseDown_mBE1F7F563AA856C3871B4911A022EE9611CB29BE (void);
// 0x00000161 System.Void GoalController::OnDisable()
extern void GoalController_OnDisable_m9FEFA4F716B9E5281EA066C7BA7C932051C3974F (void);
// 0x00000162 System.Void GoalController::FixedUpdate()
extern void GoalController_FixedUpdate_m0CF59D1047CF9DF86B83648A749458CB3872CEA1 (void);
// 0x00000163 System.Void GoalController::GoalTargetLockOnContinuation()
extern void GoalController_GoalTargetLockOnContinuation_m023D95006581C29F2F09EA88CA820741392EE7A7 (void);
// 0x00000164 System.Void GoalController::GoalRotation()
extern void GoalController_GoalRotation_m28FD718545DEF3D81BC93BECF648FBB8D83E891F (void);
// 0x00000165 System.Void GoalController::UIActiveOff()
extern void GoalController_UIActiveOff_m86AC777F0C3212E7C07B101D86D34C913C87C277 (void);
// 0x00000166 System.Void GoalController::NameInputFieldActive()
extern void GoalController_NameInputFieldActive_mDF7A6A8CE0ED58FB074B697B8D84F54C41A79D52 (void);
// 0x00000167 System.Void GoalController::GoalColiderValue()
extern void GoalController_GoalColiderValue_m56648F2AFF43BBE4A55F6612335D5F9A532F3238 (void);
// 0x00000168 System.Void GoalController::PlayerActiveOff()
extern void GoalController_PlayerActiveOff_mE845AEE22A38C61B804DA896D2004880280DF356 (void);
// 0x00000169 System.Void GoalController::.ctor()
extern void GoalController__ctor_m8A21509A91DA7E5919D5B6FD050E18B0695C8994 (void);
// 0x0000016A System.Void LimitOffButtonController::Awake()
extern void LimitOffButtonController_Awake_m2A874A34A81F0F13CDFF0C14E25EA36BD9E97CBD (void);
// 0x0000016B System.Void LimitOffButtonController::OnEnable()
extern void LimitOffButtonController_OnEnable_m33C8C5BA552277F5714F981190F854FB734B4E84 (void);
// 0x0000016C System.Void LimitOffButtonController::TransformScaleZero()
extern void LimitOffButtonController_TransformScaleZero_m979EDBFE91BC6759EBFBF1B6F95BB6C344F5D5EE (void);
// 0x0000016D System.Void LimitOffButtonController::TransformScaleActiveAnimation()
extern void LimitOffButtonController_TransformScaleActiveAnimation_m506251B2BACA053744710CB0FA2ADEE234F2BAC5 (void);
// 0x0000016E System.Void LimitOffButtonController::LimitOffSliderActiveButton()
extern void LimitOffButtonController_LimitOffSliderActiveButton_m96C83EB2C34CD40E51584A7BFB35726B3909DD59 (void);
// 0x0000016F System.Void LimitOffButtonController::.ctor()
extern void LimitOffButtonController__ctor_m26EEE89C58AF769F2ACED91FEDC81837C6DD04E4 (void);
// 0x00000170 System.Void LimitOffSliderController::Awake()
extern void LimitOffSliderController_Awake_m03129EF9CC210A5ACC0B0A051A46D0077BBA4318 (void);
// 0x00000171 System.Void LimitOffSliderController::OnEnable()
extern void LimitOffSliderController_OnEnable_m40A18B273F9E955CC2BA0EA31E8D93EE79F6646D (void);
// 0x00000172 System.Void LimitOffSliderController::FixedUpdate()
extern void LimitOffSliderController_FixedUpdate_m858974C5F42D4F51223627FB1C4F9E85C900DC5E (void);
// 0x00000173 System.Void LimitOffSliderController::TransformScaleZero()
extern void LimitOffSliderController_TransformScaleZero_m73512D7D92951E7ACCF9C1698E928445E594D129 (void);
// 0x00000174 System.Void LimitOffSliderController::TransformScaleActiveAnimation()
extern void LimitOffSliderController_TransformScaleActiveAnimation_m70A15462839C7B92276FCE6C2A0FF03CF4A5295A (void);
// 0x00000175 System.Void LimitOffSliderController::limitSliderPunchScaleAnimation()
extern void LimitOffSliderController_limitSliderPunchScaleAnimation_m1376865A87B9A0CD30B714905755F110615BC7F4 (void);
// 0x00000176 System.Void LimitOffSliderController::BusterSyncroActionValue()
extern void LimitOffSliderController_BusterSyncroActionValue_m08460ABB4E6B2579ACBB475CEF3F836AF113AE0E (void);
// 0x00000177 System.Void LimitOffSliderController::WizardEngineActionValue()
extern void LimitOffSliderController_WizardEngineActionValue_m8E7FA2482C183AC220F955BA3D8D427DB0F4BB1C (void);
// 0x00000178 System.Void LimitOffSliderController::LimitTimeInvoke()
extern void LimitOffSliderController_LimitTimeInvoke_m9512FB30EBB2A6A10F8F1496AF7CC6AFACA6AC97 (void);
// 0x00000179 System.Void LimitOffSliderController::LimitTimeEndAction()
extern void LimitOffSliderController_LimitTimeEndAction_m9B5391C7D840218C453CDF136A24DD56565DBF7D (void);
// 0x0000017A System.Void LimitOffSliderController::LimitTimeEndActionPublic()
extern void LimitOffSliderController_LimitTimeEndActionPublic_mD234B741BCEE30BB554524A4346B930D1AB0ECD5 (void);
// 0x0000017B System.Void LimitOffSliderController::BusterSyncroColorChange()
extern void LimitOffSliderController_BusterSyncroColorChange_mFF34206AEE3AC7A4BB56D5AA0BB0695C58FB97C2 (void);
// 0x0000017C System.Void LimitOffSliderController::WizardEngineColorChange()
extern void LimitOffSliderController_WizardEngineColorChange_m1422AB60AA770D392DDB13641ED4CD0F2DC0BF8F (void);
// 0x0000017D System.Void LimitOffSliderController::LimitOffSliderColorReset()
extern void LimitOffSliderController_LimitOffSliderColorReset_mBE07CC693E377D78EE348CC530FD6505E1D697AC (void);
// 0x0000017E System.Void LimitOffSliderController::LimitOffSliderValueReset()
extern void LimitOffSliderController_LimitOffSliderValueReset_m71EE11F048981C94CEDF50416E85438037EA067D (void);
// 0x0000017F System.Void LimitOffSliderController::.ctor()
extern void LimitOffSliderController__ctor_mE26920175C0C2D94F96FD60767E464CA06C57E39 (void);
// 0x00000180 System.Void LookOnMakerController::FixedUpdate()
extern void LookOnMakerController_FixedUpdate_mDAA0ED456AF74901CC1E2C88C3E446ED51C92EF3 (void);
// 0x00000181 System.Void LookOnMakerController::MakerRotation()
extern void LookOnMakerController_MakerRotation_mF69CFAE22B14919ADCE0EE5B023E092A9B11208D (void);
// 0x00000182 System.Void LookOnMakerController::.ctor()
extern void LookOnMakerController__ctor_mC97811F2FE2F5AB2C4C3858F46E68A60CEC82760 (void);
// 0x00000183 System.Void MainSoundManager::Awake()
extern void MainSoundManager_Awake_mCEC847FF5E831E30543705B07B35AED81D6C969B (void);
// 0x00000184 System.Void MainSoundManager::Start()
extern void MainSoundManager_Start_m22F9FE4B2F0E499F3772C63B200BAD8A702E2315 (void);
// 0x00000185 System.Void MainSoundManager::OnActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void MainSoundManager_OnActiveSceneChanged_mA63D4801527858B0C3D11781B04EFF77E7EB8C80 (void);
// 0x00000186 System.Void MainSoundManager::SoundManagerSystem()
extern void MainSoundManager_SoundManagerSystem_m2861E411C408B76D1D62FE35642E6D5079D3AB3F (void);
// 0x00000187 System.Void MainSoundManager::BossBattleBGMActive()
extern void MainSoundManager_BossBattleBGMActive_m0956A3CCF1977B5694772E45B01A5A355F304E92 (void);
// 0x00000188 System.Void MainSoundManager::GameBGMStop()
extern void MainSoundManager_GameBGMStop_m86BE55108F2031F529C6DCFBFE9982DAA8B4E6DF (void);
// 0x00000189 System.Void MainSoundManager::GameClearBGMActive()
extern void MainSoundManager_GameClearBGMActive_m421B923503170FD6EEE821E5C1785763C67E6CC5 (void);
// 0x0000018A System.Void MainSoundManager::.ctor()
extern void MainSoundManager__ctor_mF02C439B9FB0BC61A6111EF7C7B2FCFDF73213A0 (void);
// 0x0000018B System.Void MoveWallController::Start()
extern void MoveWallController_Start_m8DAF0B3BA31954F8ABA0CB1183408A0B476771B3 (void);
// 0x0000018C System.Void MoveWallController::OnCollisionStay(UnityEngine.Collision)
extern void MoveWallController_OnCollisionStay_mC613B8DF3ED3994CC805A9C56AC0E17D95085EA8 (void);
// 0x0000018D System.Void MoveWallController::OnCollisionExit(UnityEngine.Collision)
extern void MoveWallController_OnCollisionExit_m04E7BB366CAE49E98506CFD1059183B9A719A88B (void);
// 0x0000018E System.Void MoveWallController::MoveWallStart()
extern void MoveWallController_MoveWallStart_mBFC7E972C92830A6CAAD5C59B8AB452DC4111AA0 (void);
// 0x0000018F System.Void MoveWallController::MoveWallPositionMaxLock()
extern void MoveWallController_MoveWallPositionMaxLock_m819B84D82E1F1B9C8615EAA21DA9AE8A99248A51 (void);
// 0x00000190 System.Void MoveWallController::MoveWallReset()
extern void MoveWallController_MoveWallReset_m1C5A2DC506D1CD86E11DADED2C2C74DE33423E5A (void);
// 0x00000191 System.Void MoveWallController::MoveWallIsKinematicOff()
extern void MoveWallController_MoveWallIsKinematicOff_mFC41655B80884B3E7DF91CED0CC1F85A8394A41B (void);
// 0x00000192 System.Void MoveWallController::MoveWallRigidReset()
extern void MoveWallController_MoveWallRigidReset_m18D6FF65DD3AA656FB897C64FEA81214B9ACAFFB (void);
// 0x00000193 System.Void MoveWallController::UseGravityOff()
extern void MoveWallController_UseGravityOff_m5FA7514369B7AA89BD434DB295423866FC0E5C3B (void);
// 0x00000194 System.Void MoveWallController::UseGravityON()
extern void MoveWallController_UseGravityON_mACAE8C44489D539066A786679093175A9D4C7A23 (void);
// 0x00000195 System.Void MoveWallController::.ctor()
extern void MoveWallController__ctor_m5AC80996B2FF249BFA731627611B0C0C6F2E7216 (void);
// 0x00000196 System.Void TimeScoreLoad::LoadTimeScoreRanking()
extern void TimeScoreLoad_LoadTimeScoreRanking_mDDAF42E2EDCAB670E72B6CB245FD2A20F76D4334 (void);
// 0x00000197 System.Void TimeScoreLoad::.ctor()
extern void TimeScoreLoad__ctor_m21BFAD8404C775F13442F60F1F6359FE16C80D65 (void);
// 0x00000198 System.Void TimeScoreLoad/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mD0D18A575C3BC5ADF2E138D3C94910EA707EC11F (void);
// 0x00000199 System.Void TimeScoreLoad/<>c__DisplayClass2_0::<LoadTimeScoreRanking>b__0(System.Collections.Generic.List`1<NCMB.NCMBObject>,NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass2_0_U3CLoadTimeScoreRankingU3Eb__0_m5F1BEA815DBAB35B4485C1CE3D11157CBE227A6A (void);
// 0x0000019A System.Void TimeScoreSave::SaveTimeScore()
extern void TimeScoreSave_SaveTimeScore_mB6F626FE8BA7BCEF5519A1E82817E27C65DBE132 (void);
// 0x0000019B System.Void TimeScoreSave::.ctor()
extern void TimeScoreSave__ctor_mAFB28CE91E2146188A78E8CD92097E676A0D19BC (void);
// 0x0000019C System.Void TimeScoreSave/<>c::.cctor()
extern void U3CU3Ec__cctor_mF4108DC748AA6C0FE1531FCDF92656EC4076528D (void);
// 0x0000019D System.Void TimeScoreSave/<>c::.ctor()
extern void U3CU3Ec__ctor_m63AA73C29131A539FBCFD335C9DD97AA4A02BEB4 (void);
// 0x0000019E System.Void TimeScoreSave/<>c::<SaveTimeScore>b__4_0(NCMB.NCMBException)
extern void U3CU3Ec_U3CSaveTimeScoreU3Eb__4_0_m79B07634627531688FE016BB250BC20BC1DB9EBB (void);
// 0x0000019F System.Void ObjectInstanceController::OnTriggerEnter(UnityEngine.Collider)
extern void ObjectInstanceController_OnTriggerEnter_m191FAE222D4743E35CCB1102501987A1BFB86F93 (void);
// 0x000001A0 System.Void ObjectInstanceController::ObjectInstans()
extern void ObjectInstanceController_ObjectInstans_mC39734AD2BC5E0F48FA84F1053C28FB07D9BB483 (void);
// 0x000001A1 System.Void ObjectInstanceController::.ctor()
extern void ObjectInstanceController__ctor_m4F56B2D7C053D395BDBE578A99B42566B7EC9190 (void);
// 0x000001A2 System.Void PlayerAttackController::Start()
extern void PlayerAttackController_Start_mC6B6FF33CDA660AD556E1614FC486DD834E55AA2 (void);
// 0x000001A3 System.Void PlayerAttackController::OnEnable()
extern void PlayerAttackController_OnEnable_m212E7403F906717FF4BAB5A89E9EBC0C66C06B51 (void);
// 0x000001A4 System.Void PlayerAttackController::FixedUpdate()
extern void PlayerAttackController_FixedUpdate_mBDA4282EF58502FCDFEBA65DE768A18DDB7FB921 (void);
// 0x000001A5 System.Void PlayerAttackController::OnDisable()
extern void PlayerAttackController_OnDisable_m7F774428D18399899AF32FB5F78D9A227F134D72 (void);
// 0x000001A6 System.Void PlayerAttackController::ShellPoolSearch()
extern void PlayerAttackController_ShellPoolSearch_m5BDEB70BD9806DD6FFDB6E7FFE9F0FA586CB314B (void);
// 0x000001A7 System.Void PlayerAttackController::PlayerAttackShellInstantiate()
extern void PlayerAttackController_PlayerAttackShellInstantiate_mC6B12E43E6DBF9AB3A1883226C207380E0E25DCD (void);
// 0x000001A8 UnityEngine.GameObject PlayerAttackController::GetPlayerShell()
extern void PlayerAttackController_GetPlayerShell_mF9C279D225D0D76D91D3E39414901EEE300C02D9 (void);
// 0x000001A9 System.Void PlayerAttackController::ChargeAttackShell1Instantiate()
extern void PlayerAttackController_ChargeAttackShell1Instantiate_m19709132C051732846318BB809A7475DCA3D8764 (void);
// 0x000001AA UnityEngine.GameObject PlayerAttackController::GetChargeAttackShell1()
extern void PlayerAttackController_GetChargeAttackShell1_mF6F80FE5B76033F1DD21A722E59D22A067C296EE (void);
// 0x000001AB System.Void PlayerAttackController::ChargeAttackShell2Instantiate()
extern void PlayerAttackController_ChargeAttackShell2Instantiate_mD2FC1187109A472CA592AE66008EE23224454F9D (void);
// 0x000001AC UnityEngine.GameObject PlayerAttackController::GetChargeAttackShell2()
extern void PlayerAttackController_GetChargeAttackShell2_m80156370601AAFD71B833E36CF9E9CBA6B0144D1 (void);
// 0x000001AD System.Void PlayerAttackController::PlayerAttackButton()
extern void PlayerAttackController_PlayerAttackButton_mD60F6F64D9FA4EAE219F7651ED622035F6A39C1E (void);
// 0x000001AE System.Void PlayerAttackController::PlayerChargeAttackButton()
extern void PlayerAttackController_PlayerChargeAttackButton_m5B22523632306DF9BC0F173CB325004C21A5F17A (void);
// 0x000001AF System.Void PlayerAttackController::NomalAttack()
extern void PlayerAttackController_NomalAttack_mB2FDDCEF2DFED81FFA49513C7229E64A12047F06 (void);
// 0x000001B0 System.Void PlayerAttackController::WizardEngineNomalAttack()
extern void PlayerAttackController_WizardEngineNomalAttack_mF9A4879A6DD2B4092907F33CD469D7DF5C877E8D (void);
// 0x000001B1 System.Void PlayerAttackController::PlayerChargeAttack1()
extern void PlayerAttackController_PlayerChargeAttack1_m506927379E752A94B08F838DA1412044A87E5FC7 (void);
// 0x000001B2 System.Void PlayerAttackController::WizardEngineChargeAttack1()
extern void PlayerAttackController_WizardEngineChargeAttack1_m5B898CA69ACA258E90E3945D0F8039DCD8DC3E7F (void);
// 0x000001B3 System.Void PlayerAttackController::PlayerChargeShell1Instans()
extern void PlayerAttackController_PlayerChargeShell1Instans_m82B9B8F12945CC40338C579844A909C9FC94436A (void);
// 0x000001B4 System.Void PlayerAttackController::WizardEngineChargeShell1Instans()
extern void PlayerAttackController_WizardEngineChargeShell1Instans_m65537500BAA7DD895B3885395CE6B878AA8A6B86 (void);
// 0x000001B5 System.Collections.IEnumerator PlayerAttackController::ChangeAttackShell1Coroutine()
extern void PlayerAttackController_ChangeAttackShell1Coroutine_m2E7ACAA69E9201E10EAF08B4E0E01E16F7ED876F (void);
// 0x000001B6 System.Collections.IEnumerator PlayerAttackController::WizardEngineAttackShell1Coroutine()
extern void PlayerAttackController_WizardEngineAttackShell1Coroutine_mE74C5B4D4EF442F2D3D34FD4937DA74EF882477E (void);
// 0x000001B7 System.Void PlayerAttackController::PlayerChargeAttack2()
extern void PlayerAttackController_PlayerChargeAttack2_m4A75F450C517E472EAD88425C7A85649C9452785 (void);
// 0x000001B8 System.Void PlayerAttackController::WizardEngineChargeAttack2()
extern void PlayerAttackController_WizardEngineChargeAttack2_m75AA992384B137E87C57546D5302D0C797E0C42A (void);
// 0x000001B9 System.Void PlayerAttackController::PlayerChargeShell2Instans()
extern void PlayerAttackController_PlayerChargeShell2Instans_mA69E706A4C04CEC9E96CBAD092167761ADC51720 (void);
// 0x000001BA System.Void PlayerAttackController::WizardEngineChargeShell2Instans()
extern void PlayerAttackController_WizardEngineChargeShell2Instans_m9BB854F4591F1B8E4A75C12E7687B420C3F97637 (void);
// 0x000001BB System.Collections.IEnumerator PlayerAttackController::ChangeAttackShell2Coroutine()
extern void PlayerAttackController_ChangeAttackShell2Coroutine_mB828B6BF50EE7A2317F6B65A7524F1EA8BE29D7B (void);
// 0x000001BC System.Collections.IEnumerator PlayerAttackController::WizardEngineChangeAttackShell2Coroutine()
extern void PlayerAttackController_WizardEngineChangeAttackShell2Coroutine_m253B62E1FF54E019A3C7C69EB7F0051B68023916 (void);
// 0x000001BD System.Void PlayerAttackController::PlayerChargeAttack3()
extern void PlayerAttackController_PlayerChargeAttack3_mB9C0A890A31020D48CD7EBD008EABA682BB44318 (void);
// 0x000001BE System.Void PlayerAttackController::WizardEngineChargeAttack3()
extern void PlayerAttackController_WizardEngineChargeAttack3_m21716596BF667F0A358365F666D141A5C3CA7E99 (void);
// 0x000001BF System.Void PlayerAttackController::PlayerChargeAttackReset()
extern void PlayerAttackController_PlayerChargeAttackReset_m259247AF2575EB116470EA410B5B1EE6BE697431 (void);
// 0x000001C0 System.Void PlayerAttackController::PlayerChargeAttackCountJudge()
extern void PlayerAttackController_PlayerChargeAttackCountJudge_mDFBB018BB034B2D62816B6E38DEE41BD74609D47 (void);
// 0x000001C1 System.Void PlayerAttackController::PlayerChargeAttackCountStart()
extern void PlayerAttackController_PlayerChargeAttackCountStart_m883CFAD9896390B6C41A366E581E587FAE04A36F (void);
// 0x000001C2 System.Void PlayerAttackController::AttackChargeSound()
extern void PlayerAttackController_AttackChargeSound_m583111E4005ADD1349C4B8CF2E079B132C2FA8D4 (void);
// 0x000001C3 System.Void PlayerAttackController::ChargeAttackCountSliderColorChange()
extern void PlayerAttackController_ChargeAttackCountSliderColorChange_m7B6EADB5A8CFE1B9D5D8EDB3D27B40BF2479D484 (void);
// 0x000001C4 System.Void PlayerAttackController::LookOnMakerAction()
extern void PlayerAttackController_LookOnMakerAction_m7EFC07B3152BF0D5AFEE1254BFE10287B9BFE047 (void);
// 0x000001C5 System.Void PlayerAttackController::LookOnMakerActiveOff()
extern void PlayerAttackController_LookOnMakerActiveOff_m953A73DF5FE77EDBC5AEE5E7BDE47EDB493E2BE0 (void);
// 0x000001C6 System.Void PlayerAttackController::EnemyTargetLockOn()
extern void PlayerAttackController_EnemyTargetLockOn_mC5435522B76D52C4DA5C0A10EB1648137FEE1C62 (void);
// 0x000001C7 System.Void PlayerAttackController::BossTargetLockOn()
extern void PlayerAttackController_BossTargetLockOn_m610E1DFE84120B8EADC86A53E164405E88A1BF24 (void);
// 0x000001C8 System.Void PlayerAttackController::GuardTargetLockOn()
extern void PlayerAttackController_GuardTargetLockOn_m977332FD148DE0D11A06E66634A215066FB92D67 (void);
// 0x000001C9 System.Void PlayerAttackController::GoalTargetLockOn()
extern void PlayerAttackController_GoalTargetLockOn_mDE17A6CA6A297E02360D3E0722E749DE63ACAAC5 (void);
// 0x000001CA System.Void PlayerAttackController::LinkParticleOn()
extern void PlayerAttackController_LinkParticleOn_m4E0D6B3069069E44EB8B044C7E452974C3C90C81 (void);
// 0x000001CB System.Void PlayerAttackController::LinkParticleOff()
extern void PlayerAttackController_LinkParticleOff_m2E4D28EAA0D4DB1FE6BD32D47BEDB16F799585DD (void);
// 0x000001CC System.Void PlayerAttackController::PlayerAttackPointRotate()
extern void PlayerAttackController_PlayerAttackPointRotate_mCC22869E78DB236E6E0DD56AFF7DE4AEE59709EE (void);
// 0x000001CD System.Void PlayerAttackController::LinkParticleOffDestroyCall()
extern void PlayerAttackController_LinkParticleOffDestroyCall_mF0916C0ACAC2621E6437E2494A37A462D0FA4BB4 (void);
// 0x000001CE System.Void PlayerAttackController::AttackPointTargetReset()
extern void PlayerAttackController_AttackPointTargetReset_mE59E72DFAF83061B2B6C28D3BD6C5A0F18B3717F (void);
// 0x000001CF System.Void PlayerAttackController::WizardEngineStart()
extern void PlayerAttackController_WizardEngineStart_m5C7ACBC6CADE6D5DC730191C59B330147F310BE4 (void);
// 0x000001D0 System.Void PlayerAttackController::WizardEngineEnd()
extern void PlayerAttackController_WizardEngineEnd_mF027CB858BC801B567C47A41048D5753F8565A3D (void);
// 0x000001D1 System.Void PlayerAttackController::WizardEngineActive()
extern void PlayerAttackController_WizardEngineActive_m9498AF09D012E15692DA37A7133AB136F39BD190 (void);
// 0x000001D2 System.Void PlayerAttackController::WizardEngineActiveOff()
extern void PlayerAttackController_WizardEngineActiveOff_mF9ABDB7F2628658D2DDAF9203229E6DEEAE3C5A8 (void);
// 0x000001D3 System.Void PlayerAttackController::.ctor()
extern void PlayerAttackController__ctor_mCFB05B37EBD42F1C3F900C10661243D0232A2179 (void);
// 0x000001D4 System.Void PlayerAttackController/<ChangeAttackShell1Coroutine>d__65::.ctor(System.Int32)
extern void U3CChangeAttackShell1CoroutineU3Ed__65__ctor_mEA99C9D7C5714D6430C99AF87E2AAFE5BD16039A (void);
// 0x000001D5 System.Void PlayerAttackController/<ChangeAttackShell1Coroutine>d__65::System.IDisposable.Dispose()
extern void U3CChangeAttackShell1CoroutineU3Ed__65_System_IDisposable_Dispose_mE02A793DACD6B4D9FA3085B67D7525155F417742 (void);
// 0x000001D6 System.Boolean PlayerAttackController/<ChangeAttackShell1Coroutine>d__65::MoveNext()
extern void U3CChangeAttackShell1CoroutineU3Ed__65_MoveNext_m64B8663473919DDDC44B772DCDF7DF80FA3852C3 (void);
// 0x000001D7 System.Object PlayerAttackController/<ChangeAttackShell1Coroutine>d__65::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChangeAttackShell1CoroutineU3Ed__65_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B7DD7B010562B0ED1EEDA93D3AEEFE416A6F71C (void);
// 0x000001D8 System.Void PlayerAttackController/<ChangeAttackShell1Coroutine>d__65::System.Collections.IEnumerator.Reset()
extern void U3CChangeAttackShell1CoroutineU3Ed__65_System_Collections_IEnumerator_Reset_mE85601A9A6A6514C2500254F5930AD48C4BF7F48 (void);
// 0x000001D9 System.Object PlayerAttackController/<ChangeAttackShell1Coroutine>d__65::System.Collections.IEnumerator.get_Current()
extern void U3CChangeAttackShell1CoroutineU3Ed__65_System_Collections_IEnumerator_get_Current_mE11BFCEE9378889EB783DE08FD36AE4A4457C468 (void);
// 0x000001DA System.Void PlayerAttackController/<WizardEngineAttackShell1Coroutine>d__66::.ctor(System.Int32)
extern void U3CWizardEngineAttackShell1CoroutineU3Ed__66__ctor_mB1D7ECF4B87DFAD5DA9DC301A3035AF18132DC81 (void);
// 0x000001DB System.Void PlayerAttackController/<WizardEngineAttackShell1Coroutine>d__66::System.IDisposable.Dispose()
extern void U3CWizardEngineAttackShell1CoroutineU3Ed__66_System_IDisposable_Dispose_m4BF4CF66C62BD21CB997737EF11F13B526E3A400 (void);
// 0x000001DC System.Boolean PlayerAttackController/<WizardEngineAttackShell1Coroutine>d__66::MoveNext()
extern void U3CWizardEngineAttackShell1CoroutineU3Ed__66_MoveNext_mF35EDCF66890120A9B41BDF779E70EA857E789B0 (void);
// 0x000001DD System.Object PlayerAttackController/<WizardEngineAttackShell1Coroutine>d__66::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWizardEngineAttackShell1CoroutineU3Ed__66_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A8E19243F6A9EA2A88AA1973000D7D973E5F95D (void);
// 0x000001DE System.Void PlayerAttackController/<WizardEngineAttackShell1Coroutine>d__66::System.Collections.IEnumerator.Reset()
extern void U3CWizardEngineAttackShell1CoroutineU3Ed__66_System_Collections_IEnumerator_Reset_m8CEA498248B69A956EFB7CE5D829CFA064930BD9 (void);
// 0x000001DF System.Object PlayerAttackController/<WizardEngineAttackShell1Coroutine>d__66::System.Collections.IEnumerator.get_Current()
extern void U3CWizardEngineAttackShell1CoroutineU3Ed__66_System_Collections_IEnumerator_get_Current_mAC07F3EA1C2A02042B84DB4991C60507C6610F1B (void);
// 0x000001E0 System.Void PlayerAttackController/<ChangeAttackShell2Coroutine>d__71::.ctor(System.Int32)
extern void U3CChangeAttackShell2CoroutineU3Ed__71__ctor_m111FF37FC4C37BD764F24D41A9A753F053673CAB (void);
// 0x000001E1 System.Void PlayerAttackController/<ChangeAttackShell2Coroutine>d__71::System.IDisposable.Dispose()
extern void U3CChangeAttackShell2CoroutineU3Ed__71_System_IDisposable_Dispose_m1A0AF9E29CFF86A10EA1743A368BFB26F15E6CCB (void);
// 0x000001E2 System.Boolean PlayerAttackController/<ChangeAttackShell2Coroutine>d__71::MoveNext()
extern void U3CChangeAttackShell2CoroutineU3Ed__71_MoveNext_mDB2CF249E0BC7F5FA5010E021643461923D81A2A (void);
// 0x000001E3 System.Object PlayerAttackController/<ChangeAttackShell2Coroutine>d__71::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChangeAttackShell2CoroutineU3Ed__71_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m861585B43C33615048BF4A80CC95467FCD9F04C4 (void);
// 0x000001E4 System.Void PlayerAttackController/<ChangeAttackShell2Coroutine>d__71::System.Collections.IEnumerator.Reset()
extern void U3CChangeAttackShell2CoroutineU3Ed__71_System_Collections_IEnumerator_Reset_m4F27F921D1D506B180686487E60C248FC69336D8 (void);
// 0x000001E5 System.Object PlayerAttackController/<ChangeAttackShell2Coroutine>d__71::System.Collections.IEnumerator.get_Current()
extern void U3CChangeAttackShell2CoroutineU3Ed__71_System_Collections_IEnumerator_get_Current_m85C95E37ECF10918D185596AACD8D424913C7FD6 (void);
// 0x000001E6 System.Void PlayerAttackController/<WizardEngineChangeAttackShell2Coroutine>d__72::.ctor(System.Int32)
extern void U3CWizardEngineChangeAttackShell2CoroutineU3Ed__72__ctor_m1E0151D525ED78D68A0D0B43D14DE3D24A3A29FA (void);
// 0x000001E7 System.Void PlayerAttackController/<WizardEngineChangeAttackShell2Coroutine>d__72::System.IDisposable.Dispose()
extern void U3CWizardEngineChangeAttackShell2CoroutineU3Ed__72_System_IDisposable_Dispose_m656C603C27C6719D257DE01D8F31938FD90E27AE (void);
// 0x000001E8 System.Boolean PlayerAttackController/<WizardEngineChangeAttackShell2Coroutine>d__72::MoveNext()
extern void U3CWizardEngineChangeAttackShell2CoroutineU3Ed__72_MoveNext_m3ECE58B9DF24C9F0A7FDF9F84D7044AA27B7317F (void);
// 0x000001E9 System.Object PlayerAttackController/<WizardEngineChangeAttackShell2Coroutine>d__72::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWizardEngineChangeAttackShell2CoroutineU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m11AF48CF0C362EC496614E96FED01E96F31D62A7 (void);
// 0x000001EA System.Void PlayerAttackController/<WizardEngineChangeAttackShell2Coroutine>d__72::System.Collections.IEnumerator.Reset()
extern void U3CWizardEngineChangeAttackShell2CoroutineU3Ed__72_System_Collections_IEnumerator_Reset_m153D3C6D28017AA2788F7CB651B25712565B8331 (void);
// 0x000001EB System.Object PlayerAttackController/<WizardEngineChangeAttackShell2Coroutine>d__72::System.Collections.IEnumerator.get_Current()
extern void U3CWizardEngineChangeAttackShell2CoroutineU3Ed__72_System_Collections_IEnumerator_get_Current_m286A3876658DCDD71E1E2E3156620E28E69C10A1 (void);
// 0x000001EC System.Void PlayerController::Start()
extern void PlayerController_Start_m1D83076E8B136A71051F2F02545EE04947D3A8CF (void);
// 0x000001ED System.Void PlayerController::OnEnable()
extern void PlayerController_OnEnable_m1558047F72022F1A32FC373A927E122D1F3CF5CC (void);
// 0x000001EE System.Void PlayerController::FixedUpdate()
extern void PlayerController_FixedUpdate_m6D906D8B13844542B81CC49BA19760F747CEC8C0 (void);
// 0x000001EF System.Void PlayerController::OnDisable()
extern void PlayerController_OnDisable_m16524B89F1DDB4EB1E201CCEB7E9502967BF57DB (void);
// 0x000001F0 System.Void PlayerController::OnCollisionEnter(UnityEngine.Collision)
extern void PlayerController_OnCollisionEnter_m2FC01282FAEE546C4408E6B901DBB3EB5A5BF989 (void);
// 0x000001F1 System.Void PlayerController::OnCollisionStay(UnityEngine.Collision)
extern void PlayerController_OnCollisionStay_m1DCD5D8E99C72E2B5DE2E2F828C9C3504E31DE22 (void);
// 0x000001F2 System.Void PlayerController::OnCollisionExit(UnityEngine.Collision)
extern void PlayerController_OnCollisionExit_m4DF3784586ED7D4C4E7E053117AE5406FD71A559 (void);
// 0x000001F3 System.Void PlayerController::OnTriggerEnter(UnityEngine.Collider)
extern void PlayerController_OnTriggerEnter_mF2704872AEA62333341DCBEA2831320C385701D1 (void);
// 0x000001F4 System.Void PlayerController::DamageEffectPoolSearch()
extern void PlayerController_DamageEffectPoolSearch_mCE682144CD64276C4BD81488E57E98A2334188C8 (void);
// 0x000001F5 System.Void PlayerController::DamageEffectInstantiate()
extern void PlayerController_DamageEffectInstantiate_m9F2E8388CEDBFA4B30446B3B53FBE603C5CF51C9 (void);
// 0x000001F6 UnityEngine.GameObject PlayerController::GetDamageEffect()
extern void PlayerController_GetDamageEffect_mF25D057FB6186CFCB9994F2DBF41CFEF99A49148 (void);
// 0x000001F7 System.Void PlayerController::DamageEffectActive()
extern void PlayerController_DamageEffectActive_m7535005B73CF1A8CFE83CB7D5E55BA0725700357 (void);
// 0x000001F8 System.Void PlayerController::PlayerLevelCalledOne()
extern void PlayerController_PlayerLevelCalledOne_m7A807FB09899445F32411F415E9DEC54C6E85733 (void);
// 0x000001F9 System.Void PlayerController::SkillPointPlus()
extern void PlayerController_SkillPointPlus_mC83952ACD7043FC872346017086C26502B28AA4D (void);
// 0x000001FA System.Void PlayerController::PlayerMove()
extern void PlayerController_PlayerMove_m130CD8F4AEE12FD9853ACF892EE0417E5ECA5F3B (void);
// 0x000001FB System.Void PlayerController::PlayerRotate()
extern void PlayerController_PlayerRotate_m6E8CE70141992717778C58F28E89C084A704C687 (void);
// 0x000001FC System.Void PlayerController::PlayerJumpButton()
extern void PlayerController_PlayerJumpButton_m07EDAC299C60F528D39B17D663C4C70D87768E77 (void);
// 0x000001FD System.Void PlayerController::PlayerJumpPowerAdjustment()
extern void PlayerController_PlayerJumpPowerAdjustment_mA1E9D022D0D71F81EACB64C4DCF699EDBE271C19 (void);
// 0x000001FE System.Void PlayerController::EnemyCollideDamage()
extern void PlayerController_EnemyCollideDamage_mD36CF39C4CEBB0616FC5CE48EF0FC992A4AB5818 (void);
// 0x000001FF System.Void PlayerController::EnemyAttackDamage1()
extern void PlayerController_EnemyAttackDamage1_m70742D23038A948984CFD2D8530E20E29408BFAB (void);
// 0x00000200 System.Void PlayerController::BossAttackDamage1()
extern void PlayerController_BossAttackDamage1_m1D5FEABC6C9E82DED9D05EA2A7C9996379CCDD27 (void);
// 0x00000201 System.Void PlayerController::BossAttackDamage2()
extern void PlayerController_BossAttackDamage2_m5D2C311AE766B3A7D55B08EFF87BC1131D35E0AC (void);
// 0x00000202 System.Void PlayerController::SliderCurrentEngneerPointCall()
extern void PlayerController_SliderCurrentEngneerPointCall_m81DEC187336BD2C75D8DA8F4A41C02C7C727A60E (void);
// 0x00000203 System.Void PlayerController::EngineerPointMaxActionCall()
extern void PlayerController_EngineerPointMaxActionCall_mE5DD443C625994872A1F2718E286ABB07A553347 (void);
// 0x00000204 System.Void PlayerController::EngineerPointPlusDamage()
extern void PlayerController_EngineerPointPlusDamage_mD9A3ACC91E2CD485085648A535CF1FD65084F49C (void);
// 0x00000205 System.Void PlayerController::EngineerPointPlusNomal()
extern void PlayerController_EngineerPointPlusNomal_mA191FB5A4C3C957B46FB55FF1496F961CD7C007E (void);
// 0x00000206 System.Void PlayerController::EngineerPointPlusCharge1()
extern void PlayerController_EngineerPointPlusCharge1_mC06561D327FEC153B5C553C98A3D569B8B328300 (void);
// 0x00000207 System.Void PlayerController::EngineerPointPlusCharge2()
extern void PlayerController_EngineerPointPlusCharge2_m71319E5CD8FCC102377C8A80290AD3AC41641B56 (void);
// 0x00000208 System.Void PlayerController::EngineerPointPlusCharge3()
extern void PlayerController_EngineerPointPlusCharge3_m45C620A5176301E90570A4C7551A28D37EDFE51A (void);
// 0x00000209 UnityEngine.Vector3 PlayerController::GetAngleVector(UnityEngine.GameObject,UnityEngine.GameObject)
extern void PlayerController_GetAngleVector_m818ED8A36FD7FD12F85826A1EF837C9A3977C9C6 (void);
// 0x0000020A System.Void PlayerController::PlayerRetryButtonActiveOnInvoke()
extern void PlayerController_PlayerRetryButtonActiveOnInvoke_m490C08B484DC3ED9C04B63A5A2F8D0B03F893586 (void);
// 0x0000020B System.Void PlayerController::PlayerRetryButtonActiveOff()
extern void PlayerController_PlayerRetryButtonActiveOff_m32100753D617FAB8ED74112FAB6FCC05AB8DF47E (void);
// 0x0000020C System.Void PlayerController::PlayerActiveOnButton()
extern void PlayerController_PlayerActiveOnButton_mBC1450F0410E4EA55797EFBDBF98A366B59FF80B (void);
// 0x0000020D System.Void PlayerController::PlayerRetry()
extern void PlayerController_PlayerRetry_m23DDDE3F23ECF2ED38BF1287FA7CF58359E2FAF4 (void);
// 0x0000020E System.Void PlayerController::PlayerHpEmpty()
extern void PlayerController_PlayerHpEmpty_mBB23FE99F4AFB306A55AA557B5839A2C5D65D035 (void);
// 0x0000020F System.Void PlayerController::PlayerActiveOff()
extern void PlayerController_PlayerActiveOff_m71E8CAFA1731F207FF35A68F262EAC7123634F9F (void);
// 0x00000210 System.Void PlayerController::EnemyTargetLockOn()
extern void PlayerController_EnemyTargetLockOn_mD7DF1E8898D58DA0300B8A72A8037BF880A9223D (void);
// 0x00000211 System.Void PlayerController::BossTargetLockOn()
extern void PlayerController_BossTargetLockOn_mEF51AB2D4BE5EFBE83B46BE6DA7851727938E72F (void);
// 0x00000212 System.Void PlayerController::BossGuardTargetLockOn()
extern void PlayerController_BossGuardTargetLockOn_mBE5682F7CE38DEC72035904D3A07BFA5A86F166A (void);
// 0x00000213 System.Void PlayerController::GoalTargetLockOn()
extern void PlayerController_GoalTargetLockOn_mF2A6A71E6D05A8F4348FAFC3537B42D09AA0AD25 (void);
// 0x00000214 System.Void PlayerController::TargetLockOnSound()
extern void PlayerController_TargetLockOnSound_m041BF9C7F2251AC8AD4ED46F46F7680A18D178F3 (void);
// 0x00000215 System.Void PlayerController::GetItem()
extern void PlayerController_GetItem_mC05208185EB6CF86F6013996A52003FD4918D0F4 (void);
// 0x00000216 System.Void PlayerController::ConstrainsFreezeRotationOn()
extern void PlayerController_ConstrainsFreezeRotationOn_m6A2143B0C1A2DEAC67BCAD353874D1827FAC088A (void);
// 0x00000217 System.Void PlayerController::MoveWallOnLock()
extern void PlayerController_MoveWallOnLock_m3297C3F3A675762047179D67DC4CD0A4FCC703A6 (void);
// 0x00000218 System.Void PlayerController::UserTextEmptyAnimation()
extern void PlayerController_UserTextEmptyAnimation_mF98987D0A34BE5D9EBC9D03A667A7B37E4F27BFF (void);
// 0x00000219 System.Void PlayerController::UserTextEmpty()
extern void PlayerController_UserTextEmpty_m0312EF6DDBC7F99CFA3143011F9868A6CB85A887 (void);
// 0x0000021A System.Void PlayerController::PlayerAvatarSaving()
extern void PlayerController_PlayerAvatarSaving_mE937D704D0A3670E997A81471D8E761FBB413568 (void);
// 0x0000021B System.Void PlayerController::BusterSynchroAvatarChange()
extern void PlayerController_BusterSynchroAvatarChange_mA71748F6646EA93DB5552147F3B60151856E66A8 (void);
// 0x0000021C System.Void PlayerController::PlayerAvatarChangeBack()
extern void PlayerController_PlayerAvatarChangeBack_mC088A44C6841D8001F996423EF0A4F3C4A822DA7 (void);
// 0x0000021D System.Void PlayerController::BusterSynchroAnimation()
extern void PlayerController_BusterSynchroAnimation_m992B23637984AD685FF532201AB4A344E12E5298 (void);
// 0x0000021E System.Void PlayerController::WizardEngineAnimation()
extern void PlayerController_WizardEngineAnimation_m6F850F76E541554386AAFCCB6821D19BDB4AB45E (void);
// 0x0000021F System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mDDAB7C7D82E1A5B3E6C197B1AB9D653DFE554F33 (void);
// 0x00000220 System.Void PlayerHelthBar::Start()
extern void PlayerHelthBar_Start_m52091C67E589118483B8B49CC080E6CFFEF160B5 (void);
// 0x00000221 System.Void PlayerHelthBar::FixedUpdate()
extern void PlayerHelthBar_FixedUpdate_m9203ADA63B11ECAC6CDB202A64180B672CEA88A5 (void);
// 0x00000222 System.Void PlayerHelthBar::SliderCurrentHP()
extern void PlayerHelthBar_SliderCurrentHP_mBE751CD7180ED158662A92621FA73F01895A3D65 (void);
// 0x00000223 System.Void PlayerHelthBar::SliderCurrentHpCrisis()
extern void PlayerHelthBar_SliderCurrentHpCrisis_mFC3FFB4569F4742A6A5B0583771676E5DC0541C2 (void);
// 0x00000224 System.Void PlayerHelthBar::SliderCurrentHpColorReset()
extern void PlayerHelthBar_SliderCurrentHpColorReset_m37368F50247502D8D36DFD98C952CCD43AA2B74A (void);
// 0x00000225 System.Void PlayerHelthBar::.ctor()
extern void PlayerHelthBar__ctor_m7CD615D8DA3C42ACFE2427EA36E7B5D6B7A3038D (void);
// 0x00000226 System.Void PlayerShellController::OnEnable()
extern void PlayerShellController_OnEnable_m13C566623877A4B34996D0FE19C14F2491E7DD4B (void);
// 0x00000227 System.Void PlayerShellController::FixedUpdate()
extern void PlayerShellController_FixedUpdate_mFD2A59DB8F9BD41E1C97CB3A7FCE40FCED96CB9D (void);
// 0x00000228 System.Void PlayerShellController::OnCollisionEnter(UnityEngine.Collision)
extern void PlayerShellController_OnCollisionEnter_m4DE03A474B323C5229FE7C008CBA716CFC2F8060 (void);
// 0x00000229 System.Void PlayerShellController::OnDisable()
extern void PlayerShellController_OnDisable_m222E7028729A6AF2148B838682925D8F3C309EF6 (void);
// 0x0000022A System.Void PlayerShellController::AttackShot()
extern void PlayerShellController_AttackShot_m3234BA9F4E4FC141966EA0950C631108C3739E52 (void);
// 0x0000022B System.Void PlayerShellController::ShotLifeTimeReset()
extern void PlayerShellController_ShotLifeTimeReset_mE7354CAB71259B18ED1D932207E3F5AFBC187933 (void);
// 0x0000022C System.Void PlayerShellController::ShellActiveOff()
extern void PlayerShellController_ShellActiveOff_m2E0D23BCAB4F75B2BE0D98FB419C9B53A0502C7A (void);
// 0x0000022D System.Void PlayerShellController::ShellActiveOffWithGround()
extern void PlayerShellController_ShellActiveOffWithGround_mFA2D7629CB2378D9615422BAEF214D7326F4FD35 (void);
// 0x0000022E System.Void PlayerShellController::.ctor()
extern void PlayerShellController__ctor_m924438219A3828E221268035E1907C715C1E96B3 (void);
// 0x0000022F System.Void PlayerStatusController::Start()
extern void PlayerStatusController_Start_mE80B01F99BB2A56F11E540CF66A8234DC88EDBFE (void);
// 0x00000230 System.Void PlayerStatusController::PlayerSkillMinus()
extern void PlayerStatusController_PlayerSkillMinus_m8E2078011A53382AF856AC07372B712D79BF8B13 (void);
// 0x00000231 System.Void PlayerStatusController::PlayerSkillPointText()
extern void PlayerStatusController_PlayerSkillPointText_m65B1358B8A748CCEE496CA0EEE31DC88969E19FD (void);
// 0x00000232 System.Void PlayerStatusController::PlayerStatusUpSE()
extern void PlayerStatusController_PlayerStatusUpSE_m8E18930C28EB043530E60403491AF7F12217DF4E (void);
// 0x00000233 System.Void PlayerStatusController::MoveSpeedPointPlus()
extern void PlayerStatusController_MoveSpeedPointPlus_mE345E8483161ABCE86D60B8425C38A99DF717AB7 (void);
// 0x00000234 System.Void PlayerStatusController::MoveButtonShakeScaleAnimation()
extern void PlayerStatusController_MoveButtonShakeScaleAnimation_m1C84F806D81D892A747885E541E69DA821A060C3 (void);
// 0x00000235 System.Void PlayerStatusController::MoveButtonShakeScaleReset()
extern void PlayerStatusController_MoveButtonShakeScaleReset_m962A8F71D4C4D9A4F4E1E0016887054398CEBA21 (void);
// 0x00000236 System.Void PlayerStatusController::MoveSpeedPointPlusText()
extern void PlayerStatusController_MoveSpeedPointPlusText_m0591C6500F4EEDC767BB401CA726C4F79E4358CF (void);
// 0x00000237 System.Void PlayerStatusController::MoveSpeedUpButtonDown()
extern void PlayerStatusController_MoveSpeedUpButtonDown_m078CD7C52494C3646ACD5D8F2A3E5970FBE5A4D9 (void);
// 0x00000238 System.Void PlayerStatusController::MoveSpeedUpParameter(System.Single,System.Single,System.Single)
extern void PlayerStatusController_MoveSpeedUpParameter_m59AD0D8089EFF652DEA82586429D5F6294DDF455 (void);
// 0x00000239 System.Void PlayerStatusController::MoveSpeedUpStatus()
extern void PlayerStatusController_MoveSpeedUpStatus_mCE9665380777C89244BCDCE39E10CEA6FD935308 (void);
// 0x0000023A System.Void PlayerStatusController::AttackSpeedPointPlus()
extern void PlayerStatusController_AttackSpeedPointPlus_m969E87CDE1F4786CF5734FC8B16FE85C9ADE1329 (void);
// 0x0000023B System.Void PlayerStatusController::AttackSpeedButtonShakeScaleAnimation()
extern void PlayerStatusController_AttackSpeedButtonShakeScaleAnimation_mC1885F9F765D78E12974604B76FDD2A183BB63E0 (void);
// 0x0000023C System.Void PlayerStatusController::AttackSpeedButtonShakeScaleReset()
extern void PlayerStatusController_AttackSpeedButtonShakeScaleReset_m5E0E1F581803FDEF34A6BD3855A28874E64ED005 (void);
// 0x0000023D System.Void PlayerStatusController::AttackSpeedPointPlusText()
extern void PlayerStatusController_AttackSpeedPointPlusText_mB7C836FD1C487A278338E1605401D842B8EE255E (void);
// 0x0000023E System.Void PlayerStatusController::AttackSpeedUpButtonDown()
extern void PlayerStatusController_AttackSpeedUpButtonDown_m9A47F5B6AB73E6FDF06199764C64D3C2EAEF70CE (void);
// 0x0000023F System.Void PlayerStatusController::AttackSpeedUpParameter(System.Single)
extern void PlayerStatusController_AttackSpeedUpParameter_m51E8DC3A96AF6E9681B323C63B416610E0A9D0CA (void);
// 0x00000240 System.Void PlayerStatusController::AttackSpeedUpStatus()
extern void PlayerStatusController_AttackSpeedUpStatus_mEF21DF5018797236BA6A404EF74478EA5D3D4E8F (void);
// 0x00000241 System.Void PlayerStatusController::AttackPowerPointPlus()
extern void PlayerStatusController_AttackPowerPointPlus_mB8572503917FAEC62E5E06EE35E23B40B47BDBD5 (void);
// 0x00000242 System.Void PlayerStatusController::AttackPowerButtonShakeScaleAnimation()
extern void PlayerStatusController_AttackPowerButtonShakeScaleAnimation_m1C983CBA77B4728119590438CE650C43AA7CF05F (void);
// 0x00000243 System.Void PlayerStatusController::AttackPowerButtonShakeScaleReset()
extern void PlayerStatusController_AttackPowerButtonShakeScaleReset_mACE582EE01B77A0E8B93A593631E12B1B5F477ED (void);
// 0x00000244 System.Void PlayerStatusController::AttackPowerPointPlusText()
extern void PlayerStatusController_AttackPowerPointPlusText_mAE29293DAB6874F4A3FE2E32882853DA06E65B26 (void);
// 0x00000245 System.Void PlayerStatusController::AttackPowerUpButtonDown()
extern void PlayerStatusController_AttackPowerUpButtonDown_m4BAC9C1A2E0643B4109CADC1AE6E91E43A531870 (void);
// 0x00000246 System.Void PlayerStatusController::AttackPowerUpParameter(System.Single,System.Single,System.Single,System.Single)
extern void PlayerStatusController_AttackPowerUpParameter_m2F49AC6FBFE4DF5969B66EDF67800209179C2200 (void);
// 0x00000247 System.Void PlayerStatusController::AttackPowerUpStatus()
extern void PlayerStatusController_AttackPowerUpStatus_m457C1E398F0FEB515EB3A095F6F4F953DF291014 (void);
// 0x00000248 System.Void PlayerStatusController::Charge1SkillPointPlus()
extern void PlayerStatusController_Charge1SkillPointPlus_m8B898986867DA8D4C59CC44615CC87E25BADCFC7 (void);
// 0x00000249 System.Void PlayerStatusController::Charge1SkillPointPlusText()
extern void PlayerStatusController_Charge1SkillPointPlusText_m2A106A55D70072DC19F0EA77C9BB8E312636376F (void);
// 0x0000024A System.Void PlayerStatusController::Charge1SkillButtonShakeScaleAnimation()
extern void PlayerStatusController_Charge1SkillButtonShakeScaleAnimation_m46C28A643A2EEBE8338A674CFCFE2CBE75912294 (void);
// 0x0000024B System.Void PlayerStatusController::Charge1SkillButtonShakeScaleReset()
extern void PlayerStatusController_Charge1SkillButtonShakeScaleReset_m377F8B9EFF39E1A2FA6B2F7F15BA39AD057A38E2 (void);
// 0x0000024C System.Void PlayerStatusController::Charge1SkillUpButtonDown()
extern void PlayerStatusController_Charge1SkillUpButtonDown_mF245A9D9B8DF5925C85B3E156ABEBFEC02C3B890 (void);
// 0x0000024D System.Void PlayerStatusController::Charge1SkillUpParameter(System.Int32)
extern void PlayerStatusController_Charge1SkillUpParameter_mFDF720EAB38505CC61861B3E9CD8B9376B379A94 (void);
// 0x0000024E System.Void PlayerStatusController::Charge1SkillUpStatus()
extern void PlayerStatusController_Charge1SkillUpStatus_m10107FB563BCEE242C89A463875DA1D6F2230AF7 (void);
// 0x0000024F System.Void PlayerStatusController::Charge2SkillPointPlus()
extern void PlayerStatusController_Charge2SkillPointPlus_m9B0A22307117262FD4292F53F56755F146A14F8B (void);
// 0x00000250 System.Void PlayerStatusController::Charge2SkillButtonShakeScaleAnimation()
extern void PlayerStatusController_Charge2SkillButtonShakeScaleAnimation_mA933FCC3DE40CF69D4D38F7B61F6178322413E66 (void);
// 0x00000251 System.Void PlayerStatusController::Charge2SkillButtonShakeScaleReset()
extern void PlayerStatusController_Charge2SkillButtonShakeScaleReset_m7A98792A8899306BB606BE6BB732B95BFE242C9D (void);
// 0x00000252 System.Void PlayerStatusController::Charge2SkillPointPlusText()
extern void PlayerStatusController_Charge2SkillPointPlusText_m4718988F6F37C46BAE101A36D3789D689FE1B2D8 (void);
// 0x00000253 System.Void PlayerStatusController::Charge2SkillUpButtonDown()
extern void PlayerStatusController_Charge2SkillUpButtonDown_mB5DD66E70E7086F5E3B04217202437AF74F9436E (void);
// 0x00000254 System.Void PlayerStatusController::Charge2SkillUpParameter(System.Int32,System.Single)
extern void PlayerStatusController_Charge2SkillUpParameter_m56242DA2FE2FCD33E5EAD53C40C95F24969F986E (void);
// 0x00000255 System.Void PlayerStatusController::Charge2SkillUpStatus()
extern void PlayerStatusController_Charge2SkillUpStatus_mF7146D4B80A8A0450A1DBA34D51C6D8DD97ADCEE (void);
// 0x00000256 System.Void PlayerStatusController::Charge3SkillPointPlus()
extern void PlayerStatusController_Charge3SkillPointPlus_m2ECD39C6620C2F32AA4E1C0838025F3ECD428F0E (void);
// 0x00000257 System.Void PlayerStatusController::Charge3SkillButtonShakeScaleAnimation()
extern void PlayerStatusController_Charge3SkillButtonShakeScaleAnimation_m18959EE167BDF9D086F638073298779B1313E9B1 (void);
// 0x00000258 System.Void PlayerStatusController::Charge3SkillButtonShakeScaleReset()
extern void PlayerStatusController_Charge3SkillButtonShakeScaleReset_mBE7F323975F52EE49E351A4C2BE484A474FCCC34 (void);
// 0x00000259 System.Void PlayerStatusController::Charge3SkillPointPlusText()
extern void PlayerStatusController_Charge3SkillPointPlusText_m3B845D20C71ABB3E0DF669365B885FEB11C26FD6 (void);
// 0x0000025A System.Void PlayerStatusController::Charge3SkillUpButtonDown()
extern void PlayerStatusController_Charge3SkillUpButtonDown_mB0B0F6BD83F06A0BC76FF78B3636050E2CE6ECA7 (void);
// 0x0000025B System.Void PlayerStatusController::Charge3SkillUpParameter(System.Single,System.Single)
extern void PlayerStatusController_Charge3SkillUpParameter_mAE0623F16D9AB8AEC79439073A43D26866521B5A (void);
// 0x0000025C System.Void PlayerStatusController::Charge3SkillUpStatus()
extern void PlayerStatusController_Charge3SkillUpStatus_m7E9A65B9427E09F35D40C1124F65268B25913E7B (void);
// 0x0000025D System.Void PlayerStatusController::CurrentStatusSaving()
extern void PlayerStatusController_CurrentStatusSaving_m2C4DC185484A0467348667DBD2473C79EEC70FD2 (void);
// 0x0000025E System.Void PlayerStatusController::BusterSynchroStatusUp()
extern void PlayerStatusController_BusterSynchroStatusUp_m2F4390DED8BADD2C61B2C3760654C858A86CBF33 (void);
// 0x0000025F System.Void PlayerStatusController::BusterSynchroStatusCurrentBack()
extern void PlayerStatusController_BusterSynchroStatusCurrentBack_m4179E9C55FC03F0A08D05C592C42A093C0095328 (void);
// 0x00000260 System.Void PlayerStatusController::BusterSynchroStart()
extern void PlayerStatusController_BusterSynchroStart_mC57E5A5F1461944521A31C298D4DC48EF065A63A (void);
// 0x00000261 System.Void PlayerStatusController::BusterSynchroEnd()
extern void PlayerStatusController_BusterSynchroEnd_mCF0D2FEE74AD1CAC57EE44558447D0FF91B05843 (void);
// 0x00000262 System.Void PlayerStatusController::BusterSynchroStartSound()
extern void PlayerStatusController_BusterSynchroStartSound_m7E002ABBC4BC34CEA1E5338D29BE78A2D3D323E4 (void);
// 0x00000263 System.Void PlayerStatusController::NomalSynchroSkinActiveOn()
extern void PlayerStatusController_NomalSynchroSkinActiveOn_mC1EA5B7921CDFC8B747ED4B4A6E493E177ED06F2 (void);
// 0x00000264 System.Void PlayerStatusController::NomalSynchroSkinActiveOff()
extern void PlayerStatusController_NomalSynchroSkinActiveOff_mC074403548614DCDA5200766BEDE68AF047311C0 (void);
// 0x00000265 System.Void PlayerStatusController::BusterSynchroSkinActiveOn()
extern void PlayerStatusController_BusterSynchroSkinActiveOn_mC882CA14ABF1A7D8CF8CDCA7062A86F9CB650719 (void);
// 0x00000266 System.Void PlayerStatusController::BusterSynchroEffectActive()
extern void PlayerStatusController_BusterSynchroEffectActive_m3976BD61124DAA6A51D3F70E1FBDAE4D6BCB252D (void);
// 0x00000267 System.Void PlayerStatusController::BusterSynchroEndEffect()
extern void PlayerStatusController_BusterSynchroEndEffect_m3F826205BFA14AC1FC6BB3BE8C6858BD9ECB15C0 (void);
// 0x00000268 System.Void PlayerStatusController::BusterSynchroSkinActiveOff()
extern void PlayerStatusController_BusterSynchroSkinActiveOff_m1C601EA15B90AAC975CFCBCA36D6E30EFC2F380D (void);
// 0x00000269 System.Void PlayerStatusController::PlayerAttackPointPositionCurrentSaving()
extern void PlayerStatusController_PlayerAttackPointPositionCurrentSaving_m8BEBD378EC8699F5356CDDCE8CE40D598BD59CE4 (void);
// 0x0000026A System.Void PlayerStatusController::BusterSynchroPlayerAttackPointMeshActiveOff()
extern void PlayerStatusController_BusterSynchroPlayerAttackPointMeshActiveOff_mAA249A508588518840C155F38CEE5DD362E316D8 (void);
// 0x0000026B System.Void PlayerStatusController::BusterSynchroPlayerAttackPointMeshActiveOn()
extern void PlayerStatusController_BusterSynchroPlayerAttackPointMeshActiveOn_m4AB96750F3060F83DAF68D59EB0C25EF8C5FB243 (void);
// 0x0000026C System.Void PlayerStatusController::BusterSynchroPlayerAttackPointPosition()
extern void PlayerStatusController_BusterSynchroPlayerAttackPointPosition_mE16E9C1600BA412D59D70026C77D5A886111ED6C (void);
// 0x0000026D System.Void PlayerStatusController::BusterSynchroButtonsShakeScaleAnimation()
extern void PlayerStatusController_BusterSynchroButtonsShakeScaleAnimation_m7B8931D9F2C3ADC0D19C541F6CA4C2B31479F546 (void);
// 0x0000026E System.Void PlayerStatusController::WizardEngineStart()
extern void PlayerStatusController_WizardEngineStart_mCEE77A8A5C10FFE548C9BFA0930CAA5C2229A628 (void);
// 0x0000026F System.Void PlayerStatusController::WizardEngineEnd()
extern void PlayerStatusController_WizardEngineEnd_m8F395DE56724DA6658CDF4B29DA82D9DF697F02B (void);
// 0x00000270 System.Void PlayerStatusController::WizardEngineEffectActive()
extern void PlayerStatusController_WizardEngineEffectActive_m3DAB33B6DE0EEB54BD5773C8C675763D911ECBF7 (void);
// 0x00000271 System.Void PlayerStatusController::WizardEngineEndEffect()
extern void PlayerStatusController_WizardEngineEndEffect_m16CA3C3D4446F16156D1FE9840AF30EE32D7A3B1 (void);
// 0x00000272 System.Void PlayerStatusController::WizardEngineStartSound()
extern void PlayerStatusController_WizardEngineStartSound_m3B70E0B58BC7FE2A42DE5E6F5AEB1DDFB4097CB5 (void);
// 0x00000273 System.Void PlayerStatusController::WizardEnginePlayerAttackPointPosition()
extern void PlayerStatusController_WizardEnginePlayerAttackPointPosition_m0511494F24651D99ABA5C0C64C4AC2C885D92EDF (void);
// 0x00000274 System.Void PlayerStatusController::LimitTimeEndSound()
extern void PlayerStatusController_LimitTimeEndSound_m4050EE8EA1FF4FA653A225F0C0F1AF996ECE61D8 (void);
// 0x00000275 System.Void PlayerStatusController::PlayerAttackPointPositionCurrentBack()
extern void PlayerStatusController_PlayerAttackPointPositionCurrentBack_m72829AD8100BCCD3015D9959CB0911FED3800521 (void);
// 0x00000276 System.Void PlayerStatusController::.ctor()
extern void PlayerStatusController__ctor_mB257CAC62C6A9C50BF6427DA1A21396F64B93785 (void);
// 0x00000277 System.Void ShellPrefabController::OnEnable()
extern void ShellPrefabController_OnEnable_mD7D2C5E0138A6F663C7FE3DEB6DCB063B9F4AF31 (void);
// 0x00000278 System.Void ShellPrefabController::FixedUpdate()
extern void ShellPrefabController_FixedUpdate_m8D753CFF3DD04826EB27105E234ACB6AEF4CC0B6 (void);
// 0x00000279 System.Void ShellPrefabController::OnTriggerEnter(UnityEngine.Collider)
extern void ShellPrefabController_OnTriggerEnter_m36E5FA533ABF4210574D1D565E8D2D80070CF80D (void);
// 0x0000027A System.Void ShellPrefabController::OnDisable()
extern void ShellPrefabController_OnDisable_m5A204DBBA9D5664C4E4E1D226E5C02F40C2DFB23 (void);
// 0x0000027B System.Void ShellPrefabController::AttackShot()
extern void ShellPrefabController_AttackShot_m899EB04DF019EAB248EF6E6E6F9269879573F35E (void);
// 0x0000027C System.Void ShellPrefabController::ShotLifeTimeReset()
extern void ShellPrefabController_ShotLifeTimeReset_m9D959881C6F3EDA03A54F86650263680AD17738B (void);
// 0x0000027D System.Void ShellPrefabController::ShellActiveOff()
extern void ShellPrefabController_ShellActiveOff_m57503D34D718A2B0F6095D48985A404276EB1F14 (void);
// 0x0000027E System.Void ShellPrefabController::ShellActiveOffWithGround()
extern void ShellPrefabController_ShellActiveOffWithGround_m12C654313FFDBCE60AEA795CEB49E5235BB0494E (void);
// 0x0000027F System.Void ShellPrefabController::.ctor()
extern void ShellPrefabController__ctor_m00F524E77518043151F150AFA3463C516639E061 (void);
// 0x00000280 System.Void SliderController::Awake()
extern void SliderController_Awake_m7D561A899B4B85A8CBCE96C945CBF497FFDDEA2B (void);
// 0x00000281 System.Void SliderController::OnEnable()
extern void SliderController_OnEnable_mDDC5B9EBE49D1A0DE877BB5CDCEB937E68AA77AA (void);
// 0x00000282 System.Void SliderController::OnDrag()
extern void SliderController_OnDrag_m6B0D383CA02B03A90781144A2DFDC83925FAB767 (void);
// 0x00000283 System.Void SliderController::TransformScaleActiveAnimation()
extern void SliderController_TransformScaleActiveAnimation_m2F6EEC7085CD326D6E518A6E3F0F8E55CBAB3387 (void);
// 0x00000284 System.Void SliderController::.ctor()
extern void SliderController__ctor_mF9C897ABF6ABCCA22F3A9D0198DC1B64599849B3 (void);
// 0x00000285 System.Void StatusUpEffectController::OnEnable()
extern void StatusUpEffectController_OnEnable_mA46603ED1A3F0A337F87CB74BC44BCF1BC361410 (void);
// 0x00000286 System.Void StatusUpEffectController::FixedUpdate()
extern void StatusUpEffectController_FixedUpdate_mBA05A0B74F8EA2BA91C20AD37244D42271D9A438 (void);
// 0x00000287 System.Void StatusUpEffectController::OnDisable()
extern void StatusUpEffectController_OnDisable_m3FDFA86B7D8EA504E1597DDB57D0322FAEA3861A (void);
// 0x00000288 System.Void StatusUpEffectController::EffectActiveTimeReset()
extern void StatusUpEffectController_EffectActiveTimeReset_m182D88DA58B5790701B4BEFA064FD7BD7B6B1E0B (void);
// 0x00000289 System.Void StatusUpEffectController::StatusUpEffectActiveOff()
extern void StatusUpEffectController_StatusUpEffectActiveOff_m515E1F7D9C05651D9E137F32A835BB55FDC7ED4A (void);
// 0x0000028A System.Void StatusUpEffectController::MoveUpEffect()
extern void StatusUpEffectController_MoveUpEffect_m194B37A2BBC9DFCFD06FE0924CBF039E662EABAA (void);
// 0x0000028B System.Void StatusUpEffectController::SpeedUpEffect()
extern void StatusUpEffectController_SpeedUpEffect_mE3F88A55B197EFF761B68A8AB8BB65BA39ADB3BC (void);
// 0x0000028C System.Void StatusUpEffectController::PowerUpEffect()
extern void StatusUpEffectController_PowerUpEffect_mCC05900EAA690B3A4ECD560A3948789BCA171E0B (void);
// 0x0000028D System.Void StatusUpEffectController::Charge1UpEffect()
extern void StatusUpEffectController_Charge1UpEffect_mFB053702137A1F768A7C7EA86666FC0BBE246D2D (void);
// 0x0000028E System.Void StatusUpEffectController::Charge2UpEffect()
extern void StatusUpEffectController_Charge2UpEffect_mFF42EA89CEE15D82D83707A0FB1C8E737175AD73 (void);
// 0x0000028F System.Void StatusUpEffectController::Charge3UpEffect()
extern void StatusUpEffectController_Charge3UpEffect_mED5F86CCD9CE680AA4145532FFC8042C5F13EE7C (void);
// 0x00000290 System.Void StatusUpEffectController::.ctor()
extern void StatusUpEffectController__ctor_m0EF066873B624D37C7C640F45AECA706AFE1E68E (void);
// 0x00000291 System.Void Timer::FixedUpdate()
extern void Timer_FixedUpdate_mD7CAE6298FC559AD59E2C58D2344766534CC7F50 (void);
// 0x00000292 System.Void Timer::TimeCount()
extern void Timer_TimeCount_mAFF656996A1CAF11956C14B910D35D89C6EC587D (void);
// 0x00000293 System.Void Timer::ClearTimeTextActive()
extern void Timer_ClearTimeTextActive_mC7560C3D5C04A08EB4BAB41273B661400AF881E5 (void);
// 0x00000294 System.Void Timer::.ctor()
extern void Timer__ctor_m5FF13F1DAD0527F97E229A1904A8AD662731C4B5 (void);
// 0x00000295 System.Void TitleStartSceneDirector::Start()
extern void TitleStartSceneDirector_Start_m3842E954B40E27995D262E8BEF6E723022C3C839 (void);
// 0x00000296 System.Void TitleStartSceneDirector::OnClickTitleActiveOff()
extern void TitleStartSceneDirector_OnClickTitleActiveOff_mE3CB366D6141F54F2B2F8C62D22BCACD9CC60AA6 (void);
// 0x00000297 System.Void TitleStartSceneDirector::OnClickTutorialStartButton()
extern void TitleStartSceneDirector_OnClickTutorialStartButton_m9EDABA4C165269CD32879AFC8B98344A0583ED44 (void);
// 0x00000298 System.Void TitleStartSceneDirector::OnClickGameStartButton()
extern void TitleStartSceneDirector_OnClickGameStartButton_mFB780E80308C4F395AFCD4B9EEBB0EC7DC600C5A (void);
// 0x00000299 System.Void TitleStartSceneDirector::OnClickManualViewButton()
extern void TitleStartSceneDirector_OnClickManualViewButton_mC486AB40825725E4D25BE0299D2FD82449599E49 (void);
// 0x0000029A System.Void TitleStartSceneDirector::OnClickManualViewBackButton()
extern void TitleStartSceneDirector_OnClickManualViewBackButton_m5A525F537F2E4B5580ECAEF4EC72F8F586E2A513 (void);
// 0x0000029B System.Void TitleStartSceneDirector::OnClickRankingViewButton()
extern void TitleStartSceneDirector_OnClickRankingViewButton_mCF072B6107C39C4D20B62FC2CD5D68828B153F0A (void);
// 0x0000029C System.Void TitleStartSceneDirector::OnClickRankingViewBackButton()
extern void TitleStartSceneDirector_OnClickRankingViewBackButton_m8E5CF9FB98327E403288AEDE1D78FF445C056D86 (void);
// 0x0000029D System.Void TitleStartSceneDirector::OnClickCreditViewButton()
extern void TitleStartSceneDirector_OnClickCreditViewButton_mC62BF4A96F77D54AC97B4A50657A441038AA8FA5 (void);
// 0x0000029E System.Void TitleStartSceneDirector::OnClickCreditViewBackButton()
extern void TitleStartSceneDirector_OnClickCreditViewBackButton_mDEB6625A68F1E46405A646ADE718B44D5D526D08 (void);
// 0x0000029F System.Void TitleStartSceneDirector::SceneChangeBackImageAnimationOff()
extern void TitleStartSceneDirector_SceneChangeBackImageAnimationOff_mED3B183E119ED3D00393C1948693D3D32BB58D15 (void);
// 0x000002A0 System.Void TitleStartSceneDirector::SceneChangeBackImageSetActiveOffInvoke()
extern void TitleStartSceneDirector_SceneChangeBackImageSetActiveOffInvoke_mA50FED84967A9FD5C7AA34DF473842B8C35CB731 (void);
// 0x000002A1 System.Void TitleStartSceneDirector::StartAnimationTitle()
extern void TitleStartSceneDirector_StartAnimationTitle_mF9F49FC1EB1203F72013D2FB8DDFF8AAE6EF6582 (void);
// 0x000002A2 System.Void TitleStartSceneDirector::TitleTouchStartButtonAnimation()
extern void TitleStartSceneDirector_TitleTouchStartButtonAnimation_mCF3B614BFD56ABE00B59080A008CCB28648635A8 (void);
// 0x000002A3 System.Void TitleStartSceneDirector::TitleTextActiveOffAnimation()
extern void TitleStartSceneDirector_TitleTextActiveOffAnimation_m51B828EE88D1EF5DB1702F17C4F77DB1739FCCDA (void);
// 0x000002A4 System.Void TitleStartSceneDirector::TitleButtonTextActiveOffAnimation()
extern void TitleStartSceneDirector_TitleButtonTextActiveOffAnimation_mDF4DD2474DF0422ADDAC08F7A7D2B1E49BEBB5CC (void);
// 0x000002A5 System.Void TitleStartSceneDirector::TitleUiTextsActiveOff()
extern void TitleStartSceneDirector_TitleUiTextsActiveOff_mFE90843E8396C8319B8D1B4BDF5F16A8C740E73C (void);
// 0x000002A6 System.Void TitleStartSceneDirector::MenuScreenActiveOn()
extern void TitleStartSceneDirector_MenuScreenActiveOn_m266A513DF12263CBCFB0E1482568380F9DAA6E49 (void);
// 0x000002A7 System.Void TitleStartSceneDirector::MenuScreenActiveOnAnimation()
extern void TitleStartSceneDirector_MenuScreenActiveOnAnimation_m90A94DD783C1458946DE8636094160432374D68B (void);
// 0x000002A8 System.Void TitleStartSceneDirector::ButtonViewActiveAnimation()
extern void TitleStartSceneDirector_ButtonViewActiveAnimation_m0A17D47C03CDE3C36A6A39B353024027CCC5E525 (void);
// 0x000002A9 System.Void TitleStartSceneDirector::ButtonViewActiveOffAnimation()
extern void TitleStartSceneDirector_ButtonViewActiveOffAnimation_mAC3CBDF67E113EA377EC069D7DF06D576EF1B971 (void);
// 0x000002AA System.Void TitleStartSceneDirector::ButtonViewActiveOffInvoke()
extern void TitleStartSceneDirector_ButtonViewActiveOffInvoke_m394F6827484BC0C615AF1E1348A34C3C1D0AD54E (void);
// 0x000002AB System.Void TitleStartSceneDirector::ManualViewActiveAnimation()
extern void TitleStartSceneDirector_ManualViewActiveAnimation_m1D34C6869FD1E91C332250750E02161759E6DBF3 (void);
// 0x000002AC System.Void TitleStartSceneDirector::RankingViewActiveAnimation()
extern void TitleStartSceneDirector_RankingViewActiveAnimation_m5C87005B04288E96716DDF0ADB1862A3995565C9 (void);
// 0x000002AD System.Void TitleStartSceneDirector::CreditViewActiveAnimation()
extern void TitleStartSceneDirector_CreditViewActiveAnimation_m9431D8B2F76155DD5D46F1FF6607244C959402B3 (void);
// 0x000002AE System.Void TitleStartSceneDirector::ManualViewActiveOffAnimation()
extern void TitleStartSceneDirector_ManualViewActiveOffAnimation_m8516C8B2F43E3A912379ABE47CD2D5B836FD4AF0 (void);
// 0x000002AF System.Void TitleStartSceneDirector::RankingViewActiveOffAnimation()
extern void TitleStartSceneDirector_RankingViewActiveOffAnimation_mDF1E3F580F77835791427695C646691E2088C6DB (void);
// 0x000002B0 System.Void TitleStartSceneDirector::CreditViewActiveOffAnimation()
extern void TitleStartSceneDirector_CreditViewActiveOffAnimation_mC0540F04167244EB2C8A11954D79D509E1207107 (void);
// 0x000002B1 System.Void TitleStartSceneDirector::ManualViewActiveOffInvoke()
extern void TitleStartSceneDirector_ManualViewActiveOffInvoke_mAE7F48902FE39B950CE3C85E4004A9321740FE05 (void);
// 0x000002B2 System.Void TitleStartSceneDirector::RankingViewActiveOffInvoke()
extern void TitleStartSceneDirector_RankingViewActiveOffInvoke_m354EB914B46F92C4DFC4112974EE6A6C61901ACA (void);
// 0x000002B3 System.Void TitleStartSceneDirector::CreditViewActiveOffInvoke()
extern void TitleStartSceneDirector_CreditViewActiveOffInvoke_m9EEDF46B82477EC6132CD4F78A65A314F705EDBD (void);
// 0x000002B4 System.Void TitleStartSceneDirector::GameStart()
extern void TitleStartSceneDirector_GameStart_mD8C85A7C590D528583D0873CB0B45AB569E15C15 (void);
// 0x000002B5 System.Void TitleStartSceneDirector::TutorialStart()
extern void TitleStartSceneDirector_TutorialStart_m5F3A8F902A794FFB0BF639E5C1C8EE76B53BDC93 (void);
// 0x000002B6 System.Void TitleStartSceneDirector::.ctor()
extern void TitleStartSceneDirector__ctor_m255A16F654C4150FC2740A9CB9A4FAA21890FA46 (void);
// 0x000002B7 System.Void VirtualCameraController::FixedUpdate()
extern void VirtualCameraController_FixedUpdate_mFEECD20E60D80576A2FBD2A6DBCC27F940BDEEA9 (void);
// 0x000002B8 System.Void VirtualCameraController::CameraRotate()
extern void VirtualCameraController_CameraRotate_m45A799D553D218BBAC3348AC953B9A00C8CAFA45 (void);
// 0x000002B9 System.Void VirtualCameraController::.ctor()
extern void VirtualCameraController__ctor_mEA725BBD970EF7CA7D994407D08D0B3670163CBA (void);
// 0x000002BA System.Void WarningUIActiveController::Start()
extern void WarningUIActiveController_Start_mF40894D0E01D666420EDEA659E4F6C45DAD1C544 (void);
// 0x000002BB System.Void WarningUIActiveController::OnTriggerEnter(UnityEngine.Collider)
extern void WarningUIActiveController_OnTriggerEnter_m2C838434C841E5D6344075DEAE766B0650A70CBB (void);
// 0x000002BC System.Void WarningUIActiveController::WarningUIActiveSeting()
extern void WarningUIActiveController_WarningUIActiveSeting_mD56D7AF9897811266D39B3D11FCF54A73516CCEA (void);
// 0x000002BD System.Void WarningUIActiveController::WarningTextOn()
extern void WarningUIActiveController_WarningTextOn_m9A177ABCE64E3F64E69BD8E85F9A7E9225478095 (void);
// 0x000002BE System.Void WarningUIActiveController::WarningBackImageOn()
extern void WarningUIActiveController_WarningBackImageOn_m23F59FB5C2EFDE1F0A3137C892660A84817BD006 (void);
// 0x000002BF System.Void WarningUIActiveController::WarningLightColorChange()
extern void WarningUIActiveController_WarningLightColorChange_m7E80CE539FB3AD0271F4327DD939A0EB0286275E (void);
// 0x000002C0 System.Void WarningUIActiveController::WarningLightColorResetInvoke()
extern void WarningUIActiveController_WarningLightColorResetInvoke_mC7BD9871143C0290B9E7874329A1306D393E428F (void);
// 0x000002C1 System.Void WarningUIActiveController::WarningSoundCall()
extern void WarningUIActiveController_WarningSoundCall_mD580CEA4CA9186AFA83A6D6D7B0BC4900740E74A (void);
// 0x000002C2 System.Void WarningUIActiveController::WarningActionBGMStop()
extern void WarningUIActiveController_WarningActionBGMStop_mE036EAA6C06B8C4C9568D1857E0FB72288934DD2 (void);
// 0x000002C3 System.Void WarningUIActiveController::WarningExpression()
extern void WarningUIActiveController_WarningExpression_m83A7FD41025821CCA9AAE3BB29961A447F66C5FF (void);
// 0x000002C4 System.Void WarningUIActiveController::DestroyInvoke()
extern void WarningUIActiveController_DestroyInvoke_m2261E9752DBCCAB8FC7B4CB67AB3BCD5743DFBF8 (void);
// 0x000002C5 System.Void WarningUIActiveController::.ctor()
extern void WarningUIActiveController__ctor_m08F4637E2C53AAD00969A3D298A9CAC3D77E9760 (void);
// 0x000002C6 System.Void DisableButton::Start()
extern void DisableButton_Start_m2B1FFF17330B24D5BC57C5C19F198DFBEF76F349 (void);
// 0x000002C7 System.Void DisableButton::Update()
extern void DisableButton_Update_m081F994DA3F5C6FB1E5B2A6F3719B0CF4E031E8A (void);
// 0x000002C8 System.Void DisableButton::MenuOpen()
extern void DisableButton_MenuOpen_m128A78E05D6E31100E3A821DD45C2EE957B2AAAB (void);
// 0x000002C9 System.Void DisableButton::.ctor()
extern void DisableButton__ctor_m178210D4AF1A2204C5D1570CEFC05A608E83484C (void);
// 0x000002CA System.Void getCurrentUser::Start()
extern void getCurrentUser_Start_m9FA508F33B8EBCC550EF4D09B1AC3B610610BEEF (void);
// 0x000002CB System.Void getCurrentUser::Update()
extern void getCurrentUser_Update_m6BD344466351DBD2E93B1B6DAF319CDFBBACB59A (void);
// 0x000002CC System.Void getCurrentUser::.ctor()
extern void getCurrentUser__ctor_m4E1D19FC357C15A4253B9EFA1456897EDF7E6BBA (void);
// 0x000002CD System.Void Loginsignin::Start()
extern void Loginsignin_Start_mE9C63E90D323F71FCEFE988FDCF7A16246731E40 (void);
// 0x000002CE System.Void Loginsignin::Update()
extern void Loginsignin_Update_m1EE3B56FFB1A8EB7A06FBFEA8E6644D588B232A9 (void);
// 0x000002CF System.Void Loginsignin::Login()
extern void Loginsignin_Login_m520C3F6350492187B027C45CD9F2CF4EF203A9C9 (void);
// 0x000002D0 System.Void Loginsignin::Signin()
extern void Loginsignin_Signin_m8990693CF8FCA8E4E39E24CFAA427A3644DC6A14 (void);
// 0x000002D1 System.Void Loginsignin::.ctor()
extern void Loginsignin__ctor_m8987F07E41D4220F57F4D98AFDE774D58B5FA194 (void);
// 0x000002D2 System.Void Loginsignin/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m0A2F2C791278B1F967C58B3CCC4D37011752973C (void);
// 0x000002D3 System.Void Loginsignin/<>c__DisplayClass5_0::<Login>b__0(NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass5_0_U3CLoginU3Eb__0_m0B740D5A4D91DA7B82D6A6ADAF97D3BA89C87E04 (void);
// 0x000002D4 System.Void Loginsignin/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m899021E4B110223C9193522E518958B3F65EFAAC (void);
// 0x000002D5 System.Void Loginsignin/<>c__DisplayClass6_0::<Signin>b__0(NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass6_0_U3CSigninU3Eb__0_m3462F5A9045A71C6D06746340C675972365D8A96 (void);
// 0x000002D6 System.Void Logout::Start()
extern void Logout_Start_mFA986CEA33CFD44C9BD985C0C53C7DA9866AEDBD (void);
// 0x000002D7 System.Void Logout::Update()
extern void Logout_Update_mCFCA296C5DF26BFDF84F9141C94A179CD8A3BE72 (void);
// 0x000002D8 System.Void Logout::Logout_user()
extern void Logout_Logout_user_m23236D4A8299E6E99FDFAE2F2DD2FD18C66ABF30 (void);
// 0x000002D9 System.Void Logout::.ctor()
extern void Logout__ctor_m1AE0F3AC41DD77395FFD1C8CB81E5562D885D24E (void);
// 0x000002DA System.Void Logout/<>c::.cctor()
extern void U3CU3Ec__cctor_m03C76596A8B2D2A96AE1A893A2BE6074FD4BE74E (void);
// 0x000002DB System.Void Logout/<>c::.ctor()
extern void U3CU3Ec__ctor_m941AE6BE4EEA062168CDDEF559ADF202EB1B58E1 (void);
// 0x000002DC System.Void Logout/<>c::<Logout_user>b__3_0(NCMB.NCMBException)
extern void U3CU3Ec_U3CLogout_userU3Eb__3_0_m02782F9B9A44100D2150FB92A7F14105B7CBD811 (void);
// 0x000002DD System.Void QuickStart::Start()
extern void QuickStart_Start_mCD39E9E47B19707074D92CA7C4F0F1E787047527 (void);
// 0x000002DE System.Void QuickStart::Update()
extern void QuickStart_Update_m147D7FA2903C25B5A6700B94D73D8CC872488C25 (void);
// 0x000002DF System.Void QuickStart::.ctor()
extern void QuickStart__ctor_m0CC08B29D0E114574665902A4CE4E6C742BF35D8 (void);
// 0x000002E0 System.Void RampAsset::.ctor()
extern void RampAsset__ctor_m7CB8E98ADF762E7343D9A8AEC915FEA693ED9678 (void);
// 0x000002E1 System.Void AdjustTimeScale::Start()
extern void AdjustTimeScale_Start_m4A38ED5A991235FDD56B6F7444AC8677B55DC578 (void);
// 0x000002E2 System.Void AdjustTimeScale::Update()
extern void AdjustTimeScale_Update_m35850BFE55F334D789D2273391B993112BC1F2EE (void);
// 0x000002E3 System.Void AdjustTimeScale::OnApplicationQuit()
extern void AdjustTimeScale_OnApplicationQuit_mBF83E944940AE95B2542819682EBEAA7580EFDF1 (void);
// 0x000002E4 System.Void AdjustTimeScale::.ctor()
extern void AdjustTimeScale__ctor_m0D9D7BCA4710EF1B661F7648C598020E3483DCC6 (void);
// 0x000002E5 System.Void ProximityActivate::Start()
extern void ProximityActivate_Start_mBA100FA3ED59208F73FBE82E9584F9B1DE0450BB (void);
// 0x000002E6 System.Boolean ProximityActivate::IsTargetNear()
extern void ProximityActivate_IsTargetNear_m826C0D892D66C8B9229003E9E7A7BFC1DAC8195A (void);
// 0x000002E7 System.Void ProximityActivate::Update()
extern void ProximityActivate_Update_m4A9769EE498773365306E9DCFFEFAFE0058A83DF (void);
// 0x000002E8 System.Void ProximityActivate::.ctor()
extern void ProximityActivate__ctor_m47AA717E38E4A36C84CDFA7EFF72192658155DB0 (void);
// 0x000002E9 System.Void SimpleCharacterMotor::Awake()
extern void SimpleCharacterMotor_Awake_m347826CC4184FB98988CC63028CFA1736D438135 (void);
// 0x000002EA System.Void SimpleCharacterMotor::Update()
extern void SimpleCharacterMotor_Update_mA5966DE9D18E3A51D7E547A1DB3D6D668CA2199B (void);
// 0x000002EB System.Void SimpleCharacterMotor::UpdateLookRotation()
extern void SimpleCharacterMotor_UpdateLookRotation_m7A7D395E0D03FFDAF115286506F06BCED9C33E10 (void);
// 0x000002EC System.Void SimpleCharacterMotor::UpdateTranslation()
extern void SimpleCharacterMotor_UpdateTranslation_m73972A962820E750E7A5DA7AEF8BE1F8C512C6B1 (void);
// 0x000002ED System.Void SimpleCharacterMotor::.ctor()
extern void SimpleCharacterMotor__ctor_mDF39162F0CD433D75D7AF488911B5865F0620217 (void);
// 0x000002EE System.Void Readme::.ctor()
extern void Readme__ctor_m69C325C4C171DCB0312B646A9034AA91EA8C39C6 (void);
// 0x000002EF System.Void Readme/Section::.ctor()
extern void Section__ctor_m5F732533E4DFC0167D965E5F5DB332E46055399B (void);
// 0x000002F0 System.Void ActiveStateToggler::ToggleActive()
extern void ActiveStateToggler_ToggleActive_m2197869C55A5EA5BF4EF8800EDED32049450ADD7 (void);
// 0x000002F1 System.Void ActiveStateToggler::.ctor()
extern void ActiveStateToggler__ctor_m04F37578B0B887F86DF0AFE4BCE2AA241E9B027F (void);
// 0x000002F2 System.Void ApplicationManager::Quit()
extern void ApplicationManager_Quit_mC3416C8A1235ADDC9C0FFE0C51DF77A54E46919D (void);
// 0x000002F3 System.Void ApplicationManager::.ctor()
extern void ApplicationManager__ctor_m9728432FD921373C75D5AD1BB10C1C7BB926E45F (void);
// 0x000002F4 System.Void ChangeColor::OnEnable()
extern void ChangeColor_OnEnable_mB6645B0A82763933A04A82D7D1ED6D04F5BAEC08 (void);
// 0x000002F5 System.Void ChangeColor::SetRed(System.Single)
extern void ChangeColor_SetRed_m9371FE7C53A785B2CB67490E1FDB4923E6519DF4 (void);
// 0x000002F6 System.Void ChangeColor::SetGreen(System.Single)
extern void ChangeColor_SetGreen_mB8384D5AD9E294DCB5A66AA985B1AE2A588CB1A5 (void);
// 0x000002F7 System.Void ChangeColor::SetBlue(System.Single)
extern void ChangeColor_SetBlue_m1FEAA20434D23F2402B4FD09BC633C96E773CBD0 (void);
// 0x000002F8 System.Void ChangeColor::OnValueChanged(System.Single,System.Int32)
extern void ChangeColor_OnValueChanged_m55D60C6063192F109C7F266163FE2D1536D848DD (void);
// 0x000002F9 System.Void ChangeColor::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void ChangeColor_OnPointerClick_mA5792AD472A3B58C78C738E569BBD9F9DC7973BF (void);
// 0x000002FA System.Void ChangeColor::.ctor()
extern void ChangeColor__ctor_m8BB1B19B1A2961B218222DF5C104ECAE825C8EA1 (void);
// 0x000002FB System.Void DragMe::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragMe_OnBeginDrag_m9CAC75435F1132F97C511BC730463EF0884BCE9D (void);
// 0x000002FC System.Void DragMe::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragMe_OnDrag_mA90A49CBDFE2E55BF4408EEF4233847A5AB6E6A2 (void);
// 0x000002FD System.Void DragMe::SetDraggedPosition(UnityEngine.EventSystems.PointerEventData)
extern void DragMe_SetDraggedPosition_m7ACCE7A1453C634A5D85E1B24D68F979E8FAB536 (void);
// 0x000002FE System.Void DragMe::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragMe_OnEndDrag_m42FC355BFB6D84BAD1D5ED7CC2FF06A6B956351A (void);
// 0x000002FF T DragMe::FindInParents(UnityEngine.GameObject)
// 0x00000300 System.Void DragMe::.ctor()
extern void DragMe__ctor_m1A1D3514EBA48EFBCC7EF774684D3D7B14A1790F (void);
// 0x00000301 System.Void DragPanel::Awake()
extern void DragPanel_Awake_m976D47375F0074B0A964AC06B1E7765D57D029F1 (void);
// 0x00000302 System.Void DragPanel::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void DragPanel_OnPointerDown_mEB286E46EC39E5DC1FD7F0E7678B427EB92DECDB (void);
// 0x00000303 System.Void DragPanel::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragPanel_OnDrag_mA93336E5A4F6C0881550A04F888947A9254A04AB (void);
// 0x00000304 System.Void DragPanel::ClampToWindow()
extern void DragPanel_ClampToWindow_m2EA7DC5AF57F644BB5A256AAF714F7C85C763849 (void);
// 0x00000305 System.Void DragPanel::.ctor()
extern void DragPanel__ctor_mD24BB7D4C1206A3912DF7036A657E6734A2B45BC (void);
// 0x00000306 System.Void DropMe::OnEnable()
extern void DropMe_OnEnable_m9C62AF35ABE6569D778E35BE78C0B0DCBFED9B59 (void);
// 0x00000307 System.Void DropMe::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void DropMe_OnDrop_mA70777C2048F48E83B792112F7E1D2C6BB50DDD3 (void);
// 0x00000308 System.Void DropMe::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void DropMe_OnPointerEnter_m4B4D3573925CB30440E8AF7EF832828641CBBB87 (void);
// 0x00000309 System.Void DropMe::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void DropMe_OnPointerExit_mD29997A217A73EB182671FD9116DBC9E3A91B929 (void);
// 0x0000030A UnityEngine.Sprite DropMe::GetDropSprite(UnityEngine.EventSystems.PointerEventData)
extern void DropMe_GetDropSprite_m6978787E3015377193F8817EC1FEB79F5EE865A4 (void);
// 0x0000030B System.Void DropMe::.ctor()
extern void DropMe__ctor_mC7C9F0820A003F55CEE08BF74E17D5CE25823D89 (void);
// 0x0000030C System.Void PanelManager::OnEnable()
extern void PanelManager_OnEnable_m5A7DC3931168B59A585EAEFA1AB80F7D3EE8140F (void);
// 0x0000030D System.Void PanelManager::OpenPanel(UnityEngine.Animator)
extern void PanelManager_OpenPanel_m456B82F7B62A00937F60CE6014C1F3698414B7B4 (void);
// 0x0000030E UnityEngine.GameObject PanelManager::FindFirstEnabledSelectable(UnityEngine.GameObject)
extern void PanelManager_FindFirstEnabledSelectable_m5D4FC3073E774F4185C7992683643F28ECB1E4A8 (void);
// 0x0000030F System.Void PanelManager::CloseCurrent()
extern void PanelManager_CloseCurrent_m824C6139CDBD415DE8E4CF1FB557FCE79D69B2C7 (void);
// 0x00000310 System.Collections.IEnumerator PanelManager::DisablePanelDeleyed(UnityEngine.Animator)
extern void PanelManager_DisablePanelDeleyed_m1DF8E917EEC8FD14E76954ED3F2918C76B65E5AF (void);
// 0x00000311 System.Void PanelManager::SetSelected(UnityEngine.GameObject)
extern void PanelManager_SetSelected_m59DAF072CE220595B0AD4BC0BEA1C00856943B71 (void);
// 0x00000312 System.Void PanelManager::.ctor()
extern void PanelManager__ctor_mA86BE5A7EC580FAD466DFEC12569D38F584473BB (void);
// 0x00000313 System.Void PanelManager/<DisablePanelDeleyed>d__10::.ctor(System.Int32)
extern void U3CDisablePanelDeleyedU3Ed__10__ctor_m4282B9D12EB2BB6D413E69F116601539D4A98B99 (void);
// 0x00000314 System.Void PanelManager/<DisablePanelDeleyed>d__10::System.IDisposable.Dispose()
extern void U3CDisablePanelDeleyedU3Ed__10_System_IDisposable_Dispose_mA29BD3E3DE14A63123458C1B873D3B9520CCD0E7 (void);
// 0x00000315 System.Boolean PanelManager/<DisablePanelDeleyed>d__10::MoveNext()
extern void U3CDisablePanelDeleyedU3Ed__10_MoveNext_mDCC2DE4CAA28ED5E8135119B9A4E38D075FEC235 (void);
// 0x00000316 System.Object PanelManager/<DisablePanelDeleyed>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisablePanelDeleyedU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA0AE880F4D89D9DC62386E0448E6FE576A4AE66 (void);
// 0x00000317 System.Void PanelManager/<DisablePanelDeleyed>d__10::System.Collections.IEnumerator.Reset()
extern void U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_Reset_m17342096F195588A8C598895C595B0B029F598C9 (void);
// 0x00000318 System.Object PanelManager/<DisablePanelDeleyed>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_get_Current_m47973E705B6316F9392B95B9A84ED6431CF8A0F0 (void);
// 0x00000319 System.Void ResizePanel::Awake()
extern void ResizePanel_Awake_m177281E00557C53FA59B3E8539414DF517AEC186 (void);
// 0x0000031A System.Void ResizePanel::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ResizePanel_OnPointerDown_m9017A7FCE182034153CAB88EF3D00B8A69577082 (void);
// 0x0000031B System.Void ResizePanel::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void ResizePanel_OnDrag_mF4950C026DD520F4CFB4A885544EBC678F1CC6A1 (void);
// 0x0000031C System.Void ResizePanel::.ctor()
extern void ResizePanel__ctor_m11631FA6774A81F988DAE9E3B6F3332131EC8771 (void);
// 0x0000031D System.Void ScrollDetailTexture::OnEnable()
extern void ScrollDetailTexture_OnEnable_m2B3F092169EE265F93ADCDF198F956DF4E76E5D9 (void);
// 0x0000031E System.Void ScrollDetailTexture::OnDisable()
extern void ScrollDetailTexture_OnDisable_mB680BF76E8EB673F455346FED420D63B42AA1496 (void);
// 0x0000031F System.Void ScrollDetailTexture::Update()
extern void ScrollDetailTexture_Update_m5D6FEC73519F9A23B55BD361E25F2C8E2244EABA (void);
// 0x00000320 System.Void ScrollDetailTexture::.ctor()
extern void ScrollDetailTexture__ctor_m6210C8CA4C1EC2B032630E58B9C3ED5A9B4FC8F3 (void);
// 0x00000321 System.Void ShowSliderValue::UpdateLabel(System.Single)
extern void ShowSliderValue_UpdateLabel_mE0D1CCC1F3E8A56D953BB1C03CF4793A0563BD5D (void);
// 0x00000322 System.Void ShowSliderValue::.ctor()
extern void ShowSliderValue__ctor_mCEEB373DCD42E0CDFACBCDDBE60A2C6F4337561C (void);
// 0x00000323 System.Void TiltWindow::Start()
extern void TiltWindow_Start_m117F45F2AF4A2023B8D2438ECABE9649F66AACEA (void);
// 0x00000324 System.Void TiltWindow::Update()
extern void TiltWindow_Update_m8B983CF008A896D2A99DEF6C2271BB4A418DCA03 (void);
// 0x00000325 System.Void TiltWindow::.ctor()
extern void TiltWindow__ctor_m998EDCD27C0C488CB5469BDB8439F0F41293BA64 (void);
// 0x00000326 System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_mD5E060BED4E0C5C0D40AA39C0DA7AE44CAAB217D (void);
// 0x00000327 UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m62C42A7966EF22CB95BB847225D2BE2441A3CE0C (void);
// 0x00000328 System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_mDB9A4DA6CC1C82A704D3575DC196F44F17E5E0B5 (void);
// 0x00000329 System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_m224B705F449A3DA440346B08A6237479A91D7136 (void);
// 0x0000032A System.Void UnityTemplateProjects.SimpleCameraController/CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_m3A7302F4586F37F7C78EDA2F0C476BAA22672360 (void);
// 0x0000032B System.Void UnityTemplateProjects.SimpleCameraController/CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_m561F6D92E99F13444FBAC8D5B1A40762A26219F9 (void);
// 0x0000032C System.Void UnityTemplateProjects.SimpleCameraController/CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController/CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_mFDC0E1CA19B6791CDB6EE8AC262944DC1143FCF9 (void);
// 0x0000032D System.Void UnityTemplateProjects.SimpleCameraController/CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_m03320D760E52E809272776B58C7E741B2EE1D73E (void);
// 0x0000032E System.Void UnityTemplateProjects.SimpleCameraController/CameraState::.ctor()
extern void CameraState__ctor_m33568E59E39C953BDE6CD28D35A34BE52AE25504 (void);
// 0x0000032F System.String SimpleJSON.Json::Serialize(System.Object)
extern void Json_Serialize_m10335BD31272294C425D0A19223D3CF5643F2D84 (void);
// 0x00000330 System.Void SimpleJSON.Json/Serializer::.ctor()
extern void Serializer__ctor_m4F44C46231E6E5AE64E5A54E7C2BD9F34EDA7821 (void);
// 0x00000331 System.String SimpleJSON.Json/Serializer::Serialize(System.Object)
extern void Serializer_Serialize_m59BD9118C0D72805416DB6E86D6D7B5879B567DC (void);
// 0x00000332 System.Void SimpleJSON.Json/Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_m8C2F65A142866FADF38B93D9E629B2E7E2524AFF (void);
// 0x00000333 System.Void SimpleJSON.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_m1A05723BFB5B50CC626C68BF09D58306CFCA5E07 (void);
// 0x00000334 System.Void SimpleJSON.Json/Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_mEF7CAD420779566B151D436F9D981B296498ED3E (void);
// 0x00000335 System.Void SimpleJSON.Json/Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_m042FDB4D9299515A034B701CA404E7D6648D7E0A (void);
// 0x00000336 System.Void SimpleJSON.Json/Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_m69BC1350909211DF9AC2F73650A54BC735640EF2 (void);
// 0x00000337 System.Object MiniJSON.Json::Deserialize(System.String)
extern void Json_Deserialize_m20B6C20404B17D91C466790EDD4DFD5B687C2BB3 (void);
// 0x00000338 System.String MiniJSON.Json::Serialize(System.Object)
extern void Json_Serialize_mB6F04E24EEDCE8B987E123C8EB4CB39B0DE0A338 (void);
// 0x00000339 System.Boolean MiniJSON.Json/Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_m76B748761F3D3C4CECCD0B7CF3A9A6A8082ADF4A (void);
// 0x0000033A System.Void MiniJSON.Json/Parser::.ctor(System.String)
extern void Parser__ctor_m5F80E026C9D94E94EF918063572F0ACE937D6E62 (void);
// 0x0000033B System.Object MiniJSON.Json/Parser::Parse(System.String)
extern void Parser_Parse_m048E00F05C588343625E4A1CD3D62038E17722F1 (void);
// 0x0000033C System.Void MiniJSON.Json/Parser::Dispose()
extern void Parser_Dispose_m912B7F3C99DBEE37A3E5EA2C5F866DE2AFB7FA75 (void);
// 0x0000033D System.Collections.Generic.Dictionary`2<System.String,System.Object> MiniJSON.Json/Parser::ParseObject()
extern void Parser_ParseObject_mCE27A97DD550BE01B8F50E0B5354A2FCF5B43349 (void);
// 0x0000033E System.Collections.Generic.List`1<System.Object> MiniJSON.Json/Parser::ParseArray()
extern void Parser_ParseArray_mFB0A61F43183970F6D05E94BDF8860371C715591 (void);
// 0x0000033F System.Object MiniJSON.Json/Parser::ParseValue()
extern void Parser_ParseValue_mA4C23822F44634EA0944A828112D706B98AB0C61 (void);
// 0x00000340 System.Object MiniJSON.Json/Parser::ParseByToken(MiniJSON.Json/Parser/TOKEN)
extern void Parser_ParseByToken_mD062F21DCF05ACF7D4939F0D5D4C9D214ED15D0F (void);
// 0x00000341 System.String MiniJSON.Json/Parser::ParseString()
extern void Parser_ParseString_m875C2B5594D3C6A50C8166D7444A1EED5372CF85 (void);
// 0x00000342 System.Object MiniJSON.Json/Parser::ParseNumber()
extern void Parser_ParseNumber_mFB791DAA1FC9AEABAA6E8CD067617AEE2CDE9512 (void);
// 0x00000343 System.Void MiniJSON.Json/Parser::EatWhitespace()
extern void Parser_EatWhitespace_m912C05CB24F4164C96C7E431DF942EB1356285C7 (void);
// 0x00000344 System.Char MiniJSON.Json/Parser::get_PeekChar()
extern void Parser_get_PeekChar_mA1EAD1C91D69B29C281D03976BDCD15E2B4411E5 (void);
// 0x00000345 System.Char MiniJSON.Json/Parser::get_NextChar()
extern void Parser_get_NextChar_m800FCEF9B0C72DC84F739DBAC8CC12864D1AA430 (void);
// 0x00000346 System.String MiniJSON.Json/Parser::get_NextWord()
extern void Parser_get_NextWord_mA4DBE6A91BA864C8F7693937F89209E2E7D0220A (void);
// 0x00000347 MiniJSON.Json/Parser/TOKEN MiniJSON.Json/Parser::get_NextToken()
extern void Parser_get_NextToken_m34C299688A8015C06F6BB7068C84D6FE7A68A747 (void);
// 0x00000348 System.Void MiniJSON.Json/Serializer::.ctor()
extern void Serializer__ctor_m9DA8616EA7E0284CA21D5B6665D7798875D52A29 (void);
// 0x00000349 System.String MiniJSON.Json/Serializer::Serialize(System.Object)
extern void Serializer_Serialize_m4BE25E76C9FAEE7FF83100AD8F13DC2349251887 (void);
// 0x0000034A System.Void MiniJSON.Json/Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_m3B2D1698CC74A23CF8E1DE1829676590823459DC (void);
// 0x0000034B System.Void MiniJSON.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_m8B31489C19418217A903E21A210D76F370334FF3 (void);
// 0x0000034C System.Void MiniJSON.Json/Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_m61D80EEDA84A38B6DBF53609B88CA57B00C77FE1 (void);
// 0x0000034D System.Void MiniJSON.Json/Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_m3195D253DADD72083860BC3A85E82DDFEC958BA7 (void);
// 0x0000034E System.Void MiniJSON.Json/Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_mE8E1FF5662A69CBB95590F57A3EAB23CBCBDC711 (void);
// 0x0000034F System.String MimeTypes.MimeTypeMap::GetMimeType(System.String)
extern void MimeTypeMap_GetMimeType_m917DA67F189D7738B7A3F5EF034C70E04623341E (void);
// 0x00000350 System.String MimeTypes.MimeTypeMap::GetExtension(System.String)
extern void MimeTypeMap_GetExtension_mDC445D1B11E667BB03DDFDAC0DB59FDA50FA35F5 (void);
// 0x00000351 System.Void MimeTypes.MimeTypeMap::.cctor()
extern void MimeTypeMap__cctor_m8B0A8D555BD31EEACD30F559C238E20139D5D68A (void);
// 0x00000352 System.Void NCMB.NCMBManager::Awake()
extern void NCMBManager_Awake_m0545F9C99C9E25357E9462AFE3638765FE5BD971 (void);
// 0x00000353 System.String NCMB.NCMBManager::getInstallationProperty()
extern void NCMBManager_getInstallationProperty_mF515922339455EE566D2684A73597708391A5088 (void);
// 0x00000354 System.Boolean NCMB.NCMBManager::get_Inited()
extern void NCMBManager_get_Inited_m86C2501C0B3F2CECEC40589B36B8471FD045170C (void);
// 0x00000355 System.Void NCMB.NCMBManager::set_Inited(System.Boolean)
extern void NCMBManager_set_Inited_mDA843E664555EB9F1DE2E6D381F51E032B1DE99C (void);
// 0x00000356 System.Void NCMB.NCMBManager::OnRegistration(System.String)
extern void NCMBManager_OnRegistration_mD34A73AD1DAB870F20E7A914E5192642911A0158 (void);
// 0x00000357 System.Void NCMB.NCMBManager::OnNotificationReceived(System.String)
extern void NCMBManager_OnNotificationReceived_m84CD92F238E4904919C14015BA6B32EF79D4734D (void);
// 0x00000358 System.Void NCMB.NCMBManager::Start()
extern void NCMBManager_Start_mF9E30946E99C766B107F2BDFF2F6A57DD34C5B6D (void);
// 0x00000359 System.Void NCMB.NCMBManager::Update()
extern void NCMBManager_Update_m70DF5B5BC3F5E1618C479D9108BDECD2528ED479 (void);
// 0x0000035A System.Void NCMB.NCMBManager::ProcessNotification()
extern void NCMBManager_ProcessNotification_mFCA1FB3EC8957C56B76831A6A98CB7913ECD908B (void);
// 0x0000035B System.Void NCMB.NCMBManager::OnApplicationPause(System.Boolean)
extern void NCMBManager_OnApplicationPause_m3DBFC5B4D0975FB7C60ED4E0E67975A84B7047A3 (void);
// 0x0000035C System.Void NCMB.NCMBManager::ClearAfterOneFrame()
extern void NCMBManager_ClearAfterOneFrame_mAD8EE587ED481862E225463FD353E504C4C921F6 (void);
// 0x0000035D System.Collections.IEnumerator NCMB.NCMBManager::IEClearAfterAFrame()
extern void NCMBManager_IEClearAfterAFrame_mB5A17325B392BBE723225B02A1318C7E31997214 (void);
// 0x0000035E System.String NCMB.NCMBManager::SearchPath()
extern void NCMBManager_SearchPath_mE1FB6BE15F5F24B63EA028F0926DC61E02FD01A0 (void);
// 0x0000035F System.Void NCMB.NCMBManager::onTokenReceived(System.String)
extern void NCMBManager_onTokenReceived_m4E79BDA7D5BD3B31CE793A858F0049C24716AA40 (void);
// 0x00000360 System.Void NCMB.NCMBManager::updateExistedInstallation(NCMB.NCMBInstallation,System.String)
extern void NCMBManager_updateExistedInstallation_mFA3A06ACC84ECCE18CFA4E46BA4DEA25CB2BA62E (void);
// 0x00000361 System.Void NCMB.NCMBManager::SaveFile(System.String,System.String)
extern void NCMBManager_SaveFile_m9A999F0FCC710D04B5A650087858811FA983EE94 (void);
// 0x00000362 System.String NCMB.NCMBManager::ReadFile(System.String)
extern void NCMBManager_ReadFile_m76CA63E4977A87918F264AAB1BD2A3F259F34B35 (void);
// 0x00000363 System.Void NCMB.NCMBManager::onAnalyticsReceived(System.String)
extern void NCMBManager_onAnalyticsReceived_mB5BF99617E8FECF44A6A725B67F97ACA56FD2599 (void);
// 0x00000364 System.Void NCMB.NCMBManager::DeleteCurrentInstallation(System.String)
extern void NCMBManager_DeleteCurrentInstallation_m6694256AE1651559BC40E55E064DE776974F5D81 (void);
// 0x00000365 System.String NCMB.NCMBManager::GetCurrentInstallation()
extern void NCMBManager_GetCurrentInstallation_m1B63F70AEBABE2043F3B44EEE0009C085D36CE07 (void);
// 0x00000366 System.Void NCMB.NCMBManager::CreateInstallationProperty()
extern void NCMBManager_CreateInstallationProperty_mCB3958FDC506728207606EEF54922C10C20A651D (void);
// 0x00000367 System.Void NCMB.NCMBManager::.ctor()
extern void NCMBManager__ctor_m455300A96EC9139ABD0751174BC3DF1A5AEC616C (void);
// 0x00000368 System.Void NCMB.NCMBManager::.cctor()
extern void NCMBManager__cctor_m754B06A08304A5BD68E019907C09D5E46C6125F9 (void);
// 0x00000369 System.Void NCMB.NCMBManager/OnRegistrationDelegate::.ctor(System.Object,System.IntPtr)
extern void OnRegistrationDelegate__ctor_m30B7DF282D381ED20979A4AE8C2FF756971F8E2D (void);
// 0x0000036A System.Void NCMB.NCMBManager/OnRegistrationDelegate::Invoke(System.String)
extern void OnRegistrationDelegate_Invoke_m7217BAEF8819E24AE59A241ED57C82AF93B3D554 (void);
// 0x0000036B System.IAsyncResult NCMB.NCMBManager/OnRegistrationDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void OnRegistrationDelegate_BeginInvoke_m1A4C34224B23391D352F953527DA917C04853CAE (void);
// 0x0000036C System.Void NCMB.NCMBManager/OnRegistrationDelegate::EndInvoke(System.IAsyncResult)
extern void OnRegistrationDelegate_EndInvoke_mC3431A006E23A4566046CE283478785D3C42A8A7 (void);
// 0x0000036D System.Void NCMB.NCMBManager/OnNotificationReceivedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnNotificationReceivedDelegate__ctor_m01EEDBF537E4F040C8AA7D6AD4CF0FC92DB4BAA8 (void);
// 0x0000036E System.Void NCMB.NCMBManager/OnNotificationReceivedDelegate::Invoke(NCMB.NCMBPushPayload)
extern void OnNotificationReceivedDelegate_Invoke_mC9B673A9E7FB68450B4609568DD4DF891A49F6F6 (void);
// 0x0000036F System.IAsyncResult NCMB.NCMBManager/OnNotificationReceivedDelegate::BeginInvoke(NCMB.NCMBPushPayload,System.AsyncCallback,System.Object)
extern void OnNotificationReceivedDelegate_BeginInvoke_m4995C83BFCB93C33FDFBB6AD52A6960E3B60D688 (void);
// 0x00000370 System.Void NCMB.NCMBManager/OnNotificationReceivedDelegate::EndInvoke(System.IAsyncResult)
extern void OnNotificationReceivedDelegate_EndInvoke_m9010C4A76735231ED511749A511DC7A9029C146D (void);
// 0x00000371 System.Void NCMB.NCMBManager/<IEClearAfterAFrame>d__20::.ctor(System.Int32)
extern void U3CIEClearAfterAFrameU3Ed__20__ctor_m59C637F2538F3789FF648B9275D04844CE737B05 (void);
// 0x00000372 System.Void NCMB.NCMBManager/<IEClearAfterAFrame>d__20::System.IDisposable.Dispose()
extern void U3CIEClearAfterAFrameU3Ed__20_System_IDisposable_Dispose_m547B748932BF56073A1DB5A80EBE3145E65B2B70 (void);
// 0x00000373 System.Boolean NCMB.NCMBManager/<IEClearAfterAFrame>d__20::MoveNext()
extern void U3CIEClearAfterAFrameU3Ed__20_MoveNext_m4320DA4674E0A0DB08666166964008A208214922 (void);
// 0x00000374 System.Object NCMB.NCMBManager/<IEClearAfterAFrame>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIEClearAfterAFrameU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m27130876CCCCC32894D0478A2398DD02302438BA (void);
// 0x00000375 System.Void NCMB.NCMBManager/<IEClearAfterAFrame>d__20::System.Collections.IEnumerator.Reset()
extern void U3CIEClearAfterAFrameU3Ed__20_System_Collections_IEnumerator_Reset_m856C3563523E5F18B5B2FF333110F58EA8AF6009 (void);
// 0x00000376 System.Object NCMB.NCMBManager/<IEClearAfterAFrame>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CIEClearAfterAFrameU3Ed__20_System_Collections_IEnumerator_get_Current_mF65A8DE40F41A6D87D332F338FD63BFBA279E401 (void);
// 0x00000377 System.Void NCMB.NCMBManager/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_mD12AD87DAAE6903B3EE70A6B76767683C2D3CBCD (void);
// 0x00000378 System.Void NCMB.NCMBManager/<>c__DisplayClass22_0::<onTokenReceived>b__0(NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass22_0_U3ConTokenReceivedU3Eb__0_m130ECAF61B073A73D8B070D30496CCBEC92DBAD7 (void);
// 0x00000379 System.Void NCMB.NCMBManager/<>c__DisplayClass22_0::<onTokenReceived>b__1(NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass22_0_U3ConTokenReceivedU3Eb__1_m827ECE875164B0DEC7123039D27B31144E0B7362 (void);
// 0x0000037A System.Void NCMB.NCMBManager/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_m078426C8B417A3BBE97D9C74673A4D8DB9CB7199 (void);
// 0x0000037B System.Void NCMB.NCMBManager/<>c__DisplayClass23_0::<updateExistedInstallation>b__0(System.String,NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass23_0_U3CupdateExistedInstallationU3Eb__0_m27D75ACA55829744936FA3B93F3745199086AC72 (void);
// 0x0000037C System.Void NCMB.NCMBManager/<>c__DisplayClass23_0::<updateExistedInstallation>b__1(System.Collections.Generic.List`1<NCMB.NCMBInstallation>,NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass23_0_U3CupdateExistedInstallationU3Eb__1_m3BF22B72A2DC91427DA80B703A1733BCE330CA80 (void);
// 0x0000037D System.Void NCMB.NCMBManager/<>c__DisplayClass23_0::<updateExistedInstallation>b__2(NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass23_0_U3CupdateExistedInstallationU3Eb__2_m6866011D293A0D24ABE29453F1994CDF68F22634 (void);
// 0x0000037E System.String NCMB.NCMBPushPayload::get_PushId()
extern void NCMBPushPayload_get_PushId_m6C8366C8B6DA6DC5080B7623B925A8352E0AB187 (void);
// 0x0000037F System.Void NCMB.NCMBPushPayload::set_PushId(System.String)
extern void NCMBPushPayload_set_PushId_m0E40B6F21765988140BC2DF34D15BDC0464DD862 (void);
// 0x00000380 System.String NCMB.NCMBPushPayload::get_Data()
extern void NCMBPushPayload_get_Data_m547F94E98D06B9FE6C8C3C65F42B2C9634C5D344 (void);
// 0x00000381 System.Void NCMB.NCMBPushPayload::set_Data(System.String)
extern void NCMBPushPayload_set_Data_m4EC2EACC3AD6409A1EF879FFA7DEDB21DBA02956 (void);
// 0x00000382 System.String NCMB.NCMBPushPayload::get_Title()
extern void NCMBPushPayload_get_Title_m106CDCA861680F455B6543B27F86F7DC7ECE2C77 (void);
// 0x00000383 System.Void NCMB.NCMBPushPayload::set_Title(System.String)
extern void NCMBPushPayload_set_Title_m05F5300493676A11DBDDB05EB77A8437F104C238 (void);
// 0x00000384 System.String NCMB.NCMBPushPayload::get_Message()
extern void NCMBPushPayload_get_Message_mC588BB92EEE646A5BF914985088F232AD8D5985F (void);
// 0x00000385 System.Void NCMB.NCMBPushPayload::set_Message(System.String)
extern void NCMBPushPayload_set_Message_m6F897F2A6C26F32B8C6C4BEF1632DCB4EC9DB5DE (void);
// 0x00000386 System.String NCMB.NCMBPushPayload::get_Channel()
extern void NCMBPushPayload_get_Channel_mB2344E2E7486E1E2F9D7B20D01305AE0BC454862 (void);
// 0x00000387 System.Void NCMB.NCMBPushPayload::set_Channel(System.String)
extern void NCMBPushPayload_set_Channel_mEB1AD84E9DCC044B4B26C63DA2C540CABB36FE88 (void);
// 0x00000388 System.Boolean NCMB.NCMBPushPayload::get_Dialog()
extern void NCMBPushPayload_get_Dialog_m8713DC3D712D02A3C65A7EA22DAE2C567B3ED073 (void);
// 0x00000389 System.Void NCMB.NCMBPushPayload::set_Dialog(System.Boolean)
extern void NCMBPushPayload_set_Dialog_m459B4DF0145A6BB0236C68DC6E1DC4F37E493409 (void);
// 0x0000038A System.String NCMB.NCMBPushPayload::get_RichUrl()
extern void NCMBPushPayload_get_RichUrl_mCD464E03E0EA9A886DBA0B033D116BFD1415348A (void);
// 0x0000038B System.Void NCMB.NCMBPushPayload::set_RichUrl(System.String)
extern void NCMBPushPayload_set_RichUrl_m87750C06EB32974F9ADB68740F1C39A907F295A5 (void);
// 0x0000038C System.Collections.IDictionary NCMB.NCMBPushPayload::get_UserInfo()
extern void NCMBPushPayload_get_UserInfo_mBFA7706CE8E7BFC1DE7B14290B2A77FF8892006C (void);
// 0x0000038D System.Void NCMB.NCMBPushPayload::set_UserInfo(System.Collections.IDictionary)
extern void NCMBPushPayload_set_UserInfo_m37853C3B69FA6A1B3F4D735C90D6D80F12B3670A (void);
// 0x0000038E System.Void NCMB.NCMBPushPayload::.ctor(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.Collections.IDictionary)
extern void NCMBPushPayload__ctor_mFB30C18E4367BA4BFE77A9B77818B95ADB53F0F0 (void);
// 0x0000038F System.String NCMB.NCMBSettings::get_CurrentUser()
extern void NCMBSettings_get_CurrentUser_m2421EA6E1C07C79856D4305483B351B7A719050F (void);
// 0x00000390 System.Void NCMB.NCMBSettings::set_CurrentUser(System.String)
extern void NCMBSettings_set_CurrentUser_m771F1D8692D7E204F1CA5F4D746EB6A706B7329C (void);
// 0x00000391 System.String NCMB.NCMBSettings::get_ApplicationKey()
extern void NCMBSettings_get_ApplicationKey_m6B66031BE0C96D1AE12430B11DE9FB76ACE4D424 (void);
// 0x00000392 System.Void NCMB.NCMBSettings::set_ApplicationKey(System.String)
extern void NCMBSettings_set_ApplicationKey_m9915FB84868BBDBADF6D2DAFE0375029F4D5F81A (void);
// 0x00000393 System.String NCMB.NCMBSettings::get_ClientKey()
extern void NCMBSettings_get_ClientKey_m846CF1A8BA9F3E4DCFD2A1CA928F4DE89B462236 (void);
// 0x00000394 System.Void NCMB.NCMBSettings::set_ClientKey(System.String)
extern void NCMBSettings_set_ClientKey_m30BF2E8931C98542EB48ACA85EACC1AF568FEECD (void);
// 0x00000395 System.Boolean NCMB.NCMBSettings::get_UsePush()
extern void NCMBSettings_get_UsePush_mB4A498FB7121980BAD687F10FF9598CA4180344B (void);
// 0x00000396 System.Boolean NCMB.NCMBSettings::get_UseAnalytics()
extern void NCMBSettings_get_UseAnalytics_m9DC32E9B0650B02DD6D0F8F78212BEB3CCDC0D52 (void);
// 0x00000397 System.String NCMB.NCMBSettings::get_DomainURL()
extern void NCMBSettings_get_DomainURL_mFAC12515311D74DC8CE6214328BD3AF469D8B947 (void);
// 0x00000398 System.Void NCMB.NCMBSettings::set_DomainURL(System.String)
extern void NCMBSettings_set_DomainURL_m35B16CC0C0E3569127D035AD050349267074A563 (void);
// 0x00000399 System.String NCMB.NCMBSettings::get_APIVersion()
extern void NCMBSettings_get_APIVersion_mC944ACE77235ABBE288B66FBED85322832787465 (void);
// 0x0000039A System.Void NCMB.NCMBSettings::set_APIVersion(System.String)
extern void NCMBSettings_set_APIVersion_m54576CFF9690582028980E7848E34302A381A318 (void);
// 0x0000039B System.Void NCMB.NCMBSettings::.ctor()
extern void NCMBSettings__ctor_m8D9992539BA06C478FDFDCCC28D16DB5F0244505 (void);
// 0x0000039C System.Void NCMB.NCMBSettings::Initialize(System.String,System.String,System.String,System.String)
extern void NCMBSettings_Initialize_mAA740CF228F46F3702C22AA15F5340A2E193430F (void);
// 0x0000039D System.Void NCMB.NCMBSettings::RegisterPush(System.Boolean,System.Boolean,System.Boolean)
extern void NCMBSettings_RegisterPush_m3E9789C7508041849667E7196256E34C988E7244 (void);
// 0x0000039E System.Void NCMB.NCMBSettings::EnableResponseValidation(System.Boolean)
extern void NCMBSettings_EnableResponseValidation_m689FFD3548FF9DC9E0773E127E6D6C9349D454B6 (void);
// 0x0000039F System.Void NCMB.NCMBSettings::Awake()
extern void NCMBSettings_Awake_m84F56E50C56FFE8CC99A9574F9DC7D6646BA11F5 (void);
// 0x000003A0 System.Void NCMB.NCMBSettings::Connection(NCMB.Internal.NCMBConnection,System.Object)
extern void NCMBSettings_Connection_m72C1D8EB449150397ABC3270CE73A59AB18B923F (void);
// 0x000003A1 System.Void NCMB.NCMBSettings::.cctor()
extern void NCMBSettings__cctor_mD252AC13AD9E054FC65B00209AA2AD787F304552 (void);
// 0x000003A2 System.Void NCMB.NamespaceDoc::.ctor()
extern void NamespaceDoc__ctor_m422F291E3147DAC8BAB81F06C19E631E01C950A2 (void);
// 0x000003A3 System.Void NCMB.NCMBACL::set_PublicReadAccess(System.Boolean)
extern void NCMBACL_set_PublicReadAccess_m8B35B2A89565AD7F240BB49D07740B342E75A1E8 (void);
// 0x000003A4 System.Boolean NCMB.NCMBACL::get_PublicReadAccess()
extern void NCMBACL_get_PublicReadAccess_m8D4FB1A9FA2B995D012334CA8D52D7D763698F2C (void);
// 0x000003A5 System.Void NCMB.NCMBACL::set_PublicWriteAccess(System.Boolean)
extern void NCMBACL_set_PublicWriteAccess_mB9A3980D4EBE5D0B6C1D8FCD38A2A7161A1AE266 (void);
// 0x000003A6 System.Boolean NCMB.NCMBACL::get_PublicWriteAccess()
extern void NCMBACL_get_PublicWriteAccess_mCF84699A53B979E3326EA123801F9DAC9176019C (void);
// 0x000003A7 System.Void NCMB.NCMBACL::.ctor()
extern void NCMBACL__ctor_m4F9864F6F555A26D9E6E0E0FF1D90ACBBA9265B1 (void);
// 0x000003A8 System.Void NCMB.NCMBACL::.ctor(System.String)
extern void NCMBACL__ctor_mE565EF266656EEA7C5B0C869136DBEA9329F33A3 (void);
// 0x000003A9 System.Boolean NCMB.NCMBACL::_isShared()
extern void NCMBACL__isShared_m4A0E49EFBF794992096EB1203C3C4B198A27A1FF (void);
// 0x000003AA System.Void NCMB.NCMBACL::_setShared(System.Boolean)
extern void NCMBACL__setShared_mD1F1231EAC1BAAF4C6C3ECA0162DAC0113A65552 (void);
// 0x000003AB NCMB.NCMBACL NCMB.NCMBACL::_copy()
extern void NCMBACL__copy_m480ACAD57140B9078C927B2A0CC1F19637B92536 (void);
// 0x000003AC System.Void NCMB.NCMBACL::SetReadAccess(System.String,System.Boolean)
extern void NCMBACL_SetReadAccess_m072167EABB5E6D42E821099986A6CCC8851435C5 (void);
// 0x000003AD System.Void NCMB.NCMBACL::SetWriteAccess(System.String,System.Boolean)
extern void NCMBACL_SetWriteAccess_mF550E5540A9F61E98BFEA66A35E8B4EEC34865BB (void);
// 0x000003AE System.Void NCMB.NCMBACL::SetRoleReadAccess(System.String,System.Boolean)
extern void NCMBACL_SetRoleReadAccess_mC97584DAD0B78386939E8C491507B6E1FFFA3C6C (void);
// 0x000003AF System.Void NCMB.NCMBACL::SetRoleWriteAccess(System.String,System.Boolean)
extern void NCMBACL_SetRoleWriteAccess_m8191B1A370B459DA93775E8BCC86D1DDC504F695 (void);
// 0x000003B0 System.Void NCMB.NCMBACL::SetDefaultACL(NCMB.NCMBACL,System.Boolean)
extern void NCMBACL_SetDefaultACL_m41E2DF0358C8E7BB16AF538FE6C3487A980D72B7 (void);
// 0x000003B1 System.Void NCMB.NCMBACL::_setAccess(System.String,System.String,System.Boolean)
extern void NCMBACL__setAccess_m761D10698ED68E7ECCA18116D20FA12C664F691C (void);
// 0x000003B2 System.Boolean NCMB.NCMBACL::GetReadAccess(System.String)
extern void NCMBACL_GetReadAccess_m618DF123CEE2B4E261EC9B783E6E35D484EBDE1E (void);
// 0x000003B3 System.Boolean NCMB.NCMBACL::GetWriteAccess(System.String)
extern void NCMBACL_GetWriteAccess_m38926A3C669B3CCBF8068F89571E1E0E23DEFD1A (void);
// 0x000003B4 System.Boolean NCMB.NCMBACL::GetRoleReadAccess(System.String)
extern void NCMBACL_GetRoleReadAccess_mB40F402AD1D29D338D5E810519F754DE49DF6AC6 (void);
// 0x000003B5 System.Boolean NCMB.NCMBACL::GetRoleWriteAccess(System.String)
extern void NCMBACL_GetRoleWriteAccess_m611FB47248C07B674EC7CCC94FDA083635E337AB (void);
// 0x000003B6 NCMB.NCMBACL NCMB.NCMBACL::_getDefaultACL()
extern void NCMBACL__getDefaultACL_mC725B1AB3B47CF097568F4F5FE9C2F11ADB83F1D (void);
// 0x000003B7 System.Boolean NCMB.NCMBACL::_getAccess(System.String,System.String)
extern void NCMBACL__getAccess_m5ED3C918C7FECFD37E12072071997CC60B202A66 (void);
// 0x000003B8 System.Collections.Generic.IDictionary`2<System.String,System.Object> NCMB.NCMBACL::_toJSONObject()
extern void NCMBACL__toJSONObject_m2DB75A7B996417C0582E7D8C8D4A296884B2EDB5 (void);
// 0x000003B9 NCMB.NCMBACL NCMB.NCMBACL::_createACLFromJSONObject(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void NCMBACL__createACLFromJSONObject_m1B7CAEA2B87973FF78F15C9D3EB5DBE5A8BC4F9E (void);
// 0x000003BA System.Void NCMB.NCMBAnalytics::TrackAppOpened(System.String)
extern void NCMBAnalytics_TrackAppOpened_m49B474410DE8CDE0102EAB41C1F98937A78A2F52 (void);
// 0x000003BB System.Void NCMB.NCMBAnalytics::.ctor()
extern void NCMBAnalytics__ctor_m6F6B5015194A94F38363F4C958F6C76ECA97AC56 (void);
// 0x000003BC System.String NCMB.NCMBAnalytics::_getBaseUrl(System.String)
extern void NCMBAnalytics__getBaseUrl_m27DCE531317FF7F887DB5B05C16F2D011A75FD54 (void);
// 0x000003BD System.Void NCMB.NCMBAnalytics/<>c::.cctor()
extern void U3CU3Ec__cctor_m5F87C7E3C2220D662678F0CFEFBEFB4948E7546E (void);
// 0x000003BE System.Void NCMB.NCMBAnalytics/<>c::.ctor()
extern void U3CU3Ec__ctor_m51E42CF0A8EF9079970E7A845F7D8C9ECC9ADE5E (void);
// 0x000003BF System.Void NCMB.NCMBAnalytics/<>c::<TrackAppOpened>b__0_0(System.Int32,System.String,NCMB.NCMBException)
extern void U3CU3Ec_U3CTrackAppOpenedU3Eb__0_0_m03259F1605286A4382413DA743052C8C697AE66D (void);
// 0x000003C0 System.Void NCMB.NCMBAppleAuthenManager::.ctor()
extern void NCMBAppleAuthenManager__ctor_mFF418FE13C0F0F001633C17E9AF1173AD8971DEF (void);
// 0x000003C1 System.Void NCMB.NCMBAppleAuthenManager::NCMBiOSNativeLoginWithAppleId(System.Action`1<NCMB.NCMBAppleCredential>,System.Action`1<NCMB.NCMBAppleError>)
extern void NCMBAppleAuthenManager_NCMBiOSNativeLoginWithAppleId_mA80EFA350AE8DB1A5BA2F05519DBE8E1C2298EA5 (void);
// 0x000003C2 System.Void NCMB.NCMBAppleAuthenManager::Update()
extern void NCMBAppleAuthenManager_Update_mDE3138290E4C481BC102B4052E0D4D0A8751394E (void);
// 0x000003C3 System.Void NCMB.NCMBAppleAuthenManager/ControlCallbackAction::QueueCallback(System.UInt32,System.String)
extern void ControlCallbackAction_QueueCallback_mE957469965112D8FE512CC0F91377A10C3301FE2 (void);
// 0x000003C4 System.Void NCMB.NCMBAppleAuthenManager/ControlCallbackAction::ExecuteCallbacks()
extern void ControlCallbackAction_ExecuteCallbacks_mC94EA2F15909593AA6C92146C619CDD41B42FD89 (void);
// 0x000003C5 System.UInt32 NCMB.NCMBAppleAuthenManager/ControlCallbackAction::AddCallback(System.Action`1<System.String>)
extern void ControlCallbackAction_AddCallback_mFEEFA1D0435F3BA2D0AA14222E2D233AFA9E8D9A (void);
// 0x000003C6 System.Void NCMB.NCMBAppleAuthenManager/ControlCallbackAction::.cctor()
extern void ControlCallbackAction__cctor_m63677727F985DB49573552C15B4634FD79534AD6 (void);
// 0x000003C7 System.Void NCMB.NCMBAppleAuthenManager/ControlCallbackAction/ActionEntry::.ctor(System.Action`1<System.String>)
extern void ActionEntry__ctor_mA6146DD23DD6610758CC86BE0F07674F282BDA5B (void);
// 0x000003C8 System.Void NCMB.NCMBAppleAuthenManager/ControlCallbackAction/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m791BA098F1B2515410BD8DB9C5530BF6929F64D5 (void);
// 0x000003C9 System.Void NCMB.NCMBAppleAuthenManager/ControlCallbackAction/<>c__DisplayClass5_0::<QueueCallback>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CQueueCallbackU3Eb__0_m63CDBF5042BF4FCDCFAD5F8FFD459F314A63CD5D (void);
// 0x000003CA System.Void NCMB.NCMBAppleAuthenManager/ExecuteInvoke::HandlerCallback(System.UInt32,System.String)
extern void ExecuteInvoke_HandlerCallback_mE2BC7E5E60255E8A4AC9D08FA99C7F91A67B697B (void);
// 0x000003CB System.Void NCMB.NCMBAppleAuthenManager/ExecuteInvoke::NCMBAppleAuth_HandlerCallback(NCMB.NCMBAppleAuthenManager/ExecuteInvoke/CallbackDelegate)
extern void ExecuteInvoke_NCMBAppleAuth_HandlerCallback_m4714AE5F953F4E2645C05C6BA426169BC5D78507 (void);
// 0x000003CC System.Void NCMB.NCMBAppleAuthenManager/ExecuteInvoke::NCMBAppleAuth_LoginWithAppleId(System.UInt32)
extern void ExecuteInvoke_NCMBAppleAuth_LoginWithAppleId_m70BA84C11B0B79A1926456BB644A12820BD2545E (void);
// 0x000003CD System.Void NCMB.NCMBAppleAuthenManager/ExecuteInvoke/CallbackDelegate::.ctor(System.Object,System.IntPtr)
extern void CallbackDelegate__ctor_m83B45B3C8634F79817C8844F1686EF6E48CFAA94 (void);
// 0x000003CE System.Void NCMB.NCMBAppleAuthenManager/ExecuteInvoke/CallbackDelegate::Invoke(System.UInt32,System.String)
extern void CallbackDelegate_Invoke_m09873191CE2C619C824410479BCA625916DFF629 (void);
// 0x000003CF System.IAsyncResult NCMB.NCMBAppleAuthenManager/ExecuteInvoke/CallbackDelegate::BeginInvoke(System.UInt32,System.String,System.AsyncCallback,System.Object)
extern void CallbackDelegate_BeginInvoke_m80648916C6CEB96271CDE66FC296E03639414B46 (void);
// 0x000003D0 System.Void NCMB.NCMBAppleAuthenManager/ExecuteInvoke/CallbackDelegate::EndInvoke(System.IAsyncResult)
extern void CallbackDelegate_EndInvoke_mDA135D96B653A44640A5E64D967F82DC41996318 (void);
// 0x000003D1 System.Void NCMB.NCMBAppleAuthenManager/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mB44137FA2673E5431176CED2DC85E18A502C5614 (void);
// 0x000003D2 System.Void NCMB.NCMBAppleAuthenManager/<>c__DisplayClass1_0::<NCMBiOSNativeLoginWithAppleId>b__0(System.String)
extern void U3CU3Ec__DisplayClass1_0_U3CNCMBiOSNativeLoginWithAppleIdU3Eb__0_m676DEC7DA5747CCF901397C0F80802608381066A (void);
// 0x000003D3 NCMB.NCMBAppleError NCMB.NCMBAppleResponse::get_Error()
extern void NCMBAppleResponse_get_Error_mDBEB3789E90EEBF2E30CD32B6026F17C577CD97A (void);
// 0x000003D4 NCMB.NCMBAppleCredential NCMB.NCMBAppleResponse::get_NCMBAppleCredential()
extern void NCMBAppleResponse_get_NCMBAppleCredential_mAD29DBF58C42E7834951FAF699616BDD6A7A48C8 (void);
// 0x000003D5 System.Void NCMB.NCMBAppleResponse::OnBeforeSerialize()
extern void NCMBAppleResponse_OnBeforeSerialize_m85739EC23DC8A1C5BE995442616685F676D4C48C (void);
// 0x000003D6 System.Void NCMB.NCMBAppleResponse::OnAfterDeserialize()
extern void NCMBAppleResponse_OnAfterDeserialize_m823680B3A35E08E9D87978FF0C22E77B17F15D84 (void);
// 0x000003D7 System.Void NCMB.NCMBAppleResponse::.ctor()
extern void NCMBAppleResponse__ctor_m427F8D8ECE736C45F3674637E0854A05D625B4DD (void);
// 0x000003D8 System.Int32 NCMB.NCMBAppleError::get_Code()
extern void NCMBAppleError_get_Code_mC3FDD0F8747C9204D6B1CEB6072A926662C33B2E (void);
// 0x000003D9 System.String NCMB.NCMBAppleError::get_Domain()
extern void NCMBAppleError_get_Domain_m4AFFEFE77FAD72D836734D9C65829ECEC1755E41 (void);
// 0x000003DA System.String NCMB.NCMBAppleError::get_UserInfo()
extern void NCMBAppleError_get_UserInfo_m173FD24109F35483E74E01D64659DC11C50FC692 (void);
// 0x000003DB System.Void NCMB.NCMBAppleError::.ctor()
extern void NCMBAppleError__ctor_mAB9F09D67631852E1902DC2A4BEDE83B0F99B7F6 (void);
// 0x000003DC System.String NCMB.NCMBAppleCredential::get_AuthorizationCode()
extern void NCMBAppleCredential_get_AuthorizationCode_mBD617EE6721DF35E784055FFEA2B270B136FE36C (void);
// 0x000003DD System.String NCMB.NCMBAppleCredential::get_UserId()
extern void NCMBAppleCredential_get_UserId_m902EE44017312E65B31DD98D312175D0633F8D07 (void);
// 0x000003DE System.Void NCMB.NCMBAppleCredential::.ctor()
extern void NCMBAppleCredential__ctor_m3EF47276DE03C971E39AF5790341DF0D74B18B4E (void);
// 0x000003DF System.Void NCMB.NCMBAppleParameters::.ctor(System.String,System.String,System.String)
extern void NCMBAppleParameters__ctor_mE57498262850F4954D364986FF7903B614606EF5 (void);
// 0x000003E0 System.Void NCMB.NCMBCallback::.ctor(System.Object,System.IntPtr)
extern void NCMBCallback__ctor_m9411539EB24F21D6D798DCF4D1E3356534B07285 (void);
// 0x000003E1 System.Void NCMB.NCMBCallback::Invoke(NCMB.NCMBException)
extern void NCMBCallback_Invoke_mD4DCB39745A1D6C1B8DC6D34A291FBD197A31422 (void);
// 0x000003E2 System.IAsyncResult NCMB.NCMBCallback::BeginInvoke(NCMB.NCMBException,System.AsyncCallback,System.Object)
extern void NCMBCallback_BeginInvoke_m531688FFA7CD4C1971048DB136E60ACDD54A4D1C (void);
// 0x000003E3 System.Void NCMB.NCMBCallback::EndInvoke(System.IAsyncResult)
extern void NCMBCallback_EndInvoke_mA383185C6A1B22554B8909D498006A4309E98F14 (void);
// 0x000003E4 System.Void NCMB.NCMBQueryCallback`1::.ctor(System.Object,System.IntPtr)
// 0x000003E5 System.Void NCMB.NCMBQueryCallback`1::Invoke(System.Collections.Generic.List`1<T>,NCMB.NCMBException)
// 0x000003E6 System.IAsyncResult NCMB.NCMBQueryCallback`1::BeginInvoke(System.Collections.Generic.List`1<T>,NCMB.NCMBException,System.AsyncCallback,System.Object)
// 0x000003E7 System.Void NCMB.NCMBQueryCallback`1::EndInvoke(System.IAsyncResult)
// 0x000003E8 System.Void NCMB.NCMBGetCallback`1::.ctor(System.Object,System.IntPtr)
// 0x000003E9 System.Void NCMB.NCMBGetCallback`1::Invoke(T,NCMB.NCMBException)
// 0x000003EA System.IAsyncResult NCMB.NCMBGetCallback`1::BeginInvoke(T,NCMB.NCMBException,System.AsyncCallback,System.Object)
// 0x000003EB System.Void NCMB.NCMBGetCallback`1::EndInvoke(System.IAsyncResult)
// 0x000003EC System.Void NCMB.NCMBCountCallback::.ctor(System.Object,System.IntPtr)
extern void NCMBCountCallback__ctor_m66075D0C5E70F1FF7CD348FC030FFE2BC88BDD70 (void);
// 0x000003ED System.Void NCMB.NCMBCountCallback::Invoke(System.Int32,NCMB.NCMBException)
extern void NCMBCountCallback_Invoke_m38BE2B9D44D3F193D201007DBDC95A1138CBEDBF (void);
// 0x000003EE System.IAsyncResult NCMB.NCMBCountCallback::BeginInvoke(System.Int32,NCMB.NCMBException,System.AsyncCallback,System.Object)
extern void NCMBCountCallback_BeginInvoke_m3AA632F5A301D096E31ACEEC9415FA46202CDE7A (void);
// 0x000003EF System.Void NCMB.NCMBCountCallback::EndInvoke(System.IAsyncResult)
extern void NCMBCountCallback_EndInvoke_m654012BBE84FAB35850DE9CBA935796692573990 (void);
// 0x000003F0 System.Void NCMB.HttpClientCallback::.ctor(System.Object,System.IntPtr)
extern void HttpClientCallback__ctor_m26AB8050FFCDE3AB0390DD3C088724759EBFEA5D (void);
// 0x000003F1 System.Void NCMB.HttpClientCallback::Invoke(System.Int32,System.String,NCMB.NCMBException)
extern void HttpClientCallback_Invoke_m1BD856418EB645DE35D5674D2A5C33DE9319EDCA (void);
// 0x000003F2 System.IAsyncResult NCMB.HttpClientCallback::BeginInvoke(System.Int32,System.String,NCMB.NCMBException,System.AsyncCallback,System.Object)
extern void HttpClientCallback_BeginInvoke_m8FD05EA3835F80D5EE533CDF47C6A2C34A4F8555 (void);
// 0x000003F3 System.Void NCMB.HttpClientCallback::EndInvoke(System.IAsyncResult)
extern void HttpClientCallback_EndInvoke_mF89E6F6A0A19240E32549F89D92D6021B96E934D (void);
// 0x000003F4 System.Void NCMB.HttpClientFileDataCallback::.ctor(System.Object,System.IntPtr)
extern void HttpClientFileDataCallback__ctor_m6328725BEBF311BD1C2588DA50AE2FF7950116D1 (void);
// 0x000003F5 System.Void NCMB.HttpClientFileDataCallback::Invoke(System.Int32,System.Byte[],NCMB.NCMBException)
extern void HttpClientFileDataCallback_Invoke_mD2A545E2E8F1B14CA0FAB6459ED7DBB24A48DCB6 (void);
// 0x000003F6 System.IAsyncResult NCMB.HttpClientFileDataCallback::BeginInvoke(System.Int32,System.Byte[],NCMB.NCMBException,System.AsyncCallback,System.Object)
extern void HttpClientFileDataCallback_BeginInvoke_m0D109F1C4B34AD5942B62E9582C93152FD80C6E5 (void);
// 0x000003F7 System.Void NCMB.HttpClientFileDataCallback::EndInvoke(System.IAsyncResult)
extern void HttpClientFileDataCallback_EndInvoke_m64FE22A80399BCA7D37E124B594993F0B15E5FCD (void);
// 0x000003F8 System.Void NCMB.NCMBExecuteScriptCallback::.ctor(System.Object,System.IntPtr)
extern void NCMBExecuteScriptCallback__ctor_mF5D237F4D97E56B4846E70E4CA9FADC6C5475D26 (void);
// 0x000003F9 System.Void NCMB.NCMBExecuteScriptCallback::Invoke(System.Byte[],NCMB.NCMBException)
extern void NCMBExecuteScriptCallback_Invoke_m14F5DDD76388A19993103DF30D97665E7796351C (void);
// 0x000003FA System.IAsyncResult NCMB.NCMBExecuteScriptCallback::BeginInvoke(System.Byte[],NCMB.NCMBException,System.AsyncCallback,System.Object)
extern void NCMBExecuteScriptCallback_BeginInvoke_mC3FAF22D51F6688D19180F37ABBDB462CCB4AA85 (void);
// 0x000003FB System.Void NCMB.NCMBExecuteScriptCallback::EndInvoke(System.IAsyncResult)
extern void NCMBExecuteScriptCallback_EndInvoke_m9B9892E9A1DE0E71E0658A1DFB707167A8939BAB (void);
// 0x000003FC System.Void NCMB.NCMBGetFileCallback::.ctor(System.Object,System.IntPtr)
extern void NCMBGetFileCallback__ctor_m2ABF1C11926AC1066378C2249DA18E4A0CC5B45A (void);
// 0x000003FD System.Void NCMB.NCMBGetFileCallback::Invoke(System.Byte[],NCMB.NCMBException)
extern void NCMBGetFileCallback_Invoke_mA71E3A031CBFCCAEDAC3253F97D3674C0F8031A1 (void);
// 0x000003FE System.IAsyncResult NCMB.NCMBGetFileCallback::BeginInvoke(System.Byte[],NCMB.NCMBException,System.AsyncCallback,System.Object)
extern void NCMBGetFileCallback_BeginInvoke_mA934ED77E736D2D54A7C343D3A1037C1E71C9F94 (void);
// 0x000003FF System.Void NCMB.NCMBGetFileCallback::EndInvoke(System.IAsyncResult)
extern void NCMBGetFileCallback_EndInvoke_m5A08E829B3F72F5FAE9CD33683B1C4CA93F30376 (void);
// 0x00000400 System.Void NCMB.NCMBGetTokenCallback::.ctor(System.Object,System.IntPtr)
extern void NCMBGetTokenCallback__ctor_mAE865062B4FC5E2C51B545D8F192C6176967135C (void);
// 0x00000401 System.Void NCMB.NCMBGetTokenCallback::Invoke(System.String,NCMB.NCMBException)
extern void NCMBGetTokenCallback_Invoke_mE59F0A2342E69E7562A5804D870E00ED5F8F73F4 (void);
// 0x00000402 System.IAsyncResult NCMB.NCMBGetTokenCallback::BeginInvoke(System.String,NCMB.NCMBException,System.AsyncCallback,System.Object)
extern void NCMBGetTokenCallback_BeginInvoke_m1DF176BCDA35CA20797E1B4830AC5D4DF6E3CADC (void);
// 0x00000403 System.Void NCMB.NCMBGetTokenCallback::EndInvoke(System.IAsyncResult)
extern void NCMBGetTokenCallback_EndInvoke_mB3A533D3E7E744479D5BFAB8DAA2B8E227774AB6 (void);
// 0x00000404 System.Void NCMB.NCMBException::.ctor()
extern void NCMBException__ctor_mE63D0676BB0DA9A598FB7D11FC9962E17752D393 (void);
// 0x00000405 System.Void NCMB.NCMBException::.ctor(System.Exception)
extern void NCMBException__ctor_m21B151AFA272FA1EEB0172A4BCA661F376516CAA (void);
// 0x00000406 System.Void NCMB.NCMBException::.ctor(System.String)
extern void NCMBException__ctor_m54ACFC3C3A2A9512B6E9DB84475D8190CC6EA039 (void);
// 0x00000407 System.String NCMB.NCMBException::get_ErrorCode()
extern void NCMBException_get_ErrorCode_m66EA51AD2E678D6A99419964C21BAE5BCE54825E (void);
// 0x00000408 System.Void NCMB.NCMBException::set_ErrorCode(System.String)
extern void NCMBException_set_ErrorCode_m83B719C6614311B50FE22D79FDB000E0DD6C5935 (void);
// 0x00000409 System.String NCMB.NCMBException::get_ErrorMessage()
extern void NCMBException_get_ErrorMessage_m9CC71E7018075963359CEB1C604DF21EAFE93DAC (void);
// 0x0000040A System.Void NCMB.NCMBException::set_ErrorMessage(System.String)
extern void NCMBException_set_ErrorMessage_m9B884AC5AA5741593677250F46F8E544BD3737D0 (void);
// 0x0000040B System.String NCMB.NCMBException::get_Message()
extern void NCMBException_get_Message_m4C179B9ACA7B56CB2BF24605A4DD8B87948EE209 (void);
// 0x0000040C System.Void NCMB.NCMBException::.cctor()
extern void NCMBException__cctor_m566F6A944909B517151749379D7C14226EEA748B (void);
// 0x0000040D System.Void NCMB.NCMBFacebookParameters::.ctor(System.String,System.String,System.DateTime)
extern void NCMBFacebookParameters__ctor_mE2498257B49443BEB80FE2D010420229398C8C2F (void);
// 0x0000040E System.String NCMB.NCMBFile::get_FileName()
extern void NCMBFile_get_FileName_m60720A28CC042D9B92F4B6CB0B980CCBCDE55FA6 (void);
// 0x0000040F System.Void NCMB.NCMBFile::set_FileName(System.String)
extern void NCMBFile_set_FileName_mE6CD516F7AC47A09B2F99CA722BF0F06934E37E5 (void);
// 0x00000410 System.Byte[] NCMB.NCMBFile::get_FileData()
extern void NCMBFile_get_FileData_m1031293D72D378CA49A3D3D1DD88DD1572DAE457 (void);
// 0x00000411 System.Void NCMB.NCMBFile::set_FileData(System.Byte[])
extern void NCMBFile_set_FileData_m4421A5A0F91431FF8068903536CE3BCCAAFA68EC (void);
// 0x00000412 System.Void NCMB.NCMBFile::.ctor()
extern void NCMBFile__ctor_mE77907F9EC202A9507A099B99D7E1B2120A3F74C (void);
// 0x00000413 System.Void NCMB.NCMBFile::.ctor(System.String)
extern void NCMBFile__ctor_m3C83C75457D55474261AE1CB074B175A8CE21818 (void);
// 0x00000414 System.Void NCMB.NCMBFile::.ctor(System.String,System.Byte[])
extern void NCMBFile__ctor_m5774765EC9B541C4739D48546DB8B3D6EC8B34C0 (void);
// 0x00000415 System.Void NCMB.NCMBFile::.ctor(System.String,System.Byte[],NCMB.NCMBACL)
extern void NCMBFile__ctor_m8194EECFDF1B454FDC7EB1D1A06DF041436160ED (void);
// 0x00000416 System.Void NCMB.NCMBFile::SaveAsync(NCMB.NCMBCallback)
extern void NCMBFile_SaveAsync_mDACD4E6057DA344E214C36E2CDF89B6A7B39C863 (void);
// 0x00000417 System.Void NCMB.NCMBFile::SaveAsync()
extern void NCMBFile_SaveAsync_mD19BB36E3C8B0676B99B78C91776D85C5B02E2B4 (void);
// 0x00000418 System.Void NCMB.NCMBFile::FetchAsync(NCMB.NCMBGetFileCallback)
extern void NCMBFile_FetchAsync_m968BD3FA13BD714FAB07FD15864501FAB686DAD3 (void);
// 0x00000419 System.Void NCMB.NCMBFile::FetchAsync()
extern void NCMBFile_FetchAsync_m8DA8B970667A9065BDF444A37E2BDE498884292D (void);
// 0x0000041A NCMB.NCMBQuery`1<NCMB.NCMBFile> NCMB.NCMBFile::GetQuery()
extern void NCMBFile_GetQuery_mE50CC819115879642D1C88D340FA09C9BCD12925 (void);
// 0x0000041B System.String NCMB.NCMBFile::_getBaseUrl()
extern void NCMBFile__getBaseUrl_m7DAAA5DC181126807314B15315A1FA61455740D4 (void);
// 0x0000041C System.Void NCMB.NCMBFile/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m6741190E00554D5D661D1C2495B62BE46ED9CF50 (void);
// 0x0000041D System.Void NCMB.NCMBFile/<>c__DisplayClass10_0::<SaveAsync>b__0(System.Int32,System.String,NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass10_0_U3CSaveAsyncU3Eb__0_m60473A0C1B75AA3000F7D791FAEB812480AE1DBD (void);
// 0x0000041E System.Void NCMB.NCMBFile/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m93ED7546ECA11FADBCFA5D0D08F68691A768CEDB (void);
// 0x0000041F System.Void NCMB.NCMBFile/<>c__DisplayClass12_0::<FetchAsync>b__0(System.Int32,System.Byte[],NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass12_0_U3CFetchAsyncU3Eb__0_mC30E0FB59429A3BC9F96BF438021EC83CF6140AA (void);
// 0x00000420 System.Double NCMB.NCMBGeoPoint::get_Latitude()
extern void NCMBGeoPoint_get_Latitude_mA701AB3DD72C66ECC54B564763553EF8BD638B67 (void);
// 0x00000421 System.Void NCMB.NCMBGeoPoint::set_Latitude(System.Double)
extern void NCMBGeoPoint_set_Latitude_mF9CE6978C04BF38FC99A273EB2C25928180FF106 (void);
// 0x00000422 System.Double NCMB.NCMBGeoPoint::get_Longitude()
extern void NCMBGeoPoint_get_Longitude_m39ADA9F8A394B404F134702D649B1F28BCFF9231 (void);
// 0x00000423 System.Void NCMB.NCMBGeoPoint::set_Longitude(System.Double)
extern void NCMBGeoPoint_set_Longitude_mFCB3A006D32D628A7B917F5540EE5678BA7DF935 (void);
// 0x00000424 System.Void NCMB.NCMBGeoPoint::.ctor(System.Double,System.Double)
extern void NCMBGeoPoint__ctor_m5E7A2D8E13F647E3D341711C8C5DE64AE20EAB14 (void);
// 0x00000425 System.Void NCMB.NCMBInstallation::setDefaultProperty()
extern void NCMBInstallation_setDefaultProperty_mBB17C7B4FF9165835D2E60D54685984145BA037B (void);
// 0x00000426 System.Void NCMB.NCMBInstallation::.ctor()
extern void NCMBInstallation__ctor_mBD35C983AFEEBA45C40B4EA968DC372FA5A28B82 (void);
// 0x00000427 System.Void NCMB.NCMBInstallation::.ctor(System.String)
extern void NCMBInstallation__ctor_mBB13EA853E276A8253E9629B4AA6D4042BCBF499 (void);
// 0x00000428 System.String NCMB.NCMBInstallation::get_ApplicationName()
extern void NCMBInstallation_get_ApplicationName_mE119702B0B32B4E1E715D76E402E55BAE10B8833 (void);
// 0x00000429 System.Void NCMB.NCMBInstallation::set_ApplicationName(System.String)
extern void NCMBInstallation_set_ApplicationName_mF72BEAC02D8FB65C95B1B26DA817BE85D594E59F (void);
// 0x0000042A System.String NCMB.NCMBInstallation::get_AppVersion()
extern void NCMBInstallation_get_AppVersion_m552509DB2656BFE7820DBAFF816516482CD9474A (void);
// 0x0000042B System.Void NCMB.NCMBInstallation::set_AppVersion(System.String)
extern void NCMBInstallation_set_AppVersion_mC966B93BBBD528DB647DFBD18BF3900485B5CE09 (void);
// 0x0000042C System.Void NCMB.NCMBInstallation::set_DeviceToken(System.String)
extern void NCMBInstallation_set_DeviceToken_mB84D08E5C5FF63760A8F0AAB3B9A81CAA4F17B3C (void);
// 0x0000042D System.Void NCMB.NCMBInstallation::GetDeviceToken(NCMB.NCMBGetCallback`1<System.String>)
extern void NCMBInstallation_GetDeviceToken_mCA79AF6304BAFD9C6BDFA545287EB39EF4798DB3 (void);
// 0x0000042E System.String NCMB.NCMBInstallation::get_DeviceType()
extern void NCMBInstallation_get_DeviceType_m2A8903F151337C0449860EBCE4984722C4E116AD (void);
// 0x0000042F System.Void NCMB.NCMBInstallation::set_DeviceType(System.String)
extern void NCMBInstallation_set_DeviceType_m3F23717B727BF83FE98278F190E743DE2B6CA142 (void);
// 0x00000430 System.String NCMB.NCMBInstallation::get_SdkVersion()
extern void NCMBInstallation_get_SdkVersion_mDB726558EB94F5D7F8ED25D75A0AA0DA17B818A0 (void);
// 0x00000431 System.Void NCMB.NCMBInstallation::set_SdkVersion(System.String)
extern void NCMBInstallation_set_SdkVersion_mEFC673B205EFF82447CF184114494A0A8314C60F (void);
// 0x00000432 System.String NCMB.NCMBInstallation::get_TimeZone()
extern void NCMBInstallation_get_TimeZone_mF8103121D1627BFD836324ECE9284D70B0CA9754 (void);
// 0x00000433 System.Void NCMB.NCMBInstallation::set_TimeZone(System.String)
extern void NCMBInstallation_set_TimeZone_mE41BC12E8786E4459EB981F9FDC4898FC411D1B6 (void);
// 0x00000434 NCMB.NCMBInstallation NCMB.NCMBInstallation::getCurrentInstallation()
extern void NCMBInstallation_getCurrentInstallation_m2E694FD700A511C4105F89A45D06941DB18E18C3 (void);
// 0x00000435 NCMB.NCMBQuery`1<NCMB.NCMBInstallation> NCMB.NCMBInstallation::GetQuery()
extern void NCMBInstallation_GetQuery_m1545F8ED4F97D658F7E3FC963B6B08856022AE41 (void);
// 0x00000436 System.String NCMB.NCMBInstallation::_getBaseUrl()
extern void NCMBInstallation__getBaseUrl_mA2DD397800ED7BE8851A3772CDE2485983E7573B (void);
// 0x00000437 System.Void NCMB.NCMBInstallation::_afterSave(System.Int32,NCMB.NCMBException)
extern void NCMBInstallation__afterSave_m54F98084EB48774C4561546AF52EDA0E6B4B7F9C (void);
// 0x00000438 System.Void NCMB.NCMBInstallation::_saveInstallationToDisk(System.String)
extern void NCMBInstallation__saveInstallationToDisk_mB732D2DC117122373AEDF00F1CED34295706CAF0 (void);
// 0x00000439 System.Void NCMB.NCMBInstallation/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m7E8E5B43AE83F0CED58E85FA9BDC9A0FABF7A00D (void);
// 0x0000043A System.Void NCMB.NCMBInstallation/<>c__DisplayClass11_0::<GetDeviceToken>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CGetDeviceTokenU3Eb__0_mBF1170E354B8DCFBBF02B8E8EDD2871A44FD6070 (void);
// 0x0000043B System.Object NCMB.NCMBObject::get_Item(System.String)
extern void NCMBObject_get_Item_m115F64C13A13ED1F939DBCD2F4C637B09730FA80 (void);
// 0x0000043C System.Void NCMB.NCMBObject::set_Item(System.String,System.Object)
extern void NCMBObject_set_Item_mC0E83DCFF1B8DDD0BDD186B278E8BE512B271F56 (void);
// 0x0000043D System.Void NCMB.NCMBObject::_onSettingValue(System.String,System.Object)
extern void NCMBObject__onSettingValue_mAB73AD78BF6A9580E96830572DFDE7FD95DC6718 (void);
// 0x0000043E System.String NCMB.NCMBObject::get_ClassName()
extern void NCMBObject_get_ClassName_m01A4D20A0978673D1A68B8475DFD0ABCBB61E076 (void);
// 0x0000043F System.Void NCMB.NCMBObject::set_ClassName(System.String)
extern void NCMBObject_set_ClassName_m32FF48B1D53E27B2ABC905B8EF455142F9B6C90B (void);
// 0x00000440 System.String NCMB.NCMBObject::get_ObjectId()
extern void NCMBObject_get_ObjectId_m085515EEF298EBF439301AE2C87C9FE1E3AA8569 (void);
// 0x00000441 System.Void NCMB.NCMBObject::set_ObjectId(System.String)
extern void NCMBObject_set_ObjectId_mB9DBF772722EFA92B3075553CCC4B10E9A9184C0 (void);
// 0x00000442 System.Nullable`1<System.DateTime> NCMB.NCMBObject::get_UpdateDate()
extern void NCMBObject_get_UpdateDate_m625B56ACEBA8060954BF320C12C0CF4B55DC1F6C (void);
// 0x00000443 System.Void NCMB.NCMBObject::set_UpdateDate(System.Nullable`1<System.DateTime>)
extern void NCMBObject_set_UpdateDate_m1169A2403AD40DFCA2A8C05EC368A5BD167B26EB (void);
// 0x00000444 System.Nullable`1<System.DateTime> NCMB.NCMBObject::get_CreateDate()
extern void NCMBObject_get_CreateDate_m62DBDE53B0795DA21C34317694675FD64E8A53A4 (void);
// 0x00000445 System.Void NCMB.NCMBObject::set_CreateDate(System.Nullable`1<System.DateTime>)
extern void NCMBObject_set_CreateDate_m2D7372F0D460FB750A693E445DF845ABCFE00964 (void);
// 0x00000446 System.Void NCMB.NCMBObject::set_ACL(NCMB.NCMBACL)
extern void NCMBObject_set_ACL_m7C3E2F6DD13993FDB821FA21FD4E98F9D11F0F23 (void);
// 0x00000447 NCMB.NCMBACL NCMB.NCMBObject::get_ACL()
extern void NCMBObject_get_ACL_m1119A1B910777FBF29B2930976BFE1DB2B84B8A2 (void);
// 0x00000448 System.Boolean NCMB.NCMBObject::_checkIsDataAvailable(System.String)
extern void NCMBObject__checkIsDataAvailable_mA57A1AEBF1E1AEC03B02A04E62491C02E19B0A22 (void);
// 0x00000449 System.Void NCMB.NCMBObject::_checkGetAccess(System.String)
extern void NCMBObject__checkGetAccess_mD3E606CC6B7DC029E4B5EFF6C02BE43AA1ACE4A1 (void);
// 0x0000044A System.Boolean NCMB.NCMBObject::get_IsDirty()
extern void NCMBObject_get_IsDirty_m1E64B7ACECA022F2B45D6438288E8F3D0A97F0AD (void);
// 0x0000044B System.Void NCMB.NCMBObject::set_IsDirty(System.Boolean)
extern void NCMBObject_set_IsDirty_mD8D4A9E8ECC5071943CF15FCDCB180DC8503199F (void);
// 0x0000044C System.Boolean NCMB.NCMBObject::_checkIsDirty(System.Boolean)
extern void NCMBObject__checkIsDirty_mEC62B906D10AC3D67BECE587F7B556864CA33B02 (void);
// 0x0000044D System.Boolean NCMB.NCMBObject::_hasDirtyChildren()
extern void NCMBObject__hasDirtyChildren_m38BE3E6CC418BF659F1C6F6E769B7C40D750B7AA (void);
// 0x0000044E System.Void NCMB.NCMBObject::_findUnsavedChildren(System.Object,System.Collections.Generic.List`1<NCMB.NCMBObject>)
extern void NCMBObject__findUnsavedChildren_mB6FD6A7E4C69433345E42EBC65EA8D0CEBD12BF7 (void);
// 0x0000044F System.Collections.Generic.ICollection`1<System.String> NCMB.NCMBObject::get_Keys()
extern void NCMBObject_get_Keys_m1ED804C9455389440C544EAE4B53C89191AAACF6 (void);
// 0x00000450 System.Void NCMB.NCMBObject::.ctor()
extern void NCMBObject__ctor_mEA7161237C9168BA1783D8E48880C3329858490C (void);
// 0x00000451 NCMB.NCMBRelation`1<T> NCMB.NCMBObject::GetRelation(System.String)
// 0x00000452 System.Void NCMB.NCMBObject::.ctor(System.String)
extern void NCMBObject__ctor_m95D47F242E3377E24E9DF8863DC8859EB68658DB (void);
// 0x00000453 System.Void NCMB.NCMBObject::_performOperation(System.String,NCMB.Internal.INCMBFieldOperation)
extern void NCMBObject__performOperation_m29ACC8949E34E86F911F62FC9F890D2FB61707F9 (void);
// 0x00000454 System.Collections.Generic.IDictionary`2<System.String,NCMB.Internal.INCMBFieldOperation> NCMB.NCMBObject::get__currentOperations()
extern void NCMBObject_get__currentOperations_m5939F459E3D437F5FAB66DC71F7286A4C15F7190 (void);
// 0x00000455 System.Collections.Generic.IDictionary`2<System.String,NCMB.Internal.INCMBFieldOperation> NCMB.NCMBObject::StartSave()
extern void NCMBObject_StartSave_mEFBAD18344DF06F30842E4FA6CC39E6AEBE3BA2C (void);
// 0x00000456 System.Boolean NCMB.NCMBObject::_isValidType(System.Object)
extern void NCMBObject__isValidType_m22BA7C636A0441A8217ACD7A9F636E4F5D2DA83C (void);
// 0x00000457 System.Void NCMB.NCMBObject::_listIsValidType(System.Collections.IEnumerable)
extern void NCMBObject__listIsValidType_mBE970C6D08DFC87E899026D058EC2BA4A0FF50B3 (void);
// 0x00000458 System.Void NCMB.NCMBObject::Revert()
extern void NCMBObject_Revert_m72999B6A1ABA69D9E93635333BFE6B73D7278193 (void);
// 0x00000459 System.Void NCMB.NCMBObject::_rebuildEstimatedData()
extern void NCMBObject__rebuildEstimatedData_m617099C49011A91A17EE9F6A94C4F82049DC5D91 (void);
// 0x0000045A System.Void NCMB.NCMBObject::_updateLatestEstimatedData()
extern void NCMBObject__updateLatestEstimatedData_mF0B11E085ADE3EF72D9F9EF9F9324C767F45A7A7 (void);
// 0x0000045B System.Void NCMB.NCMBObject::_applyOperations(System.Collections.Generic.IDictionary`2<System.String,NCMB.Internal.INCMBFieldOperation>,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void NCMBObject__applyOperations_mB47FAB46348E136F2D67AD8F9034C7A0DB451198 (void);
// 0x0000045C System.Void NCMB.NCMBObject::Add(System.String,System.Object)
extern void NCMBObject_Add_m311859FC00D48E993DF810F45665D0B7A1E7BB79 (void);
// 0x0000045D System.Void NCMB.NCMBObject::Remove(System.String)
extern void NCMBObject_Remove_m464E7A1DC9CC67485D46921594F29C039CAF34EC (void);
// 0x0000045E System.Void NCMB.NCMBObject::RemoveRangeFromList(System.String,System.Collections.IEnumerable)
extern void NCMBObject_RemoveRangeFromList_m0A5D771DADE9E71719374B41208F4B1777E8930D (void);
// 0x0000045F System.Void NCMB.NCMBObject::AddToList(System.String,System.Object)
extern void NCMBObject_AddToList_mE5823E1EE42B5E8943483CD6D2033299BB572FE1 (void);
// 0x00000460 System.Void NCMB.NCMBObject::AddRangeToList(System.String,System.Collections.IEnumerable)
extern void NCMBObject_AddRangeToList_mF823BC3C35254EBE8F6134D97D4B14033FA5BCC9 (void);
// 0x00000461 System.Void NCMB.NCMBObject::AddUniqueToList(System.String,System.Object)
extern void NCMBObject_AddUniqueToList_m36F9A17F022686EA3C62D8C224C7D50BC3A466B6 (void);
// 0x00000462 System.Void NCMB.NCMBObject::AddRangeUniqueToList(System.String,System.Collections.IEnumerable)
extern void NCMBObject_AddRangeUniqueToList_m1248B082FA5E6B2A730EAABAA9CD4DCD2A8B804C (void);
// 0x00000463 System.Void NCMB.NCMBObject::Increment(System.String)
extern void NCMBObject_Increment_mD22DF5CCADA24BBAD6D858E63C742EB7724948FA (void);
// 0x00000464 System.Void NCMB.NCMBObject::Increment(System.String,System.Int64)
extern void NCMBObject_Increment_m26B2DB81BE056CE78EE2A1B3A4C0BC72D0EF9DCD (void);
// 0x00000465 System.Void NCMB.NCMBObject::Increment(System.String,System.Double)
extern void NCMBObject_Increment_m6B2EC9F8A5C66F76A706E55200688A8091FB3045 (void);
// 0x00000466 System.Void NCMB.NCMBObject::_incrementMerge(System.String,System.Object)
extern void NCMBObject__incrementMerge_m0037B122D73ECAED36C53125159C0349B6058292 (void);
// 0x00000467 System.Object NCMB.NCMBObject::_addNumbers(System.Object,System.Object)
extern void NCMBObject__addNumbers_mA5B6240BEA574BDE834F7E3253D56927E3F0BB9D (void);
// 0x00000468 System.String NCMB.NCMBObject::_getBaseUrl()
extern void NCMBObject__getBaseUrl_m8C0445F87C03CFDC15E8A59640E7150EA573903E (void);
// 0x00000469 System.Void NCMB.NCMBObject::DeleteAsync(NCMB.NCMBCallback)
extern void NCMBObject_DeleteAsync_m6D5A542935493EA43512F9ADC8F95B09D3595CDC (void);
// 0x0000046A System.Void NCMB.NCMBObject::DeleteAsync()
extern void NCMBObject_DeleteAsync_m99892770FCDDCC8A9F947640505E6D941E6F46D2 (void);
// 0x0000046B System.Void NCMB.NCMBObject::SaveAsync(NCMB.NCMBCallback)
extern void NCMBObject_SaveAsync_mB36A548E04576B1E97E63A0D24BFE0A411DF742D (void);
// 0x0000046C System.Void NCMB.NCMBObject::SaveAsync()
extern void NCMBObject_SaveAsync_m460F494A301A7ECABC69AD6304DD44F632A84324 (void);
// 0x0000046D System.Void NCMB.NCMBObject::Save()
extern void NCMBObject_Save_m7CC0CABF37DFF121977A55A4641D9974546E2BA9 (void);
// 0x0000046E System.Void NCMB.NCMBObject::Save(NCMB.NCMBCallback)
extern void NCMBObject_Save_m5C73FC83E6109267F60FD0E9C814D3AC9EF93EA4 (void);
// 0x0000046F System.Void NCMB.NCMBObject::_beforeSave()
extern void NCMBObject__beforeSave_m4C8F32550DC4C1FCAC67A77851670C3AA0BA0B58 (void);
// 0x00000470 System.Void NCMB.NCMBObject::_afterSave(System.Int32,NCMB.NCMBException)
extern void NCMBObject__afterSave_m3CA541FA76353FA85B46C6D6C40CBE471B29BCD1 (void);
// 0x00000471 System.Void NCMB.NCMBObject::_afterDelete(NCMB.NCMBException)
extern void NCMBObject__afterDelete_mB71083213FBE231B69A54EBA9DC4BBD0B235B6C3 (void);
// 0x00000472 System.String NCMB.NCMBObject::_toJSONObjectForSaving(System.Collections.Generic.IDictionary`2<System.String,NCMB.Internal.INCMBFieldOperation>)
extern void NCMBObject__toJSONObjectForSaving_mC490C7D2124F588F10EC60D3A2CB273E78243829 (void);
// 0x00000473 System.Void NCMB.NCMBObject::FetchAsync(NCMB.NCMBCallback)
extern void NCMBObject_FetchAsync_m243164026ABE12459C6BD4D2FD7A88B15D4E2CB5 (void);
// 0x00000474 System.Void NCMB.NCMBObject::FetchAsync()
extern void NCMBObject_FetchAsync_m7A087B5696C268F80AD8A4AB184D35D350388B81 (void);
// 0x00000475 System.Boolean NCMB.NCMBObject::ContainsKey(System.String)
extern void NCMBObject_ContainsKey_m47AFB7A5BD4FDD749C437289D4C03E0D0D993FC0 (void);
// 0x00000476 NCMB.NCMBObject NCMB.NCMBObject::CreateWithoutData(System.String,System.String)
extern void NCMBObject_CreateWithoutData_mBE6F91735B25EF93108309F1C95ED352DA636381 (void);
// 0x00000477 System.Void NCMB.NCMBObject::_mergeFromServer(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean)
extern void NCMBObject__mergeFromServer_m7FFA7B1DE07A640792107EEDC23454A5F1FD6334 (void);
// 0x00000478 System.Void NCMB.NCMBObject::_handleSaveResult(System.Boolean,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Collections.Generic.IDictionary`2<System.String,NCMB.Internal.INCMBFieldOperation>)
extern void NCMBObject__handleSaveResult_m5E5AE7ED299954F427E9555DD17DB43DCF3F98AC (void);
// 0x00000479 System.Void NCMB.NCMBObject::_handleFetchResult(System.Boolean,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void NCMBObject__handleFetchResult_mD3119AD3F153B5B3DBEDA9FFD4567AC86DAB29DF (void);
// 0x0000047A System.Void NCMB.NCMBObject::_handleDeleteResult(System.Boolean)
extern void NCMBObject__handleDeleteResult_m0282C531E357FC542579751433C97CB8EF4B1EB1 (void);
// 0x0000047B System.Void NCMB.NCMBObject::_setCreateDate(System.String)
extern void NCMBObject__setCreateDate_m3274FB970CAAB5156BDF688BE196EA7B599A5D0B (void);
// 0x0000047C System.Void NCMB.NCMBObject::_setUpdateDate(System.String)
extern void NCMBObject__setUpdateDate_m4B27D5B86947E6272ECCA74C45D140ED0DBFB86E (void);
// 0x0000047D System.Void NCMB.NCMBObject::_saveToVariable()
extern void NCMBObject__saveToVariable_mECFAD31A559AE1195BD9B29D13ED9F5D6F954739 (void);
// 0x0000047E NCMB.NCMBObject NCMB.NCMBObject::_getFromVariable()
extern void NCMBObject__getFromVariable_mE904A84F66AD2DAAA8115287E9F6AF7903F741EA (void);
// 0x0000047F System.Void NCMB.NCMBObject::_saveToDisk(System.String)
extern void NCMBObject__saveToDisk_m617F540DD64CFACC970DC0EC4EBFB5068A24F72C (void);
// 0x00000480 NCMB.NCMBObject NCMB.NCMBObject::_getFromDisk(System.String)
extern void NCMBObject__getFromDisk_m557CBF5554CFCBD823FAFC75BCB4A49CCE4CE143 (void);
// 0x00000481 System.String NCMB.NCMBObject::_getDiskData(System.String)
extern void NCMBObject__getDiskData_mF19CF278596A8D91D9AFA71AD48EE2A847A40106 (void);
// 0x00000482 System.String NCMB.NCMBObject::_toJsonDataForDataFile()
extern void NCMBObject__toJsonDataForDataFile_mEE0D4E53B30498EB89BB04348DF19113AC9BD42B (void);
// 0x00000483 System.Void NCMB.NCMBObject::_setDefaultValues()
extern void NCMBObject__setDefaultValues_m55D3845791B06B9328C816DC1942109E5B16489E (void);
// 0x00000484 System.Void NCMB.NCMBObject::.cctor()
extern void NCMBObject__cctor_m00D62A5F6185198247D667833DFA1F4D6B1081C8 (void);
// 0x00000485 System.Void NCMB.NCMBObject/<>c__DisplayClass67_0::.ctor()
extern void U3CU3Ec__DisplayClass67_0__ctor_m816C7346FBB53D438D377BF40EC27EFCB0558312 (void);
// 0x00000486 System.Void NCMB.NCMBObject/<>c__DisplayClass67_0::<DeleteAsync>b__0(System.Int32,System.String,NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass67_0_U3CDeleteAsyncU3Eb__0_m6CE41B345C4BC4E72A8B24D6C1536637EA6D1DB1 (void);
// 0x00000487 System.Void NCMB.NCMBObject/<>c__DisplayClass72_0::.ctor()
extern void U3CU3Ec__DisplayClass72_0__ctor_m3135DD8A9EEFA6C9F7BCDC7CBF6D1576B51F3356 (void);
// 0x00000488 System.Void NCMB.NCMBObject/<>c__DisplayClass72_0::<Save>b__0(System.Int32,System.String,NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass72_0_U3CSaveU3Eb__0_mD874ACF9B2FDDBB78E80E1F9C6CC6147E8FC729E (void);
// 0x00000489 System.Void NCMB.NCMBObject/<>c__DisplayClass77_0::.ctor()
extern void U3CU3Ec__DisplayClass77_0__ctor_m69D84D505D2747D0842226AFBBA3F80B59C651A1 (void);
// 0x0000048A System.Void NCMB.NCMBObject/<>c__DisplayClass77_0::<FetchAsync>b__0(System.Int32,System.String,NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass77_0_U3CFetchAsyncU3Eb__0_m207B0A93E8DDF2ACABB8C3BA954BF172D24A06D5 (void);
// 0x0000048B System.Void NCMB.NCMBPush::registerNotification(System.Boolean)
extern void NCMBPush_registerNotification_m9472B9C7E1874376759C25D8773C4479FB195D10 (void);
// 0x0000048C System.Void NCMB.NCMBPush::registerNotificationWithLocation()
extern void NCMBPush_registerNotificationWithLocation_m9FB9C648A7FA50C2D475FF36F797D56185F63D18 (void);
// 0x0000048D System.Void NCMB.NCMBPush::clearAll()
extern void NCMBPush_clearAll_mBAB0EB2D91F7FB60BF22944AA81A67EFACC40066 (void);
// 0x0000048E System.Void NCMB.NCMBPush::.cctor()
extern void NCMBPush__cctor_mC8A778496631C71C776F3444CB423168A2E3A9BA (void);
// 0x0000048F System.Void NCMB.NCMBPush::.ctor()
extern void NCMBPush__ctor_mE0F7878C2293EDFA3849AA71B145376AD731F89D (void);
// 0x00000490 System.Void NCMB.NCMBPush::Register(System.Boolean)
extern void NCMBPush_Register_m56C57BD92DB293E4016738EC827DA1D9E6C14A5F (void);
// 0x00000491 System.Void NCMB.NCMBPush::RegisterWithLocation()
extern void NCMBPush_RegisterWithLocation_mC395DE29754AC67E0FCB19623C27199B1D70384E (void);
// 0x00000492 System.String NCMB.NCMBPush::get_Message()
extern void NCMBPush_get_Message_m9EB50121D9E0DA875CF64AD92359C1A041211EEF (void);
// 0x00000493 System.Void NCMB.NCMBPush::set_Message(System.String)
extern void NCMBPush_set_Message_m44021DD4033474102DB50CD2845C895A50AFC6F2 (void);
// 0x00000494 System.Object NCMB.NCMBPush::get_SearchCondition()
extern void NCMBPush_get_SearchCondition_m4F0221EDC2F70B65EB790080F683672F840639B4 (void);
// 0x00000495 System.Void NCMB.NCMBPush::set_SearchCondition(System.Object)
extern void NCMBPush_set_SearchCondition_m2E700C476D0EDE57D2D0D2C5528FB99D01761909 (void);
// 0x00000496 System.DateTime NCMB.NCMBPush::get_DeliveryTime()
extern void NCMBPush_get_DeliveryTime_m0DC00872C49389BADF372DF07457DAB23390B82F (void);
// 0x00000497 System.Void NCMB.NCMBPush::set_DeliveryTime(System.DateTime)
extern void NCMBPush_set_DeliveryTime_m0BBFCCC253FEB414EE501D9B48F01C1EE3D9DB3F (void);
// 0x00000498 System.Boolean NCMB.NCMBPush::get_ImmediateDeliveryFlag()
extern void NCMBPush_get_ImmediateDeliveryFlag_mA857D03CB09C0EEF24D773A90249457B825E6522 (void);
// 0x00000499 System.Void NCMB.NCMBPush::set_ImmediateDeliveryFlag(System.Boolean)
extern void NCMBPush_set_ImmediateDeliveryFlag_m7D191253578CFC61DF50AF0AC6E9001FB1CC1821 (void);
// 0x0000049A System.String NCMB.NCMBPush::get_Title()
extern void NCMBPush_get_Title_mAF0FD5029702E12C61E97E4DA44803E6955C696C (void);
// 0x0000049B System.Void NCMB.NCMBPush::set_Title(System.String)
extern void NCMBPush_set_Title_m287464432AB535739A9962CBC20F05B32CA57E27 (void);
// 0x0000049C System.Boolean NCMB.NCMBPush::get_PushToIOS()
extern void NCMBPush_get_PushToIOS_m7C5DC31FC34C2A3B8FE6B516658EC5FC9532264F (void);
// 0x0000049D System.Void NCMB.NCMBPush::set_PushToIOS(System.Boolean)
extern void NCMBPush_set_PushToIOS_mF8D0309BF7E92230D188E443CBBE13C8061BFEB5 (void);
// 0x0000049E System.Boolean NCMB.NCMBPush::get_PushToAndroid()
extern void NCMBPush_get_PushToAndroid_m94786D29C5C39C67A2A963FAD3A17242F80544DF (void);
// 0x0000049F System.Void NCMB.NCMBPush::set_PushToAndroid(System.Boolean)
extern void NCMBPush_set_PushToAndroid_mAEB45CD239583CD66453B77CA3A95CBDB056D321 (void);
// 0x000004A0 System.Nullable`1<System.Int32> NCMB.NCMBPush::get_Badge()
extern void NCMBPush_get_Badge_mBDAC26E4155DF949B969C7EFE87F06DB6306B4BA (void);
// 0x000004A1 System.Void NCMB.NCMBPush::set_Badge(System.Nullable`1<System.Int32>)
extern void NCMBPush_set_Badge_mB5570E4CBA7CAB46DEC2BA291963DF413FF7EA54 (void);
// 0x000004A2 System.Boolean NCMB.NCMBPush::get_BadgeIncrementFlag()
extern void NCMBPush_get_BadgeIncrementFlag_mEDF5F8BA21B1A1EB9BF26A47D123F5D4294D4453 (void);
// 0x000004A3 System.Void NCMB.NCMBPush::set_BadgeIncrementFlag(System.Boolean)
extern void NCMBPush_set_BadgeIncrementFlag_m22614E426329180E9D30DA90AD8D3BDAECD299F3 (void);
// 0x000004A4 System.String NCMB.NCMBPush::get_RichUrl()
extern void NCMBPush_get_RichUrl_m6AF6EE109F162EE9CBA8B511F6D3F0BD8CC4AC77 (void);
// 0x000004A5 System.Void NCMB.NCMBPush::set_RichUrl(System.String)
extern void NCMBPush_set_RichUrl_mEAF5114CE0E853D52EE2A3F4A97D01F5160CD1B7 (void);
// 0x000004A6 System.Boolean NCMB.NCMBPush::get_Dialog()
extern void NCMBPush_get_Dialog_mEB034EBD04BEB09994BA4D30A4EAF46A5D04C3F5 (void);
// 0x000004A7 System.Void NCMB.NCMBPush::set_Dialog(System.Boolean)
extern void NCMBPush_set_Dialog_m625F3FC2B36D542493574DD453E4F2D8FA68AF5B (void);
// 0x000004A8 System.Boolean NCMB.NCMBPush::get_ContentAvailable()
extern void NCMBPush_get_ContentAvailable_m1C779BC92CEB4ED5059820762F92786BA529F375 (void);
// 0x000004A9 System.Void NCMB.NCMBPush::set_ContentAvailable(System.Boolean)
extern void NCMBPush_set_ContentAvailable_m42D4292D124EA53598298331A40C2C0A6BADD88D (void);
// 0x000004AA System.String NCMB.NCMBPush::get_Category()
extern void NCMBPush_get_Category_m6ABEAD5FDC06605731EFED2D99E6D4C803B9A034 (void);
// 0x000004AB System.Void NCMB.NCMBPush::set_Category(System.String)
extern void NCMBPush_set_Category_m0EFE2AC0E4378B8F99A7534040F6C840F3CD0679 (void);
// 0x000004AC System.Nullable`1<System.DateTime> NCMB.NCMBPush::get_DeliveryExpirationDate()
extern void NCMBPush_get_DeliveryExpirationDate_m77B6476B9F995505D165B9100C918C79A12C251F (void);
// 0x000004AD System.Void NCMB.NCMBPush::set_DeliveryExpirationDate(System.Nullable`1<System.DateTime>)
extern void NCMBPush_set_DeliveryExpirationDate_mE289BF7E234ACF22AC3DCB0D153E60B73969F73C (void);
// 0x000004AE System.String NCMB.NCMBPush::get_DeliveryExpirationTime()
extern void NCMBPush_get_DeliveryExpirationTime_m776AA08BD5E8DDA9005689F943030059B2C97773 (void);
// 0x000004AF System.Void NCMB.NCMBPush::set_DeliveryExpirationTime(System.String)
extern void NCMBPush_set_DeliveryExpirationTime_m1617A082BC26FDE2BE517978D948B8BC47357428 (void);
// 0x000004B0 System.Void NCMB.NCMBPush::SendPush()
extern void NCMBPush_SendPush_mE84915F95D414570EBDBF197E2C68DE9803E1970 (void);
// 0x000004B1 System.Void NCMB.NCMBPush::SendPush(NCMB.NCMBCallback)
extern void NCMBPush_SendPush_m25C83AC36BC7CDDC6A4DD87D500D8B233178324C (void);
// 0x000004B2 System.Void NCMB.NCMBPush::ClearAll()
extern void NCMBPush_ClearAll_mD01E25209CA40E99527360FE6A1170D16C1F53E8 (void);
// 0x000004B3 NCMB.NCMBQuery`1<NCMB.NCMBPush> NCMB.NCMBPush::GetQuery()
extern void NCMBPush_GetQuery_m3A28C82AEF940BDE86670C283090E5C184F6C014 (void);
// 0x000004B4 System.String NCMB.NCMBPush::_getBaseUrl()
extern void NCMBPush__getBaseUrl_m1264531A3FEA70703490693DFE17E74DC08EF86B (void);
// 0x000004B5 System.Void NCMB.NCMBQuery`1::.ctor(System.String)
// 0x000004B6 System.Int32 NCMB.NCMBQuery`1::get_Skip()
// 0x000004B7 System.Void NCMB.NCMBQuery`1::set_Skip(System.Int32)
// 0x000004B8 System.Int32 NCMB.NCMBQuery`1::get_Limit()
// 0x000004B9 System.Void NCMB.NCMBQuery`1::set_Limit(System.Int32)
// 0x000004BA System.String NCMB.NCMBQuery`1::get_ClassName()
// 0x000004BB NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::Or(System.Collections.Generic.List`1<NCMB.NCMBQuery`1<T>>)
// 0x000004BC NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::_whereSatifiesAnyOf(System.Collections.Generic.List`1<NCMB.NCMBQuery`1<T>>)
// 0x000004BD NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::OrderByAscending(System.String)
// 0x000004BE NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::OrderByDescending(System.String)
// 0x000004BF NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::AddAscendingOrder(System.String)
// 0x000004C0 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::AddDescendingOrder(System.String)
// 0x000004C1 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereEqualTo(System.String,System.Object)
// 0x000004C2 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereNotEqualTo(System.String,System.Object)
// 0x000004C3 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereGreaterThan(System.String,System.Object)
// 0x000004C4 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereGreaterThanOrEqualTo(System.String,System.Object)
// 0x000004C5 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereLessThan(System.String,System.Object)
// 0x000004C6 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereLessThanOrEqualTo(System.String,System.Object)
// 0x000004C7 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereContainedIn(System.String,System.Collections.IEnumerable)
// 0x000004C8 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereNotContainedIn(System.String,System.Collections.IEnumerable)
// 0x000004C9 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereContainedInArray(System.String,System.Collections.IEnumerable)
// 0x000004CA NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereContainsAll(System.String,System.Collections.IEnumerable)
// 0x000004CB NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereMatchesQuery(System.String,NCMB.NCMBQuery`1<TOther>)
// 0x000004CC NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereMatchesKeyInQuery(System.String,System.String,NCMB.NCMBQuery`1<TOther>)
// 0x000004CD System.Void NCMB.NCMBQuery`1::Include(System.String)
// 0x000004CE NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereNearGeoPoint(System.String,NCMB.NCMBGeoPoint)
// 0x000004CF NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereGeoPointWithinKilometers(System.String,NCMB.NCMBGeoPoint,System.Double)
// 0x000004D0 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereGeoPointWithinMiles(System.String,NCMB.NCMBGeoPoint,System.Double)
// 0x000004D1 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereGeoPointWithinRadians(System.String,NCMB.NCMBGeoPoint,System.Double)
// 0x000004D2 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::WhereWithinGeoBox(System.String,NCMB.NCMBGeoPoint,NCMB.NCMBGeoPoint)
// 0x000004D3 System.Collections.Generic.Dictionary`2<System.String,System.Object> NCMB.NCMBQuery`1::_geoPointToObjectWithinBox(NCMB.NCMBGeoPoint,NCMB.NCMBGeoPoint)
// 0x000004D4 System.Collections.Generic.Dictionary`2<System.String,System.Object> NCMB.NCMBQuery`1::_geoPointToObject(NCMB.NCMBGeoPoint)
// 0x000004D5 System.Void NCMB.NCMBQuery`1::_addCondition(System.String,System.String,System.Object)
// 0x000004D6 System.Void NCMB.NCMBQuery`1::FindAsync(NCMB.NCMBQueryCallback`1<T>)
// 0x000004D7 System.Void NCMB.NCMBQuery`1::Find(NCMB.NCMBQueryCallback`1<T>)
// 0x000004D8 System.Collections.ArrayList NCMB.NCMBQuery`1::_convertFindResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x000004D9 System.Void NCMB.NCMBQuery`1::GetAsync(System.String,NCMB.NCMBGetCallback`1<T>)
// 0x000004DA NCMB.NCMBObject NCMB.NCMBQuery`1::_convertGetResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x000004DB System.Void NCMB.NCMBQuery`1::CountAsync(NCMB.NCMBCountCallback)
// 0x000004DC System.String NCMB.NCMBQuery`1::_makeWhereUrl(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x000004DD System.Collections.Generic.Dictionary`2<System.String,System.Object> NCMB.NCMBQuery`1::_getFindParams()
// 0x000004DE System.Object NCMB.NCMBQuery`1::_encodeSubQueries(System.Object)
// 0x000004DF System.String NCMB.NCMBQuery`1::_join(System.Collections.Generic.List`1<System.String>,System.String)
// 0x000004E0 System.String NCMB.NCMBQuery`1::_getSearchUrl(System.String)
// 0x000004E1 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::GetQuery(System.String)
// 0x000004E2 NCMB.NCMBQuery`1<T> NCMB.NCMBQuery`1::_whereRelatedTo(NCMB.NCMBObject,System.String)
// 0x000004E3 System.Void NCMB.NCMBQuery`1/<>c__DisplayClass44_0::.ctor()
// 0x000004E4 System.Void NCMB.NCMBQuery`1/<>c__DisplayClass44_0::<Find>b__0(System.Int32,System.String,NCMB.NCMBException)
// 0x000004E5 System.Void NCMB.NCMBQuery`1/<>c__DisplayClass46_0::.ctor()
// 0x000004E6 System.Void NCMB.NCMBQuery`1/<>c__DisplayClass46_0::<GetAsync>b__0(System.Int32,System.String,NCMB.NCMBException)
// 0x000004E7 System.Void NCMB.NCMBQuery`1/<>c__DisplayClass48_0::.ctor()
// 0x000004E8 System.Void NCMB.NCMBQuery`1/<>c__DisplayClass48_0::<CountAsync>b__0(System.Int32,System.String,NCMB.NCMBException)
// 0x000004E9 System.String NCMB.NCMBRelation`1::get_TargetClass()
// 0x000004EA System.Void NCMB.NCMBRelation`1::set_TargetClass(System.String)
// 0x000004EB System.Void NCMB.NCMBRelation`1::.ctor(NCMB.NCMBObject,System.String)
// 0x000004EC System.Void NCMB.NCMBRelation`1::.ctor(System.String)
// 0x000004ED System.Void NCMB.NCMBRelation`1::Add(T)
// 0x000004EE System.Void NCMB.NCMBRelation`1::Remove(T)
// 0x000004EF System.Void NCMB.NCMBRelation`1::_removeDuplicationCheck(T)
// 0x000004F0 System.Void NCMB.NCMBRelation`1::_addDuplicationCheck(T)
// 0x000004F1 System.Void NCMB.NCMBRelation`1::_ensureParentAndKey(NCMB.NCMBObject,System.String)
// 0x000004F2 System.Collections.Generic.Dictionary`2<System.String,System.Object> NCMB.NCMBRelation`1::_encodeToJSON()
// 0x000004F3 NCMB.NCMBQuery`1<T> NCMB.NCMBRelation`1::GetQuery()
// 0x000004F4 System.Void NCMB.NCMBRole::.ctor()
extern void NCMBRole__ctor_m86317CDEC70246FE390A006CA7DB0BCB4F888C32 (void);
// 0x000004F5 System.Void NCMB.NCMBRole::.ctor(System.String)
extern void NCMBRole__ctor_mCE5612A7E30D29DE830052332E108F50225AF133 (void);
// 0x000004F6 System.Void NCMB.NCMBRole::.ctor(System.String,NCMB.NCMBACL)
extern void NCMBRole__ctor_mA79F6215A76C02C19A0A5A56106B6D24C209DDEC (void);
// 0x000004F7 System.Void NCMB.NCMBRole::set_Name(System.String)
extern void NCMBRole_set_Name_mBDB6BC0B1B99E7A49268D65716B93726D8C19893 (void);
// 0x000004F8 System.String NCMB.NCMBRole::get_Name()
extern void NCMBRole_get_Name_m757ED43FBD35BD2C43EACF3BF86349B9C47D72FC (void);
// 0x000004F9 NCMB.NCMBRelation`1<NCMB.NCMBUser> NCMB.NCMBRole::get_Users()
extern void NCMBRole_get_Users_mA4B4CC70937BF364F3D802432E07E338A47DEB3F (void);
// 0x000004FA NCMB.NCMBRelation`1<NCMB.NCMBRole> NCMB.NCMBRole::get_Roles()
extern void NCMBRole_get_Roles_mAEFC86BB906C422031F462A816EB2F913B1AFEF0 (void);
// 0x000004FB NCMB.NCMBQuery`1<NCMB.NCMBRole> NCMB.NCMBRole::GetQuery()
extern void NCMBRole_GetQuery_mED735B42BD4B9BBA7A47EE73E63ABD9DC237D117 (void);
// 0x000004FC System.Void NCMB.NCMBRole::_onSettingValue(System.String,System.Object)
extern void NCMBRole__onSettingValue_m26477B1599EE673A737D7E2CB3E6AF9C70C62C6E (void);
// 0x000004FD System.Void NCMB.NCMBRole::_beforeSave()
extern void NCMBRole__beforeSave_m06525DEBEAD22A314BB6CA62DDAA9CF39B0D1F9A (void);
// 0x000004FE System.String NCMB.NCMBRole::_getBaseUrl()
extern void NCMBRole__getBaseUrl_mCAC8A167E4C00C230DD4A6ED4C2CE8B83D6B8901 (void);
// 0x000004FF System.Void NCMB.NCMBRole::.cctor()
extern void NCMBRole__cctor_mA0F7D58FE87C3BB4663505CC4B93D247BD505704 (void);
// 0x00000500 System.String NCMB.NCMBScript::get_ScriptName()
extern void NCMBScript_get_ScriptName_m500FEB962A99E50C6E11F3C5535FD0414C17C734 (void);
// 0x00000501 System.Void NCMB.NCMBScript::set_ScriptName(System.String)
extern void NCMBScript_set_ScriptName_m4CF14086D79B6B2412F2F57406674EFA71EFB115 (void);
// 0x00000502 NCMB.NCMBScript/MethodType NCMB.NCMBScript::get_Method()
extern void NCMBScript_get_Method_m7DCACB667527C7FB005E0B75FD8B93FB444433C0 (void);
// 0x00000503 System.Void NCMB.NCMBScript::set_Method(NCMB.NCMBScript/MethodType)
extern void NCMBScript_set_Method_m8686AB2868E5EEBBEBB023BF64FD108A7EE99291 (void);
// 0x00000504 System.String NCMB.NCMBScript::get_BaseUrl()
extern void NCMBScript_get_BaseUrl_m050D19B6A8A4A8FFE401AD2CC002921E77042307 (void);
// 0x00000505 System.Void NCMB.NCMBScript::set_BaseUrl(System.String)
extern void NCMBScript_set_BaseUrl_mB725BE61B1EE870D371CC76F3E204235251A3870 (void);
// 0x00000506 System.Void NCMB.NCMBScript::.ctor(System.String,NCMB.NCMBScript/MethodType)
extern void NCMBScript__ctor_mC4621EE9598EE7B90C8BD7B7C389B5DCDEBE1591 (void);
// 0x00000507 System.Void NCMB.NCMBScript::.ctor(System.String,NCMB.NCMBScript/MethodType,System.String)
extern void NCMBScript__ctor_m615D97E9FE94F583A7B1EF9B2706DAD68ACE706D (void);
// 0x00000508 System.Void NCMB.NCMBScript::ExecuteAsync(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.Collections.Generic.IDictionary`2<System.String,System.Object>,NCMB.NCMBExecuteScriptCallback)
extern void NCMBScript_ExecuteAsync_mB76930F866DABB9D45763AEAB1A85979D9C2E1E1 (void);
// 0x00000509 System.Void NCMB.NCMBScript::Connect(NCMB.Internal.NCMBConnection,NCMB.NCMBExecuteScriptCallback)
extern void NCMBScript_Connect_mE0DD6071715C1EC3C6486C7A8EFDBC62262B1BA1 (void);
// 0x0000050A System.Void NCMB.NCMBScript::.cctor()
extern void NCMBScript__cctor_m3CD738981341BCB2DA327DFFB146D714D4E4ED76 (void);
// 0x0000050B System.Void NCMB.NCMBScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m6F1207F7F114958EB5C9DFF292B50AD150BE89CB (void);
// 0x0000050C System.Void NCMB.NCMBScript/<>c::.ctor()
extern void U3CU3Ec__ctor_m30A8023C113544FA73B9151EB561BB443DF3BE86 (void);
// 0x0000050D System.Boolean NCMB.NCMBScript/<>c::<ExecuteAsync>b__18_0(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void U3CU3Ec_U3CExecuteAsyncU3Eb__18_0_m3427557B8C334E6FAB3CE566E03205CC3EE50777 (void);
// 0x0000050E System.Void NCMB.NCMBTwitterParameters::.ctor(System.String,System.String,System.String,System.String,System.String,System.String)
extern void NCMBTwitterParameters__ctor_m3EE63C76F6ACD12311938069A2C805B681632C10 (void);
// 0x0000050F System.String NCMB.NCMBUser::get_UserName()
extern void NCMBUser_get_UserName_m919C039AAF5737AEAC417648E8CC42A604C98D8F (void);
// 0x00000510 System.Void NCMB.NCMBUser::set_UserName(System.String)
extern void NCMBUser_set_UserName_m330ACAF4F265F85AB78B1DCCA43E24E0D0026017 (void);
// 0x00000511 System.String NCMB.NCMBUser::get_Email()
extern void NCMBUser_get_Email_m5E6F575C8CC66B0906793CEF3B7ADD893515866E (void);
// 0x00000512 System.Void NCMB.NCMBUser::set_Email(System.String)
extern void NCMBUser_set_Email_mEB8CB323BF671416AD02451A1E59A09DB1F260D2 (void);
// 0x00000513 System.String NCMB.NCMBUser::get_Password()
extern void NCMBUser_get_Password_mA431A815ABB2AE300FCD356CDDF5882E72A2DDD4 (void);
// 0x00000514 System.Void NCMB.NCMBUser::set_Password(System.String)
extern void NCMBUser_set_Password_mC5F385E23948C97D496BEC755D55D99B8340E248 (void);
// 0x00000515 System.Collections.Generic.Dictionary`2<System.String,System.Object> NCMB.NCMBUser::get_AuthData()
extern void NCMBUser_get_AuthData_mA5AA13CAC097AE98E3CF2A217AFCF6D719C9D3DD (void);
// 0x00000516 System.Void NCMB.NCMBUser::set_AuthData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void NCMBUser_set_AuthData_mB1A917B9994B2CA41A279D1F3801137AB6C8A911 (void);
// 0x00000517 System.String NCMB.NCMBUser::get_SessionToken()
extern void NCMBUser_get_SessionToken_mB524F409959CB8AF2DA79429995DFD9741763885 (void);
// 0x00000518 System.Void NCMB.NCMBUser::set_SessionToken(System.String)
extern void NCMBUser_set_SessionToken_m6A738DD02F37C256E032C1CB0BD73BB6CCC3183A (void);
// 0x00000519 NCMB.NCMBUser NCMB.NCMBUser::get_CurrentUser()
extern void NCMBUser_get_CurrentUser_m9F82B3580EEBF1B2601234356E163B7886100EAF (void);
// 0x0000051A System.Void NCMB.NCMBUser::.ctor()
extern void NCMBUser__ctor_m15BC402336F19A8E9DF9F654FAE88D9E2C42F360 (void);
// 0x0000051B System.Void NCMB.NCMBUser::_onSettingValue(System.String,System.Object)
extern void NCMBUser__onSettingValue_m6AE1C590DF8A56F2FC14FF575C6D0C54A0902AD5 (void);
// 0x0000051C System.Void NCMB.NCMBUser::Add(System.String,System.Object)
extern void NCMBUser_Add_m8F9059597A5B60486C9B9E1CD5D9F59B60449D09 (void);
// 0x0000051D System.Void NCMB.NCMBUser::Remove(System.String)
extern void NCMBUser_Remove_m9B39874177715427814EA216A01A581046938F09 (void);
// 0x0000051E NCMB.NCMBQuery`1<NCMB.NCMBUser> NCMB.NCMBUser::GetQuery()
extern void NCMBUser_GetQuery_mF2B030186FE2CD359C68A3B37D269963B30E84D9 (void);
// 0x0000051F System.String NCMB.NCMBUser::_getBaseUrl()
extern void NCMBUser__getBaseUrl_mF26F115185CC0867B0454B12B237B1F86B91401B (void);
// 0x00000520 System.String NCMB.NCMBUser::_getLogInUrl()
extern void NCMBUser__getLogInUrl_m40CAF74344C3AF93CCD2D6288A788BB25E6B2779 (void);
// 0x00000521 System.String NCMB.NCMBUser::_getLogOutUrl()
extern void NCMBUser__getLogOutUrl_m261630F2365CC9F9C0B1410F794B54154A0F75F9 (void);
// 0x00000522 System.String NCMB.NCMBUser::_getRequestPasswordResetUrl()
extern void NCMBUser__getRequestPasswordResetUrl_m279D85FB3EA8052326B66F095181136249068EF3 (void);
// 0x00000523 System.String NCMB.NCMBUser::_getmailAddressUserEntryUrl()
extern void NCMBUser__getmailAddressUserEntryUrl_m06C776A931034D7A82B55840183204281994C681 (void);
// 0x00000524 System.Void NCMB.NCMBUser::_afterSave(System.Int32,NCMB.NCMBException)
extern void NCMBUser__afterSave_mC12B621CE9737AF9116035DCA38C8525E1D26A75 (void);
// 0x00000525 System.Void NCMB.NCMBUser::_afterDelete(NCMB.NCMBException)
extern void NCMBUser__afterDelete_m174428FB64A98E44A5011B379121FB9B5BD765DD (void);
// 0x00000526 System.Void NCMB.NCMBUser::DeleteAsync()
extern void NCMBUser_DeleteAsync_mB5C8606B5B085800A41E3530475302AC86E7B501 (void);
// 0x00000527 System.Void NCMB.NCMBUser::DeleteAsync(NCMB.NCMBCallback)
extern void NCMBUser_DeleteAsync_mD9F84F379F80C769DD27C77B9299FA9759823F87 (void);
// 0x00000528 System.Void NCMB.NCMBUser::SignUpAsync(NCMB.NCMBCallback)
extern void NCMBUser_SignUpAsync_mE0FC02DDACA3FF04E958F3E8D579AC4090EBBD10 (void);
// 0x00000529 System.Void NCMB.NCMBUser::SignUpAsync()
extern void NCMBUser_SignUpAsync_m77FF9F68A200498309C43002BDB1E4F15F6F702C (void);
// 0x0000052A System.Void NCMB.NCMBUser::SaveAsync()
extern void NCMBUser_SaveAsync_mBBA54B7FFDBF76872DD923B5339B720050A672B3 (void);
// 0x0000052B System.Void NCMB.NCMBUser::SaveAsync(NCMB.NCMBCallback)
extern void NCMBUser_SaveAsync_m74F32D72DE69833907DB8173C2DB2B43E533E5FC (void);
// 0x0000052C System.Void NCMB.NCMBUser::_saveCurrentUser(NCMB.NCMBUser)
extern void NCMBUser__saveCurrentUser_m431B8E45172F00C373D910186706D5B894AEB05B (void);
// 0x0000052D System.Void NCMB.NCMBUser::_logOutEvent()
extern void NCMBUser__logOutEvent_m80134FA4B6D95CA286C53D00E4CD4EEE02A001FA (void);
// 0x0000052E System.String NCMB.NCMBUser::_getCurrentSessionToken()
extern void NCMBUser__getCurrentSessionToken_m4010FADD7FAECC3A0B6D0F03D451739FF8EFA6D6 (void);
// 0x0000052F System.Boolean NCMB.NCMBUser::IsAuthenticated()
extern void NCMBUser_IsAuthenticated_mD92CD471EEEF36346E50A47489764CC61A0D12E9 (void);
// 0x00000530 System.Void NCMB.NCMBUser::RequestPasswordResetAsync(System.String)
extern void NCMBUser_RequestPasswordResetAsync_m3CD398E51C838CD3A10DF54971D01E24D793029B (void);
// 0x00000531 System.Void NCMB.NCMBUser::RequestPasswordResetAsync(System.String,NCMB.NCMBCallback)
extern void NCMBUser_RequestPasswordResetAsync_mDE94B97DBA2CED60F40DD38879E44D6E8BE7BF00 (void);
// 0x00000532 System.Void NCMB.NCMBUser::_requestPasswordReset(System.String,NCMB.NCMBCallback)
extern void NCMBUser__requestPasswordReset_m508D008E9A5386BC66923C01F53F40B79AC11347 (void);
// 0x00000533 System.Void NCMB.NCMBUser::LogInAsync(System.String,System.String)
extern void NCMBUser_LogInAsync_m1A58B6E285B79B74E799C62A7F694AA58E58850E (void);
// 0x00000534 System.Void NCMB.NCMBUser::LogInAsync(System.String,System.String,NCMB.NCMBCallback)
extern void NCMBUser_LogInAsync_m47DB940D3F8B3158F8E035F7916DC18B8DFCB7B0 (void);
// 0x00000535 System.Void NCMB.NCMBUser::_ncmbLogIn(System.String,System.String,System.String,NCMB.NCMBCallback)
extern void NCMBUser__ncmbLogIn_mAF919828D6DF7A341D1952DA0014D3F0EDFE8AF4 (void);
// 0x00000536 System.String NCMB.NCMBUser::_makeParamUrl(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void NCMBUser__makeParamUrl_m5D85BAD283D8E3BF223306D21DC2156EEC0E33BA (void);
// 0x00000537 System.Void NCMB.NCMBUser::LogInWithMailAddressAsync(System.String,System.String,NCMB.NCMBCallback)
extern void NCMBUser_LogInWithMailAddressAsync_m24FBEC49C9E00792EF4E97A329B1AA23C5F7AB94 (void);
// 0x00000538 System.Void NCMB.NCMBUser::LogInWithMailAddressAsync(System.String,System.String)
extern void NCMBUser_LogInWithMailAddressAsync_m51BC6C044004ECD1D89A663B53B3317867C44B3B (void);
// 0x00000539 System.Void NCMB.NCMBUser::RequestAuthenticationMailAsync(System.String)
extern void NCMBUser_RequestAuthenticationMailAsync_m27FB98C08F97FDEA201C6DC1EA5214DE0C3892F6 (void);
// 0x0000053A System.Void NCMB.NCMBUser::RequestAuthenticationMailAsync(System.String,NCMB.NCMBCallback)
extern void NCMBUser_RequestAuthenticationMailAsync_mD012BED84FC1755CBFEF82E83D0B9F43974DE3FE (void);
// 0x0000053B System.Void NCMB.NCMBUser::LogOutAsync()
extern void NCMBUser_LogOutAsync_m3E4D4631C8663FFB06C2FBD50ADDD32603FDD86D (void);
// 0x0000053C System.Void NCMB.NCMBUser::LogOutAsync(NCMB.NCMBCallback)
extern void NCMBUser_LogOutAsync_mD551989FDBC119BA5883F9570DEEC3B190EA06BB (void);
// 0x0000053D System.Void NCMB.NCMBUser::_logOut(NCMB.NCMBCallback)
extern void NCMBUser__logOut_mAA89150A893EB4B6B85887D8A2F3678DAC77C2CE (void);
// 0x0000053E System.Void NCMB.NCMBUser::_mergeFromServer(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean)
extern void NCMBUser__mergeFromServer_m6EABF9FE80F8BD19DB3ABEE3BECE3949A1EEDCF5 (void);
// 0x0000053F System.Void NCMB.NCMBUser::LogInWithAuthDataAsync(NCMB.NCMBCallback)
extern void NCMBUser_LogInWithAuthDataAsync_mAF23E5F0D1EB6F1C576B11BC1102EAAF86BDCC89 (void);
// 0x00000540 System.Void NCMB.NCMBUser::LogInWithAuthDataAsync()
extern void NCMBUser_LogInWithAuthDataAsync_mAFB17B8639EFAA505F86A263B44DF3192C21DA47 (void);
// 0x00000541 System.Void NCMB.NCMBUser::LoginWithAnonymousAsync(NCMB.NCMBCallback)
extern void NCMBUser_LoginWithAnonymousAsync_m67A842CCA54849D421BF151AA214BD3DA5C65E1B (void);
// 0x00000542 System.Void NCMB.NCMBUser::LoginWithAnonymousAsync()
extern void NCMBUser_LoginWithAnonymousAsync_mC3C4DCAA45F324542445BA41C4F1F72DD3CBBE0B (void);
// 0x00000543 System.Void NCMB.NCMBUser::LinkWithAuthDataAsync(System.Collections.Generic.Dictionary`2<System.String,System.Object>,NCMB.NCMBCallback)
extern void NCMBUser_LinkWithAuthDataAsync_m6E39C6A8454591FCA38EA4DF983AA10508DF5B4D (void);
// 0x00000544 System.Void NCMB.NCMBUser::LinkWithAuthDataAsync(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void NCMBUser_LinkWithAuthDataAsync_m726B7D30920923A2D77F5B714FF5BE0438D975EA (void);
// 0x00000545 System.Void NCMB.NCMBUser::UnLinkWithAuthDataAsync(System.String,NCMB.NCMBCallback)
extern void NCMBUser_UnLinkWithAuthDataAsync_m18D47110AB96AE2680863D5E412C0550F8062751 (void);
// 0x00000546 System.Void NCMB.NCMBUser::UnLinkWithAuthDataAsync(System.String)
extern void NCMBUser_UnLinkWithAuthDataAsync_m502F1BAA25E0B98521618C32FE9FA8B353110D0E (void);
// 0x00000547 System.Boolean NCMB.NCMBUser::IsLinkWith(System.String)
extern void NCMBUser_IsLinkWith_mA4D45030FD7E21EBB0AB64D3F3DF0517046DFE71 (void);
// 0x00000548 System.Collections.Generic.Dictionary`2<System.String,System.Object> NCMB.NCMBUser::GetAuthDataForProvider(System.String)
extern void NCMBUser_GetAuthDataForProvider_m12C88BFD22EBB30FE54E89351FC8FECEECE0B9AB (void);
// 0x00000549 System.String NCMB.NCMBUser::createUUID()
extern void NCMBUser_createUUID_m4DA4AD1D41E00582029E850AF93B739EEA95D27D (void);
// 0x0000054A System.Void NCMB.NCMBUser/<>c__DisplayClass43_0::.ctor()
extern void U3CU3Ec__DisplayClass43_0__ctor_m011023BC928C4BEFD0E09B6C9803692FE9A22988 (void);
// 0x0000054B System.Void NCMB.NCMBUser/<>c__DisplayClass43_0::<_requestPasswordReset>b__0(System.Int32,System.String,NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass43_0_U3C_requestPasswordResetU3Eb__0_m77B1D03C4107890DAAA256A5687DF38D76EA7620 (void);
// 0x0000054C System.Void NCMB.NCMBUser/<>c__DisplayClass46_0::.ctor()
extern void U3CU3Ec__DisplayClass46_0__ctor_m0B145C16D7EA7C3E016621CD430D95942EA539F6 (void);
// 0x0000054D System.Void NCMB.NCMBUser/<>c__DisplayClass46_0::<_ncmbLogIn>b__0(System.Int32,System.String,NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass46_0_U3C_ncmbLogInU3Eb__0_mE1E6DE0EDF59D563C4C651561D9E971D9D445286 (void);
// 0x0000054E System.Void NCMB.NCMBUser/<>c__DisplayClass51_0::.ctor()
extern void U3CU3Ec__DisplayClass51_0__ctor_m8F4137AE1CB5840E2C6F8E9DF40B80E005F37AC0 (void);
// 0x0000054F System.Void NCMB.NCMBUser/<>c__DisplayClass51_0::<RequestAuthenticationMailAsync>b__0(System.Int32,System.String,NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass51_0_U3CRequestAuthenticationMailAsyncU3Eb__0_m62082CCD1B0AE2A59448529BC594BF0FDB13179D (void);
// 0x00000550 System.Void NCMB.NCMBUser/<>c__DisplayClass54_0::.ctor()
extern void U3CU3Ec__DisplayClass54_0__ctor_m6D1090B976E6E64AF9AA0833B1DAAAD624EF693D (void);
// 0x00000551 System.Void NCMB.NCMBUser/<>c__DisplayClass54_0::<_logOut>b__0(System.Int32,System.String,NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass54_0_U3C_logOutU3Eb__0_m84B42A4D648454D7F94485473CD26CD2ED895429 (void);
// 0x00000552 System.Void NCMB.NCMBUser/<>c__DisplayClass56_0::.ctor()
extern void U3CU3Ec__DisplayClass56_0__ctor_mED4C3BEA68E77BE1F96F7BBFD50130AFB4772E95 (void);
// 0x00000553 System.Void NCMB.NCMBUser/<>c__DisplayClass56_0::<LogInWithAuthDataAsync>b__0(NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass56_0_U3CLogInWithAuthDataAsyncU3Eb__0_m9BFA6A34242429BF4D543FD67FDFF7F47DF173FD (void);
// 0x00000554 System.Void NCMB.NCMBUser/<>c__DisplayClass58_0::.ctor()
extern void U3CU3Ec__DisplayClass58_0__ctor_mD75EE33F7C862C4C1D4DF6621461AC461C536909 (void);
// 0x00000555 System.Void NCMB.NCMBUser/<>c__DisplayClass58_0::<LoginWithAnonymousAsync>b__0(NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass58_0_U3CLoginWithAnonymousAsyncU3Eb__0_m7C90A5DC9A5D27067DD798E57DF0886109593313 (void);
// 0x00000556 System.Void NCMB.NCMBUser/<>c__DisplayClass60_0::.ctor()
extern void U3CU3Ec__DisplayClass60_0__ctor_m20833A70409B1BD874DCA615FE905BC4A7D5F506 (void);
// 0x00000557 System.Void NCMB.NCMBUser/<>c__DisplayClass60_0::<LinkWithAuthDataAsync>b__0(NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass60_0_U3CLinkWithAuthDataAsyncU3Eb__0_mE6C51EB257DBBED027463F942CF8879F4CB9C521 (void);
// 0x00000558 System.Void NCMB.NCMBUser/<>c::.cctor()
extern void U3CU3Ec__cctor_mDF7EBBAC17D4CC0BE9BDBDECCDA3719608538C48 (void);
// 0x00000559 System.Void NCMB.NCMBUser/<>c::.ctor()
extern void U3CU3Ec__ctor_m8A08A950E68FA1607B1366F0076329D352D5860E (void);
// 0x0000055A System.String NCMB.NCMBUser/<>c::<LinkWithAuthDataAsync>b__60_1(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern void U3CU3Ec_U3CLinkWithAuthDataAsyncU3Eb__60_1_m29699AA5D26A3FC1019AB261D4A2AD9DCF23C45F (void);
// 0x0000055B System.Object NCMB.NCMBUser/<>c::<LinkWithAuthDataAsync>b__60_2(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern void U3CU3Ec_U3CLinkWithAuthDataAsyncU3Eb__60_2_m5417368F1E9B0E6F80D9B6F31272DD5DF9D30A25 (void);
// 0x0000055C System.Void NCMB.NCMBUser/<>c__DisplayClass62_0::.ctor()
extern void U3CU3Ec__DisplayClass62_0__ctor_mD9C13D3950D065CFBD0ABEBB155D1321054D758E (void);
// 0x0000055D System.Void NCMB.NCMBUser/<>c__DisplayClass62_0::<UnLinkWithAuthDataAsync>b__0(NCMB.NCMBException)
extern void U3CU3Ec__DisplayClass62_0_U3CUnLinkWithAuthDataAsyncU3Eb__0_mF9CCE318F44D0001C852617852E19A67CA3B8498 (void);
// 0x0000055E System.Void NCMB.Internal.CommonConstant::.cctor()
extern void CommonConstant__cctor_m38960708334610C2A0872EF1FE8C4643A28BE6E9 (void);
// 0x0000055F System.Type NCMB.Internal.ExtensionsClass::GetTypeInfo(System.Type)
extern void ExtensionsClass_GetTypeInfo_mE011E71B4F6791990F2D73563DB033D071D743A0 (void);
// 0x00000560 System.Boolean NCMB.Internal.ExtensionsClass::IsPrimitive(System.Type)
extern void ExtensionsClass_IsPrimitive_m0D22FFC7463D4C03CAD3200D1EFF74C6B5209CC8 (void);
// 0x00000561 System.Void NCMB.Internal.NCMBAddOperation::.ctor(System.Object)
extern void NCMBAddOperation__ctor_m53FD49F2C5332184F5C941C67864D4E5105D7B36 (void);
// 0x00000562 System.Object NCMB.Internal.NCMBAddOperation::Encode()
extern void NCMBAddOperation_Encode_mCD332A93BBCDE767DBBF4D63060C828398466FFB (void);
// 0x00000563 NCMB.Internal.INCMBFieldOperation NCMB.Internal.NCMBAddOperation::MergeWithPrevious(NCMB.Internal.INCMBFieldOperation)
extern void NCMBAddOperation_MergeWithPrevious_m5AEC7DE171A8159EC97FD66D4C00DBF9FD92924B (void);
// 0x00000564 System.Object NCMB.Internal.NCMBAddOperation::Apply(System.Object,NCMB.NCMBObject,System.String)
extern void NCMBAddOperation_Apply_mC80753320613F8E09C5A50845ED23B4B70B7A043 (void);
// 0x00000565 System.Void NCMB.Internal.NCMBAddUniqueOperation::.ctor(System.Object)
extern void NCMBAddUniqueOperation__ctor_mA95E90BFDF07B698D3E65319D272E262EFC8D3CF (void);
// 0x00000566 System.Object NCMB.Internal.NCMBAddUniqueOperation::Encode()
extern void NCMBAddUniqueOperation_Encode_m28874032433749DC75806B0B4B3EFCC1464C19A6 (void);
// 0x00000567 NCMB.Internal.INCMBFieldOperation NCMB.Internal.NCMBAddUniqueOperation::MergeWithPrevious(NCMB.Internal.INCMBFieldOperation)
extern void NCMBAddUniqueOperation_MergeWithPrevious_m72A13EA5140ADFFCDF3794C81A228767553ADC46 (void);
// 0x00000568 System.Object NCMB.Internal.NCMBAddUniqueOperation::Apply(System.Object,NCMB.NCMBObject,System.String)
extern void NCMBAddUniqueOperation_Apply_mD4589E5026A566941F98D51031CF5FB8FD7EEFDD (void);
// 0x00000569 System.String NCMB.Internal.NCMBClassNameAttribute::get_ClassName()
extern void NCMBClassNameAttribute_get_ClassName_mFF8C5644C837F8BD726C25239176DEF224EC15A1 (void);
// 0x0000056A System.Void NCMB.Internal.NCMBClassNameAttribute::set_ClassName(System.String)
extern void NCMBClassNameAttribute_set_ClassName_m00765296450CAD8ABB473CAB4AF398E89BB22ED2 (void);
// 0x0000056B System.Void NCMB.Internal.NCMBClassNameAttribute::.ctor(System.String)
extern void NCMBClassNameAttribute__ctor_mEB007E3A35978BE7C0EEDDFD8B2CD1D4377256F8 (void);
// 0x0000056C System.Void NCMB.Internal.NCMBConnection::.ctor(System.String,NCMB.Internal.ConnectType,System.String,System.String)
extern void NCMBConnection__ctor_mEE3376D5D44ADEFF5A42C45393C7E15BFD3C2A27 (void);
// 0x0000056D System.Void NCMB.Internal.NCMBConnection::.ctor(System.String,NCMB.Internal.ConnectType,System.String,System.String,NCMB.NCMBFile)
extern void NCMBConnection__ctor_mC96FE6644919B242E08AD5A7614F82DEAD324003 (void);
// 0x0000056E System.Void NCMB.Internal.NCMBConnection::.ctor(System.String,NCMB.Internal.ConnectType,System.String,System.String,NCMB.NCMBFile,System.String)
extern void NCMBConnection__ctor_m70E1BE9C1E10DF88CA6305842765C46E2039051E (void);
// 0x0000056F System.Void NCMB.Internal.NCMBConnection::Connect(NCMB.HttpClientFileDataCallback)
extern void NCMBConnection_Connect_m9BEE91AF0B699F65472310681E64B0EF61877472 (void);
// 0x00000570 System.Void NCMB.Internal.NCMBConnection::Connect(NCMB.HttpClientCallback)
extern void NCMBConnection_Connect_mEFAE03B5C3FCD972489EA003C466158D47835FF7 (void);
// 0x00000571 System.Void NCMB.Internal.NCMBConnection::_Connection(System.Object)
extern void NCMBConnection__Connection_m730D0BE225876F04878D20C0BA28B15B6A510047 (void);
// 0x00000572 System.Void NCMB.Internal.NCMBConnection::_signatureCheck(System.String,System.String,System.String,System.Byte[],NCMB.NCMBException&)
extern void NCMBConnection__signatureCheck_mB956390CAF681F4D16AE0073707C3F9B033831EA (void);
// 0x00000573 System.String NCMB.Internal.NCMBConnection::AsHex(System.Byte[])
extern void NCMBConnection_AsHex_m58324B263B8D7EADA3E359C7119D95658CEB57A1 (void);
// 0x00000574 UnityEngine.Networking.UnityWebRequest NCMB.Internal.NCMBConnection::_setUploadHandlerForFile(UnityEngine.Networking.UnityWebRequest)
extern void NCMBConnection__setUploadHandlerForFile_m31A1EB07D2328AAA740F952682CF8C2B5CA16028 (void);
// 0x00000575 UnityEngine.Networking.UnityWebRequest NCMB.Internal.NCMBConnection::_returnRequest()
extern void NCMBConnection__returnRequest_mD460BF78B649E3CA825A5E5619ABA8A6F8039AAA (void);
// 0x00000576 System.Text.StringBuilder NCMB.Internal.NCMBConnection::_makeSignatureHashData()
extern void NCMBConnection__makeSignatureHashData_mB2C16A88773E0BC211C6636EB79F51A0E889BD39 (void);
// 0x00000577 System.String NCMB.Internal.NCMBConnection::_makeSignature(System.String)
extern void NCMBConnection__makeSignature_mA62F20D9E6DBE2A5CEA862C071880422DC35C88C (void);
// 0x00000578 System.Void NCMB.Internal.NCMBConnection::_makeTimeStamp()
extern void NCMBConnection__makeTimeStamp_mB77DEF302B170390EBFF6E76C9FB9BF2B03433FC (void);
// 0x00000579 System.Void NCMB.Internal.NCMBConnection::_checkInvalidSessionToken(System.String)
extern void NCMBConnection__checkInvalidSessionToken_mA01C374AEAF83505D5DBE9C6D1B97EC3AC4CF9A8 (void);
// 0x0000057A System.Void NCMB.Internal.NCMBConnection::_checkResponseSignature(System.String,System.String,UnityEngine.Networking.UnityWebRequest,NCMB.NCMBException&)
extern void NCMBConnection__checkResponseSignature_m01AF87F7B4C85139934AED20D9138C100FC0F1C6 (void);
// 0x0000057B System.Collections.IEnumerator NCMB.Internal.NCMBConnection::SendRequest(NCMB.Internal.NCMBConnection,UnityEngine.Networking.UnityWebRequest,System.Object)
extern void NCMBConnection_SendRequest_mA1B1682929788DD267520466BD1D77508B6FF4A6 (void);
// 0x0000057C System.Void NCMB.Internal.NCMBConnection::.cctor()
extern void NCMBConnection__cctor_m83805C6A57EE2B9424D012EB04153011EC92DB14 (void);
// 0x0000057D System.Void NCMB.Internal.NCMBConnection/<SendRequest>d__37::.ctor(System.Int32)
extern void U3CSendRequestU3Ed__37__ctor_mE9B568D6C4D3A0D9CD66E2F14226D49D555B253E (void);
// 0x0000057E System.Void NCMB.Internal.NCMBConnection/<SendRequest>d__37::System.IDisposable.Dispose()
extern void U3CSendRequestU3Ed__37_System_IDisposable_Dispose_mD2A67BF755C60BAF15CFBCD70593148C343610AE (void);
// 0x0000057F System.Boolean NCMB.Internal.NCMBConnection/<SendRequest>d__37::MoveNext()
extern void U3CSendRequestU3Ed__37_MoveNext_mFED3BC541B330B95AF81954676F2B240E5382544 (void);
// 0x00000580 System.Object NCMB.Internal.NCMBConnection/<SendRequest>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendRequestU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m63E3C63F0B4126D668F1622257A6166BB9158192 (void);
// 0x00000581 System.Void NCMB.Internal.NCMBConnection/<SendRequest>d__37::System.Collections.IEnumerator.Reset()
extern void U3CSendRequestU3Ed__37_System_Collections_IEnumerator_Reset_mB6C2E4632FD157FBA471B2161264192BB863ECC1 (void);
// 0x00000582 System.Object NCMB.Internal.NCMBConnection/<SendRequest>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CSendRequestU3Ed__37_System_Collections_IEnumerator_get_Current_m2C2A42AADDAC569E904C7F36C00F73CAC85C91F0 (void);
// 0x00000583 System.Void NCMB.Internal.NCMBDebug::Log(System.String)
extern void NCMBDebug_Log_m76BCFA58EB357E3FA55D4DA49AEFAD20BAC205DE (void);
// 0x00000584 System.Void NCMB.Internal.NCMBDebug::LogWarning(System.String)
extern void NCMBDebug_LogWarning_m8E93D353E426E8A707AE8DC7288BE26147555AE6 (void);
// 0x00000585 System.Void NCMB.Internal.NCMBDebug::LogError(System.String)
extern void NCMBDebug_LogError_mEF02F7B3AB787C161B3DE4EDBC076B899F927B2F (void);
// 0x00000586 System.Void NCMB.Internal.NCMBDebug::LogError(System.Object,System.Object)
extern void NCMBDebug_LogError_m9567B5061E2B04E48714418176AC5CCEE7B4B22E (void);
// 0x00000587 System.Void NCMB.Internal.NCMBDebug::List(System.String,System.Collections.IList)
extern void NCMBDebug_List_m79FD6D9510A8576281201B0BF8C110EA08643D91 (void);
// 0x00000588 System.Void NCMB.Internal.NCMBDebug::Dictionary(System.String,System.Collections.Generic.Dictionary`2<T,K>)
// 0x00000589 System.Void NCMB.Internal.NCMBDebug::.cctor()
extern void NCMBDebug__cctor_m0CB906B40583CA3AB2C4AADED3DBE93ED3E87C81 (void);
// 0x0000058A System.Void NCMB.Internal.NCMBDeleteOperation::.ctor()
extern void NCMBDeleteOperation__ctor_m1D37E95F8FA88DD2AFCF194C450E4E1E66CE46CF (void);
// 0x0000058B System.Object NCMB.Internal.NCMBDeleteOperation::getValue()
extern void NCMBDeleteOperation_getValue_mE30AAA05E400D2BE6CFD7236F60D5170ADC0D10B (void);
// 0x0000058C System.Object NCMB.Internal.NCMBDeleteOperation::Encode()
extern void NCMBDeleteOperation_Encode_mC202928F31C8F07FD132298A5912254A74AEB91B (void);
// 0x0000058D NCMB.Internal.INCMBFieldOperation NCMB.Internal.NCMBDeleteOperation::MergeWithPrevious(NCMB.Internal.INCMBFieldOperation)
extern void NCMBDeleteOperation_MergeWithPrevious_m08CCAF431DECA52B329BD0E1B12D321C59EAC9CB (void);
// 0x0000058E System.Object NCMB.Internal.NCMBDeleteOperation::Apply(System.Object,NCMB.NCMBObject,System.String)
extern void NCMBDeleteOperation_Apply_m854EEF71892C7F04C9EEB9BAF9AC2328DA632D8B (void);
// 0x0000058F System.Object NCMB.Internal.INCMBFieldOperation::Encode()
// 0x00000590 NCMB.Internal.INCMBFieldOperation NCMB.Internal.INCMBFieldOperation::MergeWithPrevious(NCMB.Internal.INCMBFieldOperation)
// 0x00000591 System.Object NCMB.Internal.INCMBFieldOperation::Apply(System.Object,NCMB.NCMBObject,System.String)
// 0x00000592 System.Void NCMB.Internal.NCMBIncrementOperation::.ctor(System.Object)
extern void NCMBIncrementOperation__ctor_m020A39DA365E5B6F24C1C210DFF7C08992E4AF0B (void);
// 0x00000593 System.Object NCMB.Internal.NCMBIncrementOperation::Encode()
extern void NCMBIncrementOperation_Encode_m68ED0492AC3B1F7265A7C9D86F98D641371D1EAE (void);
// 0x00000594 NCMB.Internal.INCMBFieldOperation NCMB.Internal.NCMBIncrementOperation::MergeWithPrevious(NCMB.Internal.INCMBFieldOperation)
extern void NCMBIncrementOperation_MergeWithPrevious_m84FAC3AB545D36724A2ACB801A1AC86B34A5F137 (void);
// 0x00000595 System.Object NCMB.Internal.NCMBIncrementOperation::Apply(System.Object,NCMB.NCMBObject,System.String)
extern void NCMBIncrementOperation_Apply_m6F544EBE76B60E8EF9B85BDCEA4CF29A22A092F4 (void);
// 0x00000596 System.String NCMB.Internal.NCMBRelationOperation`1::get_TargetClass()
// 0x00000597 System.Void NCMB.Internal.NCMBRelationOperation`1::.ctor(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x00000598 System.Void NCMB.Internal.NCMBRelationOperation`1::.ctor(System.String,System.Collections.Generic.HashSet`1<System.String>,System.Collections.Generic.HashSet`1<System.String>)
// 0x00000599 System.Object NCMB.Internal.NCMBRelationOperation`1::Encode()
// 0x0000059A System.Collections.ArrayList NCMB.Internal.NCMBRelationOperation`1::_convertSetToArray(System.Collections.Generic.HashSet`1<System.String>)
// 0x0000059B NCMB.Internal.INCMBFieldOperation NCMB.Internal.NCMBRelationOperation`1::MergeWithPrevious(NCMB.Internal.INCMBFieldOperation)
// 0x0000059C System.Object NCMB.Internal.NCMBRelationOperation`1::Apply(System.Object,NCMB.NCMBObject,System.String)
// 0x0000059D System.Void NCMB.Internal.NCMBRemoveOperation::.ctor(System.Object)
extern void NCMBRemoveOperation__ctor_m3D7CFD8D142C0D2C2AD00FA730110CE5721EEDFC (void);
// 0x0000059E System.Object NCMB.Internal.NCMBRemoveOperation::Encode()
extern void NCMBRemoveOperation_Encode_m9D34274C2DD7C4EDA9693EE0D43C5FF9C56D2CEB (void);
// 0x0000059F NCMB.Internal.INCMBFieldOperation NCMB.Internal.NCMBRemoveOperation::MergeWithPrevious(NCMB.Internal.INCMBFieldOperation)
extern void NCMBRemoveOperation_MergeWithPrevious_mE67F19FD490D7565BFA56149921BAE5478AECF48 (void);
// 0x000005A0 System.Object NCMB.Internal.NCMBRemoveOperation::Apply(System.Object,NCMB.NCMBObject,System.String)
extern void NCMBRemoveOperation_Apply_mF04B84F009EA2BD7BE10F755C3219414481770C5 (void);
// 0x000005A1 System.Void NCMB.Internal.NCMBSetOperation::.ctor(System.Object)
extern void NCMBSetOperation__ctor_mC9B001F6262217A4D0590CD75E2F673923E8AFB6 (void);
// 0x000005A2 System.Object NCMB.Internal.NCMBSetOperation::getValue()
extern void NCMBSetOperation_getValue_m97A6CCE91A48FB1CE84BE5730B9A5764BFBE0207 (void);
// 0x000005A3 System.Object NCMB.Internal.NCMBSetOperation::Encode()
extern void NCMBSetOperation_Encode_mCF221B9CB5F792214F0433B98DAB48E48547398E (void);
// 0x000005A4 NCMB.Internal.INCMBFieldOperation NCMB.Internal.NCMBSetOperation::MergeWithPrevious(NCMB.Internal.INCMBFieldOperation)
extern void NCMBSetOperation_MergeWithPrevious_m6D3D8A858745723E40DE5D116EB40CBE01B83210 (void);
// 0x000005A5 System.Object NCMB.Internal.NCMBSetOperation::Apply(System.Object,NCMB.NCMBObject,System.String)
extern void NCMBSetOperation_Apply_mA87445F06076C1F2BF67974EF48B5E0B1D3F3C50 (void);
// 0x000005A6 System.String NCMB.Internal.NCMBUtility::GetClassName(System.Object)
extern void NCMBUtility_GetClassName_m53AC91D47383BDDE9AEBB60E70B033BF39424EAE (void);
// 0x000005A7 System.Void NCMB.Internal.NCMBUtility::CopyDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void NCMBUtility_CopyDictionary_m217DE7CBFE9E3F0B6221C68CC2AF4BFD520AF2A3 (void);
// 0x000005A8 System.Collections.Generic.IDictionary`2<System.String,System.Object> NCMB.Internal.NCMBUtility::_encodeJSONObject(System.Object,System.Boolean)
extern void NCMBUtility__encodeJSONObject_mDF3606682F490326F1BB20662CD11D9942B5003E (void);
// 0x000005A9 System.Collections.Generic.List`1<System.Object> NCMB.Internal.NCMBUtility::_encodeAsJSONArray(System.Collections.IList,System.Boolean)
extern void NCMBUtility__encodeAsJSONArray_m3419479578986CFB9B69AA1AE3FE05F9EBA729A4 (void);
// 0x000005AA System.Object NCMB.Internal.NCMBUtility::_maybeEncodeJSONObject(System.Object,System.Boolean)
extern void NCMBUtility__maybeEncodeJSONObject_m49A3B696D1B5E0913E6C6E01F4A3EC6AAFE8551F (void);
// 0x000005AB System.Object NCMB.Internal.NCMBUtility::decodeJSONObject(System.Object)
extern void NCMBUtility_decodeJSONObject_m4F8AD9BBBD857643DD613CC863F38042D6547AF9 (void);
// 0x000005AC System.DateTime NCMB.Internal.NCMBUtility::parseDate(System.String)
extern void NCMBUtility_parseDate_mC74F39EE06CB08FFCFA868C75A2C767CB9944679 (void);
// 0x000005AD System.String NCMB.Internal.NCMBUtility::encodeDate(System.DateTime)
extern void NCMBUtility_encodeDate_m0D9661F7CE84F7D99E0CF9B666B4ABEF08726718 (void);
// 0x000005AE System.Boolean NCMB.Internal.NCMBUtility::isContainerObject(System.Object)
extern void NCMBUtility_isContainerObject_m87E07B11471D4CADBFE1D96E2FBD7B27A8EF5615 (void);
// 0x000005AF System.String NCMB.Internal.NCMBUtility::_encodeString(System.String)
extern void NCMBUtility__encodeString_mE377C2AB94BCCEE6F354902C5724C0EE054A7800 (void);
// 0x000005B0 System.String NCMB.Internal.NCMBUtility::unicodeUnescape(System.String)
extern void NCMBUtility_unicodeUnescape_mA79F93DA1D7CEC9720AA03F77A06F0E35ECF3598 (void);
// 0x000005B1 System.Void NCMB.Internal.NCMBUtility/<>c::.cctor()
extern void U3CU3Ec__cctor_mCAE3C1FB5B2FEA7F423BD10C412289762FB56EE2 (void);
// 0x000005B2 System.Void NCMB.Internal.NCMBUtility/<>c::.ctor()
extern void U3CU3Ec__ctor_m93726F14237C8F95C809EAF8844A80091DA1F9EE (void);
// 0x000005B3 System.String NCMB.Internal.NCMBUtility/<>c::<unicodeUnescape>b__10_0(System.Text.RegularExpressions.Match)
extern void U3CU3Ec_U3CunicodeUnescapeU3Eb__10_0_m592F28AEC49B8F76DF08255082EB2BE64D7925BF (void);
static Il2CppMethodPointer s_methodPointers[1459] = 
{
	JoystickPlayerExample_FixedUpdate_m9AEDBA111F95D67A006A5D3821956048224541B7,
	JoystickPlayerExample__ctor_m702422E0AE29402330CF41FDDBEE76F0506342E2,
	JoystickSetterExample_ModeChanged_m35AF30EE3E6C8CEBF064A7AB80F5795E9AF06D23,
	JoystickSetterExample_AxisChanged_m5CA220FEA14E06BD8A445E31C5B66E4601C5E404,
	JoystickSetterExample_SnapX_m25A77C3DE4C6DBBE3B4A58E2DE8CD44B1773D6A1,
	JoystickSetterExample_SnapY_m54FE8DCB2CE8D2BF5D2CDF84C68DE263F0E25B1B,
	JoystickSetterExample_Update_m99B2432D22FE669B4DC3209696AD4A62096C7D41,
	JoystickSetterExample__ctor_m2A3D66E05BCDF78D0F116348094717BEBA73EC91,
	Joystick_get_Horizontal_m78CF4472B86063E54254AC8AE0A52126E4008AFA,
	Joystick_get_Vertical_mA2B0917896CF9CE47A6D342D1734E43441C3D4BE,
	Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC,
	Joystick_get_HandleRange_mB38F0D3B6ADE2D1557D7A3D6759C469F17509D77,
	Joystick_set_HandleRange_m686B579A1F02EFCD4878BEA27EA606FC23CD2505,
	Joystick_get_DeadZone_mCE52B635A8CF24F6DD867C14E34094515DE6AEFC,
	Joystick_set_DeadZone_mD5699A14E5284026F303C8AF8D3457DFA9920F19,
	Joystick_get_AxisOptions_mA74F5FEE31C158E5179F0B108272ED28A661E388,
	Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6,
	Joystick_get_SnapX_m51CAFDCC399606BA82986908700AAA45668A0F40,
	Joystick_set_SnapX_mB2090989F6AC933B30823751D74E799FC8D9B87A,
	Joystick_get_SnapY_m35AFC1AD1DF5EDE5FCE8BAFEBE91AD51D7451613,
	Joystick_set_SnapY_m7419D5EB972285A9E5E446CD668BEC266D11CD86,
	Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C,
	Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6,
	Joystick_OnDrag_m39E69636AEDC0E471EAD1371A99F4694ECDBA1E9,
	Joystick_HandleInput_m15A4E86369A1AF0A4A5727DEC0FD93212A99901C,
	Joystick_FormatInput_mDDF7AF40138CF227F0106811C8749180FBF45342,
	Joystick_SnapFloat_mADE5AF21C67A2298A08CD12F9A8ED73AFA866987,
	Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C,
	Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024,
	Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B,
	DynamicJoystick_get_MoveThreshold_m16C670C1DA0A45E83F6F87C4304F459EDDEEDD5A,
	DynamicJoystick_set_MoveThreshold_m000C1AE325C0B9C33172E4202F2AFB59820517F9,
	DynamicJoystick_Start_mFE16C6CE0B753F08E79A2AEC75782DEEF3B96F72,
	DynamicJoystick_OnPointerDown_mBFA3026A0DA4A6B53C0E747A1E892CBC7F43E136,
	DynamicJoystick_OnPointerUp_m10389907A9D3362F6B75FDC5F35AF37A5DD5AE7C,
	DynamicJoystick_HandleInput_m3F157F4825BE6682228C8E135581C6404D268506,
	DynamicJoystick__ctor_m9DDA6263314BD7B97679DF14A4664358BD3E58CB,
	FixedJoystick__ctor_m8C8BB74E5EA8CA2C3DD2AE084301EC91F519AD24,
	FloatingJoystick_Start_mB22059CD82AF77A8F94AC72E81A8BAE969399E81,
	FloatingJoystick_OnPointerDown_mFE5B4F54D5BBCA72F9729AB64765F558FA5C7A54,
	FloatingJoystick_OnPointerUp_m80ABA9C914E1953F91DBF74853CE84879352280D,
	FloatingJoystick__ctor_m6B72425996D46B025F9E9D22121E9D01BEC6BD3C,
	VariableJoystick_get_MoveThreshold_m8C9D3A63DB3B6CF1F0139C3504EC2E7AC4E7CF99,
	VariableJoystick_set_MoveThreshold_m23DC4187B405EB690D297379E2113568B40C705A,
	VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE,
	VariableJoystick_Start_m21743760641EA0317ACCD95949B99825446FE74D,
	VariableJoystick_OnPointerDown_m8ABE5C8EFBC8DB3A2EE135FFF3C0F67C533AF4B5,
	VariableJoystick_OnPointerUp_m65296D82A6C2E1BDC2D622B9C922FAE8E4544526,
	VariableJoystick_HandleInput_mD1BCF9A068737A9C057EE8CEB7E6DEB682CC03AB,
	VariableJoystick__ctor_m6C7B41973BE20A94F16DB5DCC9AA805C5D8DF852,
	ActiveSliderHandleAnimation_Start_mB740F795BF77D8902D11EB82EE795D33F2704E47,
	ActiveSliderHandleAnimation_HandleRotateAnimation_mB30864719A6BCE6E19F395B7F58D94ECFE708979,
	ActiveSliderHandleAnimation_HandleScaleAnimation_m7A1E89C592FA9483C1F1C777F3D5114668206CDA,
	ActiveSliderHandleAnimation__ctor_m1E65A669B3A94950A2A09200D5153DDB1AFACE61,
	AttackChargeEffectController_FixedUpdate_m062E03EDA04C50D5D9BB6304D9426637DFD2CE30,
	AttackChargeEffectController_OnDisable_m8E241DF56DDC5C3267710F49A4E17CCB74CEAF3E,
	AttackChargeEffectController_ChangeEffectColorChange_mF264E9F92807203B998E096A858D921A05306F88,
	AttackChargeEffectController_ChargeAttackCountEfectColorChange_m00EA5B0A53D6DECB1773F0809F2565BD26B766E8,
	AttackChargeEffectController_PlayerChargeTimekReset_mB9A7690D4243D404B8D36A9B29C5C122FDE9C6D0,
	AttackChargeEffectController_ChangeEffectColorReset_mE0D02E082BCED94424723E311AA4586B5DD23200,
	AttackChargeEffectController_ChangeEffectRateReset_m0406F63E4757C9EC053DCE911115CA240986C70C,
	AttackChargeEffectController__ctor_mBC60AAE1E9A758E1494FF6B07041DD2F3C762A26,
	AttackPointController_Awake_mB5D9DDFD0B5773983469658C8395B4E2BC6C1ED3,
	AttackPointController_FixedUpdate_mC2631CABA16F6C38A26798861C4E97388BF3D85B,
	AttackPointController_PlayerSearch_m09AC1F38193ECECE66377BBB087B6AD44DA87E32,
	AttackPointController_ShellPoolSearch_m123DCC0E0E2C7AF4E37ACF5FAE0D96CE0B1D1BF1,
	AttackPointController_PlayerTargetLockOn_m7FE99704FA08886746F227BCB74769B323C3FCEC,
	AttackPointController_AttakPointRange_mCC0F0DC69C3B4A48B9A19CF84F4E372AB29708F4,
	AttackPointController_BossShellInstantiate_mE8430CF2CE38F6427778197AF40E8C2B2B8E65EE,
	AttackPointController_ShellDestroy_mD19C3C33BE7936D4142384900652D96FBD4C58FC,
	AttackPointController_GetBossShell_mFA725EBF88517B80CEDC7759A136FEB02AA7EBFC,
	AttackPointController_BossAttack_m83B4EF8C409AA62034A8C4D6B438E13E429795B2,
	AttackPointController__ctor_m749097EFADD67D3BF1E2AE10309E0144D7CD863E,
	BossController_Awake_m5839437A38C7F49F5BC738F8869C3C167A0EBB7E,
	BossController_FixedUpdate_mB7A8B8400438A551C04C0B24F2991A79E47B50BB,
	BossController_OnMouseDown_m6BEC0F244A92BCD0B2A74AD90E360F1074AFBA41,
	BossController_OnTriggerEnter_m6BA80DA8F2CCD89B6C4EB7DC80EF3FEA348DF319,
	BossController_OnTriggerStay_m631031F4D748345016FFE3E5AA60163D2447EAC9,
	BossController_OnMouseDownBoolOnInvoke_mDCD769582A809A09C8CA9AE81BCCAE183908F078,
	BossController_BossAnimation_mA3F5935BAF461DC8BB1A82867C5E6133804D4B48,
	BossController_PlayerSearch_m7EF01C131B8601AE319A26A69E81E5FF1D342DCB,
	BossController_PlayerAttackPointSearch_m9210CAAF206AB47631637C49A12C2B32E9106531,
	BossController_EnemySearch_mB7D016E0422E8AEBDF86E448EA7438F8FE010A86,
	BossController_E_BossSearch_mD19B456E801F56B9BDBA835F013B06E8C93A8FE3,
	BossController_ShellPoolSearch_m9BD2323F336FAA328DBB48C63A509003455E54E8,
	BossController_DamageEffectPoolSearch_m0A5A76D6DC35D0DAFFF1DFF2630AD12E6DD3CCD1,
	BossController_GameManagerSearch_mA4EFADDA270B01AE965E9DF330FF26705CA69F21,
	BossController_BossActionPattern_m9D1EA7D9288304FBAD544A938D366D5FBA3B7D0D,
	BossController_BossActionPatternPlus_m0365F7D98558DEF4A1638EBB91E6EF8147556364,
	BossController_DamageEffectInstantiate_m338FC7CA0B063034305253EBBC58DF1E36970782,
	BossController_DamageEffectDestroy_m43CA8A135057A9F8C9F1F2E98E5A393D16CCDE50,
	BossController_DamageEffectDestroyBossGuard_mB5F746E7D3247FC22E693D2BD5E31E5E6B9CFF03,
	BossController_GetDamageEffect_m844ACC82742FF3EA3C188462C04D3FC7E219253D,
	BossController_DamageEffectActive_m32445C5FEA5A809C0F29747445DE3D690DDA3792,
	BossController_AttackPointActiveOn_mA27B19FE5D544AF91AC5135F6FEF8AF4FBF88809,
	BossController_AttackPointActiveOff_m59D635C40F0BBA42EE06B7586BE7355E8BBD55BF,
	BossController_BossLineShellInstantiate_mCA1C2271D750E2EE5A8EB2624653D32EA3E5CBDC,
	BossController_LineAttackRotate_m49D83F89E5313D36B15EF9ED64B023EC4F9F245D,
	BossController_BossLineShellDestroy_m46B9368065F0F6953950F05DBB02447B447D490F,
	BossController_PoolShellDestroy_m8DAC33558DEADA4BFE2A25A039928523FDAB0C16,
	BossController_GetBossLineShell_m391752E659A9FC38B211DA6A84AF4A4AD8FA45F0,
	BossController_BossLineAttackShellInstansLeft_m65F41C365FB0EDE9C5258A2944AEF9245E8FC27C,
	BossController_BossLineAttackShotLeft_mF37552156098CF243F7B2BAED6727B3B2702A3E2,
	BossController_BossLineAttackShellInstansRight_mB75D06D09B244119A5F85DE080AA1928BC868133,
	BossController_BossLineAttackShotRight_m9B9D36069EC1E5440C2FD730A6D4EC24E84785ED,
	BossController_ShotIntervalStandardUp_mC6AC462A63368112B5547691868ABC47DF4D0F2E,
	BossController_BossMove_mC1769BA06503B2EBD5FDAEE531B7889A1C313F38,
	BossController_MovePositionRange_m1790C9E2B4C79BED17FE5127240793DAB38ED11A,
	BossController_BossPositionReset_mE5B945F0FD29ACA0C4BE4DCCD622CA187D97547D,
	BossController_BossGuardActiveOn_m8528118E744E672A80E6E15E3B9CD7DC159C7408,
	BossController_BossDamageReduction_mAF009E741B6721CF2A338B345F321CBE751D8C51,
	BossController_BossDamage1Reduction_m39092D046FF75265D95F61B3FB0875EC7253A37C,
	BossController_BossDamage2Reduction_mE5FC58067797B3707D7570FD8FDA219091322F31,
	BossController_BossDamage3Reduction_mD664FBB89BD852A02FD5D0C88D2CB38EF92EE9F8,
	BossController_BossGuradMoveSetting_mA877730399AE34C0C1389D3786C29CC0BA4230CB,
	BossController_BossGuradMove_m390702D77667ADC242703EFB2B63D2386E5D10B0,
	BossController_BossGuradMoveCoroutine_m13EA63B4C08947F4A46F5A2736C4567C891B3F92,
	BossController_BossDamage_mC606A208B28E3EF0F09252AC04D971FD5668665A,
	BossController_BossChargeDamage1_mF496D343C91FC56CD8E80B8AD4B93445136AB255,
	BossController_BossChargeDamage2_m85995E6DFAC3BFB29067B92503895635177E3032,
	BossController_BossChargeDamage3_mBA203E23059096D19924E487FE8D193A7F73019C,
	BossController_PlayerEngineerPointPlusNomal_m42C2F5C292D31C99A4ADA768BB5DAD2447E398A2,
	BossController_PlayerEngineerPointPlusCharge1_m1C856F9E46FAB43E480333FC4839BD0ABFD33A7D,
	BossController_PlayerEngineerPointPlusCharge2_m3F89DE5572F59161F474A22FE129AD986D08EA94,
	BossController_PlayerEngineerPointPlusCharge3_m06DFF9BCAA9910C02B8FB8BBE630DC527DA03E48,
	BossController_BossTargetLockOnContinuation_m97193D6967E32C9D2BF0B75309E778F3A885E3AF,
	BossController_BossHPSliderActive_m49E0A731943E2815366D91933C460DDC82E77D21,
	BossController_BossHPSliderActiveOff_m50D6AAD8897ECCB08ED9DF5ECC7E521635AD120B,
	BossController_BossGuardForwardHPSliderActiveOff_mC0FAB83DE4625197DC068CEEBEEFC5756C835B92,
	BossController_BossGuardLeftHPSliderActiveOff_m9BDD8F3B764E64C05E5B77FA926ABD69631EA5E8,
	BossController_BossGuardRightHPSliderActiveOff_mCF1416A7E7800D4725FAED9C382FD7931964B765,
	BossController_BossGuardBackHPSliderActiveOff_m8747ED90AB2D2ED2E9B74C40BF2B2E41CF30010D,
	BossController_BossDestroyCallMethods_mB12F36F19168714D1A85A461F122E4EA19EEF481,
	BossController_DestroyAnimation_m19476EED5F67B1564F8343F88FD6316BB299B763,
	BossController_BossHpOutDestroy_m39761386CA0C9F3481D021FE04C0C6431F2A9457,
	BossController_BossMoveAreaDestroy_m11BD7A25DB4BDD2D56C8448748F2C0B0F323BABA,
	BossController_BossGuardSpawnEfect_m465CE04EC585BA85F19082C2D56C59BB2505F9BC,
	BossController_BreakDownEfectOn1_mB2BBBF0CAA061CF5E65037C3B24716A7A8B8DF0A,
	BossController_BreakDownEfectOn2_m406CC2B9B876D227DED59D8F560A08D0D54AA48F,
	BossController_BreakDownEfectOn3_mC2D8C065699718A02F3E6D37F1205DF92DC7D601,
	BossController_BossBattleBGM_m9089C40D5FF7DA27979888863B4127C88797EDAA,
	BossController_GameClearBGM_m3BD1DB2289F258D9FE75C864D7FC6DF3840653DD,
	BossController__ctor_mC4C9EEC35858E3E8D6475F9C844D40EF8F2E537A,
	U3CBossGuradMoveCoroutineU3Ed__100__ctor_mBBA2244364FB04EB53AA709ACAD576BEA75C9760,
	U3CBossGuradMoveCoroutineU3Ed__100_System_IDisposable_Dispose_m39D7620484A1AB151FFF9779F3A7B820850545B2,
	U3CBossGuradMoveCoroutineU3Ed__100_MoveNext_m1B85630DD2B9216756328CB453CFE10167B957DF,
	U3CBossGuradMoveCoroutineU3Ed__100_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C91675C1150931F6E4FB4AD893481F32F61C529,
	U3CBossGuradMoveCoroutineU3Ed__100_System_Collections_IEnumerator_Reset_mA65ED59A1D5BC835D4E113FF3C4DAABEBE658D7F,
	U3CBossGuradMoveCoroutineU3Ed__100_System_Collections_IEnumerator_get_Current_m73113E44C714D3338041C845FBE95CC808484380,
	BossGuardController_Awake_mB1E0540C3E6290F1E1A26AD9962F1850FC100863,
	BossGuardController_FixedUpdate_mC606DBB08AC874EE0E4F41F61C68AC4B90CAF67A,
	BossGuardController_OnEnable_mC1CDBAB9D9BCE1A197EE91AD352059A7BB11536B,
	BossGuardController_OnMouseDown_m57EAF87A12DEBC39D257F49C1C1C243CA412377E,
	BossGuardController_OnTriggerEnter_m5F1B37C80450292B1CE336E3212AF293A303BDB3,
	BossGuardController_OnTriggerStay_mC76E7D005700497A1D68ED4CB12FC1B73ED16D32,
	BossGuardController_BossGuardCountPlus_m1F444AF1DA9D3B6E71A68EB470F21FC4096C2F75,
	BossGuardController_BossGuardCountPlusOpposite_m52EA70E9707B47E538F49016CC35C4FC8FC2467C,
	BossGuardController_BossGuradRotateSetting_m0383D28C793A43D59F3E2EFAA63103460DB5045E,
	BossGuardController_PlayerSearch_mA6CED10F916A1BC4EC3ADFBEE1E673BEBBBFB59D,
	BossGuardController_PlayerAttackPointSearch_mF850A3F187BCC41ADDF4ED1E61BC10D69C06A1DA,
	BossGuardController_DamageEffectPoolSearch_m0BE46A39D46C2D5CA59652F10C7FA8842E03444D,
	BossGuardController_AttackPointPoolShellDestroy_mF1BA60CF3012A0E850D504793097DEADE0AFECD7,
	BossGuardController_DamageEffectInstantiate_m0209A04D6DC6AEE16BF41297E2A854849766B8BC,
	BossGuardController_DamageEffectDestroy_m0A9B1F08A6426E4866E1BBEEE5F9F71EDED7AB01,
	BossGuardController_GetDamageEffect_m2C62676E171A835A9F2DB4FE8CCAC2A3DEC607EA,
	BossGuardController_DamageEffectActive_mF48D741CB5F2C06E6C03AAE9FFE5BFCBFEA976D9,
	BossGuardController_BossGuardHPSliderActive_mA0DD1510B16DDA92B659108903201EEE65A6570C,
	BossGuardController_BossGuardHPSliderActiveOff_m797EE816325D2CA58283A9F82D42CEEF222B43CA,
	BossGuardController_BossGuardHpReset_m34E56A8455CF65A0D27B509BE89A66EE6E1337C4,
	BossGuardController_BossGuardForwardHPSliderActiveOn_m0DB5999CE1F841EF5F51A2BCC1D2668BDB4DB6D2,
	BossGuardController_BossGuardLeftHPSliderActiveOn_m9E1D57C276E8FEE47AC4E34B987A14D678B5E355,
	BossGuardController_BossGuardRightHPSliderActiveOn_m9C122AB7F84850352AEAC7F55E63FD711CA99B97,
	BossGuardController_BossGuardBackHPSliderActiveOn_mA8DF937037AB3440072D56676741E6214EBD695D,
	BossGuardController_AttackPointActiveOnInvoke_m4F7ECB32EAF30691A194E2CF7120A03E353B6E71,
	BossGuardController_BossGuardDamage_m09FCC6AA24BB651152C7FFA5EE1B4A99EF5EDFDC,
	BossGuardController_BossGuardChargeDamage1_m40643166383EBD0B0E0B51C6A69F848278ACB025,
	BossGuardController_BossGuardChargeDamage2_m6778BD4F2710F63F3137BBED77FF1708E95E70A8,
	BossGuardController_BossGuardChargeDamage3_mBBADA47D125EF6625314EBA41EF195A503C2F0D8,
	BossGuardController_PlayerEngineerPointPlusNomal_m743A874D97920F7B203380F3213B8029895EBC6B,
	BossGuardController_PlayerEngineerPointPlusCharge1_m17DDF6E337177657E5DF712F71240400D1BA10B8,
	BossGuardController_PlayerEngineerPointPlusCharge2_m4515D565D4360C12729B3A05EC796D4AEAF1CB90,
	BossGuardController_PlayerEngineerPointPlusCharge3_mF8F51D1B03DD24B76E3DE96871B44EA66A48E723,
	BossGuardController_GuardTargetLockOnContinuation_m6E0C8C613A25BF36186F91E93630898DF9B1A73E,
	BossGuardController_BossGuardDestroyCallMethods_mA3E408A6CE8F1CE687607A9F8128AAF627B1B185,
	BossGuardController_BossGuardHpOutDestroy_m0A7257F70DD17AFA9EE7F82911ABC7B9823F0839,
	BossGuardController__ctor_mDB641834D7A6F9BEAB7F299493642B0D6AE6F679,
	BossGuardHelthBar_Start_m52C94875FD62FBA9DAF7187495DBBBAFA0465971,
	BossGuardHelthBar_FixedUpdate_m64D0EADF76836AE3064FE0E4DF51C998F565A2E7,
	BossGuardHelthBar_BossCurrentHP_m60E8832BB98B13289164AF7EE4A6430BCC780941,
	BossGuardHelthBar__ctor_m468760944D7C6417B178FD32FC91C0EFDADAE242,
	BossHelthBar_Start_m0C57CD9FC9426A47476E1D667BE1599E3FC7F831,
	BossHelthBar_FixedUpdate_mE5734482BC3BF8A66814F3150BB09B73D5D5886A,
	BossHelthBar_BossCurrentHP_m53F938597D2D7151EC5E870E2067C074500408B9,
	BossHelthBar__ctor_m47C768945400507663A42C8DA447F89B32361C80,
	BossLineAttackShellController_Awake_mEB2824738FF286568326FDC770851B56525FA1C6,
	BossLineAttackShellController_OnEnable_mA7A3BECC11137B0B2E10F37F1DC9B6BBA8BEFC93,
	BossLineAttackShellController_FixedUpdate_m4ECFC7CA6E1DA4AD9781B7168CB47F2F42A42758,
	BossLineAttackShellController_OnDisable_mEA11B520AF6009E3CE5FEDC5E27A797BBE0FE700,
	BossLineAttackShellController_VelocityReset_m69B5EA1A4866542643746A981C2FDE1DD231EB18,
	BossLineAttackShellController_LineAttackShellShot_mE3462AA4D516607FABD93E68AEF83A78770CBE94,
	BossLineAttackShellController_ShellActiveOff_m42B15D4CDA9C4B1D77D72E23459F2075CF04026E,
	BossLineAttackShellController_ShotLifeTimeReset_m22A1C2B94B6E8DC8BE05911838C24D6D0B569EE1,
	BossLineAttackShellController__ctor_m07EC25ABA21A6672F766FA0BDA3591558445241B,
	BossMoveAreaController_OnTriggerStay_mBCA165DDB4D9CF74D0BFC40E2D6A18763C423C2C,
	BossMoveAreaController_BossMoveInvoke_m3AA206CBA30958F8DAE61A86516C237CED9B3F6F,
	BossMoveAreaController__ctor_m6787A76C0E5B4BB43009E0B6E9E34C2BB3630666,
	ChargeAttackShell2Controller_OnEnable_m383A7661FEEF846E29EBE1E74099DADF07205268,
	ChargeAttackShell2Controller_FixedUpdate_m4003D99FE0F6D94DCD7CD094530848DF3777FAB6,
	ChargeAttackShell2Controller_OnDisable_m8FC81EFC33C9EDC2EF70EA9A5094B39498EFCF5B,
	ChargeAttackShell2Controller_TargetMemory_m005D2253AF75A35D28E32C03AC1E78C627D0AB2B,
	ChargeAttackShell2Controller_TagetMemoryReset_m2A27D92329E895A4533AD0C966FEA6CFFFBDD78D,
	ChargeAttackShell2Controller_ChargeAttackRotate_m3FA2C62CFF56DBCDAD9F5F56D9233AA6FBEE752C,
	ChargeAttackShell2Controller_ChargeAttackShot_m3F8124BA0CBABED4C14405A7E077DCB0B93946C5,
	ChargeAttackShell2Controller_ShotLifeTimeReset_m8255D006240669BEAD6C83B1FBF4EAE910EBE0E9,
	ChargeAttackShell2Controller_ShellActiveOff_m54D0C4B07B98F133BA514D26BA5FF60944DCC503,
	ChargeAttackShell2Controller__ctor_m2FBB72AF5765B5E8A5528DEA0786D04A16B92F18,
	ChargeAttackShell3Controller_OnTriggerEnter_m47392BE9B976ECD987977437BB946D910B39E881,
	ChargeAttackShell3Controller_ChargeAttackExplosionInstantiate_mD3B9E58F00E33DB104146764EE14F26A53EFCB0F,
	ChargeAttackShell3Controller_ChargeAttackExplosionScale_m1E463DDFFB0FF53331B85FE6A183DB1C5F788453,
	ChargeAttackShell3Controller__ctor_m59DC513E15E4FD1B93076BE7C748A71063D689B5,
	CountDown_Start_m9F5188BA5A447532A1B45D2FD837A73E8BE2F2D5,
	CountDown_FixedUpdate_m3838C1ACFA068CDF031BF0A1AB168A17CDF24DD5,
	CountDown_GameStartCount_m0DAD45D3EED699334660D0B86E37CEA19E16FA91,
	CountDown_UIActive_m1AFDE8CE574E7DC34D62B8539027F076201D0025,
	CountDown_CountDownSE_mD11A32F2BD05B3C7C258545140D55EA21EF9DFA3,
	CountDown_GameStartSound_m9EEB9BF4442946DAE1170C467F3DEE59096FA8D6,
	CountDown__ctor_m0213629414162CE304073FD33AA08E59413FFD57,
	EffctOffController_OnEnable_m67C71DC3347C4C7EBB607169FF443F54DEC9002C,
	EffctOffController_EffectDestroy_mDDD5B46FAE71063370499BD37FCBE1F3D132BC42,
	EffctOffController__ctor_mFB8048F3D4F5473DA415F61EBE8FBC395811AA8C,
	EffectDestroyController_Awake_m480B81D25A0EE30E3B55455CE9BC83AB309D28E4,
	EffectDestroyController_EffectDestroy_mAF0855DAA9E6711CB28A73E40D7DFB8DAB50B8E4,
	EffectDestroyController__ctor_mE58999AD2EC33E8E1E07A88AB0B69782BE42E27C,
	EnemyController_Awake_m277E4A36C2A2089AD56826D99D8490FB399BD61B,
	EnemyController_FixedUpdate_m1A283386230E165353842B2EE804F95A47052449,
	EnemyController_OnMouseDown_m45673A6114E6EC2080205285806AA97BB59EAFA0,
	EnemyController_OnTriggerEnter_m2DADB51852D544E1329A118D76BFB058ABBB57EA,
	EnemyController_OnTriggerStay_m338B2CA7829CE15063E29DD8D67F3410D710BB87,
	EnemyController_PlayerSearch_m915599E5555613F13B8DD291CDC5AC824112758E,
	EnemyController_PlayerAttackPointSearch_m0AF75AF802F162E1BCF07E1E8B7A5734DAAE91DB,
	EnemyController_BossSearch_m0C8BC334805228711BAC552DF43D224D7B5003B5,
	EnemyController_B_EnemySearch_m30584A01A09CD9751BA16E62BE3480C88C3F316E,
	EnemyController_ShellPoolSearch_mD67210C6DE2F430D326BCA7AFDE38C8E51C7A337,
	EnemyController_DamageEffectPoolSearch_mA3096959C4F930E959D3E3C6FD0B29F0F63E53E9,
	EnemyController_GetAgent_m01DEB3C399B0763A231713595AE7EFFE49E4D72F,
	EnemyController_EnemyTargetMove_m59C4536706311F73ABB660D666190B5516BEFFE1,
	EnemyController_MovePositionRange_mC82EB866CD18EEEA02726F0D7407A99C6C693CEC,
	EnemyController_EnemyPositionReset_m062A894953B3985436D370E4672B992645E9CEBE,
	EnemyController_EnemyTargetRotate_m00D2FC0BE8EE78EBC420F26D7E118EA206457CCD,
	EnemyController_DamageEffectInstantiate_m32587C85CE4B706D6446E17F15ABFB241A130DAC,
	EnemyController_DamageEffectDestroy_m493A1EF218EB369DA217A87AB6E8FFBD373C5432,
	EnemyController_GetDamageEffect_m8B728310A0C8F9ED109223E9BE17735E625C621E,
	EnemyController_DamageEffectActive_m5D985FC372DD0B5EB606C2F466FF55C5B6DA42EF,
	EnemyController_EnemyShellInstantiate_mB6FC128A5DDFCAF6244E13AB47B4DA50EDCD20AC,
	EnemyController_EnemyShellDestroy_mAB04D00D8444E1CAEF744E2EAFB1E0B463C0CB3E,
	EnemyController_GetEnemyShell_mF0E0BD57280DB0DDE5193AE5A219BF164AF1B1EF,
	EnemyController_EnemyAttack_mD3C2EC37BD0097DDE7B83168CEB2829AAED79B56,
	EnemyController_EnemyCubeAttackEfect_m5627B270AED6D58EE9F0EEBC060E76DCA2A5EE15,
	EnemyController_EnemyCubeRigidbody_mECC334AC0B9BF4732BADC8F64F778386350E6695,
	EnemyController_AttakPointRange_m27BFF94C28BD4AA063F43839C14134515437868E,
	EnemyController_EnemyDamage_m54B70F4EF85D2F26B3E09FAC86D86AB2840E23DB,
	EnemyController_EnemyChargeDamage1_m2E6BE7E6009F9B4C71FCE401B6C3E8402D9442D6,
	EnemyController_EnemyChargeDamage2_mB0B8A0D7139315646E885FEF394C0CDABE6AD842,
	EnemyController_EnemyChargeDamage3_mA250F4E27425631CD0236A4E78E1BEF06FD4F004,
	EnemyController_PlayerEngineerPointPlusNomal_mBEFB47290E255BC72E520DCA946D4FF6316CA0AD,
	EnemyController_PlayerEngineerPointPlusCharge1_mFDDB7FDBD5F3ABB7A7610978492FD00C51B56A23,
	EnemyController_PlayerEngineerPointPlusCharge2_m0570F57226975554A33A0A3C5578E2B38E70F735,
	EnemyController_PlayerEngineerPointPlusCharge3_m28EF2A8697127D10D564CF9289A0FF314B22C055,
	EnemyController_PlayerAttackEnemyTargetLockOnContinuation_mA0B6CF326C390EBDC4F8190D48DC6FB41866391C,
	EnemyController_EnemyHPSliderActive_m184F76585BF138EBAAD39DE5166ED3AE1114367A,
	EnemyController_EnemyHPSliderActiveOff_m23D049B3D3621EE0EC30E52A349B20D5AB5A044E,
	EnemyController_CurrentEnemyHPSliderActiveOn_m48153B1571E14B6A206705C8BE5440083431F08C,
	EnemyController_CurrentNotEnemyHPSliderActiveOff_m6ED2A00E8D27A6248D0F6FCD6220D848664AAE94,
	EnemyController_BossHPSliderActiveOff_mEA861320611BC9C8DDCBA397931B821F75F23262,
	EnemyController_BossGuardHPSliderActiveOff_mE210A7F1F599C690DC1E698B0B582AF49A38DD16,
	EnemyController_EnemyDestroyCallMethods_mE08C7E3894B4446D784D0BB868D4B7E7280689A5,
	EnemyController_EnemyHpOutDestroy_mAB4C378C65A11833CD988442A4BFBB72FA2154CD,
	EnemyController__ctor_m984FF7EBF9BF2923A09FA943862B8941E9FFECCD,
	EnemyHelthBar_Start_mD1DD1B96C51842F971E264F61C5CF36E26DF0D9C,
	EnemyHelthBar_FixedUpdate_mA1F6D5840126DE8289431DA0D5E161352C68727B,
	EnemyHelthBar_EnemyCurrentHP_mF3C81F42195BE4EB173D02C17287188B1B65FD00,
	EnemyHelthBar__ctor_mFB12B425888522961438ABE7EB4E8794B5AB55DF,
	EngineerGaugeController_Start_m9C63291705F4639FAE6B913EA6C8A1712F1B467E,
	EngineerGaugeController_OnEnable_m1C22A9881643B031464C7E51B7B51D19765BC2C5,
	EngineerGaugeController_TransformScaleZero_m60FA82DA1599F54CB9DF1B2D752D12B17C29F40D,
	EngineerGaugeController_TransformScaleActiveAnimation_mA91DE1364DE76F0097757D5535BCAA0743F9BAEA,
	EngineerGaugeController_SliderCurrentEngneerPoint_m4FAC67E3DD92ADE9B7291A125F7DCF3F1D6675DD,
	EngineerGaugeController_EngineerPointMaxAction_m61C49549891250857265DC5F55A31BD815CFA164,
	EngineerGaugeController__ctor_mA5327925B1FA820E47635E0FCD6ED12FC448B246,
	GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2,
	GameManager_FixedUpdate_mCBA9C3ED1DE7FC1B9D9609D702B13EFB38245017,
	GameManager_PlayerActive_m249E9947ACA8250CE03BC31C2C565D99CBF204A8,
	GameManager_StartAnimationMovie_m0CCF1EC1466B0546149EEAFEE2E82B68280F54EB,
	GameManager_SceneChangeBackImageAnimationOn_mFB7C1AAFA55EC40209F12D28FB5AEEAD664B1084,
	GameManager_SceneChangeBackImageAnimationOff_mC266392365515BC1897630E167C995AB7F998574,
	GameManager_SceneChangeBackImageSetActiveOn_mF118E8973CCDF46E08BC8BE42EDAB1986CD0BCA4,
	GameManager_SceneChangeBackImageSetActiveOffInvoke_mE58FFC9203C554BBFEE6C7A0A89EFDD070590735,
	GameManager_BossAnimation_m672A141F62E2984EA29AD1B57104450186F09AD9,
	GameManager_StartVCameraAnimationCoroutine_m4F40509E0ECC21C10A089DB1B13256681154F8C4,
	GameManager_BossVCameraAnimationCoroutine_m9558BB31FCF6EDB732187438FA76538D05D75F5C,
	GameManager_VCamera3Move_mBEA0E4E1ABF054A8B71E48547221434759B511D0,
	GameManager_VCamera3Rotate_m4FD35F62A56591F188EEEE9BEDE9173C1D0E5F15,
	GameManager_VCamera2PriorityDown_m35D3F50FFA3CB16688F8FD0C5AC3F9E2F794834F,
	GameManager_VCamera3PriorityDown_mB525F0850CE9F9D102E8A0599032CC8218146AFB,
	GameManager_VCamera4PriorityDown_m03D1C411E34B373C09A43665771B96FE96502E45,
	GameManager_VCamera5PriorityDown_mC99AA109E6FAE36FCB3B21BFE7E7F6CA1B25F3C0,
	GameManager_VCamera6PriorityDown_mA6F56FD847C129FB35F0F30419EE419137094A6F,
	GameManager_VCamera7PriorityDown_m048F11806108426E6E128CFD86184B5C7F9A4B41,
	GameManager_VCamera5PriorityUp_mF76C5CC17A9D6D9E30E1D7C69F6956C6F95A339F,
	GameManager_VCamera6PriorityUp_mD0569BA7E4CA9347B5A7A429AA053D7E8B334DB1,
	GameManager_VCamera7PriorityUp_mE72EB451AAA494E48B5916A48F355F1DBF610B86,
	GameManager_StartCinemachineAnimationCameraActiveOff_mEFD88C23A68DE8D823F1ACE99F15AE4AC03D96B8,
	GameManager_BossCinemachineAnimationCameraActiveOff_m0253FB648687197A79931DC0A37C13870658C38C,
	GameManager_CountDownObjectActive_m412A303A5BCACB2ACD82A92922B2BA8853286F2C,
	GameManager_PlayerSpawnPoint_m6F5684B7BD2446935004DC1643A2273C0DB213C0,
	GameManager_PlayerStartLook_m4274734D67A44B6BBA9B1DF8C9AACE894302061D,
	GameManager_PlayerCameraStartLook_mB7BED31454ABBE47AA14529FCCB2E9D33C694367,
	GameManager_PlayerDeadAction_m3509182C38C8CDC07C23B276D57BF749A6C343E8,
	GameManager_PlayerFieldOut_m1C9DF3F712656EBE6D52D438BC1F0C322D982E65,
	GameManager_PlayerCurrentLevelText_mF08FCA777C30760C857F1FA498A886A4B1B68E4E,
	GameManager_GameManagerSinglton_m89EAD074AFD033C7D5E9676F2EBDBFF1282156D3,
	GameManager_PlayerLevelUpTextOn_m4DB704759E96F67C92EBCD0C5CB4F15C50B6693F,
	GameManager_PlayerLevelUpTextEmpty_mD11DA5B112AF785CC9432EAC3649D76A2C04CE3E,
	GameManager_UserTextEmpty_mBB7C454A198D4FB10246B9BCDD1594552F251AB2,
	GameManager_GetExpText_m94B174A61E8C97DA42A12024E114D0C589A15DC4,
	GameManager_GameSceneLoad_m611595F606A752E5ED401F3A46ED1E40DFB6C2D3,
	GameManager_TutorialSceneLoad_m3AD5C5168A455DC360D0AE20F77BE9D7CCBA933E,
	GameManager_EnemyDestroyCountPlus_m3BF11E3425DFD8C98AD6D795F0FD5CCB0A994729,
	GameManager_GoalActive_m715B1C6C6C1143DB79841899ECBBB9103C7E2B6F,
	GameManager_GoalActiveOff_mFA54E7993E1D3D3A2D4526F4192ED8CCC6E67D9A,
	GameManager_GameClear_mF5CE946B586CD7FA9FF5EDB85881E458D6149D11,
	GameManager_MenuButtonsActive_m6001800DEE9DD28A7E02C9B93345C9397233447F,
	GameManager_BackTitleButton_m6F887EF4E52D6699F3EF5E3EBD14172463EAD9F8,
	GameManager_ResetGameButton_mBE21C0DB49682DC6AC91FD10C583A4E5ED06DF0D,
	GameManager_ResetTutorialButton_m0207CDB71EB5BE310A2DB65FE013F5D32D54405A,
	GameManager_GameClearBackTitleButton_m979313047A526E8DC0FDAD5E3C6E58D2E5D83196,
	GameManager_TimerCountReset_m51655BE1F4C85DC4D1CD0821DDADEEBAE08D9480,
	GameManager_UIActiveOff_m8669E5EF4898969A38D2C5DB6D4B575409A0C9CC,
	GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368,
	U3CStartVCameraAnimationCoroutineU3Ed__44__ctor_mA8B16DFA9DBBA0A7F14E50587461644B6FFDCF26,
	U3CStartVCameraAnimationCoroutineU3Ed__44_System_IDisposable_Dispose_m97BE1E39FFE0BE5DB808212C68521BE697CDFB76,
	U3CStartVCameraAnimationCoroutineU3Ed__44_MoveNext_m7332C7A84F7CF83385666F344B035AD5EEDEAE77,
	U3CStartVCameraAnimationCoroutineU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCADA8309F44BC020F1702B5A1650A078CC35B91,
	U3CStartVCameraAnimationCoroutineU3Ed__44_System_Collections_IEnumerator_Reset_m8ABAA998E4846E1907C88D56855C5F6A7CD79214,
	U3CStartVCameraAnimationCoroutineU3Ed__44_System_Collections_IEnumerator_get_Current_mE55DB54AF1D447447DBF736150A0249C896E141C,
	U3CBossVCameraAnimationCoroutineU3Ed__45__ctor_mC83D3DDC5C37670415672911694DB2F10F7C2EDA,
	U3CBossVCameraAnimationCoroutineU3Ed__45_System_IDisposable_Dispose_m702A93A0D3CC4FD59CDA0C2482210C3AA7A3ED5E,
	U3CBossVCameraAnimationCoroutineU3Ed__45_MoveNext_mC1B653BEDF62CFB889B80B821CAD71574D0A304E,
	U3CBossVCameraAnimationCoroutineU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F4F5E24BF69F400FB893E84B3F443A295C3EEC0,
	U3CBossVCameraAnimationCoroutineU3Ed__45_System_Collections_IEnumerator_Reset_m3F03D0837EC9560C17B1CECCD7874D321F7FFB6E,
	U3CBossVCameraAnimationCoroutineU3Ed__45_System_Collections_IEnumerator_get_Current_mD431B289958AE9DC655DCEADD051CFFF43449E04,
	GoalController_OnMouseDown_mBE1F7F563AA856C3871B4911A022EE9611CB29BE,
	GoalController_OnDisable_m9FEFA4F716B9E5281EA066C7BA7C932051C3974F,
	GoalController_FixedUpdate_m0CF59D1047CF9DF86B83648A749458CB3872CEA1,
	GoalController_GoalTargetLockOnContinuation_m023D95006581C29F2F09EA88CA820741392EE7A7,
	GoalController_GoalRotation_m28FD718545DEF3D81BC93BECF648FBB8D83E891F,
	GoalController_UIActiveOff_m86AC777F0C3212E7C07B101D86D34C913C87C277,
	GoalController_NameInputFieldActive_mDF7A6A8CE0ED58FB074B697B8D84F54C41A79D52,
	GoalController_GoalColiderValue_m56648F2AFF43BBE4A55F6612335D5F9A532F3238,
	GoalController_PlayerActiveOff_mE845AEE22A38C61B804DA896D2004880280DF356,
	GoalController__ctor_m8A21509A91DA7E5919D5B6FD050E18B0695C8994,
	LimitOffButtonController_Awake_m2A874A34A81F0F13CDFF0C14E25EA36BD9E97CBD,
	LimitOffButtonController_OnEnable_m33C8C5BA552277F5714F981190F854FB734B4E84,
	LimitOffButtonController_TransformScaleZero_m979EDBFE91BC6759EBFBF1B6F95BB6C344F5D5EE,
	LimitOffButtonController_TransformScaleActiveAnimation_m506251B2BACA053744710CB0FA2ADEE234F2BAC5,
	LimitOffButtonController_LimitOffSliderActiveButton_m96C83EB2C34CD40E51584A7BFB35726B3909DD59,
	LimitOffButtonController__ctor_m26EEE89C58AF769F2ACED91FEDC81837C6DD04E4,
	LimitOffSliderController_Awake_m03129EF9CC210A5ACC0B0A051A46D0077BBA4318,
	LimitOffSliderController_OnEnable_m40A18B273F9E955CC2BA0EA31E8D93EE79F6646D,
	LimitOffSliderController_FixedUpdate_m858974C5F42D4F51223627FB1C4F9E85C900DC5E,
	LimitOffSliderController_TransformScaleZero_m73512D7D92951E7ACCF9C1698E928445E594D129,
	LimitOffSliderController_TransformScaleActiveAnimation_m70A15462839C7B92276FCE6C2A0FF03CF4A5295A,
	LimitOffSliderController_limitSliderPunchScaleAnimation_m1376865A87B9A0CD30B714905755F110615BC7F4,
	LimitOffSliderController_BusterSyncroActionValue_m08460ABB4E6B2579ACBB475CEF3F836AF113AE0E,
	LimitOffSliderController_WizardEngineActionValue_m8E7FA2482C183AC220F955BA3D8D427DB0F4BB1C,
	LimitOffSliderController_LimitTimeInvoke_m9512FB30EBB2A6A10F8F1496AF7CC6AFACA6AC97,
	LimitOffSliderController_LimitTimeEndAction_m9B5391C7D840218C453CDF136A24DD56565DBF7D,
	LimitOffSliderController_LimitTimeEndActionPublic_mD234B741BCEE30BB554524A4346B930D1AB0ECD5,
	LimitOffSliderController_BusterSyncroColorChange_mFF34206AEE3AC7A4BB56D5AA0BB0695C58FB97C2,
	LimitOffSliderController_WizardEngineColorChange_m1422AB60AA770D392DDB13641ED4CD0F2DC0BF8F,
	LimitOffSliderController_LimitOffSliderColorReset_mBE07CC693E377D78EE348CC530FD6505E1D697AC,
	LimitOffSliderController_LimitOffSliderValueReset_m71EE11F048981C94CEDF50416E85438037EA067D,
	LimitOffSliderController__ctor_mE26920175C0C2D94F96FD60767E464CA06C57E39,
	LookOnMakerController_FixedUpdate_mDAA0ED456AF74901CC1E2C88C3E446ED51C92EF3,
	LookOnMakerController_MakerRotation_mF69CFAE22B14919ADCE0EE5B023E092A9B11208D,
	LookOnMakerController__ctor_mC97811F2FE2F5AB2C4C3858F46E68A60CEC82760,
	MainSoundManager_Awake_mCEC847FF5E831E30543705B07B35AED81D6C969B,
	MainSoundManager_Start_m22F9FE4B2F0E499F3772C63B200BAD8A702E2315,
	MainSoundManager_OnActiveSceneChanged_mA63D4801527858B0C3D11781B04EFF77E7EB8C80,
	MainSoundManager_SoundManagerSystem_m2861E411C408B76D1D62FE35642E6D5079D3AB3F,
	MainSoundManager_BossBattleBGMActive_m0956A3CCF1977B5694772E45B01A5A355F304E92,
	MainSoundManager_GameBGMStop_m86BE55108F2031F529C6DCFBFE9982DAA8B4E6DF,
	MainSoundManager_GameClearBGMActive_m421B923503170FD6EEE821E5C1785763C67E6CC5,
	MainSoundManager__ctor_mF02C439B9FB0BC61A6111EF7C7B2FCFDF73213A0,
	MoveWallController_Start_m8DAF0B3BA31954F8ABA0CB1183408A0B476771B3,
	MoveWallController_OnCollisionStay_mC613B8DF3ED3994CC805A9C56AC0E17D95085EA8,
	MoveWallController_OnCollisionExit_m04E7BB366CAE49E98506CFD1059183B9A719A88B,
	MoveWallController_MoveWallStart_mBFC7E972C92830A6CAAD5C59B8AB452DC4111AA0,
	MoveWallController_MoveWallPositionMaxLock_m819B84D82E1F1B9C8615EAA21DA9AE8A99248A51,
	MoveWallController_MoveWallReset_m1C5A2DC506D1CD86E11DADED2C2C74DE33423E5A,
	MoveWallController_MoveWallIsKinematicOff_mFC41655B80884B3E7DF91CED0CC1F85A8394A41B,
	MoveWallController_MoveWallRigidReset_m18D6FF65DD3AA656FB897C64FEA81214B9ACAFFB,
	MoveWallController_UseGravityOff_m5FA7514369B7AA89BD434DB295423866FC0E5C3B,
	MoveWallController_UseGravityON_mACAE8C44489D539066A786679093175A9D4C7A23,
	MoveWallController__ctor_m5AC80996B2FF249BFA731627611B0C0C6F2E7216,
	TimeScoreLoad_LoadTimeScoreRanking_mDDAF42E2EDCAB670E72B6CB245FD2A20F76D4334,
	TimeScoreLoad__ctor_m21BFAD8404C775F13442F60F1F6359FE16C80D65,
	U3CU3Ec__DisplayClass2_0__ctor_mD0D18A575C3BC5ADF2E138D3C94910EA707EC11F,
	U3CU3Ec__DisplayClass2_0_U3CLoadTimeScoreRankingU3Eb__0_m5F1BEA815DBAB35B4485C1CE3D11157CBE227A6A,
	TimeScoreSave_SaveTimeScore_mB6F626FE8BA7BCEF5519A1E82817E27C65DBE132,
	TimeScoreSave__ctor_mAFB28CE91E2146188A78E8CD92097E676A0D19BC,
	U3CU3Ec__cctor_mF4108DC748AA6C0FE1531FCDF92656EC4076528D,
	U3CU3Ec__ctor_m63AA73C29131A539FBCFD335C9DD97AA4A02BEB4,
	U3CU3Ec_U3CSaveTimeScoreU3Eb__4_0_m79B07634627531688FE016BB250BC20BC1DB9EBB,
	ObjectInstanceController_OnTriggerEnter_m191FAE222D4743E35CCB1102501987A1BFB86F93,
	ObjectInstanceController_ObjectInstans_mC39734AD2BC5E0F48FA84F1053C28FB07D9BB483,
	ObjectInstanceController__ctor_m4F56B2D7C053D395BDBE578A99B42566B7EC9190,
	PlayerAttackController_Start_mC6B6FF33CDA660AD556E1614FC486DD834E55AA2,
	PlayerAttackController_OnEnable_m212E7403F906717FF4BAB5A89E9EBC0C66C06B51,
	PlayerAttackController_FixedUpdate_mBDA4282EF58502FCDFEBA65DE768A18DDB7FB921,
	PlayerAttackController_OnDisable_m7F774428D18399899AF32FB5F78D9A227F134D72,
	PlayerAttackController_ShellPoolSearch_m5BDEB70BD9806DD6FFDB6E7FFE9F0FA586CB314B,
	PlayerAttackController_PlayerAttackShellInstantiate_mC6B12E43E6DBF9AB3A1883226C207380E0E25DCD,
	PlayerAttackController_GetPlayerShell_mF9C279D225D0D76D91D3E39414901EEE300C02D9,
	PlayerAttackController_ChargeAttackShell1Instantiate_m19709132C051732846318BB809A7475DCA3D8764,
	PlayerAttackController_GetChargeAttackShell1_mF6F80FE5B76033F1DD21A722E59D22A067C296EE,
	PlayerAttackController_ChargeAttackShell2Instantiate_mD2FC1187109A472CA592AE66008EE23224454F9D,
	PlayerAttackController_GetChargeAttackShell2_m80156370601AAFD71B833E36CF9E9CBA6B0144D1,
	PlayerAttackController_PlayerAttackButton_mD60F6F64D9FA4EAE219F7651ED622035F6A39C1E,
	PlayerAttackController_PlayerChargeAttackButton_m5B22523632306DF9BC0F173CB325004C21A5F17A,
	PlayerAttackController_NomalAttack_mB2FDDCEF2DFED81FFA49513C7229E64A12047F06,
	PlayerAttackController_WizardEngineNomalAttack_mF9A4879A6DD2B4092907F33CD469D7DF5C877E8D,
	PlayerAttackController_PlayerChargeAttack1_m506927379E752A94B08F838DA1412044A87E5FC7,
	PlayerAttackController_WizardEngineChargeAttack1_m5B898CA69ACA258E90E3945D0F8039DCD8DC3E7F,
	PlayerAttackController_PlayerChargeShell1Instans_m82B9B8F12945CC40338C579844A909C9FC94436A,
	PlayerAttackController_WizardEngineChargeShell1Instans_m65537500BAA7DD895B3885395CE6B878AA8A6B86,
	PlayerAttackController_ChangeAttackShell1Coroutine_m2E7ACAA69E9201E10EAF08B4E0E01E16F7ED876F,
	PlayerAttackController_WizardEngineAttackShell1Coroutine_mE74C5B4D4EF442F2D3D34FD4937DA74EF882477E,
	PlayerAttackController_PlayerChargeAttack2_m4A75F450C517E472EAD88425C7A85649C9452785,
	PlayerAttackController_WizardEngineChargeAttack2_m75AA992384B137E87C57546D5302D0C797E0C42A,
	PlayerAttackController_PlayerChargeShell2Instans_mA69E706A4C04CEC9E96CBAD092167761ADC51720,
	PlayerAttackController_WizardEngineChargeShell2Instans_m9BB854F4591F1B8E4A75C12E7687B420C3F97637,
	PlayerAttackController_ChangeAttackShell2Coroutine_mB828B6BF50EE7A2317F6B65A7524F1EA8BE29D7B,
	PlayerAttackController_WizardEngineChangeAttackShell2Coroutine_m253B62E1FF54E019A3C7C69EB7F0051B68023916,
	PlayerAttackController_PlayerChargeAttack3_mB9C0A890A31020D48CD7EBD008EABA682BB44318,
	PlayerAttackController_WizardEngineChargeAttack3_m21716596BF667F0A358365F666D141A5C3CA7E99,
	PlayerAttackController_PlayerChargeAttackReset_m259247AF2575EB116470EA410B5B1EE6BE697431,
	PlayerAttackController_PlayerChargeAttackCountJudge_mDFBB018BB034B2D62816B6E38DEE41BD74609D47,
	PlayerAttackController_PlayerChargeAttackCountStart_m883CFAD9896390B6C41A366E581E587FAE04A36F,
	PlayerAttackController_AttackChargeSound_m583111E4005ADD1349C4B8CF2E079B132C2FA8D4,
	PlayerAttackController_ChargeAttackCountSliderColorChange_m7B6EADB5A8CFE1B9D5D8EDB3D27B40BF2479D484,
	PlayerAttackController_LookOnMakerAction_m7EFC07B3152BF0D5AFEE1254BFE10287B9BFE047,
	PlayerAttackController_LookOnMakerActiveOff_m953A73DF5FE77EDBC5AEE5E7BDE47EDB493E2BE0,
	PlayerAttackController_EnemyTargetLockOn_mC5435522B76D52C4DA5C0A10EB1648137FEE1C62,
	PlayerAttackController_BossTargetLockOn_m610E1DFE84120B8EADC86A53E164405E88A1BF24,
	PlayerAttackController_GuardTargetLockOn_m977332FD148DE0D11A06E66634A215066FB92D67,
	PlayerAttackController_GoalTargetLockOn_mDE17A6CA6A297E02360D3E0722E749DE63ACAAC5,
	PlayerAttackController_LinkParticleOn_m4E0D6B3069069E44EB8B044C7E452974C3C90C81,
	PlayerAttackController_LinkParticleOff_m2E4D28EAA0D4DB1FE6BD32D47BEDB16F799585DD,
	PlayerAttackController_PlayerAttackPointRotate_mCC22869E78DB236E6E0DD56AFF7DE4AEE59709EE,
	PlayerAttackController_LinkParticleOffDestroyCall_mF0916C0ACAC2621E6437E2494A37A462D0FA4BB4,
	PlayerAttackController_AttackPointTargetReset_mE59E72DFAF83061B2B6C28D3BD6C5A0F18B3717F,
	PlayerAttackController_WizardEngineStart_m5C7ACBC6CADE6D5DC730191C59B330147F310BE4,
	PlayerAttackController_WizardEngineEnd_mF027CB858BC801B567C47A41048D5753F8565A3D,
	PlayerAttackController_WizardEngineActive_m9498AF09D012E15692DA37A7133AB136F39BD190,
	PlayerAttackController_WizardEngineActiveOff_mF9ABDB7F2628658D2DDAF9203229E6DEEAE3C5A8,
	PlayerAttackController__ctor_mCFB05B37EBD42F1C3F900C10661243D0232A2179,
	U3CChangeAttackShell1CoroutineU3Ed__65__ctor_mEA99C9D7C5714D6430C99AF87E2AAFE5BD16039A,
	U3CChangeAttackShell1CoroutineU3Ed__65_System_IDisposable_Dispose_mE02A793DACD6B4D9FA3085B67D7525155F417742,
	U3CChangeAttackShell1CoroutineU3Ed__65_MoveNext_m64B8663473919DDDC44B772DCDF7DF80FA3852C3,
	U3CChangeAttackShell1CoroutineU3Ed__65_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B7DD7B010562B0ED1EEDA93D3AEEFE416A6F71C,
	U3CChangeAttackShell1CoroutineU3Ed__65_System_Collections_IEnumerator_Reset_mE85601A9A6A6514C2500254F5930AD48C4BF7F48,
	U3CChangeAttackShell1CoroutineU3Ed__65_System_Collections_IEnumerator_get_Current_mE11BFCEE9378889EB783DE08FD36AE4A4457C468,
	U3CWizardEngineAttackShell1CoroutineU3Ed__66__ctor_mB1D7ECF4B87DFAD5DA9DC301A3035AF18132DC81,
	U3CWizardEngineAttackShell1CoroutineU3Ed__66_System_IDisposable_Dispose_m4BF4CF66C62BD21CB997737EF11F13B526E3A400,
	U3CWizardEngineAttackShell1CoroutineU3Ed__66_MoveNext_mF35EDCF66890120A9B41BDF779E70EA857E789B0,
	U3CWizardEngineAttackShell1CoroutineU3Ed__66_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A8E19243F6A9EA2A88AA1973000D7D973E5F95D,
	U3CWizardEngineAttackShell1CoroutineU3Ed__66_System_Collections_IEnumerator_Reset_m8CEA498248B69A956EFB7CE5D829CFA064930BD9,
	U3CWizardEngineAttackShell1CoroutineU3Ed__66_System_Collections_IEnumerator_get_Current_mAC07F3EA1C2A02042B84DB4991C60507C6610F1B,
	U3CChangeAttackShell2CoroutineU3Ed__71__ctor_m111FF37FC4C37BD764F24D41A9A753F053673CAB,
	U3CChangeAttackShell2CoroutineU3Ed__71_System_IDisposable_Dispose_m1A0AF9E29CFF86A10EA1743A368BFB26F15E6CCB,
	U3CChangeAttackShell2CoroutineU3Ed__71_MoveNext_mDB2CF249E0BC7F5FA5010E021643461923D81A2A,
	U3CChangeAttackShell2CoroutineU3Ed__71_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m861585B43C33615048BF4A80CC95467FCD9F04C4,
	U3CChangeAttackShell2CoroutineU3Ed__71_System_Collections_IEnumerator_Reset_m4F27F921D1D506B180686487E60C248FC69336D8,
	U3CChangeAttackShell2CoroutineU3Ed__71_System_Collections_IEnumerator_get_Current_m85C95E37ECF10918D185596AACD8D424913C7FD6,
	U3CWizardEngineChangeAttackShell2CoroutineU3Ed__72__ctor_m1E0151D525ED78D68A0D0B43D14DE3D24A3A29FA,
	U3CWizardEngineChangeAttackShell2CoroutineU3Ed__72_System_IDisposable_Dispose_m656C603C27C6719D257DE01D8F31938FD90E27AE,
	U3CWizardEngineChangeAttackShell2CoroutineU3Ed__72_MoveNext_m3ECE58B9DF24C9F0A7FDF9F84D7044AA27B7317F,
	U3CWizardEngineChangeAttackShell2CoroutineU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m11AF48CF0C362EC496614E96FED01E96F31D62A7,
	U3CWizardEngineChangeAttackShell2CoroutineU3Ed__72_System_Collections_IEnumerator_Reset_m153D3C6D28017AA2788F7CB651B25712565B8331,
	U3CWizardEngineChangeAttackShell2CoroutineU3Ed__72_System_Collections_IEnumerator_get_Current_m286A3876658DCDD71E1E2E3156620E28E69C10A1,
	PlayerController_Start_m1D83076E8B136A71051F2F02545EE04947D3A8CF,
	PlayerController_OnEnable_m1558047F72022F1A32FC373A927E122D1F3CF5CC,
	PlayerController_FixedUpdate_m6D906D8B13844542B81CC49BA19760F747CEC8C0,
	PlayerController_OnDisable_m16524B89F1DDB4EB1E201CCEB7E9502967BF57DB,
	PlayerController_OnCollisionEnter_m2FC01282FAEE546C4408E6B901DBB3EB5A5BF989,
	PlayerController_OnCollisionStay_m1DCD5D8E99C72E2B5DE2E2F828C9C3504E31DE22,
	PlayerController_OnCollisionExit_m4DF3784586ED7D4C4E7E053117AE5406FD71A559,
	PlayerController_OnTriggerEnter_mF2704872AEA62333341DCBEA2831320C385701D1,
	PlayerController_DamageEffectPoolSearch_mCE682144CD64276C4BD81488E57E98A2334188C8,
	PlayerController_DamageEffectInstantiate_m9F2E8388CEDBFA4B30446B3B53FBE603C5CF51C9,
	PlayerController_GetDamageEffect_mF25D057FB6186CFCB9994F2DBF41CFEF99A49148,
	PlayerController_DamageEffectActive_m7535005B73CF1A8CFE83CB7D5E55BA0725700357,
	PlayerController_PlayerLevelCalledOne_m7A807FB09899445F32411F415E9DEC54C6E85733,
	PlayerController_SkillPointPlus_mC83952ACD7043FC872346017086C26502B28AA4D,
	PlayerController_PlayerMove_m130CD8F4AEE12FD9853ACF892EE0417E5ECA5F3B,
	PlayerController_PlayerRotate_m6E8CE70141992717778C58F28E89C084A704C687,
	PlayerController_PlayerJumpButton_m07EDAC299C60F528D39B17D663C4C70D87768E77,
	PlayerController_PlayerJumpPowerAdjustment_mA1E9D022D0D71F81EACB64C4DCF699EDBE271C19,
	PlayerController_EnemyCollideDamage_mD36CF39C4CEBB0616FC5CE48EF0FC992A4AB5818,
	PlayerController_EnemyAttackDamage1_m70742D23038A948984CFD2D8530E20E29408BFAB,
	PlayerController_BossAttackDamage1_m1D5FEABC6C9E82DED9D05EA2A7C9996379CCDD27,
	PlayerController_BossAttackDamage2_m5D2C311AE766B3A7D55B08EFF87BC1131D35E0AC,
	PlayerController_SliderCurrentEngneerPointCall_m81DEC187336BD2C75D8DA8F4A41C02C7C727A60E,
	PlayerController_EngineerPointMaxActionCall_mE5DD443C625994872A1F2718E286ABB07A553347,
	PlayerController_EngineerPointPlusDamage_mD9A3ACC91E2CD485085648A535CF1FD65084F49C,
	PlayerController_EngineerPointPlusNomal_mA191FB5A4C3C957B46FB55FF1496F961CD7C007E,
	PlayerController_EngineerPointPlusCharge1_mC06561D327FEC153B5C553C98A3D569B8B328300,
	PlayerController_EngineerPointPlusCharge2_m71319E5CD8FCC102377C8A80290AD3AC41641B56,
	PlayerController_EngineerPointPlusCharge3_m45C620A5176301E90570A4C7551A28D37EDFE51A,
	PlayerController_GetAngleVector_m818ED8A36FD7FD12F85826A1EF837C9A3977C9C6,
	PlayerController_PlayerRetryButtonActiveOnInvoke_m490C08B484DC3ED9C04B63A5A2F8D0B03F893586,
	PlayerController_PlayerRetryButtonActiveOff_m32100753D617FAB8ED74112FAB6FCC05AB8DF47E,
	PlayerController_PlayerActiveOnButton_mBC1450F0410E4EA55797EFBDBF98A366B59FF80B,
	PlayerController_PlayerRetry_m23DDDE3F23ECF2ED38BF1287FA7CF58359E2FAF4,
	PlayerController_PlayerHpEmpty_mBB23FE99F4AFB306A55AA557B5839A2C5D65D035,
	PlayerController_PlayerActiveOff_m71E8CAFA1731F207FF35A68F262EAC7123634F9F,
	PlayerController_EnemyTargetLockOn_mD7DF1E8898D58DA0300B8A72A8037BF880A9223D,
	PlayerController_BossTargetLockOn_mEF51AB2D4BE5EFBE83B46BE6DA7851727938E72F,
	PlayerController_BossGuardTargetLockOn_mBE5682F7CE38DEC72035904D3A07BFA5A86F166A,
	PlayerController_GoalTargetLockOn_mF2A6A71E6D05A8F4348FAFC3537B42D09AA0AD25,
	PlayerController_TargetLockOnSound_m041BF9C7F2251AC8AD4ED46F46F7680A18D178F3,
	PlayerController_GetItem_mC05208185EB6CF86F6013996A52003FD4918D0F4,
	PlayerController_ConstrainsFreezeRotationOn_m6A2143B0C1A2DEAC67BCAD353874D1827FAC088A,
	PlayerController_MoveWallOnLock_m3297C3F3A675762047179D67DC4CD0A4FCC703A6,
	PlayerController_UserTextEmptyAnimation_mF98987D0A34BE5D9EBC9D03A667A7B37E4F27BFF,
	PlayerController_UserTextEmpty_m0312EF6DDBC7F99CFA3143011F9868A6CB85A887,
	PlayerController_PlayerAvatarSaving_mE937D704D0A3670E997A81471D8E761FBB413568,
	PlayerController_BusterSynchroAvatarChange_mA71748F6646EA93DB5552147F3B60151856E66A8,
	PlayerController_PlayerAvatarChangeBack_mC088A44C6841D8001F996423EF0A4F3C4A822DA7,
	PlayerController_BusterSynchroAnimation_m992B23637984AD685FF532201AB4A344E12E5298,
	PlayerController_WizardEngineAnimation_m6F850F76E541554386AAFCCB6821D19BDB4AB45E,
	PlayerController__ctor_mDDAB7C7D82E1A5B3E6C197B1AB9D653DFE554F33,
	PlayerHelthBar_Start_m52091C67E589118483B8B49CC080E6CFFEF160B5,
	PlayerHelthBar_FixedUpdate_m9203ADA63B11ECAC6CDB202A64180B672CEA88A5,
	PlayerHelthBar_SliderCurrentHP_mBE751CD7180ED158662A92621FA73F01895A3D65,
	PlayerHelthBar_SliderCurrentHpCrisis_mFC3FFB4569F4742A6A5B0583771676E5DC0541C2,
	PlayerHelthBar_SliderCurrentHpColorReset_m37368F50247502D8D36DFD98C952CCD43AA2B74A,
	PlayerHelthBar__ctor_m7CD615D8DA3C42ACFE2427EA36E7B5D6B7A3038D,
	PlayerShellController_OnEnable_m13C566623877A4B34996D0FE19C14F2491E7DD4B,
	PlayerShellController_FixedUpdate_mFD2A59DB8F9BD41E1C97CB3A7FCE40FCED96CB9D,
	PlayerShellController_OnCollisionEnter_m4DE03A474B323C5229FE7C008CBA716CFC2F8060,
	PlayerShellController_OnDisable_m222E7028729A6AF2148B838682925D8F3C309EF6,
	PlayerShellController_AttackShot_m3234BA9F4E4FC141966EA0950C631108C3739E52,
	PlayerShellController_ShotLifeTimeReset_mE7354CAB71259B18ED1D932207E3F5AFBC187933,
	PlayerShellController_ShellActiveOff_m2E0D23BCAB4F75B2BE0D98FB419C9B53A0502C7A,
	PlayerShellController_ShellActiveOffWithGround_mFA2D7629CB2378D9615422BAEF214D7326F4FD35,
	PlayerShellController__ctor_m924438219A3828E221268035E1907C715C1E96B3,
	PlayerStatusController_Start_mE80B01F99BB2A56F11E540CF66A8234DC88EDBFE,
	PlayerStatusController_PlayerSkillMinus_m8E2078011A53382AF856AC07372B712D79BF8B13,
	PlayerStatusController_PlayerSkillPointText_m65B1358B8A748CCEE496CA0EEE31DC88969E19FD,
	PlayerStatusController_PlayerStatusUpSE_m8E18930C28EB043530E60403491AF7F12217DF4E,
	PlayerStatusController_MoveSpeedPointPlus_mE345E8483161ABCE86D60B8425C38A99DF717AB7,
	PlayerStatusController_MoveButtonShakeScaleAnimation_m1C84F806D81D892A747885E541E69DA821A060C3,
	PlayerStatusController_MoveButtonShakeScaleReset_m962A8F71D4C4D9A4F4E1E0016887054398CEBA21,
	PlayerStatusController_MoveSpeedPointPlusText_m0591C6500F4EEDC767BB401CA726C4F79E4358CF,
	PlayerStatusController_MoveSpeedUpButtonDown_m078CD7C52494C3646ACD5D8F2A3E5970FBE5A4D9,
	PlayerStatusController_MoveSpeedUpParameter_m59AD0D8089EFF652DEA82586429D5F6294DDF455,
	PlayerStatusController_MoveSpeedUpStatus_mCE9665380777C89244BCDCE39E10CEA6FD935308,
	PlayerStatusController_AttackSpeedPointPlus_m969E87CDE1F4786CF5734FC8B16FE85C9ADE1329,
	PlayerStatusController_AttackSpeedButtonShakeScaleAnimation_mC1885F9F765D78E12974604B76FDD2A183BB63E0,
	PlayerStatusController_AttackSpeedButtonShakeScaleReset_m5E0E1F581803FDEF34A6BD3855A28874E64ED005,
	PlayerStatusController_AttackSpeedPointPlusText_mB7C836FD1C487A278338E1605401D842B8EE255E,
	PlayerStatusController_AttackSpeedUpButtonDown_m9A47F5B6AB73E6FDF06199764C64D3C2EAEF70CE,
	PlayerStatusController_AttackSpeedUpParameter_m51E8DC3A96AF6E9681B323C63B416610E0A9D0CA,
	PlayerStatusController_AttackSpeedUpStatus_mEF21DF5018797236BA6A404EF74478EA5D3D4E8F,
	PlayerStatusController_AttackPowerPointPlus_mB8572503917FAEC62E5E06EE35E23B40B47BDBD5,
	PlayerStatusController_AttackPowerButtonShakeScaleAnimation_m1C983CBA77B4728119590438CE650C43AA7CF05F,
	PlayerStatusController_AttackPowerButtonShakeScaleReset_mACE582EE01B77A0E8B93A593631E12B1B5F477ED,
	PlayerStatusController_AttackPowerPointPlusText_mAE29293DAB6874F4A3FE2E32882853DA06E65B26,
	PlayerStatusController_AttackPowerUpButtonDown_m4BAC9C1A2E0643B4109CADC1AE6E91E43A531870,
	PlayerStatusController_AttackPowerUpParameter_m2F49AC6FBFE4DF5969B66EDF67800209179C2200,
	PlayerStatusController_AttackPowerUpStatus_m457C1E398F0FEB515EB3A095F6F4F953DF291014,
	PlayerStatusController_Charge1SkillPointPlus_m8B898986867DA8D4C59CC44615CC87E25BADCFC7,
	PlayerStatusController_Charge1SkillPointPlusText_m2A106A55D70072DC19F0EA77C9BB8E312636376F,
	PlayerStatusController_Charge1SkillButtonShakeScaleAnimation_m46C28A643A2EEBE8338A674CFCFE2CBE75912294,
	PlayerStatusController_Charge1SkillButtonShakeScaleReset_m377F8B9EFF39E1A2FA6B2F7F15BA39AD057A38E2,
	PlayerStatusController_Charge1SkillUpButtonDown_mF245A9D9B8DF5925C85B3E156ABEBFEC02C3B890,
	PlayerStatusController_Charge1SkillUpParameter_mFDF720EAB38505CC61861B3E9CD8B9376B379A94,
	PlayerStatusController_Charge1SkillUpStatus_m10107FB563BCEE242C89A463875DA1D6F2230AF7,
	PlayerStatusController_Charge2SkillPointPlus_m9B0A22307117262FD4292F53F56755F146A14F8B,
	PlayerStatusController_Charge2SkillButtonShakeScaleAnimation_mA933FCC3DE40CF69D4D38F7B61F6178322413E66,
	PlayerStatusController_Charge2SkillButtonShakeScaleReset_m7A98792A8899306BB606BE6BB732B95BFE242C9D,
	PlayerStatusController_Charge2SkillPointPlusText_m4718988F6F37C46BAE101A36D3789D689FE1B2D8,
	PlayerStatusController_Charge2SkillUpButtonDown_mB5DD66E70E7086F5E3B04217202437AF74F9436E,
	PlayerStatusController_Charge2SkillUpParameter_m56242DA2FE2FCD33E5EAD53C40C95F24969F986E,
	PlayerStatusController_Charge2SkillUpStatus_mF7146D4B80A8A0450A1DBA34D51C6D8DD97ADCEE,
	PlayerStatusController_Charge3SkillPointPlus_m2ECD39C6620C2F32AA4E1C0838025F3ECD428F0E,
	PlayerStatusController_Charge3SkillButtonShakeScaleAnimation_m18959EE167BDF9D086F638073298779B1313E9B1,
	PlayerStatusController_Charge3SkillButtonShakeScaleReset_mBE7F323975F52EE49E351A4C2BE484A474FCCC34,
	PlayerStatusController_Charge3SkillPointPlusText_m3B845D20C71ABB3E0DF669365B885FEB11C26FD6,
	PlayerStatusController_Charge3SkillUpButtonDown_mB0B0F6BD83F06A0BC76FF78B3636050E2CE6ECA7,
	PlayerStatusController_Charge3SkillUpParameter_mAE0623F16D9AB8AEC79439073A43D26866521B5A,
	PlayerStatusController_Charge3SkillUpStatus_m7E9A65B9427E09F35D40C1124F65268B25913E7B,
	PlayerStatusController_CurrentStatusSaving_m2C4DC185484A0467348667DBD2473C79EEC70FD2,
	PlayerStatusController_BusterSynchroStatusUp_m2F4390DED8BADD2C61B2C3760654C858A86CBF33,
	PlayerStatusController_BusterSynchroStatusCurrentBack_m4179E9C55FC03F0A08D05C592C42A093C0095328,
	PlayerStatusController_BusterSynchroStart_mC57E5A5F1461944521A31C298D4DC48EF065A63A,
	PlayerStatusController_BusterSynchroEnd_mCF0D2FEE74AD1CAC57EE44558447D0FF91B05843,
	PlayerStatusController_BusterSynchroStartSound_m7E002ABBC4BC34CEA1E5338D29BE78A2D3D323E4,
	PlayerStatusController_NomalSynchroSkinActiveOn_mC1EA5B7921CDFC8B747ED4B4A6E493E177ED06F2,
	PlayerStatusController_NomalSynchroSkinActiveOff_mC074403548614DCDA5200766BEDE68AF047311C0,
	PlayerStatusController_BusterSynchroSkinActiveOn_mC882CA14ABF1A7D8CF8CDCA7062A86F9CB650719,
	PlayerStatusController_BusterSynchroEffectActive_m3976BD61124DAA6A51D3F70E1FBDAE4D6BCB252D,
	PlayerStatusController_BusterSynchroEndEffect_m3F826205BFA14AC1FC6BB3BE8C6858BD9ECB15C0,
	PlayerStatusController_BusterSynchroSkinActiveOff_m1C601EA15B90AAC975CFCBCA36D6E30EFC2F380D,
	PlayerStatusController_PlayerAttackPointPositionCurrentSaving_m8BEBD378EC8699F5356CDDCE8CE40D598BD59CE4,
	PlayerStatusController_BusterSynchroPlayerAttackPointMeshActiveOff_mAA249A508588518840C155F38CEE5DD362E316D8,
	PlayerStatusController_BusterSynchroPlayerAttackPointMeshActiveOn_m4AB96750F3060F83DAF68D59EB0C25EF8C5FB243,
	PlayerStatusController_BusterSynchroPlayerAttackPointPosition_mE16E9C1600BA412D59D70026C77D5A886111ED6C,
	PlayerStatusController_BusterSynchroButtonsShakeScaleAnimation_m7B8931D9F2C3ADC0D19C541F6CA4C2B31479F546,
	PlayerStatusController_WizardEngineStart_mCEE77A8A5C10FFE548C9BFA0930CAA5C2229A628,
	PlayerStatusController_WizardEngineEnd_m8F395DE56724DA6658CDF4B29DA82D9DF697F02B,
	PlayerStatusController_WizardEngineEffectActive_m3DAB33B6DE0EEB54BD5773C8C675763D911ECBF7,
	PlayerStatusController_WizardEngineEndEffect_m16CA3C3D4446F16156D1FE9840AF30EE32D7A3B1,
	PlayerStatusController_WizardEngineStartSound_m3B70E0B58BC7FE2A42DE5E6F5AEB1DDFB4097CB5,
	PlayerStatusController_WizardEnginePlayerAttackPointPosition_m0511494F24651D99ABA5C0C64C4AC2C885D92EDF,
	PlayerStatusController_LimitTimeEndSound_m4050EE8EA1FF4FA653A225F0C0F1AF996ECE61D8,
	PlayerStatusController_PlayerAttackPointPositionCurrentBack_m72829AD8100BCCD3015D9959CB0911FED3800521,
	PlayerStatusController__ctor_mB257CAC62C6A9C50BF6427DA1A21396F64B93785,
	ShellPrefabController_OnEnable_mD7D2C5E0138A6F663C7FE3DEB6DCB063B9F4AF31,
	ShellPrefabController_FixedUpdate_m8D753CFF3DD04826EB27105E234ACB6AEF4CC0B6,
	ShellPrefabController_OnTriggerEnter_m36E5FA533ABF4210574D1D565E8D2D80070CF80D,
	ShellPrefabController_OnDisable_m5A204DBBA9D5664C4E4E1D226E5C02F40C2DFB23,
	ShellPrefabController_AttackShot_m899EB04DF019EAB248EF6E6E6F9269879573F35E,
	ShellPrefabController_ShotLifeTimeReset_m9D959881C6F3EDA03A54F86650263680AD17738B,
	ShellPrefabController_ShellActiveOff_m57503D34D718A2B0F6095D48985A404276EB1F14,
	ShellPrefabController_ShellActiveOffWithGround_m12C654313FFDBCE60AEA795CEB49E5235BB0494E,
	ShellPrefabController__ctor_m00F524E77518043151F150AFA3463C516639E061,
	SliderController_Awake_m7D561A899B4B85A8CBCE96C945CBF497FFDDEA2B,
	SliderController_OnEnable_mDDC5B9EBE49D1A0DE877BB5CDCEB937E68AA77AA,
	SliderController_OnDrag_m6B0D383CA02B03A90781144A2DFDC83925FAB767,
	SliderController_TransformScaleActiveAnimation_m2F6EEC7085CD326D6E518A6E3F0F8E55CBAB3387,
	SliderController__ctor_mF9C897ABF6ABCCA22F3A9D0198DC1B64599849B3,
	StatusUpEffectController_OnEnable_mA46603ED1A3F0A337F87CB74BC44BCF1BC361410,
	StatusUpEffectController_FixedUpdate_mBA05A0B74F8EA2BA91C20AD37244D42271D9A438,
	StatusUpEffectController_OnDisable_m3FDFA86B7D8EA504E1597DDB57D0322FAEA3861A,
	StatusUpEffectController_EffectActiveTimeReset_m182D88DA58B5790701B4BEFA064FD7BD7B6B1E0B,
	StatusUpEffectController_StatusUpEffectActiveOff_m515E1F7D9C05651D9E137F32A835BB55FDC7ED4A,
	StatusUpEffectController_MoveUpEffect_m194B37A2BBC9DFCFD06FE0924CBF039E662EABAA,
	StatusUpEffectController_SpeedUpEffect_mE3F88A55B197EFF761B68A8AB8BB65BA39ADB3BC,
	StatusUpEffectController_PowerUpEffect_mCC05900EAA690B3A4ECD560A3948789BCA171E0B,
	StatusUpEffectController_Charge1UpEffect_mFB053702137A1F768A7C7EA86666FC0BBE246D2D,
	StatusUpEffectController_Charge2UpEffect_mFF42EA89CEE15D82D83707A0FB1C8E737175AD73,
	StatusUpEffectController_Charge3UpEffect_mED5F86CCD9CE680AA4145532FFC8042C5F13EE7C,
	StatusUpEffectController__ctor_m0EF066873B624D37C7C640F45AECA706AFE1E68E,
	Timer_FixedUpdate_mD7CAE6298FC559AD59E2C58D2344766534CC7F50,
	Timer_TimeCount_mAFF656996A1CAF11956C14B910D35D89C6EC587D,
	Timer_ClearTimeTextActive_mC7560C3D5C04A08EB4BAB41273B661400AF881E5,
	Timer__ctor_m5FF13F1DAD0527F97E229A1904A8AD662731C4B5,
	TitleStartSceneDirector_Start_m3842E954B40E27995D262E8BEF6E723022C3C839,
	TitleStartSceneDirector_OnClickTitleActiveOff_mE3CB366D6141F54F2B2F8C62D22BCACD9CC60AA6,
	TitleStartSceneDirector_OnClickTutorialStartButton_m9EDABA4C165269CD32879AFC8B98344A0583ED44,
	TitleStartSceneDirector_OnClickGameStartButton_mFB780E80308C4F395AFCD4B9EEBB0EC7DC600C5A,
	TitleStartSceneDirector_OnClickManualViewButton_mC486AB40825725E4D25BE0299D2FD82449599E49,
	TitleStartSceneDirector_OnClickManualViewBackButton_m5A525F537F2E4B5580ECAEF4EC72F8F586E2A513,
	TitleStartSceneDirector_OnClickRankingViewButton_mCF072B6107C39C4D20B62FC2CD5D68828B153F0A,
	TitleStartSceneDirector_OnClickRankingViewBackButton_m8E5CF9FB98327E403288AEDE1D78FF445C056D86,
	TitleStartSceneDirector_OnClickCreditViewButton_mC62BF4A96F77D54AC97B4A50657A441038AA8FA5,
	TitleStartSceneDirector_OnClickCreditViewBackButton_mDEB6625A68F1E46405A646ADE718B44D5D526D08,
	TitleStartSceneDirector_SceneChangeBackImageAnimationOff_mED3B183E119ED3D00393C1948693D3D32BB58D15,
	TitleStartSceneDirector_SceneChangeBackImageSetActiveOffInvoke_mA50FED84967A9FD5C7AA34DF473842B8C35CB731,
	TitleStartSceneDirector_StartAnimationTitle_mF9F49FC1EB1203F72013D2FB8DDFF8AAE6EF6582,
	TitleStartSceneDirector_TitleTouchStartButtonAnimation_mCF3B614BFD56ABE00B59080A008CCB28648635A8,
	TitleStartSceneDirector_TitleTextActiveOffAnimation_m51B828EE88D1EF5DB1702F17C4F77DB1739FCCDA,
	TitleStartSceneDirector_TitleButtonTextActiveOffAnimation_mDF4DD2474DF0422ADDAC08F7A7D2B1E49BEBB5CC,
	TitleStartSceneDirector_TitleUiTextsActiveOff_mFE90843E8396C8319B8D1B4BDF5F16A8C740E73C,
	TitleStartSceneDirector_MenuScreenActiveOn_m266A513DF12263CBCFB0E1482568380F9DAA6E49,
	TitleStartSceneDirector_MenuScreenActiveOnAnimation_m90A94DD783C1458946DE8636094160432374D68B,
	TitleStartSceneDirector_ButtonViewActiveAnimation_m0A17D47C03CDE3C36A6A39B353024027CCC5E525,
	TitleStartSceneDirector_ButtonViewActiveOffAnimation_mAC3CBDF67E113EA377EC069D7DF06D576EF1B971,
	TitleStartSceneDirector_ButtonViewActiveOffInvoke_m394F6827484BC0C615AF1E1348A34C3C1D0AD54E,
	TitleStartSceneDirector_ManualViewActiveAnimation_m1D34C6869FD1E91C332250750E02161759E6DBF3,
	TitleStartSceneDirector_RankingViewActiveAnimation_m5C87005B04288E96716DDF0ADB1862A3995565C9,
	TitleStartSceneDirector_CreditViewActiveAnimation_m9431D8B2F76155DD5D46F1FF6607244C959402B3,
	TitleStartSceneDirector_ManualViewActiveOffAnimation_m8516C8B2F43E3A912379ABE47CD2D5B836FD4AF0,
	TitleStartSceneDirector_RankingViewActiveOffAnimation_mDF1E3F580F77835791427695C646691E2088C6DB,
	TitleStartSceneDirector_CreditViewActiveOffAnimation_mC0540F04167244EB2C8A11954D79D509E1207107,
	TitleStartSceneDirector_ManualViewActiveOffInvoke_mAE7F48902FE39B950CE3C85E4004A9321740FE05,
	TitleStartSceneDirector_RankingViewActiveOffInvoke_m354EB914B46F92C4DFC4112974EE6A6C61901ACA,
	TitleStartSceneDirector_CreditViewActiveOffInvoke_m9EEDF46B82477EC6132CD4F78A65A314F705EDBD,
	TitleStartSceneDirector_GameStart_mD8C85A7C590D528583D0873CB0B45AB569E15C15,
	TitleStartSceneDirector_TutorialStart_m5F3A8F902A794FFB0BF639E5C1C8EE76B53BDC93,
	TitleStartSceneDirector__ctor_m255A16F654C4150FC2740A9CB9A4FAA21890FA46,
	VirtualCameraController_FixedUpdate_mFEECD20E60D80576A2FBD2A6DBCC27F940BDEEA9,
	VirtualCameraController_CameraRotate_m45A799D553D218BBAC3348AC953B9A00C8CAFA45,
	VirtualCameraController__ctor_mEA725BBD970EF7CA7D994407D08D0B3670163CBA,
	WarningUIActiveController_Start_mF40894D0E01D666420EDEA659E4F6C45DAD1C544,
	WarningUIActiveController_OnTriggerEnter_m2C838434C841E5D6344075DEAE766B0650A70CBB,
	WarningUIActiveController_WarningUIActiveSeting_mD56D7AF9897811266D39B3D11FCF54A73516CCEA,
	WarningUIActiveController_WarningTextOn_m9A177ABCE64E3F64E69BD8E85F9A7E9225478095,
	WarningUIActiveController_WarningBackImageOn_m23F59FB5C2EFDE1F0A3137C892660A84817BD006,
	WarningUIActiveController_WarningLightColorChange_m7E80CE539FB3AD0271F4327DD939A0EB0286275E,
	WarningUIActiveController_WarningLightColorResetInvoke_mC7BD9871143C0290B9E7874329A1306D393E428F,
	WarningUIActiveController_WarningSoundCall_mD580CEA4CA9186AFA83A6D6D7B0BC4900740E74A,
	WarningUIActiveController_WarningActionBGMStop_mE036EAA6C06B8C4C9568D1857E0FB72288934DD2,
	WarningUIActiveController_WarningExpression_m83A7FD41025821CCA9AAE3BB29961A447F66C5FF,
	WarningUIActiveController_DestroyInvoke_m2261E9752DBCCAB8FC7B4CB67AB3BCD5743DFBF8,
	WarningUIActiveController__ctor_m08F4637E2C53AAD00969A3D298A9CAC3D77E9760,
	DisableButton_Start_m2B1FFF17330B24D5BC57C5C19F198DFBEF76F349,
	DisableButton_Update_m081F994DA3F5C6FB1E5B2A6F3719B0CF4E031E8A,
	DisableButton_MenuOpen_m128A78E05D6E31100E3A821DD45C2EE957B2AAAB,
	DisableButton__ctor_m178210D4AF1A2204C5D1570CEFC05A608E83484C,
	getCurrentUser_Start_m9FA508F33B8EBCC550EF4D09B1AC3B610610BEEF,
	getCurrentUser_Update_m6BD344466351DBD2E93B1B6DAF319CDFBBACB59A,
	getCurrentUser__ctor_m4E1D19FC357C15A4253B9EFA1456897EDF7E6BBA,
	Loginsignin_Start_mE9C63E90D323F71FCEFE988FDCF7A16246731E40,
	Loginsignin_Update_m1EE3B56FFB1A8EB7A06FBFEA8E6644D588B232A9,
	Loginsignin_Login_m520C3F6350492187B027C45CD9F2CF4EF203A9C9,
	Loginsignin_Signin_m8990693CF8FCA8E4E39E24CFAA427A3644DC6A14,
	Loginsignin__ctor_m8987F07E41D4220F57F4D98AFDE774D58B5FA194,
	U3CU3Ec__DisplayClass5_0__ctor_m0A2F2C791278B1F967C58B3CCC4D37011752973C,
	U3CU3Ec__DisplayClass5_0_U3CLoginU3Eb__0_m0B740D5A4D91DA7B82D6A6ADAF97D3BA89C87E04,
	U3CU3Ec__DisplayClass6_0__ctor_m899021E4B110223C9193522E518958B3F65EFAAC,
	U3CU3Ec__DisplayClass6_0_U3CSigninU3Eb__0_m3462F5A9045A71C6D06746340C675972365D8A96,
	Logout_Start_mFA986CEA33CFD44C9BD985C0C53C7DA9866AEDBD,
	Logout_Update_mCFCA296C5DF26BFDF84F9141C94A179CD8A3BE72,
	Logout_Logout_user_m23236D4A8299E6E99FDFAE2F2DD2FD18C66ABF30,
	Logout__ctor_m1AE0F3AC41DD77395FFD1C8CB81E5562D885D24E,
	U3CU3Ec__cctor_m03C76596A8B2D2A96AE1A893A2BE6074FD4BE74E,
	U3CU3Ec__ctor_m941AE6BE4EEA062168CDDEF559ADF202EB1B58E1,
	U3CU3Ec_U3CLogout_userU3Eb__3_0_m02782F9B9A44100D2150FB92A7F14105B7CBD811,
	QuickStart_Start_mCD39E9E47B19707074D92CA7C4F0F1E787047527,
	QuickStart_Update_m147D7FA2903C25B5A6700B94D73D8CC872488C25,
	QuickStart__ctor_m0CC08B29D0E114574665902A4CE4E6C742BF35D8,
	RampAsset__ctor_m7CB8E98ADF762E7343D9A8AEC915FEA693ED9678,
	AdjustTimeScale_Start_m4A38ED5A991235FDD56B6F7444AC8677B55DC578,
	AdjustTimeScale_Update_m35850BFE55F334D789D2273391B993112BC1F2EE,
	AdjustTimeScale_OnApplicationQuit_mBF83E944940AE95B2542819682EBEAA7580EFDF1,
	AdjustTimeScale__ctor_m0D9D7BCA4710EF1B661F7648C598020E3483DCC6,
	ProximityActivate_Start_mBA100FA3ED59208F73FBE82E9584F9B1DE0450BB,
	ProximityActivate_IsTargetNear_m826C0D892D66C8B9229003E9E7A7BFC1DAC8195A,
	ProximityActivate_Update_m4A9769EE498773365306E9DCFFEFAFE0058A83DF,
	ProximityActivate__ctor_m47AA717E38E4A36C84CDFA7EFF72192658155DB0,
	SimpleCharacterMotor_Awake_m347826CC4184FB98988CC63028CFA1736D438135,
	SimpleCharacterMotor_Update_mA5966DE9D18E3A51D7E547A1DB3D6D668CA2199B,
	SimpleCharacterMotor_UpdateLookRotation_m7A7D395E0D03FFDAF115286506F06BCED9C33E10,
	SimpleCharacterMotor_UpdateTranslation_m73972A962820E750E7A5DA7AEF8BE1F8C512C6B1,
	SimpleCharacterMotor__ctor_mDF39162F0CD433D75D7AF488911B5865F0620217,
	Readme__ctor_m69C325C4C171DCB0312B646A9034AA91EA8C39C6,
	Section__ctor_m5F732533E4DFC0167D965E5F5DB332E46055399B,
	ActiveStateToggler_ToggleActive_m2197869C55A5EA5BF4EF8800EDED32049450ADD7,
	ActiveStateToggler__ctor_m04F37578B0B887F86DF0AFE4BCE2AA241E9B027F,
	ApplicationManager_Quit_mC3416C8A1235ADDC9C0FFE0C51DF77A54E46919D,
	ApplicationManager__ctor_m9728432FD921373C75D5AD1BB10C1C7BB926E45F,
	ChangeColor_OnEnable_mB6645B0A82763933A04A82D7D1ED6D04F5BAEC08,
	ChangeColor_SetRed_m9371FE7C53A785B2CB67490E1FDB4923E6519DF4,
	ChangeColor_SetGreen_mB8384D5AD9E294DCB5A66AA985B1AE2A588CB1A5,
	ChangeColor_SetBlue_m1FEAA20434D23F2402B4FD09BC633C96E773CBD0,
	ChangeColor_OnValueChanged_m55D60C6063192F109C7F266163FE2D1536D848DD,
	ChangeColor_OnPointerClick_mA5792AD472A3B58C78C738E569BBD9F9DC7973BF,
	ChangeColor__ctor_m8BB1B19B1A2961B218222DF5C104ECAE825C8EA1,
	DragMe_OnBeginDrag_m9CAC75435F1132F97C511BC730463EF0884BCE9D,
	DragMe_OnDrag_mA90A49CBDFE2E55BF4408EEF4233847A5AB6E6A2,
	DragMe_SetDraggedPosition_m7ACCE7A1453C634A5D85E1B24D68F979E8FAB536,
	DragMe_OnEndDrag_m42FC355BFB6D84BAD1D5ED7CC2FF06A6B956351A,
	NULL,
	DragMe__ctor_m1A1D3514EBA48EFBCC7EF774684D3D7B14A1790F,
	DragPanel_Awake_m976D47375F0074B0A964AC06B1E7765D57D029F1,
	DragPanel_OnPointerDown_mEB286E46EC39E5DC1FD7F0E7678B427EB92DECDB,
	DragPanel_OnDrag_mA93336E5A4F6C0881550A04F888947A9254A04AB,
	DragPanel_ClampToWindow_m2EA7DC5AF57F644BB5A256AAF714F7C85C763849,
	DragPanel__ctor_mD24BB7D4C1206A3912DF7036A657E6734A2B45BC,
	DropMe_OnEnable_m9C62AF35ABE6569D778E35BE78C0B0DCBFED9B59,
	DropMe_OnDrop_mA70777C2048F48E83B792112F7E1D2C6BB50DDD3,
	DropMe_OnPointerEnter_m4B4D3573925CB30440E8AF7EF832828641CBBB87,
	DropMe_OnPointerExit_mD29997A217A73EB182671FD9116DBC9E3A91B929,
	DropMe_GetDropSprite_m6978787E3015377193F8817EC1FEB79F5EE865A4,
	DropMe__ctor_mC7C9F0820A003F55CEE08BF74E17D5CE25823D89,
	PanelManager_OnEnable_m5A7DC3931168B59A585EAEFA1AB80F7D3EE8140F,
	PanelManager_OpenPanel_m456B82F7B62A00937F60CE6014C1F3698414B7B4,
	PanelManager_FindFirstEnabledSelectable_m5D4FC3073E774F4185C7992683643F28ECB1E4A8,
	PanelManager_CloseCurrent_m824C6139CDBD415DE8E4CF1FB557FCE79D69B2C7,
	PanelManager_DisablePanelDeleyed_m1DF8E917EEC8FD14E76954ED3F2918C76B65E5AF,
	PanelManager_SetSelected_m59DAF072CE220595B0AD4BC0BEA1C00856943B71,
	PanelManager__ctor_mA86BE5A7EC580FAD466DFEC12569D38F584473BB,
	U3CDisablePanelDeleyedU3Ed__10__ctor_m4282B9D12EB2BB6D413E69F116601539D4A98B99,
	U3CDisablePanelDeleyedU3Ed__10_System_IDisposable_Dispose_mA29BD3E3DE14A63123458C1B873D3B9520CCD0E7,
	U3CDisablePanelDeleyedU3Ed__10_MoveNext_mDCC2DE4CAA28ED5E8135119B9A4E38D075FEC235,
	U3CDisablePanelDeleyedU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA0AE880F4D89D9DC62386E0448E6FE576A4AE66,
	U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_Reset_m17342096F195588A8C598895C595B0B029F598C9,
	U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_get_Current_m47973E705B6316F9392B95B9A84ED6431CF8A0F0,
	ResizePanel_Awake_m177281E00557C53FA59B3E8539414DF517AEC186,
	ResizePanel_OnPointerDown_m9017A7FCE182034153CAB88EF3D00B8A69577082,
	ResizePanel_OnDrag_mF4950C026DD520F4CFB4A885544EBC678F1CC6A1,
	ResizePanel__ctor_m11631FA6774A81F988DAE9E3B6F3332131EC8771,
	ScrollDetailTexture_OnEnable_m2B3F092169EE265F93ADCDF198F956DF4E76E5D9,
	ScrollDetailTexture_OnDisable_mB680BF76E8EB673F455346FED420D63B42AA1496,
	ScrollDetailTexture_Update_m5D6FEC73519F9A23B55BD361E25F2C8E2244EABA,
	ScrollDetailTexture__ctor_m6210C8CA4C1EC2B032630E58B9C3ED5A9B4FC8F3,
	ShowSliderValue_UpdateLabel_mE0D1CCC1F3E8A56D953BB1C03CF4793A0563BD5D,
	ShowSliderValue__ctor_mCEEB373DCD42E0CDFACBCDDBE60A2C6F4337561C,
	TiltWindow_Start_m117F45F2AF4A2023B8D2438ECABE9649F66AACEA,
	TiltWindow_Update_m8B983CF008A896D2A99DEF6C2271BB4A418DCA03,
	TiltWindow__ctor_m998EDCD27C0C488CB5469BDB8439F0F41293BA64,
	SimpleCameraController_OnEnable_mD5E060BED4E0C5C0D40AA39C0DA7AE44CAAB217D,
	SimpleCameraController_GetInputTranslationDirection_m62C42A7966EF22CB95BB847225D2BE2441A3CE0C,
	SimpleCameraController_Update_mDB9A4DA6CC1C82A704D3575DC196F44F17E5E0B5,
	SimpleCameraController__ctor_m224B705F449A3DA440346B08A6237479A91D7136,
	CameraState_SetFromTransform_m3A7302F4586F37F7C78EDA2F0C476BAA22672360,
	CameraState_Translate_m561F6D92E99F13444FBAC8D5B1A40762A26219F9,
	CameraState_LerpTowards_mFDC0E1CA19B6791CDB6EE8AC262944DC1143FCF9,
	CameraState_UpdateTransform_m03320D760E52E809272776B58C7E741B2EE1D73E,
	CameraState__ctor_m33568E59E39C953BDE6CD28D35A34BE52AE25504,
	Json_Serialize_m10335BD31272294C425D0A19223D3CF5643F2D84,
	Serializer__ctor_m4F44C46231E6E5AE64E5A54E7C2BD9F34EDA7821,
	Serializer_Serialize_m59BD9118C0D72805416DB6E86D6D7B5879B567DC,
	Serializer_SerializeValue_m8C2F65A142866FADF38B93D9E629B2E7E2524AFF,
	Serializer_SerializeObject_m1A05723BFB5B50CC626C68BF09D58306CFCA5E07,
	Serializer_SerializeArray_mEF7CAD420779566B151D436F9D981B296498ED3E,
	Serializer_SerializeString_m042FDB4D9299515A034B701CA404E7D6648D7E0A,
	Serializer_SerializeOther_m69BC1350909211DF9AC2F73650A54BC735640EF2,
	Json_Deserialize_m20B6C20404B17D91C466790EDD4DFD5B687C2BB3,
	Json_Serialize_mB6F04E24EEDCE8B987E123C8EB4CB39B0DE0A338,
	Parser_IsWordBreak_m76B748761F3D3C4CECCD0B7CF3A9A6A8082ADF4A,
	Parser__ctor_m5F80E026C9D94E94EF918063572F0ACE937D6E62,
	Parser_Parse_m048E00F05C588343625E4A1CD3D62038E17722F1,
	Parser_Dispose_m912B7F3C99DBEE37A3E5EA2C5F866DE2AFB7FA75,
	Parser_ParseObject_mCE27A97DD550BE01B8F50E0B5354A2FCF5B43349,
	Parser_ParseArray_mFB0A61F43183970F6D05E94BDF8860371C715591,
	Parser_ParseValue_mA4C23822F44634EA0944A828112D706B98AB0C61,
	Parser_ParseByToken_mD062F21DCF05ACF7D4939F0D5D4C9D214ED15D0F,
	Parser_ParseString_m875C2B5594D3C6A50C8166D7444A1EED5372CF85,
	Parser_ParseNumber_mFB791DAA1FC9AEABAA6E8CD067617AEE2CDE9512,
	Parser_EatWhitespace_m912C05CB24F4164C96C7E431DF942EB1356285C7,
	Parser_get_PeekChar_mA1EAD1C91D69B29C281D03976BDCD15E2B4411E5,
	Parser_get_NextChar_m800FCEF9B0C72DC84F739DBAC8CC12864D1AA430,
	Parser_get_NextWord_mA4DBE6A91BA864C8F7693937F89209E2E7D0220A,
	Parser_get_NextToken_m34C299688A8015C06F6BB7068C84D6FE7A68A747,
	Serializer__ctor_m9DA8616EA7E0284CA21D5B6665D7798875D52A29,
	Serializer_Serialize_m4BE25E76C9FAEE7FF83100AD8F13DC2349251887,
	Serializer_SerializeValue_m3B2D1698CC74A23CF8E1DE1829676590823459DC,
	Serializer_SerializeObject_m8B31489C19418217A903E21A210D76F370334FF3,
	Serializer_SerializeArray_m61D80EEDA84A38B6DBF53609B88CA57B00C77FE1,
	Serializer_SerializeString_m3195D253DADD72083860BC3A85E82DDFEC958BA7,
	Serializer_SerializeOther_mE8E1FF5662A69CBB95590F57A3EAB23CBCBDC711,
	MimeTypeMap_GetMimeType_m917DA67F189D7738B7A3F5EF034C70E04623341E,
	MimeTypeMap_GetExtension_mDC445D1B11E667BB03DDFDAC0DB59FDA50FA35F5,
	MimeTypeMap__cctor_m8B0A8D555BD31EEACD30F559C238E20139D5D68A,
	NCMBManager_Awake_m0545F9C99C9E25357E9462AFE3638765FE5BD971,
	NCMBManager_getInstallationProperty_mF515922339455EE566D2684A73597708391A5088,
	NCMBManager_get_Inited_m86C2501C0B3F2CECEC40589B36B8471FD045170C,
	NCMBManager_set_Inited_mDA843E664555EB9F1DE2E6D381F51E032B1DE99C,
	NCMBManager_OnRegistration_mD34A73AD1DAB870F20E7A914E5192642911A0158,
	NCMBManager_OnNotificationReceived_m84CD92F238E4904919C14015BA6B32EF79D4734D,
	NCMBManager_Start_mF9E30946E99C766B107F2BDFF2F6A57DD34C5B6D,
	NCMBManager_Update_m70DF5B5BC3F5E1618C479D9108BDECD2528ED479,
	NCMBManager_ProcessNotification_mFCA1FB3EC8957C56B76831A6A98CB7913ECD908B,
	NCMBManager_OnApplicationPause_m3DBFC5B4D0975FB7C60ED4E0E67975A84B7047A3,
	NCMBManager_ClearAfterOneFrame_mAD8EE587ED481862E225463FD353E504C4C921F6,
	NCMBManager_IEClearAfterAFrame_mB5A17325B392BBE723225B02A1318C7E31997214,
	NCMBManager_SearchPath_mE1FB6BE15F5F24B63EA028F0926DC61E02FD01A0,
	NCMBManager_onTokenReceived_m4E79BDA7D5BD3B31CE793A858F0049C24716AA40,
	NCMBManager_updateExistedInstallation_mFA3A06ACC84ECCE18CFA4E46BA4DEA25CB2BA62E,
	NCMBManager_SaveFile_m9A999F0FCC710D04B5A650087858811FA983EE94,
	NCMBManager_ReadFile_m76CA63E4977A87918F264AAB1BD2A3F259F34B35,
	NCMBManager_onAnalyticsReceived_mB5BF99617E8FECF44A6A725B67F97ACA56FD2599,
	NCMBManager_DeleteCurrentInstallation_m6694256AE1651559BC40E55E064DE776974F5D81,
	NCMBManager_GetCurrentInstallation_m1B63F70AEBABE2043F3B44EEE0009C085D36CE07,
	NCMBManager_CreateInstallationProperty_mCB3958FDC506728207606EEF54922C10C20A651D,
	NCMBManager__ctor_m455300A96EC9139ABD0751174BC3DF1A5AEC616C,
	NCMBManager__cctor_m754B06A08304A5BD68E019907C09D5E46C6125F9,
	OnRegistrationDelegate__ctor_m30B7DF282D381ED20979A4AE8C2FF756971F8E2D,
	OnRegistrationDelegate_Invoke_m7217BAEF8819E24AE59A241ED57C82AF93B3D554,
	OnRegistrationDelegate_BeginInvoke_m1A4C34224B23391D352F953527DA917C04853CAE,
	OnRegistrationDelegate_EndInvoke_mC3431A006E23A4566046CE283478785D3C42A8A7,
	OnNotificationReceivedDelegate__ctor_m01EEDBF537E4F040C8AA7D6AD4CF0FC92DB4BAA8,
	OnNotificationReceivedDelegate_Invoke_mC9B673A9E7FB68450B4609568DD4DF891A49F6F6,
	OnNotificationReceivedDelegate_BeginInvoke_m4995C83BFCB93C33FDFBB6AD52A6960E3B60D688,
	OnNotificationReceivedDelegate_EndInvoke_m9010C4A76735231ED511749A511DC7A9029C146D,
	U3CIEClearAfterAFrameU3Ed__20__ctor_m59C637F2538F3789FF648B9275D04844CE737B05,
	U3CIEClearAfterAFrameU3Ed__20_System_IDisposable_Dispose_m547B748932BF56073A1DB5A80EBE3145E65B2B70,
	U3CIEClearAfterAFrameU3Ed__20_MoveNext_m4320DA4674E0A0DB08666166964008A208214922,
	U3CIEClearAfterAFrameU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m27130876CCCCC32894D0478A2398DD02302438BA,
	U3CIEClearAfterAFrameU3Ed__20_System_Collections_IEnumerator_Reset_m856C3563523E5F18B5B2FF333110F58EA8AF6009,
	U3CIEClearAfterAFrameU3Ed__20_System_Collections_IEnumerator_get_Current_mF65A8DE40F41A6D87D332F338FD63BFBA279E401,
	U3CU3Ec__DisplayClass22_0__ctor_mD12AD87DAAE6903B3EE70A6B76767683C2D3CBCD,
	U3CU3Ec__DisplayClass22_0_U3ConTokenReceivedU3Eb__0_m130ECAF61B073A73D8B070D30496CCBEC92DBAD7,
	U3CU3Ec__DisplayClass22_0_U3ConTokenReceivedU3Eb__1_m827ECE875164B0DEC7123039D27B31144E0B7362,
	U3CU3Ec__DisplayClass23_0__ctor_m078426C8B417A3BBE97D9C74673A4D8DB9CB7199,
	U3CU3Ec__DisplayClass23_0_U3CupdateExistedInstallationU3Eb__0_m27D75ACA55829744936FA3B93F3745199086AC72,
	U3CU3Ec__DisplayClass23_0_U3CupdateExistedInstallationU3Eb__1_m3BF22B72A2DC91427DA80B703A1733BCE330CA80,
	U3CU3Ec__DisplayClass23_0_U3CupdateExistedInstallationU3Eb__2_m6866011D293A0D24ABE29453F1994CDF68F22634,
	NCMBPushPayload_get_PushId_m6C8366C8B6DA6DC5080B7623B925A8352E0AB187,
	NCMBPushPayload_set_PushId_m0E40B6F21765988140BC2DF34D15BDC0464DD862,
	NCMBPushPayload_get_Data_m547F94E98D06B9FE6C8C3C65F42B2C9634C5D344,
	NCMBPushPayload_set_Data_m4EC2EACC3AD6409A1EF879FFA7DEDB21DBA02956,
	NCMBPushPayload_get_Title_m106CDCA861680F455B6543B27F86F7DC7ECE2C77,
	NCMBPushPayload_set_Title_m05F5300493676A11DBDDB05EB77A8437F104C238,
	NCMBPushPayload_get_Message_mC588BB92EEE646A5BF914985088F232AD8D5985F,
	NCMBPushPayload_set_Message_m6F897F2A6C26F32B8C6C4BEF1632DCB4EC9DB5DE,
	NCMBPushPayload_get_Channel_mB2344E2E7486E1E2F9D7B20D01305AE0BC454862,
	NCMBPushPayload_set_Channel_mEB1AD84E9DCC044B4B26C63DA2C540CABB36FE88,
	NCMBPushPayload_get_Dialog_m8713DC3D712D02A3C65A7EA22DAE2C567B3ED073,
	NCMBPushPayload_set_Dialog_m459B4DF0145A6BB0236C68DC6E1DC4F37E493409,
	NCMBPushPayload_get_RichUrl_mCD464E03E0EA9A886DBA0B033D116BFD1415348A,
	NCMBPushPayload_set_RichUrl_m87750C06EB32974F9ADB68740F1C39A907F295A5,
	NCMBPushPayload_get_UserInfo_mBFA7706CE8E7BFC1DE7B14290B2A77FF8892006C,
	NCMBPushPayload_set_UserInfo_m37853C3B69FA6A1B3F4D735C90D6D80F12B3670A,
	NCMBPushPayload__ctor_mFB30C18E4367BA4BFE77A9B77818B95ADB53F0F0,
	NCMBSettings_get_CurrentUser_m2421EA6E1C07C79856D4305483B351B7A719050F,
	NCMBSettings_set_CurrentUser_m771F1D8692D7E204F1CA5F4D746EB6A706B7329C,
	NCMBSettings_get_ApplicationKey_m6B66031BE0C96D1AE12430B11DE9FB76ACE4D424,
	NCMBSettings_set_ApplicationKey_m9915FB84868BBDBADF6D2DAFE0375029F4D5F81A,
	NCMBSettings_get_ClientKey_m846CF1A8BA9F3E4DCFD2A1CA928F4DE89B462236,
	NCMBSettings_set_ClientKey_m30BF2E8931C98542EB48ACA85EACC1AF568FEECD,
	NCMBSettings_get_UsePush_mB4A498FB7121980BAD687F10FF9598CA4180344B,
	NCMBSettings_get_UseAnalytics_m9DC32E9B0650B02DD6D0F8F78212BEB3CCDC0D52,
	NCMBSettings_get_DomainURL_mFAC12515311D74DC8CE6214328BD3AF469D8B947,
	NCMBSettings_set_DomainURL_m35B16CC0C0E3569127D035AD050349267074A563,
	NCMBSettings_get_APIVersion_mC944ACE77235ABBE288B66FBED85322832787465,
	NCMBSettings_set_APIVersion_m54576CFF9690582028980E7848E34302A381A318,
	NCMBSettings__ctor_m8D9992539BA06C478FDFDCCC28D16DB5F0244505,
	NCMBSettings_Initialize_mAA740CF228F46F3702C22AA15F5340A2E193430F,
	NCMBSettings_RegisterPush_m3E9789C7508041849667E7196256E34C988E7244,
	NCMBSettings_EnableResponseValidation_m689FFD3548FF9DC9E0773E127E6D6C9349D454B6,
	NCMBSettings_Awake_m84F56E50C56FFE8CC99A9574F9DC7D6646BA11F5,
	NCMBSettings_Connection_m72C1D8EB449150397ABC3270CE73A59AB18B923F,
	NCMBSettings__cctor_mD252AC13AD9E054FC65B00209AA2AD787F304552,
	NamespaceDoc__ctor_m422F291E3147DAC8BAB81F06C19E631E01C950A2,
	NCMBACL_set_PublicReadAccess_m8B35B2A89565AD7F240BB49D07740B342E75A1E8,
	NCMBACL_get_PublicReadAccess_m8D4FB1A9FA2B995D012334CA8D52D7D763698F2C,
	NCMBACL_set_PublicWriteAccess_mB9A3980D4EBE5D0B6C1D8FCD38A2A7161A1AE266,
	NCMBACL_get_PublicWriteAccess_mCF84699A53B979E3326EA123801F9DAC9176019C,
	NCMBACL__ctor_m4F9864F6F555A26D9E6E0E0FF1D90ACBBA9265B1,
	NCMBACL__ctor_mE565EF266656EEA7C5B0C869136DBEA9329F33A3,
	NCMBACL__isShared_m4A0E49EFBF794992096EB1203C3C4B198A27A1FF,
	NCMBACL__setShared_mD1F1231EAC1BAAF4C6C3ECA0162DAC0113A65552,
	NCMBACL__copy_m480ACAD57140B9078C927B2A0CC1F19637B92536,
	NCMBACL_SetReadAccess_m072167EABB5E6D42E821099986A6CCC8851435C5,
	NCMBACL_SetWriteAccess_mF550E5540A9F61E98BFEA66A35E8B4EEC34865BB,
	NCMBACL_SetRoleReadAccess_mC97584DAD0B78386939E8C491507B6E1FFFA3C6C,
	NCMBACL_SetRoleWriteAccess_m8191B1A370B459DA93775E8BCC86D1DDC504F695,
	NCMBACL_SetDefaultACL_m41E2DF0358C8E7BB16AF538FE6C3487A980D72B7,
	NCMBACL__setAccess_m761D10698ED68E7ECCA18116D20FA12C664F691C,
	NCMBACL_GetReadAccess_m618DF123CEE2B4E261EC9B783E6E35D484EBDE1E,
	NCMBACL_GetWriteAccess_m38926A3C669B3CCBF8068F89571E1E0E23DEFD1A,
	NCMBACL_GetRoleReadAccess_mB40F402AD1D29D338D5E810519F754DE49DF6AC6,
	NCMBACL_GetRoleWriteAccess_m611FB47248C07B674EC7CCC94FDA083635E337AB,
	NCMBACL__getDefaultACL_mC725B1AB3B47CF097568F4F5FE9C2F11ADB83F1D,
	NCMBACL__getAccess_m5ED3C918C7FECFD37E12072071997CC60B202A66,
	NCMBACL__toJSONObject_m2DB75A7B996417C0582E7D8C8D4A296884B2EDB5,
	NCMBACL__createACLFromJSONObject_m1B7CAEA2B87973FF78F15C9D3EB5DBE5A8BC4F9E,
	NCMBAnalytics_TrackAppOpened_m49B474410DE8CDE0102EAB41C1F98937A78A2F52,
	NCMBAnalytics__ctor_m6F6B5015194A94F38363F4C958F6C76ECA97AC56,
	NCMBAnalytics__getBaseUrl_m27DCE531317FF7F887DB5B05C16F2D011A75FD54,
	U3CU3Ec__cctor_m5F87C7E3C2220D662678F0CFEFBEFB4948E7546E,
	U3CU3Ec__ctor_m51E42CF0A8EF9079970E7A845F7D8C9ECC9ADE5E,
	U3CU3Ec_U3CTrackAppOpenedU3Eb__0_0_m03259F1605286A4382413DA743052C8C697AE66D,
	NCMBAppleAuthenManager__ctor_mFF418FE13C0F0F001633C17E9AF1173AD8971DEF,
	NCMBAppleAuthenManager_NCMBiOSNativeLoginWithAppleId_mA80EFA350AE8DB1A5BA2F05519DBE8E1C2298EA5,
	NCMBAppleAuthenManager_Update_mDE3138290E4C481BC102B4052E0D4D0A8751394E,
	ControlCallbackAction_QueueCallback_mE957469965112D8FE512CC0F91377A10C3301FE2,
	ControlCallbackAction_ExecuteCallbacks_mC94EA2F15909593AA6C92146C619CDD41B42FD89,
	ControlCallbackAction_AddCallback_mFEEFA1D0435F3BA2D0AA14222E2D233AFA9E8D9A,
	ControlCallbackAction__cctor_m63677727F985DB49573552C15B4634FD79534AD6,
	ActionEntry__ctor_mA6146DD23DD6610758CC86BE0F07674F282BDA5B,
	U3CU3Ec__DisplayClass5_0__ctor_m791BA098F1B2515410BD8DB9C5530BF6929F64D5,
	U3CU3Ec__DisplayClass5_0_U3CQueueCallbackU3Eb__0_m63CDBF5042BF4FCDCFAD5F8FFD459F314A63CD5D,
	ExecuteInvoke_HandlerCallback_mE2BC7E5E60255E8A4AC9D08FA99C7F91A67B697B,
	ExecuteInvoke_NCMBAppleAuth_HandlerCallback_m4714AE5F953F4E2645C05C6BA426169BC5D78507,
	ExecuteInvoke_NCMBAppleAuth_LoginWithAppleId_m70BA84C11B0B79A1926456BB644A12820BD2545E,
	CallbackDelegate__ctor_m83B45B3C8634F79817C8844F1686EF6E48CFAA94,
	CallbackDelegate_Invoke_m09873191CE2C619C824410479BCA625916DFF629,
	CallbackDelegate_BeginInvoke_m80648916C6CEB96271CDE66FC296E03639414B46,
	CallbackDelegate_EndInvoke_mDA135D96B653A44640A5E64D967F82DC41996318,
	U3CU3Ec__DisplayClass1_0__ctor_mB44137FA2673E5431176CED2DC85E18A502C5614,
	U3CU3Ec__DisplayClass1_0_U3CNCMBiOSNativeLoginWithAppleIdU3Eb__0_m676DEC7DA5747CCF901397C0F80802608381066A,
	NCMBAppleResponse_get_Error_mDBEB3789E90EEBF2E30CD32B6026F17C577CD97A,
	NCMBAppleResponse_get_NCMBAppleCredential_mAD29DBF58C42E7834951FAF699616BDD6A7A48C8,
	NCMBAppleResponse_OnBeforeSerialize_m85739EC23DC8A1C5BE995442616685F676D4C48C,
	NCMBAppleResponse_OnAfterDeserialize_m823680B3A35E08E9D87978FF0C22E77B17F15D84,
	NCMBAppleResponse__ctor_m427F8D8ECE736C45F3674637E0854A05D625B4DD,
	NCMBAppleError_get_Code_mC3FDD0F8747C9204D6B1CEB6072A926662C33B2E,
	NCMBAppleError_get_Domain_m4AFFEFE77FAD72D836734D9C65829ECEC1755E41,
	NCMBAppleError_get_UserInfo_m173FD24109F35483E74E01D64659DC11C50FC692,
	NCMBAppleError__ctor_mAB9F09D67631852E1902DC2A4BEDE83B0F99B7F6,
	NCMBAppleCredential_get_AuthorizationCode_mBD617EE6721DF35E784055FFEA2B270B136FE36C,
	NCMBAppleCredential_get_UserId_m902EE44017312E65B31DD98D312175D0633F8D07,
	NCMBAppleCredential__ctor_m3EF47276DE03C971E39AF5790341DF0D74B18B4E,
	NCMBAppleParameters__ctor_mE57498262850F4954D364986FF7903B614606EF5,
	NCMBCallback__ctor_m9411539EB24F21D6D798DCF4D1E3356534B07285,
	NCMBCallback_Invoke_mD4DCB39745A1D6C1B8DC6D34A291FBD197A31422,
	NCMBCallback_BeginInvoke_m531688FFA7CD4C1971048DB136E60ACDD54A4D1C,
	NCMBCallback_EndInvoke_mA383185C6A1B22554B8909D498006A4309E98F14,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NCMBCountCallback__ctor_m66075D0C5E70F1FF7CD348FC030FFE2BC88BDD70,
	NCMBCountCallback_Invoke_m38BE2B9D44D3F193D201007DBDC95A1138CBEDBF,
	NCMBCountCallback_BeginInvoke_m3AA632F5A301D096E31ACEEC9415FA46202CDE7A,
	NCMBCountCallback_EndInvoke_m654012BBE84FAB35850DE9CBA935796692573990,
	HttpClientCallback__ctor_m26AB8050FFCDE3AB0390DD3C088724759EBFEA5D,
	HttpClientCallback_Invoke_m1BD856418EB645DE35D5674D2A5C33DE9319EDCA,
	HttpClientCallback_BeginInvoke_m8FD05EA3835F80D5EE533CDF47C6A2C34A4F8555,
	HttpClientCallback_EndInvoke_mF89E6F6A0A19240E32549F89D92D6021B96E934D,
	HttpClientFileDataCallback__ctor_m6328725BEBF311BD1C2588DA50AE2FF7950116D1,
	HttpClientFileDataCallback_Invoke_mD2A545E2E8F1B14CA0FAB6459ED7DBB24A48DCB6,
	HttpClientFileDataCallback_BeginInvoke_m0D109F1C4B34AD5942B62E9582C93152FD80C6E5,
	HttpClientFileDataCallback_EndInvoke_m64FE22A80399BCA7D37E124B594993F0B15E5FCD,
	NCMBExecuteScriptCallback__ctor_mF5D237F4D97E56B4846E70E4CA9FADC6C5475D26,
	NCMBExecuteScriptCallback_Invoke_m14F5DDD76388A19993103DF30D97665E7796351C,
	NCMBExecuteScriptCallback_BeginInvoke_mC3FAF22D51F6688D19180F37ABBDB462CCB4AA85,
	NCMBExecuteScriptCallback_EndInvoke_m9B9892E9A1DE0E71E0658A1DFB707167A8939BAB,
	NCMBGetFileCallback__ctor_m2ABF1C11926AC1066378C2249DA18E4A0CC5B45A,
	NCMBGetFileCallback_Invoke_mA71E3A031CBFCCAEDAC3253F97D3674C0F8031A1,
	NCMBGetFileCallback_BeginInvoke_mA934ED77E736D2D54A7C343D3A1037C1E71C9F94,
	NCMBGetFileCallback_EndInvoke_m5A08E829B3F72F5FAE9CD33683B1C4CA93F30376,
	NCMBGetTokenCallback__ctor_mAE865062B4FC5E2C51B545D8F192C6176967135C,
	NCMBGetTokenCallback_Invoke_mE59F0A2342E69E7562A5804D870E00ED5F8F73F4,
	NCMBGetTokenCallback_BeginInvoke_m1DF176BCDA35CA20797E1B4830AC5D4DF6E3CADC,
	NCMBGetTokenCallback_EndInvoke_mB3A533D3E7E744479D5BFAB8DAA2B8E227774AB6,
	NCMBException__ctor_mE63D0676BB0DA9A598FB7D11FC9962E17752D393,
	NCMBException__ctor_m21B151AFA272FA1EEB0172A4BCA661F376516CAA,
	NCMBException__ctor_m54ACFC3C3A2A9512B6E9DB84475D8190CC6EA039,
	NCMBException_get_ErrorCode_m66EA51AD2E678D6A99419964C21BAE5BCE54825E,
	NCMBException_set_ErrorCode_m83B719C6614311B50FE22D79FDB000E0DD6C5935,
	NCMBException_get_ErrorMessage_m9CC71E7018075963359CEB1C604DF21EAFE93DAC,
	NCMBException_set_ErrorMessage_m9B884AC5AA5741593677250F46F8E544BD3737D0,
	NCMBException_get_Message_m4C179B9ACA7B56CB2BF24605A4DD8B87948EE209,
	NCMBException__cctor_m566F6A944909B517151749379D7C14226EEA748B,
	NCMBFacebookParameters__ctor_mE2498257B49443BEB80FE2D010420229398C8C2F,
	NCMBFile_get_FileName_m60720A28CC042D9B92F4B6CB0B980CCBCDE55FA6,
	NCMBFile_set_FileName_mE6CD516F7AC47A09B2F99CA722BF0F06934E37E5,
	NCMBFile_get_FileData_m1031293D72D378CA49A3D3D1DD88DD1572DAE457,
	NCMBFile_set_FileData_m4421A5A0F91431FF8068903536CE3BCCAAFA68EC,
	NCMBFile__ctor_mE77907F9EC202A9507A099B99D7E1B2120A3F74C,
	NCMBFile__ctor_m3C83C75457D55474261AE1CB074B175A8CE21818,
	NCMBFile__ctor_m5774765EC9B541C4739D48546DB8B3D6EC8B34C0,
	NCMBFile__ctor_m8194EECFDF1B454FDC7EB1D1A06DF041436160ED,
	NCMBFile_SaveAsync_mDACD4E6057DA344E214C36E2CDF89B6A7B39C863,
	NCMBFile_SaveAsync_mD19BB36E3C8B0676B99B78C91776D85C5B02E2B4,
	NCMBFile_FetchAsync_m968BD3FA13BD714FAB07FD15864501FAB686DAD3,
	NCMBFile_FetchAsync_m8DA8B970667A9065BDF444A37E2BDE498884292D,
	NCMBFile_GetQuery_mE50CC819115879642D1C88D340FA09C9BCD12925,
	NCMBFile__getBaseUrl_m7DAAA5DC181126807314B15315A1FA61455740D4,
	U3CU3Ec__DisplayClass10_0__ctor_m6741190E00554D5D661D1C2495B62BE46ED9CF50,
	U3CU3Ec__DisplayClass10_0_U3CSaveAsyncU3Eb__0_m60473A0C1B75AA3000F7D791FAEB812480AE1DBD,
	U3CU3Ec__DisplayClass12_0__ctor_m93ED7546ECA11FADBCFA5D0D08F68691A768CEDB,
	U3CU3Ec__DisplayClass12_0_U3CFetchAsyncU3Eb__0_mC30E0FB59429A3BC9F96BF438021EC83CF6140AA,
	NCMBGeoPoint_get_Latitude_mA701AB3DD72C66ECC54B564763553EF8BD638B67,
	NCMBGeoPoint_set_Latitude_mF9CE6978C04BF38FC99A273EB2C25928180FF106,
	NCMBGeoPoint_get_Longitude_m39ADA9F8A394B404F134702D649B1F28BCFF9231,
	NCMBGeoPoint_set_Longitude_mFCB3A006D32D628A7B917F5540EE5678BA7DF935,
	NCMBGeoPoint__ctor_m5E7A2D8E13F647E3D341711C8C5DE64AE20EAB14,
	NCMBInstallation_setDefaultProperty_mBB17C7B4FF9165835D2E60D54685984145BA037B,
	NCMBInstallation__ctor_mBD35C983AFEEBA45C40B4EA968DC372FA5A28B82,
	NCMBInstallation__ctor_mBB13EA853E276A8253E9629B4AA6D4042BCBF499,
	NCMBInstallation_get_ApplicationName_mE119702B0B32B4E1E715D76E402E55BAE10B8833,
	NCMBInstallation_set_ApplicationName_mF72BEAC02D8FB65C95B1B26DA817BE85D594E59F,
	NCMBInstallation_get_AppVersion_m552509DB2656BFE7820DBAFF816516482CD9474A,
	NCMBInstallation_set_AppVersion_mC966B93BBBD528DB647DFBD18BF3900485B5CE09,
	NCMBInstallation_set_DeviceToken_mB84D08E5C5FF63760A8F0AAB3B9A81CAA4F17B3C,
	NCMBInstallation_GetDeviceToken_mCA79AF6304BAFD9C6BDFA545287EB39EF4798DB3,
	NCMBInstallation_get_DeviceType_m2A8903F151337C0449860EBCE4984722C4E116AD,
	NCMBInstallation_set_DeviceType_m3F23717B727BF83FE98278F190E743DE2B6CA142,
	NCMBInstallation_get_SdkVersion_mDB726558EB94F5D7F8ED25D75A0AA0DA17B818A0,
	NCMBInstallation_set_SdkVersion_mEFC673B205EFF82447CF184114494A0A8314C60F,
	NCMBInstallation_get_TimeZone_mF8103121D1627BFD836324ECE9284D70B0CA9754,
	NCMBInstallation_set_TimeZone_mE41BC12E8786E4459EB981F9FDC4898FC411D1B6,
	NCMBInstallation_getCurrentInstallation_m2E694FD700A511C4105F89A45D06941DB18E18C3,
	NCMBInstallation_GetQuery_m1545F8ED4F97D658F7E3FC963B6B08856022AE41,
	NCMBInstallation__getBaseUrl_mA2DD397800ED7BE8851A3772CDE2485983E7573B,
	NCMBInstallation__afterSave_m54F98084EB48774C4561546AF52EDA0E6B4B7F9C,
	NCMBInstallation__saveInstallationToDisk_mB732D2DC117122373AEDF00F1CED34295706CAF0,
	U3CU3Ec__DisplayClass11_0__ctor_m7E8E5B43AE83F0CED58E85FA9BDC9A0FABF7A00D,
	U3CU3Ec__DisplayClass11_0_U3CGetDeviceTokenU3Eb__0_mBF1170E354B8DCFBBF02B8E8EDD2871A44FD6070,
	NCMBObject_get_Item_m115F64C13A13ED1F939DBCD2F4C637B09730FA80,
	NCMBObject_set_Item_mC0E83DCFF1B8DDD0BDD186B278E8BE512B271F56,
	NCMBObject__onSettingValue_mAB73AD78BF6A9580E96830572DFDE7FD95DC6718,
	NCMBObject_get_ClassName_m01A4D20A0978673D1A68B8475DFD0ABCBB61E076,
	NCMBObject_set_ClassName_m32FF48B1D53E27B2ABC905B8EF455142F9B6C90B,
	NCMBObject_get_ObjectId_m085515EEF298EBF439301AE2C87C9FE1E3AA8569,
	NCMBObject_set_ObjectId_mB9DBF772722EFA92B3075553CCC4B10E9A9184C0,
	NCMBObject_get_UpdateDate_m625B56ACEBA8060954BF320C12C0CF4B55DC1F6C,
	NCMBObject_set_UpdateDate_m1169A2403AD40DFCA2A8C05EC368A5BD167B26EB,
	NCMBObject_get_CreateDate_m62DBDE53B0795DA21C34317694675FD64E8A53A4,
	NCMBObject_set_CreateDate_m2D7372F0D460FB750A693E445DF845ABCFE00964,
	NCMBObject_set_ACL_m7C3E2F6DD13993FDB821FA21FD4E98F9D11F0F23,
	NCMBObject_get_ACL_m1119A1B910777FBF29B2930976BFE1DB2B84B8A2,
	NCMBObject__checkIsDataAvailable_mA57A1AEBF1E1AEC03B02A04E62491C02E19B0A22,
	NCMBObject__checkGetAccess_mD3E606CC6B7DC029E4B5EFF6C02BE43AA1ACE4A1,
	NCMBObject_get_IsDirty_m1E64B7ACECA022F2B45D6438288E8F3D0A97F0AD,
	NCMBObject_set_IsDirty_mD8D4A9E8ECC5071943CF15FCDCB180DC8503199F,
	NCMBObject__checkIsDirty_mEC62B906D10AC3D67BECE587F7B556864CA33B02,
	NCMBObject__hasDirtyChildren_m38BE3E6CC418BF659F1C6F6E769B7C40D750B7AA,
	NCMBObject__findUnsavedChildren_mB6FD6A7E4C69433345E42EBC65EA8D0CEBD12BF7,
	NCMBObject_get_Keys_m1ED804C9455389440C544EAE4B53C89191AAACF6,
	NCMBObject__ctor_mEA7161237C9168BA1783D8E48880C3329858490C,
	NULL,
	NCMBObject__ctor_m95D47F242E3377E24E9DF8863DC8859EB68658DB,
	NCMBObject__performOperation_m29ACC8949E34E86F911F62FC9F890D2FB61707F9,
	NCMBObject_get__currentOperations_m5939F459E3D437F5FAB66DC71F7286A4C15F7190,
	NCMBObject_StartSave_mEFBAD18344DF06F30842E4FA6CC39E6AEBE3BA2C,
	NCMBObject__isValidType_m22BA7C636A0441A8217ACD7A9F636E4F5D2DA83C,
	NCMBObject__listIsValidType_mBE970C6D08DFC87E899026D058EC2BA4A0FF50B3,
	NCMBObject_Revert_m72999B6A1ABA69D9E93635333BFE6B73D7278193,
	NCMBObject__rebuildEstimatedData_m617099C49011A91A17EE9F6A94C4F82049DC5D91,
	NCMBObject__updateLatestEstimatedData_mF0B11E085ADE3EF72D9F9EF9F9324C767F45A7A7,
	NCMBObject__applyOperations_mB47FAB46348E136F2D67AD8F9034C7A0DB451198,
	NCMBObject_Add_m311859FC00D48E993DF810F45665D0B7A1E7BB79,
	NCMBObject_Remove_m464E7A1DC9CC67485D46921594F29C039CAF34EC,
	NCMBObject_RemoveRangeFromList_m0A5D771DADE9E71719374B41208F4B1777E8930D,
	NCMBObject_AddToList_mE5823E1EE42B5E8943483CD6D2033299BB572FE1,
	NCMBObject_AddRangeToList_mF823BC3C35254EBE8F6134D97D4B14033FA5BCC9,
	NCMBObject_AddUniqueToList_m36F9A17F022686EA3C62D8C224C7D50BC3A466B6,
	NCMBObject_AddRangeUniqueToList_m1248B082FA5E6B2A730EAABAA9CD4DCD2A8B804C,
	NCMBObject_Increment_mD22DF5CCADA24BBAD6D858E63C742EB7724948FA,
	NCMBObject_Increment_m26B2DB81BE056CE78EE2A1B3A4C0BC72D0EF9DCD,
	NCMBObject_Increment_m6B2EC9F8A5C66F76A706E55200688A8091FB3045,
	NCMBObject__incrementMerge_m0037B122D73ECAED36C53125159C0349B6058292,
	NCMBObject__addNumbers_mA5B6240BEA574BDE834F7E3253D56927E3F0BB9D,
	NCMBObject__getBaseUrl_m8C0445F87C03CFDC15E8A59640E7150EA573903E,
	NCMBObject_DeleteAsync_m6D5A542935493EA43512F9ADC8F95B09D3595CDC,
	NCMBObject_DeleteAsync_m99892770FCDDCC8A9F947640505E6D941E6F46D2,
	NCMBObject_SaveAsync_mB36A548E04576B1E97E63A0D24BFE0A411DF742D,
	NCMBObject_SaveAsync_m460F494A301A7ECABC69AD6304DD44F632A84324,
	NCMBObject_Save_m7CC0CABF37DFF121977A55A4641D9974546E2BA9,
	NCMBObject_Save_m5C73FC83E6109267F60FD0E9C814D3AC9EF93EA4,
	NCMBObject__beforeSave_m4C8F32550DC4C1FCAC67A77851670C3AA0BA0B58,
	NCMBObject__afterSave_m3CA541FA76353FA85B46C6D6C40CBE471B29BCD1,
	NCMBObject__afterDelete_mB71083213FBE231B69A54EBA9DC4BBD0B235B6C3,
	NCMBObject__toJSONObjectForSaving_mC490C7D2124F588F10EC60D3A2CB273E78243829,
	NCMBObject_FetchAsync_m243164026ABE12459C6BD4D2FD7A88B15D4E2CB5,
	NCMBObject_FetchAsync_m7A087B5696C268F80AD8A4AB184D35D350388B81,
	NCMBObject_ContainsKey_m47AFB7A5BD4FDD749C437289D4C03E0D0D993FC0,
	NCMBObject_CreateWithoutData_mBE6F91735B25EF93108309F1C95ED352DA636381,
	NCMBObject__mergeFromServer_m7FFA7B1DE07A640792107EEDC23454A5F1FD6334,
	NCMBObject__handleSaveResult_m5E5AE7ED299954F427E9555DD17DB43DCF3F98AC,
	NCMBObject__handleFetchResult_mD3119AD3F153B5B3DBEDA9FFD4567AC86DAB29DF,
	NCMBObject__handleDeleteResult_m0282C531E357FC542579751433C97CB8EF4B1EB1,
	NCMBObject__setCreateDate_m3274FB970CAAB5156BDF688BE196EA7B599A5D0B,
	NCMBObject__setUpdateDate_m4B27D5B86947E6272ECCA74C45D140ED0DBFB86E,
	NCMBObject__saveToVariable_mECFAD31A559AE1195BD9B29D13ED9F5D6F954739,
	NCMBObject__getFromVariable_mE904A84F66AD2DAAA8115287E9F6AF7903F741EA,
	NCMBObject__saveToDisk_m617F540DD64CFACC970DC0EC4EBFB5068A24F72C,
	NCMBObject__getFromDisk_m557CBF5554CFCBD823FAFC75BCB4A49CCE4CE143,
	NCMBObject__getDiskData_mF19CF278596A8D91D9AFA71AD48EE2A847A40106,
	NCMBObject__toJsonDataForDataFile_mEE0D4E53B30498EB89BB04348DF19113AC9BD42B,
	NCMBObject__setDefaultValues_m55D3845791B06B9328C816DC1942109E5B16489E,
	NCMBObject__cctor_m00D62A5F6185198247D667833DFA1F4D6B1081C8,
	U3CU3Ec__DisplayClass67_0__ctor_m816C7346FBB53D438D377BF40EC27EFCB0558312,
	U3CU3Ec__DisplayClass67_0_U3CDeleteAsyncU3Eb__0_m6CE41B345C4BC4E72A8B24D6C1536637EA6D1DB1,
	U3CU3Ec__DisplayClass72_0__ctor_m3135DD8A9EEFA6C9F7BCDC7CBF6D1576B51F3356,
	U3CU3Ec__DisplayClass72_0_U3CSaveU3Eb__0_mD874ACF9B2FDDBB78E80E1F9C6CC6147E8FC729E,
	U3CU3Ec__DisplayClass77_0__ctor_m69D84D505D2747D0842226AFBBA3F80B59C651A1,
	U3CU3Ec__DisplayClass77_0_U3CFetchAsyncU3Eb__0_m207B0A93E8DDF2ACABB8C3BA954BF172D24A06D5,
	NCMBPush_registerNotification_m9472B9C7E1874376759C25D8773C4479FB195D10,
	NCMBPush_registerNotificationWithLocation_m9FB9C648A7FA50C2D475FF36F797D56185F63D18,
	NCMBPush_clearAll_mBAB0EB2D91F7FB60BF22944AA81A67EFACC40066,
	NCMBPush__cctor_mC8A778496631C71C776F3444CB423168A2E3A9BA,
	NCMBPush__ctor_mE0F7878C2293EDFA3849AA71B145376AD731F89D,
	NCMBPush_Register_m56C57BD92DB293E4016738EC827DA1D9E6C14A5F,
	NCMBPush_RegisterWithLocation_mC395DE29754AC67E0FCB19623C27199B1D70384E,
	NCMBPush_get_Message_m9EB50121D9E0DA875CF64AD92359C1A041211EEF,
	NCMBPush_set_Message_m44021DD4033474102DB50CD2845C895A50AFC6F2,
	NCMBPush_get_SearchCondition_m4F0221EDC2F70B65EB790080F683672F840639B4,
	NCMBPush_set_SearchCondition_m2E700C476D0EDE57D2D0D2C5528FB99D01761909,
	NCMBPush_get_DeliveryTime_m0DC00872C49389BADF372DF07457DAB23390B82F,
	NCMBPush_set_DeliveryTime_m0BBFCCC253FEB414EE501D9B48F01C1EE3D9DB3F,
	NCMBPush_get_ImmediateDeliveryFlag_mA857D03CB09C0EEF24D773A90249457B825E6522,
	NCMBPush_set_ImmediateDeliveryFlag_m7D191253578CFC61DF50AF0AC6E9001FB1CC1821,
	NCMBPush_get_Title_mAF0FD5029702E12C61E97E4DA44803E6955C696C,
	NCMBPush_set_Title_m287464432AB535739A9962CBC20F05B32CA57E27,
	NCMBPush_get_PushToIOS_m7C5DC31FC34C2A3B8FE6B516658EC5FC9532264F,
	NCMBPush_set_PushToIOS_mF8D0309BF7E92230D188E443CBBE13C8061BFEB5,
	NCMBPush_get_PushToAndroid_m94786D29C5C39C67A2A963FAD3A17242F80544DF,
	NCMBPush_set_PushToAndroid_mAEB45CD239583CD66453B77CA3A95CBDB056D321,
	NCMBPush_get_Badge_mBDAC26E4155DF949B969C7EFE87F06DB6306B4BA,
	NCMBPush_set_Badge_mB5570E4CBA7CAB46DEC2BA291963DF413FF7EA54,
	NCMBPush_get_BadgeIncrementFlag_mEDF5F8BA21B1A1EB9BF26A47D123F5D4294D4453,
	NCMBPush_set_BadgeIncrementFlag_m22614E426329180E9D30DA90AD8D3BDAECD299F3,
	NCMBPush_get_RichUrl_m6AF6EE109F162EE9CBA8B511F6D3F0BD8CC4AC77,
	NCMBPush_set_RichUrl_mEAF5114CE0E853D52EE2A3F4A97D01F5160CD1B7,
	NCMBPush_get_Dialog_mEB034EBD04BEB09994BA4D30A4EAF46A5D04C3F5,
	NCMBPush_set_Dialog_m625F3FC2B36D542493574DD453E4F2D8FA68AF5B,
	NCMBPush_get_ContentAvailable_m1C779BC92CEB4ED5059820762F92786BA529F375,
	NCMBPush_set_ContentAvailable_m42D4292D124EA53598298331A40C2C0A6BADD88D,
	NCMBPush_get_Category_m6ABEAD5FDC06605731EFED2D99E6D4C803B9A034,
	NCMBPush_set_Category_m0EFE2AC0E4378B8F99A7534040F6C840F3CD0679,
	NCMBPush_get_DeliveryExpirationDate_m77B6476B9F995505D165B9100C918C79A12C251F,
	NCMBPush_set_DeliveryExpirationDate_mE289BF7E234ACF22AC3DCB0D153E60B73969F73C,
	NCMBPush_get_DeliveryExpirationTime_m776AA08BD5E8DDA9005689F943030059B2C97773,
	NCMBPush_set_DeliveryExpirationTime_m1617A082BC26FDE2BE517978D948B8BC47357428,
	NCMBPush_SendPush_mE84915F95D414570EBDBF197E2C68DE9803E1970,
	NCMBPush_SendPush_m25C83AC36BC7CDDC6A4DD87D500D8B233178324C,
	NCMBPush_ClearAll_mD01E25209CA40E99527360FE6A1170D16C1F53E8,
	NCMBPush_GetQuery_m3A28C82AEF940BDE86670C283090E5C184F6C014,
	NCMBPush__getBaseUrl_m1264531A3FEA70703490693DFE17E74DC08EF86B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NCMBRole__ctor_m86317CDEC70246FE390A006CA7DB0BCB4F888C32,
	NCMBRole__ctor_mCE5612A7E30D29DE830052332E108F50225AF133,
	NCMBRole__ctor_mA79F6215A76C02C19A0A5A56106B6D24C209DDEC,
	NCMBRole_set_Name_mBDB6BC0B1B99E7A49268D65716B93726D8C19893,
	NCMBRole_get_Name_m757ED43FBD35BD2C43EACF3BF86349B9C47D72FC,
	NCMBRole_get_Users_mA4B4CC70937BF364F3D802432E07E338A47DEB3F,
	NCMBRole_get_Roles_mAEFC86BB906C422031F462A816EB2F913B1AFEF0,
	NCMBRole_GetQuery_mED735B42BD4B9BBA7A47EE73E63ABD9DC237D117,
	NCMBRole__onSettingValue_m26477B1599EE673A737D7E2CB3E6AF9C70C62C6E,
	NCMBRole__beforeSave_m06525DEBEAD22A314BB6CA62DDAA9CF39B0D1F9A,
	NCMBRole__getBaseUrl_mCAC8A167E4C00C230DD4A6ED4C2CE8B83D6B8901,
	NCMBRole__cctor_mA0F7D58FE87C3BB4663505CC4B93D247BD505704,
	NCMBScript_get_ScriptName_m500FEB962A99E50C6E11F3C5535FD0414C17C734,
	NCMBScript_set_ScriptName_m4CF14086D79B6B2412F2F57406674EFA71EFB115,
	NCMBScript_get_Method_m7DCACB667527C7FB005E0B75FD8B93FB444433C0,
	NCMBScript_set_Method_m8686AB2868E5EEBBEBB023BF64FD108A7EE99291,
	NCMBScript_get_BaseUrl_m050D19B6A8A4A8FFE401AD2CC002921E77042307,
	NCMBScript_set_BaseUrl_mB725BE61B1EE870D371CC76F3E204235251A3870,
	NCMBScript__ctor_mC4621EE9598EE7B90C8BD7B7C389B5DCDEBE1591,
	NCMBScript__ctor_m615D97E9FE94F583A7B1EF9B2706DAD68ACE706D,
	NCMBScript_ExecuteAsync_mB76930F866DABB9D45763AEAB1A85979D9C2E1E1,
	NCMBScript_Connect_mE0DD6071715C1EC3C6486C7A8EFDBC62262B1BA1,
	NCMBScript__cctor_m3CD738981341BCB2DA327DFFB146D714D4E4ED76,
	U3CU3Ec__cctor_m6F1207F7F114958EB5C9DFF292B50AD150BE89CB,
	U3CU3Ec__ctor_m30A8023C113544FA73B9151EB561BB443DF3BE86,
	U3CU3Ec_U3CExecuteAsyncU3Eb__18_0_m3427557B8C334E6FAB3CE566E03205CC3EE50777,
	NCMBTwitterParameters__ctor_m3EE63C76F6ACD12311938069A2C805B681632C10,
	NCMBUser_get_UserName_m919C039AAF5737AEAC417648E8CC42A604C98D8F,
	NCMBUser_set_UserName_m330ACAF4F265F85AB78B1DCCA43E24E0D0026017,
	NCMBUser_get_Email_m5E6F575C8CC66B0906793CEF3B7ADD893515866E,
	NCMBUser_set_Email_mEB8CB323BF671416AD02451A1E59A09DB1F260D2,
	NCMBUser_get_Password_mA431A815ABB2AE300FCD356CDDF5882E72A2DDD4,
	NCMBUser_set_Password_mC5F385E23948C97D496BEC755D55D99B8340E248,
	NCMBUser_get_AuthData_mA5AA13CAC097AE98E3CF2A217AFCF6D719C9D3DD,
	NCMBUser_set_AuthData_mB1A917B9994B2CA41A279D1F3801137AB6C8A911,
	NCMBUser_get_SessionToken_mB524F409959CB8AF2DA79429995DFD9741763885,
	NCMBUser_set_SessionToken_m6A738DD02F37C256E032C1CB0BD73BB6CCC3183A,
	NCMBUser_get_CurrentUser_m9F82B3580EEBF1B2601234356E163B7886100EAF,
	NCMBUser__ctor_m15BC402336F19A8E9DF9F654FAE88D9E2C42F360,
	NCMBUser__onSettingValue_m6AE1C590DF8A56F2FC14FF575C6D0C54A0902AD5,
	NCMBUser_Add_m8F9059597A5B60486C9B9E1CD5D9F59B60449D09,
	NCMBUser_Remove_m9B39874177715427814EA216A01A581046938F09,
	NCMBUser_GetQuery_mF2B030186FE2CD359C68A3B37D269963B30E84D9,
	NCMBUser__getBaseUrl_mF26F115185CC0867B0454B12B237B1F86B91401B,
	NCMBUser__getLogInUrl_m40CAF74344C3AF93CCD2D6288A788BB25E6B2779,
	NCMBUser__getLogOutUrl_m261630F2365CC9F9C0B1410F794B54154A0F75F9,
	NCMBUser__getRequestPasswordResetUrl_m279D85FB3EA8052326B66F095181136249068EF3,
	NCMBUser__getmailAddressUserEntryUrl_m06C776A931034D7A82B55840183204281994C681,
	NCMBUser__afterSave_mC12B621CE9737AF9116035DCA38C8525E1D26A75,
	NCMBUser__afterDelete_m174428FB64A98E44A5011B379121FB9B5BD765DD,
	NCMBUser_DeleteAsync_mB5C8606B5B085800A41E3530475302AC86E7B501,
	NCMBUser_DeleteAsync_mD9F84F379F80C769DD27C77B9299FA9759823F87,
	NCMBUser_SignUpAsync_mE0FC02DDACA3FF04E958F3E8D579AC4090EBBD10,
	NCMBUser_SignUpAsync_m77FF9F68A200498309C43002BDB1E4F15F6F702C,
	NCMBUser_SaveAsync_mBBA54B7FFDBF76872DD923B5339B720050A672B3,
	NCMBUser_SaveAsync_m74F32D72DE69833907DB8173C2DB2B43E533E5FC,
	NCMBUser__saveCurrentUser_m431B8E45172F00C373D910186706D5B894AEB05B,
	NCMBUser__logOutEvent_m80134FA4B6D95CA286C53D00E4CD4EEE02A001FA,
	NCMBUser__getCurrentSessionToken_m4010FADD7FAECC3A0B6D0F03D451739FF8EFA6D6,
	NCMBUser_IsAuthenticated_mD92CD471EEEF36346E50A47489764CC61A0D12E9,
	NCMBUser_RequestPasswordResetAsync_m3CD398E51C838CD3A10DF54971D01E24D793029B,
	NCMBUser_RequestPasswordResetAsync_mDE94B97DBA2CED60F40DD38879E44D6E8BE7BF00,
	NCMBUser__requestPasswordReset_m508D008E9A5386BC66923C01F53F40B79AC11347,
	NCMBUser_LogInAsync_m1A58B6E285B79B74E799C62A7F694AA58E58850E,
	NCMBUser_LogInAsync_m47DB940D3F8B3158F8E035F7916DC18B8DFCB7B0,
	NCMBUser__ncmbLogIn_mAF919828D6DF7A341D1952DA0014D3F0EDFE8AF4,
	NCMBUser__makeParamUrl_m5D85BAD283D8E3BF223306D21DC2156EEC0E33BA,
	NCMBUser_LogInWithMailAddressAsync_m24FBEC49C9E00792EF4E97A329B1AA23C5F7AB94,
	NCMBUser_LogInWithMailAddressAsync_m51BC6C044004ECD1D89A663B53B3317867C44B3B,
	NCMBUser_RequestAuthenticationMailAsync_m27FB98C08F97FDEA201C6DC1EA5214DE0C3892F6,
	NCMBUser_RequestAuthenticationMailAsync_mD012BED84FC1755CBFEF82E83D0B9F43974DE3FE,
	NCMBUser_LogOutAsync_m3E4D4631C8663FFB06C2FBD50ADDD32603FDD86D,
	NCMBUser_LogOutAsync_mD551989FDBC119BA5883F9570DEEC3B190EA06BB,
	NCMBUser__logOut_mAA89150A893EB4B6B85887D8A2F3678DAC77C2CE,
	NCMBUser__mergeFromServer_m6EABF9FE80F8BD19DB3ABEE3BECE3949A1EEDCF5,
	NCMBUser_LogInWithAuthDataAsync_mAF23E5F0D1EB6F1C576B11BC1102EAAF86BDCC89,
	NCMBUser_LogInWithAuthDataAsync_mAFB17B8639EFAA505F86A263B44DF3192C21DA47,
	NCMBUser_LoginWithAnonymousAsync_m67A842CCA54849D421BF151AA214BD3DA5C65E1B,
	NCMBUser_LoginWithAnonymousAsync_mC3C4DCAA45F324542445BA41C4F1F72DD3CBBE0B,
	NCMBUser_LinkWithAuthDataAsync_m6E39C6A8454591FCA38EA4DF983AA10508DF5B4D,
	NCMBUser_LinkWithAuthDataAsync_m726B7D30920923A2D77F5B714FF5BE0438D975EA,
	NCMBUser_UnLinkWithAuthDataAsync_m18D47110AB96AE2680863D5E412C0550F8062751,
	NCMBUser_UnLinkWithAuthDataAsync_m502F1BAA25E0B98521618C32FE9FA8B353110D0E,
	NCMBUser_IsLinkWith_mA4D45030FD7E21EBB0AB64D3F3DF0517046DFE71,
	NCMBUser_GetAuthDataForProvider_m12C88BFD22EBB30FE54E89351FC8FECEECE0B9AB,
	NCMBUser_createUUID_m4DA4AD1D41E00582029E850AF93B739EEA95D27D,
	U3CU3Ec__DisplayClass43_0__ctor_m011023BC928C4BEFD0E09B6C9803692FE9A22988,
	U3CU3Ec__DisplayClass43_0_U3C_requestPasswordResetU3Eb__0_m77B1D03C4107890DAAA256A5687DF38D76EA7620,
	U3CU3Ec__DisplayClass46_0__ctor_m0B145C16D7EA7C3E016621CD430D95942EA539F6,
	U3CU3Ec__DisplayClass46_0_U3C_ncmbLogInU3Eb__0_mE1E6DE0EDF59D563C4C651561D9E971D9D445286,
	U3CU3Ec__DisplayClass51_0__ctor_m8F4137AE1CB5840E2C6F8E9DF40B80E005F37AC0,
	U3CU3Ec__DisplayClass51_0_U3CRequestAuthenticationMailAsyncU3Eb__0_m62082CCD1B0AE2A59448529BC594BF0FDB13179D,
	U3CU3Ec__DisplayClass54_0__ctor_m6D1090B976E6E64AF9AA0833B1DAAAD624EF693D,
	U3CU3Ec__DisplayClass54_0_U3C_logOutU3Eb__0_m84B42A4D648454D7F94485473CD26CD2ED895429,
	U3CU3Ec__DisplayClass56_0__ctor_mED4C3BEA68E77BE1F96F7BBFD50130AFB4772E95,
	U3CU3Ec__DisplayClass56_0_U3CLogInWithAuthDataAsyncU3Eb__0_m9BFA6A34242429BF4D543FD67FDFF7F47DF173FD,
	U3CU3Ec__DisplayClass58_0__ctor_mD75EE33F7C862C4C1D4DF6621461AC461C536909,
	U3CU3Ec__DisplayClass58_0_U3CLoginWithAnonymousAsyncU3Eb__0_m7C90A5DC9A5D27067DD798E57DF0886109593313,
	U3CU3Ec__DisplayClass60_0__ctor_m20833A70409B1BD874DCA615FE905BC4A7D5F506,
	U3CU3Ec__DisplayClass60_0_U3CLinkWithAuthDataAsyncU3Eb__0_mE6C51EB257DBBED027463F942CF8879F4CB9C521,
	U3CU3Ec__cctor_mDF7EBBAC17D4CC0BE9BDBDECCDA3719608538C48,
	U3CU3Ec__ctor_m8A08A950E68FA1607B1366F0076329D352D5860E,
	U3CU3Ec_U3CLinkWithAuthDataAsyncU3Eb__60_1_m29699AA5D26A3FC1019AB261D4A2AD9DCF23C45F,
	U3CU3Ec_U3CLinkWithAuthDataAsyncU3Eb__60_2_m5417368F1E9B0E6F80D9B6F31272DD5DF9D30A25,
	U3CU3Ec__DisplayClass62_0__ctor_mD9C13D3950D065CFBD0ABEBB155D1321054D758E,
	U3CU3Ec__DisplayClass62_0_U3CUnLinkWithAuthDataAsyncU3Eb__0_mF9CCE318F44D0001C852617852E19A67CA3B8498,
	CommonConstant__cctor_m38960708334610C2A0872EF1FE8C4643A28BE6E9,
	ExtensionsClass_GetTypeInfo_mE011E71B4F6791990F2D73563DB033D071D743A0,
	ExtensionsClass_IsPrimitive_m0D22FFC7463D4C03CAD3200D1EFF74C6B5209CC8,
	NCMBAddOperation__ctor_m53FD49F2C5332184F5C941C67864D4E5105D7B36,
	NCMBAddOperation_Encode_mCD332A93BBCDE767DBBF4D63060C828398466FFB,
	NCMBAddOperation_MergeWithPrevious_m5AEC7DE171A8159EC97FD66D4C00DBF9FD92924B,
	NCMBAddOperation_Apply_mC80753320613F8E09C5A50845ED23B4B70B7A043,
	NCMBAddUniqueOperation__ctor_mA95E90BFDF07B698D3E65319D272E262EFC8D3CF,
	NCMBAddUniqueOperation_Encode_m28874032433749DC75806B0B4B3EFCC1464C19A6,
	NCMBAddUniqueOperation_MergeWithPrevious_m72A13EA5140ADFFCDF3794C81A228767553ADC46,
	NCMBAddUniqueOperation_Apply_mD4589E5026A566941F98D51031CF5FB8FD7EEFDD,
	NCMBClassNameAttribute_get_ClassName_mFF8C5644C837F8BD726C25239176DEF224EC15A1,
	NCMBClassNameAttribute_set_ClassName_m00765296450CAD8ABB473CAB4AF398E89BB22ED2,
	NCMBClassNameAttribute__ctor_mEB007E3A35978BE7C0EEDDFD8B2CD1D4377256F8,
	NCMBConnection__ctor_mEE3376D5D44ADEFF5A42C45393C7E15BFD3C2A27,
	NCMBConnection__ctor_mC96FE6644919B242E08AD5A7614F82DEAD324003,
	NCMBConnection__ctor_m70E1BE9C1E10DF88CA6305842765C46E2039051E,
	NCMBConnection_Connect_m9BEE91AF0B699F65472310681E64B0EF61877472,
	NCMBConnection_Connect_mEFAE03B5C3FCD972489EA003C466158D47835FF7,
	NCMBConnection__Connection_m730D0BE225876F04878D20C0BA28B15B6A510047,
	NCMBConnection__signatureCheck_mB956390CAF681F4D16AE0073707C3F9B033831EA,
	NCMBConnection_AsHex_m58324B263B8D7EADA3E359C7119D95658CEB57A1,
	NCMBConnection__setUploadHandlerForFile_m31A1EB07D2328AAA740F952682CF8C2B5CA16028,
	NCMBConnection__returnRequest_mD460BF78B649E3CA825A5E5619ABA8A6F8039AAA,
	NCMBConnection__makeSignatureHashData_mB2C16A88773E0BC211C6636EB79F51A0E889BD39,
	NCMBConnection__makeSignature_mA62F20D9E6DBE2A5CEA862C071880422DC35C88C,
	NCMBConnection__makeTimeStamp_mB77DEF302B170390EBFF6E76C9FB9BF2B03433FC,
	NCMBConnection__checkInvalidSessionToken_mA01C374AEAF83505D5DBE9C6D1B97EC3AC4CF9A8,
	NCMBConnection__checkResponseSignature_m01AF87F7B4C85139934AED20D9138C100FC0F1C6,
	NCMBConnection_SendRequest_mA1B1682929788DD267520466BD1D77508B6FF4A6,
	NCMBConnection__cctor_m83805C6A57EE2B9424D012EB04153011EC92DB14,
	U3CSendRequestU3Ed__37__ctor_mE9B568D6C4D3A0D9CD66E2F14226D49D555B253E,
	U3CSendRequestU3Ed__37_System_IDisposable_Dispose_mD2A67BF755C60BAF15CFBCD70593148C343610AE,
	U3CSendRequestU3Ed__37_MoveNext_mFED3BC541B330B95AF81954676F2B240E5382544,
	U3CSendRequestU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m63E3C63F0B4126D668F1622257A6166BB9158192,
	U3CSendRequestU3Ed__37_System_Collections_IEnumerator_Reset_mB6C2E4632FD157FBA471B2161264192BB863ECC1,
	U3CSendRequestU3Ed__37_System_Collections_IEnumerator_get_Current_m2C2A42AADDAC569E904C7F36C00F73CAC85C91F0,
	NCMBDebug_Log_m76BCFA58EB357E3FA55D4DA49AEFAD20BAC205DE,
	NCMBDebug_LogWarning_m8E93D353E426E8A707AE8DC7288BE26147555AE6,
	NCMBDebug_LogError_mEF02F7B3AB787C161B3DE4EDBC076B899F927B2F,
	NCMBDebug_LogError_m9567B5061E2B04E48714418176AC5CCEE7B4B22E,
	NCMBDebug_List_m79FD6D9510A8576281201B0BF8C110EA08643D91,
	NULL,
	NCMBDebug__cctor_m0CB906B40583CA3AB2C4AADED3DBE93ED3E87C81,
	NCMBDeleteOperation__ctor_m1D37E95F8FA88DD2AFCF194C450E4E1E66CE46CF,
	NCMBDeleteOperation_getValue_mE30AAA05E400D2BE6CFD7236F60D5170ADC0D10B,
	NCMBDeleteOperation_Encode_mC202928F31C8F07FD132298A5912254A74AEB91B,
	NCMBDeleteOperation_MergeWithPrevious_m08CCAF431DECA52B329BD0E1B12D321C59EAC9CB,
	NCMBDeleteOperation_Apply_m854EEF71892C7F04C9EEB9BAF9AC2328DA632D8B,
	NULL,
	NULL,
	NULL,
	NCMBIncrementOperation__ctor_m020A39DA365E5B6F24C1C210DFF7C08992E4AF0B,
	NCMBIncrementOperation_Encode_m68ED0492AC3B1F7265A7C9D86F98D641371D1EAE,
	NCMBIncrementOperation_MergeWithPrevious_m84FAC3AB545D36724A2ACB801A1AC86B34A5F137,
	NCMBIncrementOperation_Apply_m6F544EBE76B60E8EF9B85BDCEA4CF29A22A092F4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NCMBRemoveOperation__ctor_m3D7CFD8D142C0D2C2AD00FA730110CE5721EEDFC,
	NCMBRemoveOperation_Encode_m9D34274C2DD7C4EDA9693EE0D43C5FF9C56D2CEB,
	NCMBRemoveOperation_MergeWithPrevious_mE67F19FD490D7565BFA56149921BAE5478AECF48,
	NCMBRemoveOperation_Apply_mF04B84F009EA2BD7BE10F755C3219414481770C5,
	NCMBSetOperation__ctor_mC9B001F6262217A4D0590CD75E2F673923E8AFB6,
	NCMBSetOperation_getValue_m97A6CCE91A48FB1CE84BE5730B9A5764BFBE0207,
	NCMBSetOperation_Encode_mCF221B9CB5F792214F0433B98DAB48E48547398E,
	NCMBSetOperation_MergeWithPrevious_m6D3D8A858745723E40DE5D116EB40CBE01B83210,
	NCMBSetOperation_Apply_mA87445F06076C1F2BF67974EF48B5E0B1D3F3C50,
	NCMBUtility_GetClassName_m53AC91D47383BDDE9AEBB60E70B033BF39424EAE,
	NCMBUtility_CopyDictionary_m217DE7CBFE9E3F0B6221C68CC2AF4BFD520AF2A3,
	NCMBUtility__encodeJSONObject_mDF3606682F490326F1BB20662CD11D9942B5003E,
	NCMBUtility__encodeAsJSONArray_m3419479578986CFB9B69AA1AE3FE05F9EBA729A4,
	NCMBUtility__maybeEncodeJSONObject_m49A3B696D1B5E0913E6C6E01F4A3EC6AAFE8551F,
	NCMBUtility_decodeJSONObject_m4F8AD9BBBD857643DD613CC863F38042D6547AF9,
	NCMBUtility_parseDate_mC74F39EE06CB08FFCFA868C75A2C767CB9944679,
	NCMBUtility_encodeDate_m0D9661F7CE84F7D99E0CF9B666B4ABEF08726718,
	NCMBUtility_isContainerObject_m87E07B11471D4CADBFE1D96E2FBD7B27A8EF5615,
	NCMBUtility__encodeString_mE377C2AB94BCCEE6F354902C5724C0EE054A7800,
	NCMBUtility_unicodeUnescape_mA79F93DA1D7CEC9720AA03F77A06F0E35ECF3598,
	U3CU3Ec__cctor_mCAE3C1FB5B2FEA7F423BD10C412289762FB56EE2,
	U3CU3Ec__ctor_m93726F14237C8F95C809EAF8844A80091DA1F9EE,
	U3CU3Ec_U3CunicodeUnescapeU3Eb__10_0_m592F28AEC49B8F76DF08255082EB2BE64D7925BF,
};
extern void NCMBGeoPoint_get_Latitude_mA701AB3DD72C66ECC54B564763553EF8BD638B67_AdjustorThunk (void);
extern void NCMBGeoPoint_set_Latitude_mF9CE6978C04BF38FC99A273EB2C25928180FF106_AdjustorThunk (void);
extern void NCMBGeoPoint_get_Longitude_m39ADA9F8A394B404F134702D649B1F28BCFF9231_AdjustorThunk (void);
extern void NCMBGeoPoint_set_Longitude_mFCB3A006D32D628A7B917F5540EE5678BA7DF935_AdjustorThunk (void);
extern void NCMBGeoPoint__ctor_m5E7A2D8E13F647E3D341711C8C5DE64AE20EAB14_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[5] = 
{
	{ 0x06000420, NCMBGeoPoint_get_Latitude_mA701AB3DD72C66ECC54B564763553EF8BD638B67_AdjustorThunk },
	{ 0x06000421, NCMBGeoPoint_set_Latitude_mF9CE6978C04BF38FC99A273EB2C25928180FF106_AdjustorThunk },
	{ 0x06000422, NCMBGeoPoint_get_Longitude_m39ADA9F8A394B404F134702D649B1F28BCFF9231_AdjustorThunk },
	{ 0x06000423, NCMBGeoPoint_set_Longitude_mFCB3A006D32D628A7B917F5540EE5678BA7DF935_AdjustorThunk },
	{ 0x06000424, NCMBGeoPoint__ctor_m5E7A2D8E13F647E3D341711C8C5DE64AE20EAB14_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1459] = 
{
	5284,
	5284,
	4212,
	4212,
	4140,
	4140,
	5284,
	5284,
	5220,
	5220,
	5274,
	5220,
	4273,
	5220,
	4273,
	5150,
	4212,
	5081,
	4140,
	5081,
	4140,
	5284,
	4237,
	4237,
	935,
	5284,
	1972,
	4237,
	3923,
	5284,
	5220,
	4273,
	5284,
	4237,
	4237,
	935,
	5284,
	5284,
	5284,
	4237,
	4237,
	5284,
	5220,
	4273,
	4212,
	5284,
	4237,
	4237,
	935,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5176,
	5284,
	5284,
	5284,
	5284,
	5284,
	4237,
	4237,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5176,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5176,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5176,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	4212,
	5284,
	5081,
	5176,
	5284,
	5176,
	5284,
	5284,
	5284,
	5284,
	4237,
	4237,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5176,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	4237,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	4237,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	4237,
	4237,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5176,
	5284,
	5284,
	5284,
	5176,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5176,
	5176,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	8146,
	8186,
	8186,
	8186,
	5284,
	5284,
	5284,
	8186,
	8186,
	5284,
	8186,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	4212,
	5284,
	5081,
	5176,
	5284,
	5176,
	4212,
	5284,
	5081,
	5176,
	5284,
	5176,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	2499,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	4237,
	4237,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	2461,
	5284,
	5284,
	8186,
	5284,
	4237,
	4237,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5176,
	5284,
	5176,
	5284,
	5176,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5176,
	5176,
	5284,
	5284,
	5284,
	5284,
	5176,
	5176,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	4212,
	5284,
	5081,
	5176,
	5284,
	5176,
	4212,
	5284,
	5081,
	5176,
	5284,
	5176,
	4212,
	5284,
	5081,
	5176,
	5284,
	5176,
	4212,
	5284,
	5081,
	5176,
	5284,
	5176,
	5284,
	5284,
	5284,
	5284,
	4237,
	4237,
	4237,
	4237,
	5284,
	5284,
	5176,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	1996,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	4237,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	1375,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	4273,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	934,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	4212,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	2279,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	2505,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	4237,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	4237,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	4237,
	5284,
	4237,
	5284,
	5284,
	5284,
	5284,
	8186,
	5284,
	4237,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5081,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	5284,
	4273,
	4273,
	4273,
	2503,
	4237,
	5284,
	4237,
	4237,
	4237,
	4237,
	0,
	5284,
	5284,
	4237,
	4237,
	5284,
	5284,
	5284,
	4237,
	4237,
	4237,
	3769,
	5284,
	5284,
	4237,
	7808,
	5284,
	3769,
	4237,
	5284,
	4212,
	5284,
	5081,
	5176,
	5284,
	5176,
	5284,
	4237,
	4237,
	5284,
	5284,
	5284,
	5284,
	5284,
	4273,
	5284,
	5284,
	5284,
	5284,
	5284,
	5276,
	5284,
	5284,
	4237,
	4322,
	1358,
	4237,
	5284,
	7808,
	5284,
	7808,
	4237,
	4237,
	4237,
	4237,
	4237,
	7808,
	7808,
	7626,
	4237,
	7808,
	5284,
	5176,
	5176,
	5176,
	3762,
	5176,
	5176,
	5284,
	5270,
	5270,
	5176,
	5150,
	5284,
	7808,
	4237,
	4237,
	4237,
	4237,
	4237,
	7808,
	7808,
	8186,
	5284,
	8146,
	8115,
	7995,
	4237,
	4237,
	5284,
	5284,
	5284,
	4140,
	5284,
	5176,
	8146,
	4237,
	2461,
	2461,
	7808,
	4237,
	8006,
	8146,
	8186,
	5284,
	8186,
	2458,
	4237,
	1105,
	4237,
	2458,
	4237,
	1105,
	4237,
	4212,
	5284,
	5081,
	5176,
	5284,
	5176,
	5284,
	4237,
	4237,
	5284,
	2461,
	2461,
	4237,
	5176,
	4237,
	5176,
	4237,
	5176,
	4237,
	5176,
	4237,
	5176,
	4237,
	5081,
	4140,
	5176,
	4237,
	5176,
	4237,
	91,
	8146,
	8006,
	8146,
	8006,
	8146,
	8006,
	8115,
	8115,
	8146,
	8006,
	8146,
	8006,
	5284,
	6387,
	6817,
	7995,
	5284,
	2461,
	8186,
	5284,
	4140,
	5081,
	4140,
	5081,
	5284,
	4237,
	5081,
	4140,
	5176,
	2445,
	2445,
	2445,
	2445,
	7434,
	1341,
	3038,
	3038,
	3038,
	3038,
	8146,
	1508,
	5176,
	7808,
	8006,
	5284,
	7808,
	8186,
	5284,
	1288,
	5284,
	2461,
	5284,
	7473,
	8186,
	7955,
	8186,
	4237,
	5284,
	5284,
	7473,
	8006,
	8015,
	2458,
	2522,
	759,
	4237,
	5284,
	4237,
	5176,
	5176,
	5284,
	5284,
	5284,
	5150,
	5176,
	5176,
	5284,
	5176,
	5176,
	5284,
	1347,
	2458,
	4237,
	1105,
	4237,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	2458,
	2255,
	736,
	4237,
	2458,
	1288,
	365,
	4237,
	2458,
	1288,
	365,
	4237,
	2458,
	2461,
	754,
	4237,
	2458,
	2461,
	754,
	4237,
	2458,
	2461,
	754,
	4237,
	5284,
	4237,
	4237,
	5176,
	4237,
	5176,
	4237,
	5176,
	8186,
	1343,
	5176,
	4237,
	5176,
	4237,
	5284,
	4237,
	2461,
	1347,
	4237,
	5284,
	4237,
	5284,
	8146,
	5176,
	5284,
	1288,
	5284,
	1288,
	5102,
	4165,
	5102,
	4165,
	2050,
	5284,
	5284,
	4237,
	5176,
	4237,
	5176,
	4237,
	4237,
	4237,
	5176,
	4237,
	5176,
	4237,
	5176,
	4237,
	8146,
	8146,
	5176,
	2255,
	4237,
	5284,
	5284,
	3769,
	2461,
	2461,
	5176,
	4237,
	5176,
	4237,
	4991,
	4062,
	4991,
	4062,
	4237,
	5176,
	3038,
	4237,
	5081,
	4140,
	2949,
	5081,
	7446,
	5176,
	5284,
	0,
	4237,
	2461,
	5176,
	5176,
	7617,
	4237,
	5284,
	5284,
	5284,
	2461,
	2461,
	4237,
	2461,
	2461,
	2461,
	2461,
	2461,
	4237,
	2456,
	2451,
	2461,
	7226,
	5176,
	4237,
	5284,
	4237,
	5284,
	5284,
	4237,
	5284,
	2255,
	4237,
	3769,
	4237,
	5284,
	3038,
	7226,
	2445,
	1232,
	2040,
	4140,
	4237,
	4237,
	5284,
	8146,
	4237,
	7808,
	7808,
	5176,
	5284,
	8186,
	5284,
	1288,
	5284,
	1288,
	5284,
	1288,
	7995,
	8186,
	8186,
	8186,
	5284,
	7995,
	8186,
	5176,
	4237,
	5176,
	4237,
	5097,
	4160,
	5081,
	4140,
	5176,
	4237,
	5081,
	4140,
	5081,
	4140,
	4995,
	4064,
	5081,
	4140,
	5176,
	4237,
	5081,
	4140,
	5081,
	4140,
	5176,
	4237,
	4991,
	4062,
	5176,
	4237,
	5284,
	4237,
	5284,
	8146,
	5176,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	5284,
	4237,
	2461,
	4237,
	5176,
	5176,
	5176,
	8146,
	2461,
	5284,
	5176,
	8186,
	5176,
	4237,
	5150,
	4212,
	5176,
	4237,
	2455,
	1326,
	903,
	2461,
	8186,
	8186,
	5284,
	483,
	227,
	5176,
	4237,
	5176,
	4237,
	5176,
	4237,
	5176,
	4237,
	5176,
	4237,
	8146,
	5284,
	2461,
	2461,
	4237,
	8146,
	5176,
	8146,
	8146,
	8146,
	8146,
	2255,
	4237,
	5284,
	4237,
	4237,
	5284,
	5284,
	4237,
	8006,
	8186,
	8146,
	5081,
	8006,
	7446,
	7446,
	7446,
	6866,
	6387,
	7226,
	6866,
	7446,
	8006,
	7446,
	8186,
	8006,
	8006,
	2445,
	4237,
	5284,
	4237,
	5284,
	2461,
	4237,
	2461,
	4237,
	3038,
	3769,
	8146,
	5284,
	1288,
	5284,
	1288,
	5284,
	1288,
	5284,
	1288,
	5284,
	4237,
	5284,
	4237,
	5284,
	4237,
	8186,
	5284,
	3747,
	3747,
	5284,
	4237,
	8186,
	7808,
	7617,
	4237,
	5176,
	3769,
	1105,
	4237,
	5176,
	3769,
	1105,
	5176,
	4237,
	4237,
	878,
	424,
	217,
	4237,
	4237,
	4237,
	439,
	7808,
	3769,
	5176,
	5176,
	3769,
	5284,
	4237,
	899,
	6654,
	8186,
	4212,
	5284,
	5081,
	5176,
	5284,
	5176,
	8006,
	8006,
	8006,
	7446,
	7446,
	0,
	8186,
	5284,
	5176,
	5176,
	3769,
	1105,
	0,
	0,
	0,
	4237,
	5176,
	3769,
	1105,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4237,
	5176,
	3769,
	1105,
	4237,
	5176,
	5176,
	3769,
	1105,
	7808,
	7446,
	7216,
	7216,
	7216,
	7808,
	7644,
	7799,
	7617,
	7808,
	7808,
	8186,
	5284,
	3769,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[1] = 
{
	{ 0x060003CA, 3,  (void**)&ExecuteInvoke_HandlerCallback_mE2BC7E5E60255E8A4AC9D08FA99C7F91A67B697B_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x0200008B, { 6, 31 } },
	{ 0x0200008C, { 37, 7 } },
	{ 0x0200008D, { 44, 4 } },
	{ 0x0200008F, { 48, 13 } },
	{ 0x020000AB, { 74, 14 } },
	{ 0x060002FF, { 0, 2 } },
	{ 0x06000451, { 2, 4 } },
	{ 0x06000588, { 61, 13 } },
};
extern const uint32_t g_rgctx_GameObject_GetComponent_TisT_t28CB1661FF7BDEDFBEE527928DE91AAF71B0053A_m434819A31A3BA0618593B1C358F484D0D617E3EE;
extern const uint32_t g_rgctx_T_t28CB1661FF7BDEDFBEE527928DE91AAF71B0053A;
extern const uint32_t g_rgctx_NCMBRelation_1_t61E256B1E650D6024CE3B28F820B1C3C12EE01F3;
extern const uint32_t g_rgctx_NCMBRelation_1__ctor_m41C38D65C5D9D43E9569D527224735668D397550;
extern const uint32_t g_rgctx_NCMBRelation_1_get_TargetClass_mBC36DA2BF49B87B651D2E3CBBBA20DB8C85DF699;
extern const uint32_t g_rgctx_NCMBRelation_1_set_TargetClass_mF73B0D71EF7472837346466BFA6790351C758B0D;
extern const uint32_t g_rgctx_List_1_tA96B51B9817964D6A04E52FEAED08E5864FEF58E;
extern const uint32_t g_rgctx_List_1__ctor_mDD41F7F9F1BE8A3F6EAC19C107575B7E25630784;
extern const uint32_t g_rgctx_List_1_get_Count_mF7C661AAC5367C3D2C29471BB37A08A15DBF2261;
extern const uint32_t g_rgctx_List_1_get_Item_m25A5A081A02821E414EAD99920FCD983F6078215;
extern const uint32_t g_rgctx_List_1_Add_m5ECE3D191107E80C5C83AD090C2142CF56B16980;
extern const uint32_t g_rgctx_NCMBQuery_1_t0A2385AE7097CD5948867449587E1ADC4F0D15F3;
extern const uint32_t g_rgctx_NCMBQuery_1__ctor_mD584F35D6932CEF25314DE41CCD1D91921D7F24E;
extern const uint32_t g_rgctx_NCMBQuery_1__whereSatifiesAnyOf_mDD6B3A9E30B819922DF9E553D99BAD360DC71769;
extern const uint32_t g_rgctx_NCMBQuery_1__addCondition_m893EF5A5472AAC6B862F7A47BAED3006915ADB71;
extern const uint32_t g_rgctx_NCMBQuery_1__geoPointToObject_m8BAE9804DB127E59C9FE3EF42EAB00118AD0C52E;
extern const uint32_t g_rgctx_NCMBQuery_1__geoPointToObjectWithinBox_mEE51A8FFAC0D8A7C4D91C28398A4148AE9BEBDDA;
extern const uint32_t g_rgctx_NCMBQuery_1_Find_mA32AB3B19FF722E74CD858A653963BE13D51A8C7;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass44_0_t0A93307D2972FFACC70B6679CC80E2598B8163F3;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass44_0__ctor_m7A38DF11BC8289F2EBF7086E5C63624B2F7896D1;
extern const uint32_t g_rgctx_NCMBQuery_1__getSearchUrl_m2034284FC85BF3DC7EDE7C9839906AE08FAA8B18;
extern const uint32_t g_rgctx_NCMBQuery_1__getFindParams_m61DA0111196E375BB2F641C3DE0FC32FEEB48117;
extern const uint32_t g_rgctx_NCMBQuery_1__makeWhereUrl_mBBD8C952A9BB5166E4823CEB24FE4C64801BD9DD;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass44_0_U3CFindU3Eb__0_mB67CB6034520FC7B14FF66C96211EF6CEE4B5502;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass46_0_t0C741F3EDD3BF874F6AAC240894EC882C93C34B7;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass46_0__ctor_m8FF66E772F389DFE3CB644BBB8AC59DCB12DD0A3;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass46_0_U3CGetAsyncU3Eb__0_mD2601FA8BE09229D0F5191F8A0BA44308903697B;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass48_0_t97369286A59E639FEEFB2B8DC652D55D7D20D0E9;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass48_0__ctor_m19D172FBCA5BE9734EDDCC9F196FA779B27CB026;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass48_0_U3CCountAsyncU3Eb__0_m463CE211D2B92F4E3EC8E79A0A22ACA1BD8DACEE;
extern const uint32_t g_rgctx_List_1_GetEnumerator_m4ECDFD82325639A5D8DFF2C9C59AC3EF95984370;
extern const uint32_t g_rgctx_Enumerator_get_Current_mB04E213F60B6D609203F60F32347227DC3772258;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m3848BBDBF34DACA924EE1FA13F6D576E964974CD;
extern const uint32_t g_rgctx_Enumerator_tF2B4189CC60218FD437E9A8E97900A691339ABCD;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_tF2B4189CC60218FD437E9A8E97900A691339ABCD_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_NCMBQuery_1__encodeSubQueries_m89560AA1977CB86945C0650BDC71CD893B95520E;
extern const uint32_t g_rgctx_NCMBQuery_1__join_m2DDDD5E7294E1B4F3162750AEA84C2B1D2793139;
extern const uint32_t g_rgctx_List_1_t14CB789D22426480B94FE19B2603DC75F751ECED;
extern const uint32_t g_rgctx_List_1__ctor_m4C1034814573AB463918B1387CA2C9263204A93D;
extern const uint32_t g_rgctx_NCMBQuery_1__convertFindResponse_m0F4DE5EFE1F35D60A1351570FD9678FC86421F2F;
extern const uint32_t g_rgctx_T_t854AEE0D945A18411557E64AD8E60518AF9D9F67;
extern const uint32_t g_rgctx_List_1_Add_m87C49BE66224C0D51B646AE8C7232E7C4BD2E2E3;
extern const uint32_t g_rgctx_NCMBQueryCallback_1_tA30D51E4414E9C103B7F9FB4DCE6738699BFF16E;
extern const uint32_t g_rgctx_NCMBQueryCallback_1_Invoke_m88CEE2E4316244F620D431B3427870F1726D78AF;
extern const uint32_t g_rgctx_NCMBQuery_1__convertGetResponse_m4E48CA34BB65280A69A31B0E98B78968537969F4;
extern const uint32_t g_rgctx_T_tACF0F3CC61D6652211B14545736D08D0A8C21730;
extern const uint32_t g_rgctx_NCMBGetCallback_1_tF3D5BBA686A68F546BC1B111707E0D6EBC193511;
extern const uint32_t g_rgctx_NCMBGetCallback_1_Invoke_m46F1DE6174B8AC14253CBDC5D761685BAB6A4C82;
extern const uint32_t g_rgctx_NCMBRelation_1__addDuplicationCheck_m934BED42D01F435DBC5CE84C1C47241BECFBAFBD;
extern const uint32_t g_rgctx_HashSet_1_tB54E698E4CC565189FC77BAE6B1CAEE26212B659;
extern const uint32_t g_rgctx_HashSet_1__ctor_m50898E1A2D53B58D29805ECCA493E6552D9132F6;
extern const uint32_t g_rgctx_HashSet_1_Add_mF3551B4F5102D6D5C090227C87CCA5BF9295A3A3;
extern const uint32_t g_rgctx_NCMBRelationOperation_1_t28D29FA556A47996AB55B948039D80A016230F7A;
extern const uint32_t g_rgctx_NCMBRelationOperation_1__ctor_mA76C86EBBF18B7B28EC223DC69D30BC160804EFD;
extern const uint32_t g_rgctx_NCMBRelationOperation_1_get_TargetClass_m43D8720060D9D02512551BE86ABCE0AD9FAB1F5C;
extern const uint32_t g_rgctx_NCMBRelation_1__removeDuplicationCheck_mFA47F401CB45ECA791589071D7B36E45582D0949;
extern const uint32_t g_rgctx_T_t4BDC4413B1A0720CE1B9BCAACE0A310EC3F6F58F;
extern const uint32_t g_rgctx_NCMBQuery_1_GetQuery_m069C2D4E66404F3AE3890D7D3DBF376F8CCFECB9;
extern const uint32_t g_rgctx_NCMBQuery_1_tFD12DDF82BA1154C92C198FC406DEFA35C8E67A6;
extern const uint32_t g_rgctx_NCMBQuery_1_tFD12DDF82BA1154C92C198FC406DEFA35C8E67A6;
extern const uint32_t g_rgctx_NCMBQuery_1__whereRelatedTo_mDE90C344D4D125BF8C40F251D5BF5C13FEC80FE6;
extern const uint32_t g_rgctx_Dictionary_2_t9027AEBA050153E15AFE003F8EF3FF12E7DBC6A5;
extern const uint32_t g_rgctx_Dictionary_2_GetEnumerator_m8B715AD4024417A93C7F421E3D5A5811320DA401;
extern const uint32_t g_rgctx_Enumerator_get_Current_mF6A8DCF9D51AB0D84C123976AE6E0DE9128FBFA7;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Key_mD77A9E445C8A77FE65B288F4BDDDE04B8C6DC3E3;
extern const uint32_t g_rgctx_T_tC82FED6FD54F407F612C226F2F30FF72D39DA146;
extern const Il2CppRGCTXConstrainedData g_rgctx_T_tC82FED6FD54F407F612C226F2F30FF72D39DA146_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Value_mB6147E46B5A073BDE3793DAF272C8D7A03E93E64;
extern const uint32_t g_rgctx_K_t3CD812B2CEE49AAAD057E957ED541B6B842E44A5;
extern const Il2CppRGCTXConstrainedData g_rgctx_K_t3CD812B2CEE49AAAD057E957ED541B6B842E44A5_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_Dictionary_2_get_Count_m598776374170FD5DB20FBA6FB521AEAD21FB30F7;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m6C097C163D9B49F093504EE266AC45B0559A1CF5;
extern const uint32_t g_rgctx_Enumerator_t5D2235F28F891CB57929D3225FAA626F837BC954;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_t5D2235F28F891CB57929D3225FAA626F837BC954_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_HashSet_1_tFF9A9D1AAE77587220F492E8F08BA1DACB4D9ABC;
extern const uint32_t g_rgctx_HashSet_1_GetEnumerator_m86F69C5F99074D0D884FF7C133210B9879E7008E;
extern const uint32_t g_rgctx_Enumerator_get_Current_m93BD412591BCF9054D589B148ED92C1DD48F8EA3;
extern const uint32_t g_rgctx_T_t7BED4CDC556DD72D1FAA40E93A5249F28B50736F;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m73B261C71FF1357827166418FBADA9EC4837D457;
extern const uint32_t g_rgctx_Enumerator_t5E21A3F1D9CADF5EB9BAC4DFD923904E3A1F3E8F;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_t5E21A3F1D9CADF5EB9BAC4DFD923904E3A1F3E8F_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_NCMBRelationOperation_1__convertSetToArray_mF56E2094117E1FD22107C3083F7BDEE8A08D23D8;
extern const uint32_t g_rgctx_NCMBRelationOperation_1_t8690FB230B318957AD5B3880DFD9AFD3D6450FF2;
extern const uint32_t g_rgctx_NCMBRelationOperation_1__ctor_m8CEE27A94EA5D935A177B95B880D3DC155C81426;
extern const uint32_t g_rgctx_NCMBRelation_1_t7158D59F786808ACEB9DF9EE2C2781060A574060;
extern const uint32_t g_rgctx_NCMBRelation_1__ctor_mF2885036A83491DC4FDB79FFBCAD75320FAFE8AB;
extern const uint32_t g_rgctx_NCMBRelation_1_set_TargetClass_mE954FA3C98DEDF1C7FE8F6D266C9B552A4695633;
extern const uint32_t g_rgctx_NCMBRelation_1_get_TargetClass_m0A7DCAD353DDD93FA3EDF275919CE1D0E021556F;
static const Il2CppRGCTXDefinition s_rgctxValues[88] = 
{
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_GameObject_GetComponent_TisT_t28CB1661FF7BDEDFBEE527928DE91AAF71B0053A_m434819A31A3BA0618593B1C358F484D0D617E3EE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t28CB1661FF7BDEDFBEE527928DE91AAF71B0053A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_NCMBRelation_1_t61E256B1E650D6024CE3B28F820B1C3C12EE01F3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBRelation_1__ctor_m41C38D65C5D9D43E9569D527224735668D397550 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBRelation_1_get_TargetClass_mBC36DA2BF49B87B651D2E3CBBBA20DB8C85DF699 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBRelation_1_set_TargetClass_mF73B0D71EF7472837346466BFA6790351C758B0D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tA96B51B9817964D6A04E52FEAED08E5864FEF58E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_mDD41F7F9F1BE8A3F6EAC19C107575B7E25630784 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_mF7C661AAC5367C3D2C29471BB37A08A15DBF2261 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_m25A5A081A02821E414EAD99920FCD983F6078215 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_m5ECE3D191107E80C5C83AD090C2142CF56B16980 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_NCMBQuery_1_t0A2385AE7097CD5948867449587E1ADC4F0D15F3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1__ctor_mD584F35D6932CEF25314DE41CCD1D91921D7F24E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1__whereSatifiesAnyOf_mDD6B3A9E30B819922DF9E553D99BAD360DC71769 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1__addCondition_m893EF5A5472AAC6B862F7A47BAED3006915ADB71 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1__geoPointToObject_m8BAE9804DB127E59C9FE3EF42EAB00118AD0C52E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1__geoPointToObjectWithinBox_mEE51A8FFAC0D8A7C4D91C28398A4148AE9BEBDDA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1_Find_mA32AB3B19FF722E74CD858A653963BE13D51A8C7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass44_0_t0A93307D2972FFACC70B6679CC80E2598B8163F3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass44_0__ctor_m7A38DF11BC8289F2EBF7086E5C63624B2F7896D1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1__getSearchUrl_m2034284FC85BF3DC7EDE7C9839906AE08FAA8B18 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1__getFindParams_m61DA0111196E375BB2F641C3DE0FC32FEEB48117 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1__makeWhereUrl_mBBD8C952A9BB5166E4823CEB24FE4C64801BD9DD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass44_0_U3CFindU3Eb__0_mB67CB6034520FC7B14FF66C96211EF6CEE4B5502 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass46_0_t0C741F3EDD3BF874F6AAC240894EC882C93C34B7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass46_0__ctor_m8FF66E772F389DFE3CB644BBB8AC59DCB12DD0A3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass46_0_U3CGetAsyncU3Eb__0_mD2601FA8BE09229D0F5191F8A0BA44308903697B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass48_0_t97369286A59E639FEEFB2B8DC652D55D7D20D0E9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass48_0__ctor_m19D172FBCA5BE9734EDDCC9F196FA779B27CB026 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass48_0_U3CCountAsyncU3Eb__0_m463CE211D2B92F4E3EC8E79A0A22ACA1BD8DACEE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_GetEnumerator_m4ECDFD82325639A5D8DFF2C9C59AC3EF95984370 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_mB04E213F60B6D609203F60F32347227DC3772258 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_m3848BBDBF34DACA924EE1FA13F6D576E964974CD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_tF2B4189CC60218FD437E9A8E97900A691339ABCD },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_tF2B4189CC60218FD437E9A8E97900A691339ABCD_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1__encodeSubQueries_m89560AA1977CB86945C0650BDC71CD893B95520E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1__join_m2DDDD5E7294E1B4F3162750AEA84C2B1D2793139 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t14CB789D22426480B94FE19B2603DC75F751ECED },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_m4C1034814573AB463918B1387CA2C9263204A93D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1__convertFindResponse_m0F4DE5EFE1F35D60A1351570FD9678FC86421F2F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t854AEE0D945A18411557E64AD8E60518AF9D9F67 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_m87C49BE66224C0D51B646AE8C7232E7C4BD2E2E3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_NCMBQueryCallback_1_tA30D51E4414E9C103B7F9FB4DCE6738699BFF16E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQueryCallback_1_Invoke_m88CEE2E4316244F620D431B3427870F1726D78AF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1__convertGetResponse_m4E48CA34BB65280A69A31B0E98B78968537969F4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tACF0F3CC61D6652211B14545736D08D0A8C21730 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_NCMBGetCallback_1_tF3D5BBA686A68F546BC1B111707E0D6EBC193511 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBGetCallback_1_Invoke_m46F1DE6174B8AC14253CBDC5D761685BAB6A4C82 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBRelation_1__addDuplicationCheck_m934BED42D01F435DBC5CE84C1C47241BECFBAFBD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_HashSet_1_tB54E698E4CC565189FC77BAE6B1CAEE26212B659 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HashSet_1__ctor_m50898E1A2D53B58D29805ECCA493E6552D9132F6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HashSet_1_Add_mF3551B4F5102D6D5C090227C87CCA5BF9295A3A3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_NCMBRelationOperation_1_t28D29FA556A47996AB55B948039D80A016230F7A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBRelationOperation_1__ctor_mA76C86EBBF18B7B28EC223DC69D30BC160804EFD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBRelationOperation_1_get_TargetClass_m43D8720060D9D02512551BE86ABCE0AD9FAB1F5C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBRelation_1__removeDuplicationCheck_mFA47F401CB45ECA791589071D7B36E45582D0949 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t4BDC4413B1A0720CE1B9BCAACE0A310EC3F6F58F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1_GetQuery_m069C2D4E66404F3AE3890D7D3DBF376F8CCFECB9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_NCMBQuery_1_tFD12DDF82BA1154C92C198FC406DEFA35C8E67A6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_NCMBQuery_1_tFD12DDF82BA1154C92C198FC406DEFA35C8E67A6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBQuery_1__whereRelatedTo_mDE90C344D4D125BF8C40F251D5BF5C13FEC80FE6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_t9027AEBA050153E15AFE003F8EF3FF12E7DBC6A5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_GetEnumerator_m8B715AD4024417A93C7F421E3D5A5811320DA401 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_mF6A8DCF9D51AB0D84C123976AE6E0DE9128FBFA7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Key_mD77A9E445C8A77FE65B288F4BDDDE04B8C6DC3E3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tC82FED6FD54F407F612C226F2F30FF72D39DA146 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T_tC82FED6FD54F407F612C226F2F30FF72D39DA146_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Value_mB6147E46B5A073BDE3793DAF272C8D7A03E93E64 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_K_t3CD812B2CEE49AAAD057E957ED541B6B842E44A5 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_K_t3CD812B2CEE49AAAD057E957ED541B6B842E44A5_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_get_Count_m598776374170FD5DB20FBA6FB521AEAD21FB30F7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_m6C097C163D9B49F093504EE266AC45B0559A1CF5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t5D2235F28F891CB57929D3225FAA626F837BC954 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_t5D2235F28F891CB57929D3225FAA626F837BC954_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_HashSet_1_tFF9A9D1AAE77587220F492E8F08BA1DACB4D9ABC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HashSet_1_GetEnumerator_m86F69C5F99074D0D884FF7C133210B9879E7008E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_m93BD412591BCF9054D589B148ED92C1DD48F8EA3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t7BED4CDC556DD72D1FAA40E93A5249F28B50736F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_m73B261C71FF1357827166418FBADA9EC4837D457 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t5E21A3F1D9CADF5EB9BAC4DFD923904E3A1F3E8F },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_t5E21A3F1D9CADF5EB9BAC4DFD923904E3A1F3E8F_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBRelationOperation_1__convertSetToArray_mF56E2094117E1FD22107C3083F7BDEE8A08D23D8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_NCMBRelationOperation_1_t8690FB230B318957AD5B3880DFD9AFD3D6450FF2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBRelationOperation_1__ctor_m8CEE27A94EA5D935A177B95B880D3DC155C81426 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_NCMBRelation_1_t7158D59F786808ACEB9DF9EE2C2781060A574060 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBRelation_1__ctor_mF2885036A83491DC4FDB79FFBCAD75320FAFE8AB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBRelation_1_set_TargetClass_mE954FA3C98DEDF1C7FE8F6D266C9B552A4695633 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_NCMBRelation_1_get_TargetClass_m0A7DCAD353DDD93FA3EDF275919CE1D0E021556F },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1459,
	s_methodPointers,
	5,
	s_adjustorThunks,
	s_InvokerIndices,
	1,
	s_reversePInvokeIndices,
	8,
	s_rgctxIndices,
	88,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
